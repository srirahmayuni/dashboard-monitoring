<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@halamanawal');
Route::get('operator_benchmark', 'HomeController@operator_benchmark');
Route::get('operator_benchmark_home', 'HomeController@operator_benchmark_home');
Route::get('operator_benchmark_home_month/{months}', 'HomeController@operator_benchmark_home_month');
Route::get('operator_benchmark_home_week/{week}', 'HomeController@operator_benchmark_home_week');
Route::get('super_4g', 'HomeController@super_4g');
Route::get('wcc_62', 'HomeController@wcc_62');
Route::get('csat_48', 'HomeController@csat_48');
Route::get('chart_region', 'HomeController@chart_region');
// Route::get('region', 'HomeController@region');
Route::get('region/{regional}', 'HomeController@region');
Route::get('regionmonth/{regional}/month/{months}', 'HomeController@regionmonth');
Route::get('regionweek/{regional}/week/{week}', 'HomeController@regionweek');
Route::get('city/{city}', 'HomeController@city');
Route::get('citymonth/{city}/month/{months}', 'HomeController@citymonth');
Route::get('cityweek/{city}/week/{week}', 'HomeController@cityweek');
//login
Route::get('login', 'HomeController@ldap');
Route::post('proses_login', 'HomeController@proses_login'); 
Route::get('logout', 'HomeController@logout');
// Route::post('chart1', 'HomeController@chart1'); 