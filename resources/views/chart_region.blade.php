<?php header('Access-Control-Allow-Origin: *'); ?> 
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>DASHBOARD OOKLA</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{url('')}}/template/tsel.jpg" rel="icon" type="image/png">
  <link rel="stylesheet" type="text/css" href="{{url('')}}/css/isotope.css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/flexslider.css" type="text/css">
  <link rel="stylesheet" href="{{url('')}}/js/fancybox/jquery.fancybox.css" type="text/css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/bootstrap.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="{{url('')}}/css/style.css">
  <link rel="stylesheet" href="{{url('')}}/skin/default.css">
  <script src="https://unpkg.com/ag-grid-enterprise@20.2.0/dist/ag-grid-enterprise.min.js"></script>
  <script src = "https://code.highcharts.com/highcharts.js"> </script>
    <script src="{{url('')}}/js/jquery.js"></script>
  <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <script>
    Highcharts.theme = {
      colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
      '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
      chart: {
        backgroundColor: {
          linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
          stops: [
          [0, '#2a2a2b'],
          [1, '#3e3e40']
          ]
        },
        style: {
          fontFamily: '\'Unica One\', sans-serif'
        },
        plotBorderColor: '#606063',
        marginTop: 15
      },
      title: {
        style: {
          color: '#E0E0E3',
          textTransform: 'uppercase',
          fontSize: '20px'
        }
      },
      subtitle: {
        style: {
          color: '#E0E0E3',
          textTransform: 'uppercase'
        }
      },
      xAxis: {
        gridLineColor: '#707073',
        labels: {
          style: {
            color: '#E0E0E3'
          }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        title: {
          style: {
            color: '#A0A0A3'

          }
        }
      },
      yAxis: {
        gridLineColor: '#707073',
        labels: {
          style: {
            color: '#E0E0E3'
          }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        tickWidth: 1,
        title: {
          style: {
            color: '#A0A0A3'
          }
        }
      },
      tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.85)',
        style: {
          color: '#F0F0F0'
        }
      },
      plotOptions: {
        series: {
          dataLabels: {
            color: '#B0B0B3'
          },
          marker: {
            lineColor: '#333'
          }
        },
        boxplot: {
          fillColor: '#505053'
        },
        candlestick: {
          lineColor: 'white'
        },
        errorbar: {
          color: 'white'
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '9px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '9px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '9px'
        },
      },
      credits: {
        enabled: false
      },
      labels: {
        style: {
          color: '#707073'
        }
      },

      drilldown: {
        activeAxisLabelStyle: {
          color: '#F0F0F3'
        },
        activeDataLabelStyle: {
          color: '#F0F0F3'
        }
      },

      navigation: {
        buttonOptions: {
          symbolStroke: '#DDDDDD',
          theme: {
            fill: '#505053'
          }
        }
      },

      rangeSelector: {
        buttonTheme: {
          fill: '#505053',
          stroke: '#000000',
          style: {
            color: '#CCC'
          },
          states: {
            hover: {
              fill: '#707073',
              stroke: '#000000',
              style: {
                color: 'white'
              }
            },
            select: {
              fill: '#000003',
              stroke: '#000000',
              style: {
                color: 'white'
              }
            }
          }
        },
        inputBoxBorderColor: '#505053',
        inputStyle: {
          backgroundColor: '#333',
          color: 'silver'
        },
        labelStyle: {
          color: 'silver'
        }
      },

      navigator: {
        handles: {
          backgroundColor: '#666',
          borderColor: '#AAA'
        },
        outlineColor: '#CCC',
        maskFill: 'rgba(255,255,255,0.1)',
        series: {
          color: '#7798BF',
          lineColor: '#A6C7ED'
        },
        xAxis: {
          gridLineColor: '#505053'
        }
      },

      scrollbar: {
        barBackgroundColor: '#808083',
        barBorderColor: '#808083',
        buttonArrowColor: '#CCC',
        buttonBackgroundColor: '#606063',
        buttonBorderColor: '#606063',
        rifleColor: '#FFF',
        trackBackgroundColor: '#404043',
        trackBorderColor: '#404043'
      },

      legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
      background2: '#505053',
      dataLabelsColor: '#B0B0B3',
      textColor: '#C0C0C0',
      contrastTextColor: '#F0F0F3',
      maskColor: 'rgba(255,255,255,0.3)'
    };
    $(document).ready(function(){
      $("#week").change(function() {
        var week = $("#week").val();
        $.ajax({
          url:"http://10.54.36.49/api-ookla/public/home/chart_download4g/"+week,
          type:"get",
          success: function(result){
            // $('#chart1').empty();
            var res = JSON.parse(result); 
            var telkomseldownload4g = []
            var threedownload4g = []
            var indosatdownload4g = []
            var smartfrendownload4g = []
            var xldownload4g = []
            var week_download4g = []

            res.forEach(myFunction);
            function myFunction(value) {
              telkomseldownload4g.push(value['TELKOMSEL'])
            }
            res.forEach(myFunction2);
            function myFunction2(value2) {
              threedownload4g.push(value2['THREE'])
            }
            res.forEach(myFunction3);
            function myFunction3(value3) {
              indosatdownload4g.push(value3['INDOSAT'])
            }
            res.forEach(myFunction4);
            function myFunction4(value4) {
              smartfrendownload4g.push(value4['SMARTFREN'])
            }
            res.forEach(myFunction5);
            function myFunction5(value5) {
              xldownload4g.push(value5['XL'])
            }
            res.forEach(myFunction6);
            function myFunction6(value6) {
              week_download4g.push(value6['WEEK'])
            }

            Highcharts.chart('charts1', {
              title: {
                text: ''
              },
              xAxis: {
                categories: week_download4g
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              exporting: {
                enabled: false
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              legend: {
                itemStyle: {
                  color: '#E0E0E3',
                  fontSize: '12px'
                },
                itemHoverStyle: {
                  color: '#FFF',
                  fontSize: '12px'
                },
                itemHiddenStyle: {
                  color: '#606063',
                  fontSize: '12px'
                }
              },
              series: [{
                name: 'TSEL',
                data: telkomseldownload4g,
                color: '#FF0000'

              }, {
                name: 'ISAT',
                data: indosatdownload4g,
                color: '#FFD700'

              }, {
                name: 'XL',
                data: xldownload4g,
                color: '#0000FF'

              }, {
                name: '3',
                data: threedownload4g,
                color: '#808080'
              }, {
                name: 'SF',
                data: smartfrendownload4g,
                color: '#FF7F00'

              }]
            });

            Highcharts.chart('charts11', {
              title: {
                text: ''
              },
              xAxis: {
                categories: week_download4g
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              navigation: {
                buttonOptions: {
                  verticalAlign: 'bottom',
                  align:'right',
                  y: -5
                }
              },
              legend: {
                itemStyle: {
                  color: '#E0E0E3',
                  fontSize: '12px'
                },
                itemHoverStyle: {
                  color: '#FFF',
                  fontSize: '12px'
                },
                itemHiddenStyle: {
                  color: '#606063',
                  fontSize: '12px'
                }
              },
              series: [{
                name: 'TSEL',
                data: telkomseldownload4g,
                color: '#FF0000'

              }, {
                name: 'ISAT',
                data: indosatdownload4g,
                color: '#FFD700'

              }, {
                name: 'XL',
                data: xldownload4g,
                color: '#0000FF'

              }, {
                name: '3',
                data: threedownload4g,
                color: '#808080'
              }, {
                name: 'SF',
                data: smartfrendownload4g,
                color: '#FF7F00'

              }]
            });


          }
        });

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_download3g/"+week,
  type:"get",
  success: function(result){
    // $('#chart2').empty();
    var res = JSON.parse(result); 
    var telkomseldownload3g = []
    var threedownload3g = []
    var indosatdownload3g = []
    var smartfrendownload3g = []
    var xldownload3g = []
    var week_download3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomseldownload3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threedownload3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatdownload3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrendownload3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xldownload3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_download3g.push(value6['WEEK'])
    } 

  Highcharts.chart('charts2', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts22', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g,
    color: '#FF7F00'

  }]
});


  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_upload4g/"+week,
  type:"get",
  success: function(result){
    // $('#chart3').empty();
    var res = JSON.parse(result); 
    var telkomselupload4g = []
    var threeupload4g = []
    var indosatupload4g = []
    var smartfrenupload4g = []
    var xlupload4g = []
    var week_upload4g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomselupload4g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threeupload4g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatupload4g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenupload4g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xlupload4g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_upload4g.push(value6['WEEK'])
    }
    console.log('chart3',week_upload4g)

    Highcharts.chart('charts3', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload4g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts33', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload4g,
      color: '#FF7F00'

    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_upload3g/"+week,
  type:"get",
  success: function(result){
    // $('#chart4').empty();
    var res = JSON.parse(result); 
    var telkomselupload3g = []
    var threeupload3g = []
    var indosatupload3g = []
    var smartfrenupload3g = []
    var xlupload3g = []
    var week_upload3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomselupload3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threeupload3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatupload3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenupload3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xlupload3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_upload3g.push(value6['WEEK'])
    }
    console.log('chart4',week_upload3g)

    Highcharts.chart('charts4', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     exporting: {
      enabled: false
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload3g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts44', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload3g,
      color: '#FF7F00'

    }]
  });


  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_latency4g/"+week,
  type:"get",
  success: function(result){
    // $('#chart5').empty();
    var res = JSON.parse(result); 
    var telkomsellatency4g = []
    var threelatency4g = []
    var indosatlatency4g = []
    var smartfrenlatency4g = []
    var xllatency4g = []
    var week_latency4g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomsellatency4g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threelatency4g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatlatency4g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenlatency4g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xllatency4g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_latency4g.push(value6['WEEK'])
    }
    console.log('chart5',week_latency4g)

    Highcharts.chart('charts5', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: 'TSEL',
      data: telkomsellatency4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency4g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts55', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     series: [{
      name: 'TSEL',
      data: telkomsellatency4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency4g,
      color: '#FF7F00'

    }]
  });


  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_latency3g/"+week,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var telkomsellatency3g = []
    var threelatency3g = []
    var indosatlatency3g = []
    var smartfrenlatency3g = []
    var xllatency3g = []
    var week_latency3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomsellatency3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threelatency3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatlatency3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenlatency3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xllatency3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_latency3g.push(value6['WEEK'])
    }
    // console.log('chart6',week_latency3g)

    Highcharts.chart('charts6', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomsellatency3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency3g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts66', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomsellatency3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency3g,
      color: '#FF7F00'

    }]
  });

  }
});


});
});
</script>
</head>
<body>
  <section id="header" class="appear">
    <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:40px; height:40px; background-color:rgba(0,0,0,0.3);" data-300="line-height:40px; height:40px; background-color:rgba(5, 42, 62, 1);">
      <!-- <div class="container"> -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="fa fa-bars color-white"></span>
          </button>
          <div class="navbar-logo">
            <img data-0="width:170px;" src="img/oklaa.png" alt="" style="padding-left: 50px; width: 170px;">
          </div>
        </div>
        <div class="navbar-collapse collapse" style="padding-right: 150px;">
          <ul class="nav navbar-nav" data-0="margin-top:0px;" data-300="margin-top:0px;">
            <li class="active"><a href="{{url('/')}}">Home</a>
            </li>
            <li><a href="{{url('super_4g')}}">SUPER 4G</a></li>
            <li><a href="{{url('wcc_62')}}">WCC 62</a></li>
            <li><a href="{{url('csat_48')}}">CSAT 48</a></li>
            <li><a href="{{url('logout')}}">Logout</a></li>
          </ul>
        </div>
      <!-- </div> -->
    </div>
  </section>
  <section id="intro">
    <div class="intro-content">
      <div class="row" style="padding-top: 35px; padding-left: 0px; padding-right: 0px;">
        <div class="col-lg-4" style="padding-right: 0px; padding-top: 20px;">
          <div class="panel-heading" style="background-color: #800000;">
            <h4 style="font-family: calibri; color: white;"><strong>Download</strong></h4>
          </div>
          <div class="small-box boks boksBody" style="height: 675px;">
            <div class="inner">
              <div class="row"> 
                <div class="col-md-12" style="margin-top:5px; padding-right: 15px;">
                  <div class="card-header1" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal1"></i>
                  </div>
                  <div class="modal fade" id="modal1" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537;"> 
                        <div class="modal-header" style="text-align:center; color: white;"> 4G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartdownload4g_" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body1" style="width:100%; height:310px; background-color: #343a40">
                    <div id="chartdownload4g" style="width:100%; height:310px;"></div>
                  </div> 
                </div>  
              </div>

              <div class="row"> 
                <div class="col-md-12" style="margin-top:5px; padding-right: 15px;">
                  <div class="card-header2" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> 3G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal2"></i>
                  </div>
                  <div class="modal fade" id="modal2" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> 3G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartdownload3g_" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body2" style="width:100%; height:310px; background-color: #343a40">
                    <div id="chartdownload3g" style="width:100%; height:310px;"></div>
                  </div> 
                </div>  
              </div>
        </div>
      </div>
  </div>
  <div class="col-lg-4" style="padding-right: 0px; padding-top: 20px; padding-left: 5px">
          <div class="panel-heading" style="background-color: #800000;">
            <h4 style="font-family: calibri; color: white;"><strong>Upload</strong></h4>
          </div>
          <div class="small-box boks boksBody" style="height: 675px;">
            <div class="inner">
              <div class="row"> 
                <div class="col-md-12" style="margin-top:5px; padding-right: 15px;">
                  <div class="card-header3" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal3"></i>
                  </div>
                  <div class="modal fade" id="modal3" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537;"> 
                        <div class="modal-header" style="text-align:center; color: white;"> 4G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartupload4g_" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body3" style="width:100%; height:310px; background-color: #343a40">
                    <div id="chartupload4g" style="width:100%; height:310px;"></div>
                  </div> 
                </div>  
              </div>

              <div class="row"> 
                <div class="col-md-12" style="margin-top:5px; padding-right: 15px;">
                  <div class="card-header4" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> 3G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal4"></i>
                  </div>
                  <div class="modal fade" id="modal4" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> 3G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartupload3g_" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body4" style="width:100%; height:310px; background-color: #343a40">
                    <div id="chartupload3g" style="width:100%; height:310px;"></div>
                  </div> 
                </div>  
              </div>
        </div>
      </div>
  </div>
  <div class="col-lg-4" style="padding-top: 20px; padding-left: 5px">
          <div class="panel-heading" style="background-color: #800000;">
            <h4 style="font-family: calibri; color: white;"><strong>Latency</strong></h4>
          </div>
          <div class="small-box boks boksBody" style="height: 675px;">
            <div class="inner">
              <div class="row"> 
                <div class="col-md-12" style="margin-top:5px; padding-right: 15px;">
                  <div class="card-header5" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal5"></i>
                  </div>
                  <div class="modal fade" id="modal5" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537;"> 
                        <div class="modal-header" style="text-align:center; color: white;"> 4G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartlatency4g_" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body5" style="width:100%; height:310px; background-color: #343a40">
                    <div id="chartlatency4g" style="width:100%; height:310px;"></div>
                  </div> 
                </div>  
              </div>

              <div class="row"> 
                <div class="col-md-12" style="margin-top:5px; padding-right: 15px;">
                  <div class="card-header6" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> 3G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal6"></i>
                  </div>
                  <div class="modal fade" id="modal6" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> 3G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartlatency3g_" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body6" style="width:100%; height:310px; background-color: #343a40">
                    <div id="chartlatency3g" style="width:100%; height:310px;"></div>
                  </div> 
                </div>  
              </div>
        </div>
      </div>
  </div>
 </div>
  <marquee behavior="scroll" direction="left" scrollamount="5" style="font-size:13px; background: rgba(17, 34, 41, 0.9); color: white; padding-top: 0px;">Last Data&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Download : {{$lastupdate_download}}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Upload : {{$lastupdate_upload}}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Latency : {{$lastupdate_latency}}</marquee>
</div>
</div>
</section>
<script src="{{url('')}}/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="{{url('')}}/js/jquery.js"></script>
<script src="{{url('')}}/js/jquery.easing.1.3.js"></script>
<script src="{{url('')}}/js/bootstrap.min.js"></script>
<script src="{{url('')}}/js/jquery.isotope.min.js"></script>
<script src="{{url('')}}/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="{{url('')}}/js/skrollr.min.js"></script>
<script src="{{url('')}}/js/jquery.scrollTo.min.js"></script>
<script src="{{url('')}}/js/jquery.localScroll.min.js"></script>
<script src="{{url('')}}/js/stellar.js"></script>
<script src="{{url('')}}/js/jquery.appear.js"></script>
<script src="{{url('')}}/js/jquery.flexslider-min.js"></script>
<script src="{{url('')}}/contactform/contactform.js"></script>
<script src="{{url('')}}/js/main.js"></script>
<script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
<script src="{{url('')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="{{url('')}}/js/index.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<script>
  $(document).ready(function() {
    $('[data-toggle="toggle"]').change(function(){
      $(this).parents().next('.hide').toggle();
    });
  });
</script>
<script>
  
  Highcharts.theme = {
    colors: ['#aaeeee'],
    chart: {
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
        [0, '#2a2a2b'],
        [1, '#3e3e40']
        ]
      },
      style: {
        fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063',
      marginTop: 15
    },
    title: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase',
        fontSize: '20px'
      }
    },
    subtitle: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase'
      }
    },
    xAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
        style: {
          color: '#A0A0A3'

        }
      }
    },
    yAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
        style: {
          color: '#A0A0A3'
        }
      }
    },
    tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
        color: '#F0F0F0'
      }
    },
    plotOptions: {
      series: {
        dataLabels: {
          color: '#B0B0B3'
        },
        marker: {
          lineColor: '#333'
        }
      },
      boxplot: {
        fillColor: '#505053'
      },
      candlestick: {
        lineColor: 'white'
      },
      errorbar: {
        color: 'white'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '9px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '9px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '9px'
      }
    },
    credits: {
      enabled: false
    },
    labels: {
      style: {
        color: '#707073'
      }
    },

    drilldown: {
      activeAxisLabelStyle: {
        color: '#F0F0F3'
      },
      activeDataLabelStyle: {
        color: '#F0F0F3'
      }
    },

    navigation: {
      buttonOptions: {
        symbolStroke: '#DDDDDD',
        theme: {
          fill: '#505053'
        }
      }
    },

    rangeSelector: {
      buttonTheme: {
        fill: '#505053',
        stroke: '#000000',
        style: {
          color: '#CCC'
        },
        states: {
          hover: {
            fill: '#707073',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          },
          select: {
            fill: '#000003',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          }
        }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
        backgroundColor: '#333',
        color: 'silver'
      },
      labelStyle: {
        color: 'silver'
      }
    },

    navigator: {
      handles: {
        backgroundColor: '#666',
        borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
        color: '#7798BF',
        lineColor: '#A6C7ED'
      },
      xAxis: {
        gridLineColor: '#505053'
      }
    },

    scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
    },

    legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
    background2: '#505053',
    dataLabelsColor: '#B0B0B3',
    textColor: '#C0C0C0',
    contrastTextColor: '#F0F0F3',
    maskColor: 'rgba(255,255,255,0.3)'
  };

  Highcharts.setOptions(Highcharts.theme);
  Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
      return {
        radialGradient: {
          cx: 0.5,
          cy: 0.3,
          r: 0.7
        },
        stops: [
        [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
              };
            })
  });

  var regional4g_download = <?php echo json_encode($regional4g_download); ?>;
  var downloadregional_4g = <?php echo json_encode($downloadregional_4g); ?>;
  var regional4g_upload = <?php echo json_encode($regional4g_upload); ?>;
  var uploadregional_4g = <?php echo json_encode($uploadregional_4g); ?>;
  var regional4g_latency = <?php echo json_encode($regional4g_latency); ?>;
  var latencyregional_4g = <?php echo json_encode($latencyregional_4g); ?>;

  var regional3g_download = <?php echo json_encode($regional3g_download); ?>;
  var downloadregional_3g = <?php echo json_encode($downloadregional_3g); ?>;
  var regional3g_upload = <?php echo json_encode($regional3g_upload); ?>;
  var uploadregional_3g = <?php echo json_encode($uploadregional_3g); ?>;
  var regional3g_latency = <?php echo json_encode($regional3g_latency); ?>;
  var latencyregional_3g = <?php echo json_encode($latencyregional_3g); ?>;

Highcharts.chart('chartdownload4g', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional4g_download,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      },
      series: {
        borderWidth: 0
      }
    },
    legend: {
        // layout: 'vertical',
        // align: 'left',
        // verticalAlign: 'bottom',
        // x: -10,
        // y: 0,
        // floating: true,
        // borderWidth: 1,
        // backgroundColor:
        //     Highcharts.defaultOptions.legend.backgroundColor || '#00000',
        // shadow: true
        enabled: false
    },
    exporting: {
      enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Download',
        data: downloadregional_4g
    }]
});
Highcharts.chart('chartdownload4g_', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional4g_download,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
       enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Download',
        data: downloadregional_4g
    }]
});

//download 3g
Highcharts.chart('chartdownload3g', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional3g_download,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
        enabled: false
    },
    exporting: {
      enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Download',
        data: downloadregional_3g
    }]
});
Highcharts.chart('chartdownload3g_', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional3g_download,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Download',
        data: downloadregional_3g
    }]
});

//upload 4g
Highcharts.chart('chartupload4g', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional4g_upload,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
        enabled: false
    },
    exporting: {
      enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Upload',
        data: uploadregional_4g
    }]
});
Highcharts.chart('chartupload4g_', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional4g_upload,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Upload',
        data: uploadregional_4g
    }]
});

//upload 3g
Highcharts.chart('chartupload3g', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional3g_upload,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
       enabled: false
    },
    exporting: {
      enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Upload',
        data: uploadregional_3g
    }]
});
Highcharts.chart('chartupload3g_', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional3g_upload,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Upload',
        data: uploadregional_3g
    }]
});

//latency 4g
Highcharts.chart('chartlatency4g', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional4g_latency,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
        enabled: false
    },
    exporting: {
      enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Latency',
        data: latencyregional_4g
    }]
});
Highcharts.chart('chartlatency4g_', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional4g_latency,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
       enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Latency',
        data: latencyregional_4g
    }]
});

//latency 3g
Highcharts.chart('chartlatency3g', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional3g_latency,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
        enabled: false
    },
    exporting: {
      enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Latency',
        data: latencyregional_3g
    }]
});
Highcharts.chart('chartlatency3g_', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: regional3g_latency,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Mbps'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
        borderWidth: 0
      }
    },
    legend: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Latency',
        data: latencyregional_4g
    }]
});

</script>
<script type="text/javascript">
  var nativePicker = document.querySelector('.nativeWeekPicker');
  var fallbackPicker = document.querySelector('.fallbackWeekPicker');
  var fallbackLabel = document.querySelector('.fallbackLabel');

  var yearSelect = document.querySelector('#year');
  var weekSelect = document.querySelector('#fallbackWeek');

  fallbackPicker.style.display = 'none';
  fallbackLabel.style.display = 'none';

  var test = document.createElement('input');
  test.type = 'week';

  if(test.type === 'text') {
    nativePicker.style.display = 'none';
    fallbackPicker.style.display = 'block';
    fallbackLabel.style.display = 'block';

    populateWeeks();
  }

  function populateWeeks() {
    for(var i = 1; i <= 52; i++) {
      var option = document.createElement('option');
      option.textContent = (i < 10) ? ("0" + i) : i;
      weekSelect.appendChild(option);
    }
  }
</script>
</body>

</html>
