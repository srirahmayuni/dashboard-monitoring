<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>NEISA | Under Maintenance</title>
  
  <link href="{{url('')}}/template/tsel.jpg" rel="icon" type="image/png">
  <!-- Bootstrap core CSS -->
  <link href="{{url('')}}/template/bootstrap-maintenance/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
  <link href="{{url('')}}/template/font-awesome-4.4.0/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template -->
  <link href="{{url('')}}/template/coming-soon.min.css" rel="stylesheet">

  <style>
  .updet {
    background: rgba(255, 255, 255, .9);
    width: 100%;
    color: #000;
    left: 0;
    position: fixed;
    font-size: larger;
    /* font-weight: bolder; */
  }
  .tsel{
    opacity: .9; 
    max-height: 125px;
    margin: 20px;
  }
  </style>

</head>

<body>

  <div class="overlay"></div>
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="{{url('')}}/template/mp4/bg.mp4" type="video/mp4">
  </video>

  <div class="masthead">
    <div class="masthead-bg"></div>
    <div class="container h-100">
      <div class="row h-100">
          <img src="{{url('')}}/dist/img/tsel-white.png" class="tsel">
        <div class="col-12" style="margin-top: -25%;">
          <div class="masthead-content text-white py-5 py-md-0">
            {{-- <h1 class="mb-3">Under Maintenance!</h1> --}}
            <h3 class="" id="timer">
            <h1 class="mb-3">Be Patient, NEISA is Upgrading!</h1>
            {{-- <p class="mb-5">We're working hard to finish the development of this site. --}}
            <p class="mb-5">We're working hard to enhance new features to this site.
          </div>
          <div class="masthead-content py-5 py-md-0 updet">
            <p class="mb-5" style="margin-bottom: 1rem !important;">
              {{-- This update will bring features as below: --}}
              What's New:
              <ul>
                <li>Split flow nodin (engineering) dan remedy (project)</li>
                <li>Multiple site creation</li>
                <li>Enodin integration</li>
              </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <input type="hidden" name="_token" value="{{csrf_token()}}">
  <!-- Bootstrap core JavaScript -->
  <script src="{{url('')}}/template/jquery/jquery.min.js"></script>
  <script src="{{url('')}}/template/bootstrap-maintenance/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="{{url('')}}/template/coming-soon.min.js"></script>

  {{-- <script>
      // Set the date we're counting down to
      var countDownDate = new Date("Jan 5, 2021 15:37:25").getTime();
      
      // Update the count down every 1 second
      var x = setInterval(function() {
      
        // Get todays date and time
        var now = new Date().getTime();
      
        // Find the distance between now and the count down date
        var distance = countDownDate - now;
      
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      
        // Display the result in the element with id="timer"
        document.getElementById("timer").innerHTML = days + "d " + hours + "h "
        + minutes + "m " + seconds + "s ";
      
        // If the count down is finished, write some text 
        if (distance < 0) {
          clearInterval(x);
          document.getElementById("timer").innerHTML = "EXPIRED";
        }
      }, 1000);
      </script> --}}

</body>

</html>
