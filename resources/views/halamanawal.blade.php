<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Hotel Template">
    <meta name="keywords" content="Hotel, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OOKLA</title>

    <!-- Google Font -->
    <link
        href="https://fonts.googleapis.com/css?family=Montserrat+Alternates:100,200,300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{url('')}}/new/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{url('')}}/new/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{url('')}}/new/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="{{url('')}}/new/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="{{url('')}}/new/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="{{url('')}}/new/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="{{url('')}}/new/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="{{url('')}}/new/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="{{url('')}}/new/css/style.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="container-fluid">
            <div class="inner-header">
                <div class="logo">
                    <a href="{{url('/')}}"><img src="{{url('')}}/img/tsel-white.png" alt="" style="width: 170px; height: 50px"></a>
                </div>
                <nav class="main-menu mobile-menu">
                </nav>
                <div id="mobile-menu-wrap"></div>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Hero Area Section Begin -->
    <div class="hero-area set-bg other-page" data-setbg="{{url('')}}/img/intro-background.jpg">
    </div>
    <!-- Hero Area Section End -->

    <!-- Search Filter Section Begin -->
    <section class="search-filter other-page-filter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form action="#" class="check-form">
                        <h4 style="padding-top: 5px;">Choose The Menu !</h4>
                        <!-- <div class="datepicker" style="height: 120px;" background = "img/oklaa.png"> -->
                    <div class="col-md-12">
                          <div class="row">
                          <div class="col-sm-1 align-center">
                             
                          </div>
                          <div class="col-sm-2 align-center">
                              <a href="{{url('/')}}"><img alt="logo" src="{{url('')}}/new/img/oklaa.png" style="padding-top: 0px; height: 100px; padding-left: 0px; padding-right: 10px">
                          </div>

                          <div class="col-sm-2 align-center">
                              <a href="{{url('operator_benchmark')}}"><img alt="logo" src="{{url('')}}/new/img/data-analytics.png" style="padding-top: 0px; height: 90px; padding-right: 10px; position: relative;"><p style="font-size: 20px; padding-top: 0px; position: absolute; top: 75px; z-index: 2; color: #fff; padding-left: 20px;">Analytics</p></a>
                          </div>

                          <div class="col-sm-2 align-center">
                              <a href="http://10.251.16.100/nsqd/"><img alt="logo" src="{{url('')}}/new/img/opensignal-4.png" style="padding-top: 0px; height: 100px; padding-right: 10px"></a>
                          </div>

                          <div class="col-sm-2 align-center">
                              <a href="{{url('#')}}"><img alt="logo" src="{{url('')}}/new/img/drivetest.png" style="padding-top: 0px; height: 100px; padding-right: 10px"></a>
                          </div>

                          <div class="col-sm-2 align-center">
                            <a href="{{url('#')}}"><img alt="logo" src="{{url('')}}/new/img/qos.png" style="padding-top: 0px; height: 100px; padding-right: 0px"></a>
                          </div>
                          <div class="col-sm-1 align-center">
                              
                          </div>
                      </div>
                  </div>

                        </form>
                    </div>
                </div>
            </div>
        </section>

    <!-- Call To Action End  -->

    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="footer-item">
                        <div class="footer-logo">
                            <a href="#"><img src="{{url('')}}/new/img/tsel-white.png" alt="" style="width: 250px;"></a>
                        </div>
                        <p>Welcome To Website OOKLA</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="footer-item">
                        <!-- <h5>Newsletter</h5>
                        <div class="newslatter-form">
                            <input type="text" placeholder="Your Email Here">
                            <button type="submit">Subscribe</button>
                        </div> -->
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="footer-item">
                        <h5>Contact Info</h5>
                        <ul>
                            <li><img src="{{url('')}}/new/img/placeholder.png" alt="" style="margin-right: 7px">12710 Telkom Landmark Tower - The Telkom Hub,<br />Jakarta Selatan, Indonesia</li>
                            <li><img src="{{url('')}}/new/img/phone.png" alt="" style="margin-right: 7px">+62 (811-1990-476)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="row pt-5">
                    <div class="col-lg-12 ">
                        <div class="small text-white text-center"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
&copy; PT Telekomunikasi Selular <script>document.write(new Date().getFullYear());</script>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{url('')}}/new/js/jquery-3.3.1.min.js"></script>
    <script src="{{url('')}}/new/js/bootstrap.min.js"></script>
    <script src="{{url('')}}/new/js/jquery.magnific-popup.min.js"></script>
    <script src="{{url('')}}/new/js/jquery-ui.min.js"></script>
    <script src="{{url('')}}/new/js/jquery.nice-select.min.js"></script>
    <script src="{{url('')}}/new/js/jquery.slicknav.js"></script>
    <script src="{{url('')}}/new/js/owl.carousel.min.js"></script>
    <script src="{{url('')}}/new/js/main.js"></script>
</body>

</html>