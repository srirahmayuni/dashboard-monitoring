<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>DASHBOARD OOKLA</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="{{url('')}}/css/isotope.css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/flexslider.css" type="text/css">
  <link rel="stylesheet" href="{{url('')}}/js/fancybox/jquery.fancybox.css" type="text/css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/bootstrap.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="{{url('')}}/css/style.css">
  <link rel="stylesheet" href="{{url('')}}/skin/default.css">
  <script src="https://unpkg.com/ag-grid-community@20.2.0/dist/ag-grid-community.min.js"></script>
  <style type="text/css">
    #myScrollTable tbody {
      clear: both;
      border: 1px solid #000000;
      height: 475px;
      overflow:auto;
      float:left;
      width:100%;
    }
  </style>
  <style type="text/css">
    * {
      box-sizing: border-box;
    }

    body {
      font-family: Arial, Helvetica, sans-serif;
    }
    .column {
      float: left;
      width: 33%;
      padding: 0 10px;
    }

    .row {margin: 0 -5px;}

    .row:after {
      content: "";
      display: table;
      clear: both;
    }
    .card {
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); /* this adds the "card" effect */
      padding: 30%;
      text-align: center;
      background-color: #f1f1f1;
    }
    @media screen and (max-width: 600px) {
      .column {
        width: 100%;
        display: block;
        margin-bottom: 20px;
      }
    }
  </style>
  <style>
    .dropClass {
      visibility: hidden;
      opacity: 0;
      transition:
      all .5s ease;
      background-color: transparent;
      min-width: 160px;
      overflow: auto;
      z-index: 1;
      height: 0;
    }
    .dropClass a {
      color: black;
      padding: 12px 16px;
      display: block;
    }
    .dropClass a:hover {background-color: rgba(255,255,255,.1);}
    .show {
      visibility: visible;
      opacity: 1;
      height: 100%;
      padding: 0.5rem 1rem;
      background-color: rgba(255,255,255,.3);
      margin-bottom: 1.5%;
      border-radius: 3%;
    }
    .putar {
      transform: rotate(90deg);
      transition: all .5s ease;
    }
    .brand-image {
      line-height: .8;
      max-height: 53px;
      width: auto;
      margin-left: 0.7rem;
      margin-right: .5rem;
      margin-top: -3px;
      float: none;
      opacity: .9;
    }
    .backgroundImg {
      width: auto;
      height: 100%;
      opacity: 1;
      position: absolute;
    }
    .backgroundImg2 {
      position: fixed;
      width: 100%;
      max-height: 56px;
      margin-left: -2%;
      opacity: 1;
    }
    .nav-item:hover {
      background-color: rgba(255,255,255,.3);
      border-radius: 5%;
      transition: all .2s ease;
    }
    .berisik {
      min-height:500px !important
    }
    .tesDiv {
      z-index: -1;
      opacity: .4;
      background: url(./dist/img/tesblek.png) center center
    }
    .tesDiv .bekgron{
      z-index: 1;
      opacity: 1
    }
    #bungkus {
      background: url(./dist/img/darkwall6.jpg) center center;
    }
    .garisijo {
      background-color: #800000;
      height: 3px;
      width: 100%;
      position: center;
      bottom: 0;
      left: 0;
    }
    .teksboks {
      width: 85%;
      position: absolute;
      bottom: 0;
      left: 0;
    }
    .teksne{
      margin:auto;
      position: absolute;
    }
    .boksHead {
      font-size: 30px;
      padding: 10px;
      font-weight: 500;
      border-radius: 0;
      background-color: rgba(255, 255, 255, .2);
      color: #96b28a;
      font-weight: bold;
      box-shadow: none;
      margin-bottom:0 !important;
    }
    .boksBody {
      height: 107px;
      border-radius: 0;
      background-color: rgba(255, 255, 255, .2);
      box-shadow: none;
    }
    .inner {
      padding:0 !important;
    }
    .card.chartcard {
      background-color:transparent;
      border: 0;
      border-radius: 0;
      box-shadow: none;
    }
    table {
      font-size: 14px;
      background-color: rgba(1, 14, 23,.9)!important;
      color:lightgrey;
    }
    tr > td:hover {
      background-color: rgba(1, 14, 23,.9)!important;
      color:lightgrey;
    }

    .table td, .table th {
      border: 1px solid #ddd;
    }

    thead {
      background-color: rgba(1, 14, 23,.9)!important;
      color:lightgrey;
    }
    .kepanjangan {
      font-size: 10px;
    }
    .kepanjangantot {
      font-size: 12px;
    }

    .page-link {
      color: #fff;
      background-color: #262c2e;
    }

    .social-part .fa{
      padding-right:20px;
    }
    ul li a{
      margin-right: 20px;
    }

    .bg-light {
      color:#fff;
      background-image:linear-gradient(to right, #1c1f20, #2d3436, #2d3436, #2d3436);
    } 

    .bg-light, .bg-light a {
      color: #fff!important;
    }

    .dropdown-menu {
      background-color: #262c2e;
      color: #0F2027;
    }

    .active1 {
      background-color: #6c7173; !important;
      color: #343a40 !important;
      font-weight: 600;
    }

    .item:hover {
      background-color: #0F2027;
    }

    .invisible {
      visibility: hidden;
    }

    .dropdown-item:focus, .dropdown-item:hover {
      background-color: #6c7173;
    }

    .card {
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
      transition: 0.3s; 
      border-radius: 5px; /* 5px rounded corners */
    }

    .card:hover {
      box-shadow: 0 8px 50px 0 rgba(0,0,0,0.2);
    } 

    .ag-header-cell-label {
      float: none !important;
      width: auto !important;
      justify-content: left;
    }

    .ag-fresh .ag-header-cell-menu-button{
      position: absolute;
      float: none;
    }

    .container {
      max-width: 100%; !important;
    }

    #my{
      zoom: 100%;
    }

  </style>
</head>

<body>
  <section id="header" class="appear">
    <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:40px; height:40px; background-color:rgba(0,0,0,0.3);" data-300="line-height:40px; height:40px; background-color:rgba(5, 42, 62, 1);">
      <!-- <div class="container"> -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="fa fa-bars color-white"></span>
          </button>
          <div class="navbar-logo">
            <img data-0="width:170px;" data-300=" width:100px;" src="img/oklaa.png" alt="" style="padding-left: 50px;">
          </div>
        </div>
        <div class="navbar-collapse collapse" style="padding-right: 150px;">
          <ul class="nav navbar-nav" data-0="margin-top:0px;" data-300="margin-top:0px;">
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="{{url('super_4g')}}">SUPER 4G</a></li>
            <li class="active"><a href="{{url('wcc_62')}}">WCC 62</a></li>
            <li><a href="{{url('csat_48')}}">CSAT 48</a></li>
          </ul>
        </div>
      <!-- </div> -->
    </div>
  </section>
  <section id="intro">
    <div class="intro-content">
      <div class="row" style="padding-top: 6%; padding-left: 0px; padding-right: 10px; margin-bottom: 0px;">
        <div class="col-lg-6" style="padding-right: 0px;">
          <div class="panel-heading" style="background-color: #800000;">
            <h4 style="font-family: calibri; color: white;"><strong>Operator Benchmark</strong></h4>
          </div>
          <div class="small-box boks boksBody" style="height: 445px;">
            <div class="inner">
              <div class="row">
                <div class="col-xs-6" style="padding-top: 0px; padding-left: 5px; padding-right: 0px; padding-bottom: 0px">
                  <div class="card-body1" style="width:100%; height:224px; border-right: 2px solid #800000; background-color: #343a40; border-bottom: 2px solid #800000">
                    <div id="chartorange1" style="width:100%; height:224px;"></div>
                  </div> 
                </div>
                <div class="col-xs-6" style="padding-top: 0px; padding-left: 0px; padding-right: 5px; padding-bottom: 0px">
                  <div class="card-body2" style="width:100%; height:224px; border-left: 2px solid #800000; background-color: #343a40; border-bottom: 2px solid #800000" >
                    <div id="charthijau" style="width:100%; height:224px;"></div>
                  </div> 
                </div>
              </div>
              <div class="row">
                <div class="col-xs-6" style="padding-top: 0px; padding-left: 5px; padding-right: 0px; padding-bottom: 0px">
                  <div class="card-body3" style="width:100%; height:223px; border-right: 2px solid #800000; background-color: #343a40; border-top: 2px solid #800000">
                    <div id="chartmerah" style="width:100%; height:223px;"></div>
                  </div> 
                </div>
                <div class="col-xs-6" style="padding-top: 0px; padding-left: 0px; padding-right: 5px; padding-bottom: 0px">
                  <div class="card-body4" style="width:100%; height:224px; border-left: 2px solid #800000; background-color: #343a40; border-top: 2px solid #800000">
                    <div id="chartorange2" style="width:100%; height:224px;"></div>
                  </div> 
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; padding-top: 10px;">
            <div class="panel-heading" style="background-color: #800000;">
              <h4 style="font-family: calibri; color: white;"><strong>Operator Benchmark – MC Loose</strong></h4>
            </div>

            <div class="small-box boks boksBody" style="height: 240px;">
              <div class="inner">
                <div class="col-xs-12" style="margin-top: 0px; padding-right: 0px; padding-left: 0px;">
                  <div id="area2g" style="width: 100%; height: 200px;" class="ag-theme-balham-dark"></div>
                  <script type="text/javascript" charset="utf-8">
                    (function() {

                      document.addEventListener('DOMContentLoaded', function() {

                        var gridDiv = document.querySelector('#area2g');  
                        var gridOptions = {
                          columnDefs: [
                          {headerName: 'CITIES', field: 'regional', width: 220},
                          {headerName: 'TOTAL MC SAMPLE', field: 'data1', width: 150, cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}},
                          {headerName: 'TSEL WIN', field: 'data2', width: 100,cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}},
                          {headerName: 'TSEL LOSE', field: 'data2', width: 100,cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}},
                          {headerName: 'NO TSEL', field: 'data2', width: 100,cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}},
                          {headerName: 'NO COMPETITOR', field: 'data2', width: 150,cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}}
                          ],

                          rowData: [
                          {regional: 'SOLO', data1: '100', data2: '1'},
                          {regional: 'PADANG', data1: '200', data2: '2'},
                          {regional: 'ACEH', data1: '300', data2: '3'}
                          ]

                        };

                        new agGrid.Grid(gridDiv, gridOptions);
                      });

                    })();

                  </script> 
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-6" style="padding-right: 0px;">
          <div class="panel-heading" style="background-color: #800000;">
            <h4 style="font-family: calibri; color: white;"><strong><span style="padding-left: 250px;">DOWNLOAD</span><span style="padding-left: 120px;">UPLOAD</span><span style="padding-left: 120px;">LATENCY</span></strong></h4>
          </div>

          <div class="small-box boks boksBody" style="height: 690px;">
            <div class="inner">
              <div class="col-xs-12" style="margin-top: 0px; padding-right: 0px; padding-left: 0px;">
                <div id="area3g" style="width: 100%; height: 685px;" class="ag-theme-balham-dark"></div>
                <script type="text/javascript" charset="utf-8">
                  (function() {

                    document.addEventListener('DOMContentLoaded', function() {

                      var gridDiv = document.querySelector('#area3g');  
                      var gridOptions = {
                        columnDefs: [
                        {headerName: 'CITIES', field: 'regional', width: 200,filter: 'agTextColumnFilter',cellStyle: {'border-right': '1px solid #696969'}},
                        {headerName: 'VAL', field: 'data1', width: 100,filter: 'agTextColumnFilter', cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}},
                        {headerName: 'RANK', field: 'data2', width: 100,filter: 'agTextColumnFilter',cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C','border-right': '1px solid #696969'};}},
                        {headerName: 'VAL', field: 'data3', width: 100,filter: 'agTextColumnFilter',cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}},
                        {headerName: 'RANK', field: 'data4', width: 100,filter: 'agTextColumnFilter',cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C','border-right': '1px solid #696969'};}},
                        {headerName: 'VAL', field: 'data5', width: 100,filter: 'agTextColumnFilter',cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}},
                        {headerName: 'RANK', field: 'data6', width: 100,filter: 'agTextColumnFilter',cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}}
                        ],


                        rowData: [
                        {regional: 'MEDAN', data1: '100', data2: '1', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'PALEMBANG', data1: '200', data2: '2', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'JAKARTA PUSAT', data1: '300', data2: '3', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'SURABAYA', data1: '500', data2: '5', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'BALIKPAPAN', data1: '600', data2: '6', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'MAKASAR', data1: '700', data2: '7', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'MEDAN', data1: '100', data2: '1', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'PALEMBANG', data1: '200', data2: '2', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'JAKARTA PUSAT', data1: '300', data2: '3', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'SURABAYA', data1: '500', data2: '5', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'BALIKPAPAN', data1: '600', data2: '6', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'MEDAN', data1: '100', data2: '1', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'PALEMBANG', data1: '200', data2: '2', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'JAKARTA PUSAT', data1: '300', data2: '3', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'SURABAYA', data1: '500', data2: '5', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'BALIKPAPAN', data1: '600', data2: '6', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'MEDAN', data1: '100', data2: '1', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'PALEMBANG', data1: '200', data2: '2', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'JAKARTA PUSAT', data1: '300', data2: '3', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'},
                        {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'}
                        ]

                      };

                      new agGrid.Grid(gridDiv, gridOptions);
                    });

})();

</script> 
</div>
</div>
</div><!-- BOKSBODY-->
</div>
</div>

</div>
</section>

<a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>
<script src="{{url('')}}/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="{{url('')}}/js/jquery.js"></script>
<script src="{{url('')}}/js/jquery.easing.1.3.js"></script>
<script src="{{url('')}}/js/bootstrap.min.js"></script>
<script src="{{url('')}}/js/jquery.isotope.min.js"></script>
<script src="{{url('')}}/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="{{url('')}}/js/skrollr.min.js"></script>
<script src="{{url('')}}/js/jquery.scrollTo.min.js"></script>
<script src="{{url('')}}/js/jquery.localScroll.min.js"></script>
<script src="{{url('')}}/js/stellar.js"></script>
<script src="{{url('')}}/js/jquery.appear.js"></script>
<script src="{{url('')}}/js/jquery.flexslider-min.js"></script>
<script src="{{url('')}}/contactform/contactform.js"></script>
<script src="{{url('')}}/js/main.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script type="text/javascript">

  Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
    '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
        [0, '#2a2a2b'],
        [1, '#3e3e40']
        ]
      },
      style: {
        fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063'
    },
    title: {
      style: {
        color: '#E0E0E3',
        fontSize: '20px'
      }
    },
    subtitle: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase'
      }
    },
    xAxis: {
      gridLineColor: '#343a40',
      labels: {
        style: {
          color: '#343a40'
        }
      },
      plotLines: [{
        dashStyle: 'Solid'
      }],
      lineColor: '#343a40',
      minorGridLineColor: '#343a40',
      tickColor: '#343a40',
      title: {
        style: {
          color: '#343a40'

        }
      }
    },
    exporting: {
      enabled: false
    },
    yAxis: {
      gridLineColor: '#343a40',
      labels: {
        style: {
          color: '#343a40'
        }
      },
      lineColor: '#343a40',
      minorGridLineColor: '#343a40',
      tickColor: '#343a40',
      tickWidth: 1,
      title: {
        style: {
          color: '#343a40'
        }
      }
    },
    tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
        color: '#F0F0F0'
      }
    },
    plotOptions: {
      series: {
        dataLabels: {
          color: '#F0F0F3',
          style: {
            fontSize: '13px'
          }
        },
        marker: {
          lineColor: '#333'
        }
      },
      boxplot: {
        fillColor: '#505053'
      },
      candlestick: {
        lineColor: 'white'
      },
      errorbar: {
        color: 'white'
      }
    },
    legend: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      itemStyle: {
        color: '#E0E0E3'
      },
      itemHoverStyle: {
        color: '#FFF'
      },
      itemHiddenStyle: {
        color: '#606063'
      },
      title: {
        style: {
          color: '#C0C0C0'
        }
      }
    },
    credits: {
      enabled: false
    },
    labels: {
      style: {
        color: '#707073'
      }
    },

    drilldown: {
      activeAxisLabelStyle: {
        color: '#F0F0F3'
      },
      activeDataLabelStyle: {
        color: '#F0F0F3'
      }
    },
    legend: {
      enabled: false
    },

    navigation: {
      buttonOptions: {
        symbolStroke: '#DDDDDD',
        theme: {
          fill: '#505053'
        }
      }
    },

    rangeSelector: {
      buttonTheme: {
        fill: '#505053',
        stroke: '#000000',
        style: {
          color: '#CCC'
        },
        states: {
          hover: {
            fill: '#707073',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          },
          select: {
            fill: '#000003',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          }
        }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
        backgroundColor: '#333',
        color: 'silver'
      },
      labelStyle: {
        color: 'silver'
      }
    },

    navigator: {
      handles: {
        backgroundColor: '#666',
        borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
        color: '#7798BF',
        lineColor: '#A6C7ED'
      },
      xAxis: {
        gridLineColor: '#505053'
      }
    },

    scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
    }
  };

  Highcharts.setOptions(Highcharts.theme);
  Highcharts.chart('charthijau', {

    chart: {
      type: 'scatter'
    },
    title: {
      text: ''
    },
    xAxis: {
      title: {
        enabled: true,
        text: ''
      },
      labels: {
        enabled: false
      },
      startOnTick: true,
      endOnTick: true,
      showLastLabel: true
    },
    yAxis: {
      title: {
        text: ''
      },
      labels: {
        enabled: false
      },
    },

    tooltip: {
      useHTML: true,
      headerFormat: '<table>',
      pointFormat: '<tr><th colspan="2"><span style="font-size:14px">{point.country}</span></th></tr>' +
      '<tr><th>X:</th><td>{point.x} Mbps</td></tr>' +
      '<tr><th>Y:</th><td>{point.y} Mbps</td></tr>',
      footerFormat: '</table>',
      followPointer: true
    },

    plotOptions: {
      series: {
        dataLabels: {
          enabled: true,
          format: '{point.name}'
        }
      }
    },
    credits: {
      enabled: false
    },

    series: [{
      name: 'Hijau',
      color: '#00FF00',
      data: [
      { x: 26, y: 30, z: 13, name: 'SUKOHARJO', country: 'SUKOHARJO' },
      { x: 30, y: 12, z: 14, name: 'PURWAKARTA', country: 'PURWAKARTA' },
      { x: 22, y: 33, z: 15, name: 'BANTUL', country: 'BANTUL' },
      { x: 32, y: 26, z: 12, name: 'KOTA PONTIANAK', country: 'KOTA PONTIANAK' },
      { x: 37, y: 22, z: 11, name: 'KOTA BANJARMASIN', country: 'KOTA BANJARMASIN' },
      { x: 49, y: 31, z: 16, name: 'KOTA CIREBON', country: 'KOTA CIREBON' },
      { x: 30, y: 40, z: 14, name: 'NOJOKERTO', country: 'NOJOKERTO' },
      { x: 23, y: 13, z: 10, name: 'KOTA SURABAYA', country: 'KOTA SURABAYA' },
      { x: 31, y: 23, z: 24, name: 'CIREBON', country: 'CIREBON' },
      { x: 20, y: 14, z: 10, name: 'SUBANG', country: 'SUBANG' },
      { x: 26, y: 24, z: 16, name: 'PASURUAN', country: 'PASURUAN' },
      { x: 35, y: 12, z: 35, name: 'KOTA MATARAM', country: 'KOTA MATARAM' },
      { x: 25, y: 20, z: 28, name: 'BOGOR', country: 'BOGOR' },
      { x: 24, y: 28, z: 15, name: 'KOTA DEPOK', country: 'KOTA DEPOK' },
      { x: 34, y: 13, z: 31, name: 'KOTA BOGOR', country: 'KOTA BOGOR' }
      ]
    }]

  });

  Highcharts.chart('chartorange1', {

    chart: {
      type: 'scatter'
    },
    title: {
      text: ''
    },
    xAxis: {
      title: {
        enabled: true,
        text: ''
      },
      labels: {
        enabled: false
      },
      startOnTick: true,
      endOnTick: true,
      showLastLabel: true
    },
    yAxis: {
      title: {
        text: ''
      },
      labels: {
        enabled: false
      },
    },

    tooltip: {
      useHTML: true,
      headerFormat: '<table>',
      pointFormat: '<tr><th colspan="2"><span style="font-size:14px">{point.country}</span></th></tr>' +
      '<tr><th>X:</th><td>{point.x} Mbps</td></tr>' +
      '<tr><th>Y:</th><td>{point.y} Mbps</td></tr>',
      footerFormat: '</table>',
      followPointer: true
    },

    plotOptions: {
      series: {
        dataLabels: {
          enabled: true,
          format: '{point.name}'
        }
      }
    },
    credits: {
      enabled: false
    },

    series: [{
      name: 'Orange',
      color: '#FF7F00',
      data: [
      { x: 9, y: 27, z: 13, name: 'INDRAMAYU', country: 'INDRAMAYU' },
      { x: 9, y: 18, z: 11, name: 'BANYUMAS', country: 'BANYUMAS' },
      { x: 7, y: 15, z: 12, name: 'KOTA MAGELANG', country: 'KOTA MAGELANG' },
      { x: 16, y: 8, z: 12, name: 'KOTA BANDUNG', country: 'KOTA BANDUNG' },
      { x: 25, y: 6, z: 12, name: 'PALEMBANG', country: 'PALEMBANG' }
      ]
    }]

  });
  Highcharts.chart('chartorange2', {

    chart: {
      type: 'scatter'
    },
    title: {
      text: ''
    },
    xAxis: {
      title: {
        enabled: true,
        text: ''
      },
      labels: {
        enabled: false
      },
      startOnTick: true,
      endOnTick: true,
      showLastLabel: true
    },
    yAxis: {
      title: {
        text: ''
      },
      labels: {
        enabled: false
      },
    },

    tooltip: {
      useHTML: true,
      headerFormat: '<table>',
      pointFormat: '<tr><th colspan="2"><span style="font-size:14px">{point.country}</span></th></tr>' +
      '<tr><th>X:</th><td>{point.x} Mbps</td></tr>' +
      '<tr><th>Y:</th><td>{point.y} Mbps</td></tr>',
      footerFormat: '</table>',
      followPointer: true
    },

    plotOptions: {
      series: {
        dataLabels: {
          enabled: true,
          format: '{point.name}'
        }
      }
    },
    credits: {
      enabled: false
    },

    series: [{
      name: 'Orange',
      color: '#FF7F00',
      data: [
      { x: 9, y: 27, z: 13, name: 'INDRAMAYU', country: 'INDRAMAYU' },
      { x: 9, y: 18, z: 11, name: 'BANYUMAS', country: 'BANYUMAS' },
      { x: 7, y: 15, z: 12, name: 'KOTA MAGELANG', country: 'KOTA MAGELANG' },
      { x: 16, y: 8, z: 12, name: 'KOTA BANDUNG', country: 'KOTA BANDUNG' },
      { x: 25, y: 6, z: 12, name: 'PALEMBANG', country: 'PALEMBANG' }
      ]
    }]

  });
  Highcharts.chart('chartmerah', {

    chart: {
      type: 'scatter'
    },
    title: {
      text: ''
    },
    xAxis: {
      title: {
        enabled: true,
        text: ''
      },
      labels: {
        enabled: false
      },
      startOnTick: true,
      endOnTick: true,
      showLastLabel: true
    },
    yAxis: {
      title: {
        text: ''
      },
      labels: {
        enabled: false
      },
    },

    tooltip: {
      useHTML: true,
      headerFormat: '<table>',
      pointFormat: '<tr><th colspan="2"><span style="font-size:14px">{point.country}</span></th></tr>' +
      '<tr><th>X:</th><td>{point.x} Mbps</td></tr>' +
      '<tr><th>Y:</th><td>{point.y} Mbps</td></tr>',
      footerFormat: '</table>',
      followPointer: true
    },

    plotOptions: {
      series: {
        dataLabels: {
          enabled: true,
          format: '{point.name}'
        }
      }
    },
    credits: {
      enabled: false
    },

    series: [{
      name: 'Merah',
      color: '#FF0000',
      data: [
      { x: 3, y: 6, z: 13, name: 'PADANG', country: 'PADANG' },
      { x: 9, y: 0, z: 11, name: 'ACEH', country: 'ACEH' },
      { x: 6, y: 9, z: 12, name: 'SOLO', country: 'SOLO' }
      ]
    }]

  });
</script>
</body>
</html>
