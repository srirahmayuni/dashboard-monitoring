<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>DASHBOARD OOKLA</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{url('')}}/template/tsel.jpg" rel="icon" type="image/png">
  <link rel="stylesheet" type="text/css" href="{{url('')}}/css/isotope.css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/flexslider.css" type="text/css">
  <link rel="stylesheet" href="{{url('')}}/js/fancybox/jquery.fancybox.css" type="text/css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/bootstrap.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="{{url('')}}/css/style.css">
  <link rel="stylesheet" href="{{url('')}}/skin/default.css">
  <script src="https://unpkg.com/ag-grid-enterprise@20.2.0/dist/ag-grid-enterprise.min.js"></script>
  <script src = "https://code.highcharts.com/highcharts.js"> </script>
  <script src="{{url('')}}/js/jquery.js"></script>
  <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <style type="text/css">
    #myScrollTable tbody {
      clear: both;
      border: 1px solid #000000;
      height: 475px;
      overflow:auto;
      float:left;
    }
  </style>
  <style type="text/css">
    * {
      box-sizing: border-box;
    }

    body {
      font-family: Arial, Helvetica, sans-serif;
    }
    .column {
      float: left;
      width: 33%;
      padding: 0 10px;
    }

    .row {margin: 0 -5px;}

    .row:after {
      content: "";
      display: table;
      clear: both;
    }

    .card {
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); 
      padding: 30%;
      text-align: center;
      background-color: #f1f1f1;
    }

    @media screen and (max-width: 600px) {
      .column {
        width: 100%;
        display: block;
        margin-bottom: 20px;
      }
    }
  </style>


  <style>
    .dropClass {
      visibility: hidden;
      opacity: 0;
      transition:
      all .5s ease;
      background-color: transparent;
      min-width: 160px;
      overflow: auto;
      z-index: 1;
      height: 0;
    }
    .dropClass a {
      color: black;
      padding: 12px 16px;
      display: block;
    }
    .dropClass a:hover {background-color: rgba(255,255,255,.1);}
    .show {
      visibility: visible;
      opacity: 1;
      height: 200%;
      /*padding: 0.5rem 1rem;*/
      background-color: rgba(255,255,255,.3);
      /*margin-bottom: 1.5%;*/
      border-radius: 3%;
    }
    .putar {
      transform: rotate(90deg);
      transition: all .5s ease;
    }
    .brand-image {
      line-height: .8;
      max-height: 53px;
      width: auto;
      margin-left: 0.7rem;
      margin-right: .5rem;
      margin-top: -3px;
      float: none;
      opacity: .9;
    }
    .backgroundImg {
      width: auto;
      height: 100%;
      opacity: 1;
      position: absolute;
    }
    .backgroundImg2 {
      position: fixed;
      width: 100%;
      max-height: 56px;
      margin-left: -2%;
      opacity: 1;
    }
    .nav-item:hover {
      background-color: rgba(255,255,255,.3);
      border-radius: 5%;
      transition: all .2s ease;
    }
    .berisik {
      min-height:500px !important
    }
    .tesDiv {
      z-index: -1;
      opacity: .4;
      background: url(./dist/img/tesblek.png) center center
    }
    .tesDiv .bekgron{
      z-index: 1;
      opacity: 1
    }
    #bungkus {
      background: url(./dist/img/darkwall6.jpg) center center;
    }
    .garisijo {
      background-color: #800000;
      height: 3px;
      width: 100%;
      position: center;
      bottom: 0;
      left: 0;
    }
    .teksboks {
      width: 85%;
      position: absolute;
      bottom: 0;
      left: 0;
    }
    .teksne{
      margin:auto;
      position: absolute;
    }
    .boksHead {
      font-size: 30px;
      padding: 10px;
      font-weight: 500;
      border-radius: 0;
      background-color: rgba(255, 255, 255, .2);
      color: #96b28a;
      font-weight: bold;
      box-shadow: none;
      margin-bottom:0 !important;
    }
    .boksBody {
      height: 107px;
      border-radius: 0;
      background-color: rgba(255, 255, 255, .2);
      box-shadow: none;
    }
    .inner {
      padding:0 !important;
    }
    .card.chartcard {
      background-color:transparent;
      border: 0;
      border-radius: 0;
      box-shadow: none;
    }
    table {
      font-size: 14px;
      background-color: rgba(1, 14, 23,.9)!important;
      color:lightgrey;
    }
    tr > td:hover {
      background-color: rgba(1, 14, 23,.9)!important;
      color:lightgrey;
    }

    .table td, .table th {
      border: 1px solid #ddd;
    }

    thead {
      background-color: rgba(1, 14, 23,.9)!important;
      color:lightgrey;
    }
    .kepanjangan {
      font-size: 10px;
    }
    .kepanjangantot {
      font-size: 12px;
    }
    .page-link {
      color: #fff;
      background-color: #262c2e;
    }

    .social-part .fa{
      padding-right:20px;
    }
    ul li a{
      margin-right: 20px;
    }

    .bg-light {
      color:#fff;
      background-image:linear-gradient(to right, #1c1f20, #2d3436, #2d3436, #2d3436);
    } 

    .bg-light, .bg-light a {
      color: #fff!important;
    }

    .dropdown-menu {
      background-color: #262c2e;
      color: #0F2027;
    }

    .active1 {
      background-color: #6c7173; !important;
      color: #343a40 !important;
      font-weight: 600;
    }

    .item:hover {
      background-color: #0F2027;
    }

    .invisible {
      visibility: hidden;
    }

    .dropdown-item:focus, .dropdown-item:hover {
      background-color: #6c7173;
    }

    .card {
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
      transition: 0.3s; 
      border-radius: 5px; /* 5px rounded corners */
    }

    .card:hover {
      box-shadow: 0 8px 50px 0 rgba(0,0,0,0.2);
    } 

    .ag-header-cell-label {
      float: none !important;
      width: auto !important;
      justify-content: left;
    }

    .ag-fresh .ag-header-cell-menu-button{
      position: absolute;
      float: none;
    }

    .container {
      max-width: 100%; !important;
    }

    #my{
      zoom: 100%;
    }

  </style>
  <style>
    .previous {
      background-color: #f1f1f1;
      color: black;
    }

    .next {
      background-color: #800000;
      color: white;
    }

    .round {
      border-radius: 50%;
    }
  </style>
  <style type="text/css">
    .select-css {
      font-size: 14px;
      font-family: sans-serif;
      font-weight: 600;
      color: #444;
      line-height: 1;
      padding: .5em 1.4em .4em .8em;
      width: 140px;
      height: 30px;
      max-width: 100%; 
      box-sizing: border-box;
      margin: 0;
      border: 1px solid #aaa;
      box-shadow: 0 1px 0 1px rgba(0,0,0,.04);
      border-radius: .5em;
      -moz-appearance: none;
      -webkit-appearance: none;
      appearance: none;
      background-color: #fff;
      background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E'),
      linear-gradient(to bottom, #ffffff 0%,#e5e5e5 100%);
      background-repeat: no-repeat, repeat;
      background-position: right .7em top 50%, 0 0;
      background-size: .65em auto, 100%;
      margin-top: 5px;
    }
    .select-css::-ms-expand {
      display: none;
    }
    .select-css:hover {
      border-color: #888;
    }
    .select-css:focus {
      border-color: #aaa;
      box-shadow: 0 0 1px 3px rgba(59, 153, 252, .7);
      box-shadow: 0 0 0 3px -moz-mac-focusring;
      color: #222; 
      outline: none;
    }
    .select-css option {
      font-weight:normal;
    }
  </style>
</head>

<body>
  <section id="header" class="appear">
    <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:40px; height:40px; background-color:rgba(0,0,0,0.3);" data-300="line-height:40px; height:40px; background-color:rgba(5, 42, 62, 1);">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="fa fa-bars color-white"></span>
          </button>
          <div class="navbar-logo">
            <!-- <a href="index.html"><img data-0="width:150px;" data-300=" width:120px;" src="img/tsel-white.png" alt=""></a> -->
            <img data-0="width:120px;" data-300=" width:100px;" src="img/oklaa.png" alt="">
          </div>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav" data-0="margin-top:0px;" data-300="margin-top:0px;">
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="{{url('super_4g')}}">SUPER 4G</a></li>
            <li><a href="{{url('wcc_62')}}">WCC 62</a></li>
            <li class="active"><a href="{{url('csat_48')}}">CSAT 48</a></li>
          </ul>
        </div>
        <!--/.navbar-collapse -->
      </div>
    </div>
  </section>
  <!-- awal section baris pertama-->
  <section id="intro">
    <div class="intro-content">
      <div class="row" style="padding-top: 70px; padding-left: 10px; padding-right: 10px; margin-bottom: 0px;">
        <div class="col-lg-6" style="padding-right: 0px;">
          <!-- <div class="panel panel-default"> -->
            <div class="panel-heading" style="background-color: #800000;">
              <h4 style="font-family: calibri; color: white;"><strong>Operator Benchmark</strong></h4>
            </div>
            <div class="small-box boks boksBody" style="height: 475px;">
              <div class="inner">
                <div class="row"> 
                  <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                    <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                      <div class="card-header1" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 4G
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal1"></i>
                      </div>
                      <!-- </div>                      -->
                      <div class="modal fade" id="modal1" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537;"> 
                            <div class="modal-header" style="text-align:left; color: white;"> DOWNLOAD 4G
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="chart11" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body1" style="width:100%; height:130px; background-color: #343a40">
                        <div id="chart1" style="width:100%; height:130px;"></div>
                      </div> 
                    </div> 

                    <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                      <div class="card-header2" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 3G
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal2"></i>
                      </div>
                      <!-- </div>                      -->
                      <div class="modal fade" id="modal2" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537"> 
                            <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 3G
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="chart22" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body2" style="width:100%; height:130px; background-color: #343a40">
                        <div id="chart2" style="width:100%; height:130px;"></div>
                      </div>          
                    </div>  
                  </div>

                  <div class="row"> 
                    <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                      <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                        <div class="card-header3" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD 4G
                          <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal3"></i>
                        </div>
                        <!-- </div>                      -->
                        <div class="modal fade" id="modal3" style="width: 100%;">
                          <div class="modal-dialog modal-lg" style="margin-top:90px;">
                            <div class="modal-content" style="background-color:#353537"> 
                              <div class="modal-header" style="text-align:center; color: white;"> UPLOAD 4G
                                <button type="button" class="close" data-dismiss="modal">

                                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                </button>
                              </div>
                              <div class="modal-body" style="color:#000"> 
                                <div id="chart33" style="width:100%; height:100%;"></div>
                              </div>
                            </div> 
                          </div>
                        </div>
                        <div class="card-body3" style="width:100%; height:130px; background-color: #343a40">
                          <div id="chart3" style="width:100%; height:130px;"></div>
                        </div> 
                      </div> 

                      <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                        <div class="card-header4" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD 3G
                          <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal4"></i>
                        </div>
                        <!-- </div>                      -->
                        <div class="modal fade" id="modal4" style="width: 100%;">
                          <div class="modal-dialog modal-lg" style="margin-top:90px;">
                            <div class="modal-content" style="background-color:#353537"> 
                              <div class="modal-header" style="text-align:center; color: white;"> UPLOAD 3G
                                <button type="button" class="close" data-dismiss="modal">

                                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                </button>
                              </div>
                              <div class="modal-body" style="color:#000"> 
                                <div id="chart44" style="width:100%; height:100%;"></div>
                              </div>
                            </div> 
                          </div>
                        </div>
                        <div class="card-body4" style="width:100%; height:130px; background-color: #343a40">
                          <div id="chart4" style="width:100%; height:130px;"></div>
                        </div>          
                      </div>  
                    </div>

                    <div class="row"> 
                      <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                        <div class="card-header5" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> LATENCY 4G
                          <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal5"></i>
                        </div>
                        <div class="modal fade" id="modal5" style="width: 100%;">
                          <div class="modal-dialog modal-lg" style="margin-top:90px;">
                            <div class="modal-content" style="background-color:#353537"> 
                              <div class="modal-header" style="text-align:center; color: white;"> LATENCY 4G
                                <button type="button" class="close" data-dismiss="modal">

                                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                </button>
                              </div>
                              <div class="modal-body" style="color:#000"> 
                                <div id="chart55" style="width:100%; height:100%;"></div>
                              </div>
                            </div> 
                          </div>
                        </div>
                        <div class="card-body5" style="width:100%; height:130px; background-color: #343a40">
                          <div id="chart5" style="width:100%; height:130px;"></div>
                        </div> 
                      </div> 

                      <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                        <div class="card-header6" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> LATENCY 3G
                          <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal6"></i>
                        </div>
                        <div class="modal fade" id="modal6" style="width: 100%;">
                          <div class="modal-dialog modal-lg" style="margin-top:90px;">
                            <div class="modal-content" style="background-color:#353537"> 
                              <div class="modal-header" style="text-align:center; color: white;"> LATENCY 3G
                                <button type="button" class="close" data-dismiss="modal">

                                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                </button>
                              </div>
                              <div class="modal-body" style="color:#000"> 
                                <div id="chart66" style="width:100%; height:100%;"></div>
                              </div>
                            </div> 
                          </div>
                        </div>
                        <div class="card-body6" style="width:100%; height:130px; background-color: #343a40">
                          <div id="chart6" style="width:100%; height:130px;"></div>
                        </div>          
                      </div>  
                    </div>
                  </div>
                </div><!-- BOKSBODY-->

                <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; padding-top: 10px;">
                  <div class="panel-heading" style="background-color: #800000;">
                    <h4 style="font-family: calibri; color: white;"><strong>Speed Test Distribution</strong></h4>
                  </div>
                  <div class="small-box boks boksBody" style="height: 160px;">
                    <div class="inner">
                      <div class="row">
                        <div class="col-md-4" style="margin-top:5px; padding-right: 0px;">
                          <div class="card-header7" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD
                            <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal7"></i>
                          </div>
                          <div class="modal fade" id="modal7" style="width: 100%;">
                            <div class="modal-dialog modal-lg" style="margin-top:90px;">
                              <div class="modal-content" style="background-color:#353537"> 
                                <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD
                                  <button type="button" class="close" data-dismiss="modal">

                                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                  </button>
                                </div>
                                <div class="modal-body" style="color:#000"> 
                                  <div id="chart77" style="width:100%; height:100%;"></div>
                                </div>
                              </div> 
                            </div>
                          </div>
                          <div class="card-body7" style="width:100%; height:130px; background-color: #343a40">
                            <div id="chart7" style="width:100%; height:130px;"></div>
                          </div>
                        </div>
                        <div class="col-md-4" style="margin-top:5px; padding-right: 0px;">
                          <div class="card-header8" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD
                            <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal8"></i>
                          </div>
                          <div class="modal fade" id="modal8" style="width: 100%;">
                            <div class="modal-dialog modal-lg" style="margin-top:90px;">
                              <div class="modal-content" style="background-color:#353537"> 
                                <div class="modal-header" style="text-align:center; color: white;"> UPLOAD
                                  <button type="button" class="close" data-dismiss="modal">

                                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                  </button>
                                </div>
                                <div class="modal-body" style="color:#000"> 
                                  <div id="chart88" style="width:100%; height:100%;"></div>
                                </div>
                              </div> 
                            </div>
                          </div>
                          <div class="card-body8" style="width:100%; height:130px; background-color: #343a40">
                            <div id="chart8" style="width:100%; height:130px;"></div>
                          </div>
                        </div>
                        <div class="col-md-4" style="margin-top:5px; padding-right: 15px;">
                          <div class="card-header9" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> WIFI
                            <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal9"></i>
                          </div>
                          <div class="modal fade" id="modal9" style="width: 100%;">
                            <div class="modal-dialog modal-lg" style="margin-top:90px;">
                              <div class="modal-content" style="background-color:#353537"> 
                                <div class="modal-header" style="text-align:center; color: white;"> WIFI
                                  <button type="button" class="close" data-dismiss="modal">

                                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                  </button>
                                </div>
                                <div class="modal-body" style="color:#000"> 
                                  <div id="chart99" style="width:100%; height:100%;"></div>
                                </div>
                              </div> 
                            </div>
                          </div>
                          <div class="card-body9" style="width:100%; height:130px; background-color: #343a40">
                            <div id="chart9" style="width:100%; height:130px;"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-md-6" style="padding-right: 0px;">
                <div class="panel-heading" style="background-color: #800000;">
                  <h4 style="font-family: calibri; color: white;"><strong><span style="padding-left: 250px;">DOWNLOAD</span><span style="padding-left: 120px;">UPLOAD</span><span style="padding-left: 120px;">LATENCY</span></strong></h4>
                </div>

                <div class="small-box boks boksBody" style="height: 690px;">
                  <div class="inner">
                    <div class="col-xs-12" style="margin-top: 0px; padding-right: 0px; padding-left: 0px;">
                      <div id="area3g" style="width: 100%; height: 685px;" class="ag-theme-balham-dark"></div>
                      <!-- <LABEL id="area2g" style="width: 100%; height: 467px; margin-bottom: 200px;" class="ag-theme-balham-dark"> -->
                        <script type="text/javascript" charset="utf-8">
                          (function() {

                            document.addEventListener('DOMContentLoaded', function() {

                              var gridDiv = document.querySelector('#area3g');  
                              var gridOptions = {
                                columnDefs: [
                                {headerName: 'CITIES', field: 'regional', width: 200,filter: 'agTextColumnFilter',cellStyle: {'border-right': '1px solid #696969'}},
                                {headerName: 'VAL', field: 'data1', width: 100,filter: 'agTextColumnFilter', cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}},
                                {headerName: 'RANK', field: 'data2', width: 100,filter: 'agTextColumnFilter',cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C','border-right': '1px solid #696969'};}},
                                {headerName: 'VAL', field: 'data3', width: 100,filter: 'agTextColumnFilter',cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}},
                                {headerName: 'RANK', field: 'data4', width: 100,filter: 'agTextColumnFilter',cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C','border-right': '1px solid #696969'};}},
                                {headerName: 'VAL', field: 'data5', width: 100,filter: 'agTextColumnFilter',cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}},
                                {headerName: 'RANK', field: 'data6', width: 100,filter: 'agTextColumnFilter',cellStyle: function(params){return {textAlign: 'left', color: '#78DE5C'};}}
                                ],


                                rowData: [
                                {regional: 'MEDAN', data1: '100', data2: '1', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'PALEMBANG', data1: '200', data2: '2', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'JAKARTA PUSAT', data1: '300', data2: '3', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'SURABAYA', data1: '500', data2: '5', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'BALIKPAPAN', data1: '600', data2: '6', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'MAKASAR', data1: '700', data2: '7', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'MEDAN', data1: '100', data2: '1', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'PALEMBANG', data1: '200', data2: '2', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'JAKARTA PUSAT', data1: '300', data2: '3', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'SURABAYA', data1: '500', data2: '5', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'BALIKPAPAN', data1: '600', data2: '6', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'MEDAN', data1: '100', data2: '1', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'PALEMBANG', data1: '200', data2: '2', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'JAKARTA PUSAT', data1: '300', data2: '3', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'SURABAYA', data1: '500', data2: '5', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'BALIKPAPAN', data1: '600', data2: '6', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'MEDAN', data1: '100', data2: '1', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'PALEMBANG', data1: '200', data2: '2', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'JAKARTA PUSAT', data1: '300', data2: '3', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'},
                                {regional: 'BANDUNG', data1: '400', data2: '4', data3: '100', data4: '1', data5: '100', data6: '1'}
                                ]

                              };

                              new agGrid.Grid(gridDiv, gridOptions);
                            });

})();

</script> 
</div>
</div>
</div><!-- BOKSBODY-->
</div>
</div>

</div>
</section>

<a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>
<script src="{{url('')}}/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="{{url('')}}/js/jquery.js"></script>
<script src="{{url('')}}/js/jquery.easing.1.3.js"></script>
<script src="{{url('')}}/js/bootstrap.min.js"></script>
<script src="{{url('')}}/js/jquery.isotope.min.js"></script>
<script src="{{url('')}}/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="{{url('')}}/js/skrollr.min.js"></script>
<script src="{{url('')}}/js/jquery.scrollTo.min.js"></script>
<script src="{{url('')}}/js/jquery.localScroll.min.js"></script>
<script src="{{url('')}}/js/stellar.js"></script>
<script src="{{url('')}}/js/jquery.appear.js"></script>
<script src="{{url('')}}/js/jquery.flexslider-min.js"></script>
<script src="{{url('')}}/contactform/contactform.js"></script>
<script src="{{url('')}}/js/main.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

</body>
</html>
