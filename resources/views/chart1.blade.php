<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
    	<div class="card-header1" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 4G
    		<i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal1"></i>
    	</div>
    	<div class="modal fade" id="modal1" style="width: 100%;">
    		<div class="modal-dialog modal-lg" style="margin-top:90px;">
    			<div class="modal-content" style="background-color:#353537;"> 
    				<div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 4G
    					<button type="button" class="close" data-dismiss="modal">
    						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    					</button>
    				</div>
    				<div class="modal-body" style="color:#000"> 
    					<div id="chart11" style="width:100%; height:100%;"></div>
    				</div>
    			</div> 
    		</div>
    	</div>
    	<div class="card-body1" style="width:100%; height:130px; background-color: #343a40">
    		<div id="chart1" style="width:100%; height:130px;"></div>
    	</div> 
    </body>
    <script src="{{url('')}}/js/jquery.js"></script>
    <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>

<script>

  Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
    '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
        [0, '#2a2a2b'],
        [1, '#3e3e40']
        ]
      },
      style: {
        fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063',
      marginTop: 15
    },
    title: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase',
        fontSize: '20px'
      }
    },
    subtitle: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase'
      }
    },
    xAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
        style: {
          color: '#A0A0A3'

        }
      }
    },
    yAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
        style: {
          color: '#A0A0A3'
        }
      }
    },
    tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
        color: '#F0F0F0'
      }
    },
    plotOptions: {
      series: {
        dataLabels: {
          color: '#B0B0B3'
        },
        marker: {
          lineColor: '#333'
        }
      },
      boxplot: {
        fillColor: '#505053'
      },
      candlestick: {
        lineColor: 'white'
      },
      errorbar: {
        color: 'white'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '9px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '9px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '9px'
      }
    },
    credits: {
      enabled: false
    },
    labels: {
      style: {
        color: '#707073'
      }
    },

    drilldown: {
      activeAxisLabelStyle: {
        color: '#F0F0F3'
      },
      activeDataLabelStyle: {
        color: '#F0F0F3'
      }
    },

    navigation: {
      buttonOptions: {
        symbolStroke: '#DDDDDD',
        theme: {
          fill: '#505053'
        }
      }
    },

    rangeSelector: {
      buttonTheme: {
        fill: '#505053',
        stroke: '#000000',
        style: {
          color: '#CCC'
        },
        states: {
          hover: {
            fill: '#707073',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          },
          select: {
            fill: '#000003',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          }
        }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
        backgroundColor: '#333',
        color: 'silver'
      },
      labelStyle: {
        color: 'silver'
      }
    },

    navigator: {
      handles: {
        backgroundColor: '#666',
        borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
        color: '#7798BF',
        lineColor: '#A6C7ED'
      },
      xAxis: {
        gridLineColor: '#505053'
      }
    },

    scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
    },

    legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
    background2: '#505053',
    dataLabelsColor: '#B0B0B3',
    textColor: '#C0C0C0',
    contrastTextColor: '#F0F0F3',
    maskColor: 'rgba(255,255,255,0.3)'
  };

  Highcharts.setOptions(Highcharts.theme);
  Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
      return {
        radialGradient: {
          cx: 0.5,
          cy: 0.3,
          r: 0.7
        },
        stops: [
        [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
              };
            })
  });

  var threedownload4g = <?php echo json_encode($threedownload4g); ?>;
  var indosatdownload4g = <?php echo json_encode($indosatdownload4g); ?>;
  var smartfrendownload4g = <?php echo json_encode($smartfrendownload4g); ?>;
  var xldownload4g = <?php echo json_encode($xldownload4g); ?>;
  var telkomseldownload4g = <?php echo json_encode($telkomseldownload4g); ?>;
  var week_download4g = <?php echo json_encode($week_download4g); ?>;

  var threedownload4g_ = <?php echo json_encode($threedownload4g[3]); ?>;
  var indosatdownload4g_ = <?php echo json_encode($indosatdownload4g[3]); ?>;
  var smartfrendownload4g_ = <?php echo json_encode($smartfrendownload4g[3]); ?>;
  var xldownload4g_ = <?php echo json_encode($xldownload4g[3]); ?>;
  var telkomseldownload4g_ = <?php echo json_encode($telkomseldownload4g[3]); ?>;
  var week_download4g_ = <?php echo json_encode($week_download4g[3]); ?>;


  Highcharts.chart('chart11', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_download4g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: [telkomseldownload4g_],
    color: '#FF0000'

  }, {
    name: 'ISAT',
    data: [indosatdownload4g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xldownload4g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threedownload4g_],
    color: '#808080'
  }, {
    name: 'SF',
    data: [smartfrendownload4g_],
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart1', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_download4g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    exporting: {
      enabled: false
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
   },
   series: [{
    name: 'TSEL',
    data: [telkomseldownload4g_],
    color: '#FF0000'

  }, {
    name: 'ISAT',
    data: [indosatdownload4g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xldownload4g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threedownload4g_],
    color: '#808080'
  }, {
    name: 'SF',
    data: [smartfrendownload4g_],
    color: '#FF7F00'

  }]
});

  var threedownload3g = <?php echo json_encode($threedownload3g); ?>;
  var indosatdownload3g = <?php echo json_encode($indosatdownload3g); ?>;
  var smartfrendownload3g = <?php echo json_encode($smartfrendownload3g); ?>;
  var xldownload3g = <?php echo json_encode($xldownload3g); ?>;
  var telkomseldownload3g = <?php echo json_encode($telkomseldownload3g); ?>;
  var week_download3g = <?php echo json_encode($week_download3g); ?>;

  var threedownload3g_ = <?php echo json_encode($threedownload3g[3]); ?>;
  var indosatdownload3g_ = <?php echo json_encode($indosatdownload3g[3]); ?>;
  var smartfrendownload3g_ = <?php echo json_encode($smartfrendownload3g[3]); ?>;
  var xldownload3g_ = <?php echo json_encode($xldownload3g[3]); ?>;
  var telkomseldownload3g_ = <?php echo json_encode($telkomseldownload3g[3]); ?>;
  var week_download3g_ = <?php echo json_encode($week_download3g[3]); ?>;

Highcharts.chart('chart22', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_download3g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    // exporting: {
    //   enabled: false
    // },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: [telkomseldownload3g_],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatdownload3g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xldownload3g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threedownload3g_],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrendownload3g_],
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart2', {
   chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_download3g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   series: [{
    name: 'TSEL',
    data: [telkomseldownload3g_],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatdownload3g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xldownload3g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threedownload3g_],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrendownload3g_],
    color: '#FF7F00'

  }]
});

  var threeupload4g = <?php echo json_encode($threeupload4g); ?>;
  var indosatupload4g = <?php echo json_encode($indosatupload4g); ?>;
  var smartfrenupload4g = <?php echo json_encode($smartfrenupload4g); ?>;
  var xlupload4g = <?php echo json_encode($xlupload4g); ?>;
  var telkomselupload4g = <?php echo json_encode($telkomselupload4g); ?>;
  var week_upload4g = <?php echo json_encode($week_upload4g); ?>;

  var threeupload4g_ = <?php echo json_encode($threeupload4g[3]); ?>;
  var indosatupload4g_ = <?php echo json_encode($indosatupload4g[3]); ?>;
  var smartfrenupload4g_ = <?php echo json_encode($smartfrenupload4g[3]); ?>;
  var xlupload4g_ = <?php echo json_encode($xlupload4g[3]); ?>;
  var telkomselupload4g_ = <?php echo json_encode($telkomselupload4g[3]); ?>;
  var week_upload4g_ = <?php echo json_encode($week_upload4g[3]); ?>;

  Highcharts.chart('chart3', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_upload4g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomselupload4g_],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatupload4g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xlupload4g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threeupload4g_],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenupload4g_],
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart33', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_upload4g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomselupload4g_],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatupload4g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xlupload4g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threeupload4g_],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenupload4g_],
    color: '#FF7F00'

  }]
});

  var threeupload3g = <?php echo json_encode($threeupload3g); ?>;
  var indosatupload3g = <?php echo json_encode($indosatupload3g); ?>;
  var smartfrenupload3g = <?php echo json_encode($smartfrenupload3g); ?>;
  var xlupload3g = <?php echo json_encode($xlupload3g); ?>;
  var telkomselupload3g = <?php echo json_encode($telkomselupload3g); ?>;
  var week_upload3g = <?php echo json_encode($week_upload3g); ?>;

  var threeupload3g_ = <?php echo json_encode($threeupload3g[3]); ?>;
  var indosatupload3g_ = <?php echo json_encode($indosatupload3g[3]); ?>;
  var smartfrenupload3g_ = <?php echo json_encode($smartfrenupload3g[3]); ?>;
  var xlupload3g_ = <?php echo json_encode($xlupload3g[3]); ?>;
  var telkomselupload3g_ = <?php echo json_encode($telkomselupload3g[3]); ?>;
  var week_upload3g_ = <?php echo json_encode($week_upload3g[3]); ?>;

Highcharts.chart('chart4', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_upload3g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   exporting: {
    enabled: false
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomselupload3g_],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatupload3g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xlupload3g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threeupload3g_],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenupload3g_],
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart44', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_upload3g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
  //  exporting: {
  //   enabled: false
  // },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomselupload3g_],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatupload3g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xlupload3g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threeupload3g_],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenupload3g_],
    color: '#FF7F00'

  }]
});

  var threelatency4g = <?php echo json_encode($threelatency4g); ?>;
  var indosatlatency4g = <?php echo json_encode($indosatlatency4g); ?>;
  var smartfrenlatency4g = <?php echo json_encode($smartfrenlatency4g); ?>;
  var xllatency4g = <?php echo json_encode($xllatency4g); ?>;
  var telkomsellatency4g = <?php echo json_encode($telkomsellatency4g); ?>;
  var week_latency4g = <?php echo json_encode($week_latency4g); ?>;

  var threelatency4g_ = <?php echo json_encode($threelatency4g[3]); ?>;
  var indosatlatency4g_ = <?php echo json_encode($indosatlatency4g[3]); ?>;
  var smartfrenlatency4g_ = <?php echo json_encode($smartfrenlatency4g[3]); ?>;
  var xllatency4g_ = <?php echo json_encode($xllatency4g[3]); ?>;
  var telkomsellatency4g_ = <?php echo json_encode($telkomsellatency4g[3]); ?>;
  var week_latency4g_ = <?php echo json_encode($week_latency4g[3]); ?>;

  Highcharts.chart('chart5', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_latency4g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  exporting: {
    enabled: false
  },
  series: [{
    name: 'TSEL',
    data: [telkomsellatency4g_],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatlatency4g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xllatency4g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threelatency4g_],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenlatency4g_],
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart55', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_latency4g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: [telkomsellatency4g_],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatlatency4g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xllatency4g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threelatency4g_],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenlatency4g_],
    color: '#FF7F00'

  }]
});

  var threelatency3g = <?php echo json_encode($threelatency3g); ?>;
  var indosatlatency3g = <?php echo json_encode($indosatlatency3g); ?>;
  var smartfrenlatency3g = <?php echo json_encode($smartfrenlatency3g); ?>;
  var xllatency3g = <?php echo json_encode($xllatency3g); ?>;
  var telkomsellatency3g = <?php echo json_encode($telkomsellatency3g); ?>;
  var week_latency3g = <?php echo json_encode($week_latency3g); ?>;

  var threelatency3g_ = <?php echo json_encode($threelatency3g[3]); ?>;
  var indosatlatency3g_ = <?php echo json_encode($indosatlatency3g[3]); ?>;
  var smartfrenlatency3g_ = <?php echo json_encode($smartfrenlatency3g[3]); ?>;
  var xllatency3g_ = <?php echo json_encode($xllatency3g[3]); ?>;
  var telkomsellatency3g_ = <?php echo json_encode($telkomsellatency3g[3]); ?>;
  var week_latency3g_ = <?php echo json_encode($week_latency3g[3]); ?>;

Highcharts.chart('chart6', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_latency3g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomsellatency3g_],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatlatency3g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xllatency3g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threelatency3g_],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenlatency3g_],
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart66', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_latency3g_]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
   legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomsellatency3g_],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatlatency3g_],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xllatency3g_],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threelatency3g_],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenlatency3g_],
    color: '#FF7F00'

  }]
});


  Highcharts.chart('charts1', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload4g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts11', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload4g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts3', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   exporting: {
    enabled: false
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload4g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts33', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload4g,
    color: '#FF7F00'

  }]
});


  Highcharts.chart('charts5', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency4g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts55', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
   legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency4g,
    color: '#FF7F00'

  }]
});


  Highcharts.chart('charts2', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts22', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g,
    color: '#FF7F00'

  }]
});


  Highcharts.chart('charts4', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   exporting: {
    enabled: false
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts44', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts6', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts66', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
   legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency3g,
    color: '#FF7F00'

  }]
});

  var android_tsel = <?php echo json_encode($android_tsel); ?>;
  var windows_tsel = <?php echo json_encode($windows_tsel); ?>;
  var iphone_tsel = <?php echo json_encode($iphone_tsel); ?>;
  Highcharts.chart('piechart1', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        size: 120,
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: "TELKOMSEL ONLY",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_tsel),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_tsel),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_tsel),
        drilldown: "iPhone"
      }
      ]
    }]
  });


  Highcharts.chart('piechart11', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    series: [{
      name: "TELKOMSEL ONLY",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_tsel),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_tsel),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_tsel),
        drilldown: "iPhone"
      }
      ]
    }]
  });

  var android_all = <?php echo json_encode($android_all); ?>;
  var windows_all = <?php echo json_encode($windows_all); ?>;
  var iphone_all = <?php echo json_encode($iphone_all); ?>;

  Highcharts.chart('piechart2', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        size: 120,
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: "ALL OPERATOR",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_all),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_all),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_all),
        drilldown: "iPhone"
      }
      ]
    }]
  });

  Highcharts.chart('piechart22', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    series: [{
      name: "ALL OPERATOR",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_all),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_all),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_all),
        drilldown: "iPhone"
      }
      ]
    }]
  });


  var subscribers = <?php echo json_encode($subscribers); ?>;
  var throughput = <?php echo json_encode($throughput); ?>;
  var vendor = <?php echo json_encode($vendor); ?>;

  Highcharts.chart('combichart', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },
    xAxis: [{
      categories: vendor,
      crosshair: true
    }],
    yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: 'green'
        }
      },
      title: {
        text: 'Subscribers',
        style: {
          color: 'green'
        }
      }
    }, { // Secondary yAxis
      title: {
        text: 'Throughput',
        style: {
          color: '#008B8B'
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: '#008B8B'
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },
    exporting: {
      enabled: false
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      x: -15,
      verticalAlign: 'top',
      y: -10,
      floating: true,
      backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            '#00000'
    },
    series: [{
      name: 'Throughput',
      // color: '#008B8B',
      data: throughput,
      tooltip: {
        valueSuffix: 'Mbps'
      }

    }, {
      name: 'Subscribers',
      // color: '#FFD700',
      data: subscribers,
      tooltip: {
        valueSuffix: ''
      }
    }]
  });

  Highcharts.chart('combichart2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    subtitle: {
      text: ''
    },
    xAxis: [{
      categories: vendor,
      crosshair: true
    }],
    yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: 'green'
        }
      },
      title: {
        text: 'Subscribers',
        style: {
          color: 'green'
        }
      }
    }, { // Secondary yAxis
      title: {
        text: 'Throughput',
        style: {
          color: '#008B8B'
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: '#008B8B'
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },
    legend: {
      layout: 'vertical',
      align: 'left',
      x: 60,
      verticalAlign: 'top',
      y: 0,
      floating: true,
      backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            '#00000'
          },
          series: [{
            name: 'Throughput',
            // color: '#008B8B',
            data: throughput,
            tooltip: {
              valueSuffix: ' Mbps'
            }

          }, {
            name: 'Subscribers',
            // color: '#FFD700',
            data: subscribers,
            tooltip: {
              valueSuffix: ''
            }
          }]
        });

  var subecribersdownload = <?php echo json_encode($subecribersdownload); ?>;
  var speedrangedownload = <?php echo json_encode($speedrangedownload); ?>;

  Highcharts.chart('line1', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangedownload
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Subscribers'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Download',
      data: subecribersdownload
    }]
  });

  Highcharts.chart('line11', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangedownload
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Subscribers'
      }
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Download',
      data: subecribersdownload
    }]
  });

  var subecribersupload = <?php echo json_encode($subecribersupload); ?>;
  var speedrangeupload = <?php echo json_encode($speedrangeupload); ?>;

  Highcharts.chart('line2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangeupload
    },
    yAxis: {
      title: {
        text: 'Subscribers'
      }
    },
    tooltip: {
     pointFormat: '<b>{point.y:.0f} Kbps</b>'
   },
   legend: {
    enabled: false
  },
  exporting: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0
    }
  },
  series: [{
    name: 'Nation Upload',
    data: subecribersupload
  }],


});

  Highcharts.chart('line22', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangeupload
    },
    yAxis: {
      title: {
        text: 'Subscribers'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    legend: {
      layout: 'vertical',
      align: 'center',
      verticalAlign: 'top'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Upload',
      data: subecribersupload
    }],

  });
</script>
</html>