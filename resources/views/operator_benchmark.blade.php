<!DOCTYPE html>
<html>

<head>
  <!-- BASICS -->
  <meta charset="utf-8">
  <title>DASHBOARD OOKLA</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{url('')}}/template/tsel.jpg" rel="icon" type="image/png">
  <link rel="stylesheet" type="text/css" href="{{url('')}}/css/isotope.css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/flexslider.css" type="text/css">
  <link rel="stylesheet" href="{{url('')}}/js/fancybox/jquery.fancybox.css" type="text/css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/bootstrap.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="{{url('')}}/css/style.css">
  <link rel="stylesheet" href="{{url('')}}/skin/default.css">
  <script src="https://unpkg.com/ag-grid-enterprise@20.2.0/dist/ag-grid-enterprise.min.js"></script>
  <script src = "https://code.highcharts.com/highcharts.js"> </script>
  <script src="{{url('')}}/js/jquery.js"></script>
  <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>  
<script>
    Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
    '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
        [0, '#2a2a2b'],
        [1, '#3e3e40']
        ]
      },
      style: {
        fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063'
    },
    title: {
      style: {
        color: '#E0E0E3',
        fontSize: '20px'
      }
    },
    subtitle: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase'
      }
    },
    xAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
        style: {
          color: '#A0A0A3'

        }
      }
    },
    yAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
        style: {
          color: '#A0A0A3'
        }
      }
    },
    tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
        color: '#F0F0F0'
      }
    },
    plotOptions: {
      series: {
        dataLabels: {
          color: '#F0F0F3',
          style: {
            fontSize: '13px'
          }
        },
        marker: {
          lineColor: '#333'
        }
      },
      boxplot: {
        fillColor: '#505053'
      },
      candlestick: {
        lineColor: 'white'
      },
      errorbar: {
        color: 'white'
      }
    },
    legend: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      itemStyle: {
        color: '#E0E0E3'
      },
      itemHoverStyle: {
        color: '#FFF'
      },
      itemHiddenStyle: {
        color: '#606063'
      },
      title: {
        style: {
          color: '#C0C0C0'
        }
      }
    },
    credits: {
      style: {
        color: '#666'
      }
    },
    labels: {
      style: {
        color: '#707073'
      }
    },

    drilldown: {
      activeAxisLabelStyle: {
        color: '#F0F0F3'
      },
      activeDataLabelStyle: {
        color: '#F0F0F3'
      }
    },

    navigation: {
      buttonOptions: {
        symbolStroke: '#DDDDDD',
        theme: {
          fill: '#505053'
        }
      }
    },

    rangeSelector: {
      buttonTheme: {
        fill: '#505053',
        stroke: '#000000',
        style: {
          color: '#CCC'
        },
        states: {
          hover: {
            fill: '#707073',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          },
          select: {
            fill: '#000003',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          }
        }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
        backgroundColor: '#333',
        color: 'silver'
      },
      labelStyle: {
        color: 'silver'
      }
    },

    navigator: {
      handles: {
        backgroundColor: '#666',
        borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
        color: '#7798BF',
        lineColor: '#A6C7ED'
      },
      xAxis: {
        gridLineColor: '#505053'
      }
    },

    scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
    }
  };

  Highcharts.setOptions(Highcharts.theme);
   Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
      return {
        radialGradient: {
          cx: 0.5,
          cy: 0.3,
          r: 0.7
        },
        stops: [
        [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
              };
            })
  });

   $(document).ready(function(){
    $("#week").change(function() {
      var week = $("#week").val();
      $.ajax({
        url:"http://10.54.36.49/api-ookla/public/analytics/quadran_target/week/"+week,
        type:"get",
        success: function(result){
          var res = JSON.parse(result); 

          res.forEach(myFunction);
          function myFunction(value) {
            var quadrantarget_hijau = value['hijau']
            console.log('data1',quadrantarget_hijau)
            var quadrantarget_orange = value['orange']
            console.log('data2',quadrantarget_orange)
            var quadrantarget_red = value['red']
            console.log('data3',quadrantarget_red)
            

          // res.forEach(myFunction2);
          // function myFunction2(value2) {
          //   var quadrantarget_orange = value2['orange']
          //   console.log('data2',quadrantarget_orange)
          // }
          // res.forEach(myFunction3);
          // function myFunction3(value3) {
          //   var quadrantarget_red = value3['red']
          //    console.log('data3',quadrantarget_red)
          // }
          console.log('data1',quadrantarget_hijau)
          console.log('data2',quadrantarget_orange)
          console.log('data3',quadrantarget_red)

          Highcharts.chart('chart1', {

            chart: {
              type: 'scatter',
              plotBorderWidth: 1
            },

            legend: {
              enabled: false
            },

            title: {
              text: 'Quadran Competition City Level'
            },

            legend: {
              layout: 'vertical',
              align: 'left',
              verticalAlign: 'top',
              x: 20,
              y: 0,
              floating: true,
              backgroundColor: '#800000',
              borderWidth: 1
            },
            xAxis: {
              gridLineWidth: 1,
              title: {
                text: 'OOKLA'
              },
              labels: {
                format: '{value}'
              },
              plotLines: [{
                color: '#800000',
                dashStyle: 'Solid',
                width: 3,
                value: 0,
                label: {
                  rotation: 0,
                  y: 15,
                  style: {
                    fontStyle: 'italic'
                  },
                  text: ''
                },
                zIndex: 3
              }]
            },

            yAxis: {
              startOnTick: false,
              endOnTick: false,
              title: {
                text: 'Open Signal'
              },
              labels: {
                format: '{value}'
              },
              maxPadding: 0.2,
              plotLines: [{
                color: '#800000',
                dashStyle: 'Solid',
                width: 3,
                value: 0,
                label: {
                  align: 'right',
                  style: {
                    fontStyle: 'italic'
                  },
                  text: '',
                  x: -10
                },
                zIndex: 3
              }]
            },

            tooltip: {
              useHTML: true,
              headerFormat: '<table>',
              pointFormat: '<tr><th colspan="2"><span style="font-size:14px">{point.name}</span></th></tr>' +
              '<tr><th>Detail &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.detail}</td></tr>' +
              '<tr><th>OOKLA GAP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</th><td>{point.x}</td></tr>' +
              '<tr><th>Open Signal GAP &nbsp;: &nbsp;</th><td>{point.y}</td></tr>' + 
              '<tr><th>Info OOKKLA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.info_ookla}</td></tr>' +
              '<tr><th>Info Open Signal &nbsp;&nbsp;: &nbsp;</th><td>{point.info_opensignal}</td></tr>',
              footerFormat: '</table>',
              followPointer: true
            },

            plotOptions: {
              series: {
                dataLabels: {
                  enabled: true,
                  format: '{point.name}'
                }
              }
            },

            series: [{
              name: 'Hijau',
              color: '#00FF00',
              data: quadrantarget_hijau
            },{
              name: 'Orange',
              color: '#FF7F00',
              data: quadrantarget_orange
            },{
              name: 'Merah',
              color: '#FF0000',
              data: quadrantarget_red
            }]

          });

          Highcharts.chart('chart11', {

            chart: {
              type: 'scatter',
              plotBorderWidth: 1
            },

            legend: {
              enabled: false
            },

            title: {
              text: 'Quadran Competition City Level'
            },

            legend: {
              layout: 'vertical',
              align: 'left',
              verticalAlign: 'top',
              x: 20,
              y: 0,
              floating: true,
              backgroundColor: '#800000',
              borderWidth: 1
            },

    // subtitle: {
    //     text: 'Source: <a href="http://www.euromonitor.com/">Euromonitor</a> and <a href="https://data.oecd.org/">OECD</a>'
    // },

    xAxis: {
      gridLineWidth: 1,
      title: {
        text: 'OOKLA'
      },
      labels: {
        format: '{value}'
      },
      plotLines: [{
        color: '#800000',
        dashStyle: 'Solid',
        width: 3,
        value: 0,
        label: {
          rotation: 0,
          y: 15,
          style: {
            fontStyle: 'italic'
          },
          text: ''
        },
        zIndex: 3
      }]
    },

    yAxis: {
      startOnTick: false,
      endOnTick: false,
      title: {
        text: 'Open Signal'
      },
      labels: {
        format: '{value}'
      },
      maxPadding: 0.2,
      plotLines: [{
        color: '#800000',
        dashStyle: 'Solid',
        width: 3,
        value: 0,
        label: {
          align: 'right',
          style: {
            fontStyle: 'italic'
          },
          text: '',
          x: -10
        },
        zIndex: 3
      }]
    },

    tooltip: {
      useHTML: true,
      headerFormat: '<table>',
      pointFormat: '<tr><th colspan="2"><span style="font-size:14px">{point.name}</span></th></tr>' +
      '<tr><th>Detail &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.detail}</td></tr>' +
      '<tr><th>OOKLA GAP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</th><td>{point.x}</td></tr>' +
      '<tr><th>Open Signal GAP &nbsp;: &nbsp;</th><td>{point.y}</td></tr>' + 
      '<tr><th>Info OOKKLA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.info_ookla}</td></tr>' +
      '<tr><th>Info Open Signal &nbsp;&nbsp;: &nbsp;</th><td>{point.info_opensignal}</td></tr>',
      footerFormat: '</table>',
      followPointer: true
    },

    plotOptions: {
      series: {
        dataLabels: {
          enabled: true,
          format: '{point.name}'
        }
      }
    },

    series: [{
      name: 'Hijau',
      color: '#00FF00',
      data: quadrantarget_hijau
    },{
      name: 'Orange',
      color: '#FF7F00',
      data: quadrantarget_orange
    },{
      name: 'Merah',
      color: '#FF0000',
      data: quadrantarget_red
    }]

  });
          
        }
      }
    });


});
});
</script>
<script>
    Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
    '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
        [0, '#2a2a2b'],
        [1, '#3e3e40']
        ]
      },
      style: {
        fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063'
    },
    title: {
      style: {
        color: '#E0E0E3',
        fontSize: '20px'
      }
    },
    subtitle: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase'
      }
    },
    xAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
        style: {
          color: '#A0A0A3'

        }
      }
    },
    yAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
        style: {
          color: '#A0A0A3'
        }
      }
    },
    tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
        color: '#F0F0F0'
      }
    },
    plotOptions: {
      series: {
        dataLabels: {
          color: '#F0F0F3',
          style: {
            fontSize: '13px'
          }
        },
        marker: {
          lineColor: '#333'
        }
      },
      boxplot: {
        fillColor: '#505053'
      },
      candlestick: {
        lineColor: 'white'
      },
      errorbar: {
        color: 'white'
      }
    },
    legend: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      itemStyle: {
        color: '#E0E0E3'
      },
      itemHoverStyle: {
        color: '#FFF'
      },
      itemHiddenStyle: {
        color: '#606063'
      },
      title: {
        style: {
          color: '#C0C0C0'
        }
      }
    },
    credits: {
      style: {
        color: '#666'
      }
    },
    labels: {
      style: {
        color: '#707073'
      }
    },

    drilldown: {
      activeAxisLabelStyle: {
        color: '#F0F0F3'
      },
      activeDataLabelStyle: {
        color: '#F0F0F3'
      }
    },

    navigation: {
      buttonOptions: {
        symbolStroke: '#DDDDDD',
        theme: {
          fill: '#505053'
        }
      }
    },

    rangeSelector: {
      buttonTheme: {
        fill: '#505053',
        stroke: '#000000',
        style: {
          color: '#CCC'
        },
        states: {
          hover: {
            fill: '#707073',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          },
          select: {
            fill: '#000003',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          }
        }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
        backgroundColor: '#333',
        color: 'silver'
      },
      labelStyle: {
        color: 'silver'
      }
    },

    navigator: {
      handles: {
        backgroundColor: '#666',
        borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
        color: '#7798BF',
        lineColor: '#A6C7ED'
      },
      xAxis: {
        gridLineColor: '#505053'
      }
    },

    scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
    }
  };

  Highcharts.setOptions(Highcharts.theme);
   Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
      return {
        radialGradient: {
          cx: 0.5,
          cy: 0.3,
          r: 0.7
        },
        stops: [
        [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
              };
            })
  });

   $(document).ready(function(){
    $("#months").change(function() {
      var monthss = $("#months").val();
      var months = monthss.replace("-", "");
      $.ajax({
        url:"http://10.54.36.49/api-ookla/public/analytics/quadran_target/month/"+months,
        type:"get",
        success: function(result){
          var res = JSON.parse(result); 

          res.forEach(myFunction);
          function myFunction(value) {
            var quadrantarget_hijau = value['hijau']
            console.log('data1',quadrantarget_hijau)
            var quadrantarget_orange = value['orange']
            console.log('data2',quadrantarget_orange)
            var quadrantarget_red = value['red']
            console.log('data3',quadrantarget_red)

          Highcharts.chart('chart1', {

            chart: {
              type: 'scatter',
              plotBorderWidth: 1
            },

            legend: {
              enabled: false
            },

            title: {
              text: 'Quadran Competition City Level'
            },

            legend: {
              layout: 'vertical',
              align: 'left',
              verticalAlign: 'top',
              x: 20,
              y: 0,
              floating: true,
              backgroundColor: '#800000',
              borderWidth: 1
            },
            xAxis: {
              gridLineWidth: 1,
              title: {
                text: 'OOKLA'
              },
              labels: {
                format: '{value}'
              },
              plotLines: [{
                color: '#800000',
                dashStyle: 'Solid',
                width: 3,
                value: 0,
                label: {
                  rotation: 0,
                  y: 15,
                  style: {
                    fontStyle: 'italic'
                  },
                  text: ''
                },
                zIndex: 3
              }]
            },

            yAxis: {
              startOnTick: false,
              endOnTick: false,
              title: {
                text: 'Open Signal'
              },
              labels: {
                format: '{value}'
              },
              maxPadding: 0.2,
              plotLines: [{
                color: '#800000',
                dashStyle: 'Solid',
                width: 3,
                value: 0,
                label: {
                  align: 'right',
                  style: {
                    fontStyle: 'italic'
                  },
                  text: '',
                  x: -10
                },
                zIndex: 3
              }]
            },

            tooltip: {
              useHTML: true,
              headerFormat: '<table>',
              pointFormat: '<tr><th colspan="2"><span style="font-size:14px">{point.name}</span></th></tr>' +
              '<tr><th>Detail &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.detail}</td></tr>' +
              '<tr><th>OOKLA GAP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</th><td>{point.x}</td></tr>' +
              '<tr><th>Open Signal GAP &nbsp;: &nbsp;</th><td>{point.y}</td></tr>' + 
              '<tr><th>Info OOKKLA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.info_ookla}</td></tr>' +
              '<tr><th>Info Open Signal &nbsp;&nbsp;: &nbsp;</th><td>{point.info_opensignal}</td></tr>',
              footerFormat: '</table>',
              followPointer: true
            },

            plotOptions: {
              series: {
                dataLabels: {
                  enabled: true,
                  format: '{point.name}'
                }
              }
            },

            series: [{
              name: 'Hijau',
              color: '#00FF00',
              data: quadrantarget_hijau
            },{
              name: 'Orange',
              color: '#FF7F00',
              data: quadrantarget_orange
            },{
              name: 'Merah',
              color: '#FF0000',
              data: quadrantarget_red
            }]

          });

          Highcharts.chart('chart11', {

            chart: {
              type: 'scatter',
              plotBorderWidth: 1
            },

            legend: {
              enabled: false
            },

            title: {
              text: 'Quadran Competition City Level'
            },

            legend: {
              layout: 'vertical',
              align: 'left',
              verticalAlign: 'top',
              x: 20,
              y: 0,
              floating: true,
              backgroundColor: '#800000',
              borderWidth: 1
            },

    // subtitle: {
    //     text: 'Source: <a href="http://www.euromonitor.com/">Euromonitor</a> and <a href="https://data.oecd.org/">OECD</a>'
    // },

    xAxis: {
      gridLineWidth: 1,
      title: {
        text: 'OOKLA'
      },
      labels: {
        format: '{value}'
      },
      plotLines: [{
        color: '#800000',
        dashStyle: 'Solid',
        width: 3,
        value: 0,
        label: {
          rotation: 0,
          y: 15,
          style: {
            fontStyle: 'italic'
          },
          text: ''
        },
        zIndex: 3
      }]
    },

    yAxis: {
      startOnTick: false,
      endOnTick: false,
      title: {
        text: 'Open Signal'
      },
      labels: {
        format: '{value}'
      },
      maxPadding: 0.2,
      plotLines: [{
        color: '#800000',
        dashStyle: 'Solid',
        width: 3,
        value: 0,
        label: {
          align: 'right',
          style: {
            fontStyle: 'italic'
          },
          text: '',
          x: -10
        },
        zIndex: 3
      }]
    },

    tooltip: {
      useHTML: true,
      headerFormat: '<table>',
      pointFormat: '<tr><th colspan="2"><span style="font-size:14px">{point.name}</span></th></tr>' +
      '<tr><th>Detail &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.detail}</td></tr>' +
      '<tr><th>OOKLA GAP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</th><td>{point.x}</td></tr>' +
      '<tr><th>Open Signal GAP &nbsp;: &nbsp;</th><td>{point.y}</td></tr>' + 
      '<tr><th>Info OOKKLA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.info_ookla}</td></tr>' +
      '<tr><th>Info Open Signal &nbsp;&nbsp;: &nbsp;</th><td>{point.info_opensignal}</td></tr>',
      footerFormat: '</table>',
      followPointer: true
    },

    plotOptions: {
      series: {
        dataLabels: {
          enabled: true,
          format: '{point.name}'
        }
      }
    },

    series: [{
      name: 'Hijau',
      color: '#00FF00',
      data: quadrantarget_hijau
    },{
      name: 'Orange',
      color: '#FF7F00',
      data: quadrantarget_orange
    },{
      name: 'Merah',
      color: '#FF0000',
      data: quadrantarget_red
    }]

  });
          
        }
      }
    });


});
});

</script>

  <script type="text/javascript" charset="utf-8">
   var columnDefs = [ 
   {headerName: 'CITIES', field: 'city', width: 225},
   {headerName: 'TOTAL MC SAMPLE', field: 'total_mc_sample', width: 160, cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
   {headerName: 'TSEL WIN', field: 'tsel_win', width: 100,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
   {headerName: 'TSEL LOSE', field: 'tsel_lose', width: 100,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
   {headerName: 'NO TSEL', field: 'no_tsel', width: 100,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
   {headerName: 'NO COMPETITOR', field: 'no_competitor', width: 150,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}}
   ]; 
   var gridOptions = {
    defaultColDef: {
      resizable: true,
      filter: true
    },
    columnDefs: columnDefs,
    rowData: null,
    paginationAutoPageSize:true,
    pagination: true,
    headerHeight: 38,
    rowHeight: 30.2,
  };

  function onPaginationChanged() {
    console.log('onPaginationPageLoaded');

    if (gridOptions.api) {
      setText('#lbLastPageFound', gridOptions.api.paginationIsLastPageFound());
      setText('#lbPageSize', gridOptions.api.paginationGetPageSize());
      setText('#lbCurrentPage', gridOptions.api.paginationGetCurrentPage() + 1);
      setText('#lbTotalPages', gridOptions.api.paginationGetTotalPages());

      setLastButtonDisabled(!gridOptions.api.paginationIsLastPageFound());
    }  
  } 

  function setLastButtonDisabled(disabled) {
    document.querySelector('#btLast').disabled = disabled;
  }

  function setRowData(rowData) {
    allOfTheData = rowData;
    createNewDatasource();
  }

  function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();

    var selectedRowsString = []; 
    selectedRows.forEach( function(selectedRow, index) {
      if (index!==0) {
        selectedRowsString += ', ';
      }
      selectedRowsString.push(selectedRow);
    });
    return JSON.stringify({ data : selectedRowsString });
  }

  function onBtNext() {
    id = 0; 
    offset = limit;
    limit += 50;  

    console.log(limit);

    document.getElementById("btnNext").addEventListener('click', function() {
      // document.querySelector('#loose');
      var httpRequest = new XMLHttpRequest();
      $(document).ready(function(){
        $("#months").change(function() {
          var monthss = $("#months").val();
          var months = monthss.replace("-", "");
          search = document.getElementById("search_data").value; 
          if(search) { 
            var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics/month/'+months);
          } else { 
            var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics/month/'+months);
          }

          httpRequest.open('GET', api); 
          httpRequest.send(); 
          httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
              var httpResponse = JSON.parse(httpRequest.responseText);
              gridOptions.api.setRowData(httpResponse.data);
            }
          };
        });
      });
    });

    gridOptions.api.paginationGoToNextPage();
  }

  function onBtPrevious() { 
    id = 0; 
    offset -= limit;
    limit -= 50;

    document.getElementById("btnPrevious").addEventListener('click', function() {
      // document.querySelector('#loose');
      var httpRequest = new XMLHttpRequest();
      $(document).ready(function(){
        $("#months").change(function() {
          var monthss = $("#months").val();
          var months = monthss.replace("-", "");
          search = document.getElementById("search_data").value; 
          if(search) { 
            var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics/month/'+months);
          } else {
            var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics/month/'+months);
          }

          httpRequest.open('GET', api);
          httpRequest.send();
          httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
              var httpResponse = JSON.parse(httpRequest.responseText);
              gridOptions.api.setRowData(httpResponse.data);
            }
          };
        });
      });
    });

    gridOptions.api.paginationGoToPreviousPage();
  } 

  document.addEventListener('DOMContentLoaded', function() {
    // var gridDiv = document.querySelector('#loose');
    // new agGrid.Grid(gridDiv, gridOptions);
    var httpRequest = new XMLHttpRequest(); 
    $(document).ready(function(){
      $("#months").change(function() {
        var monthss = $("#months").val();
        var months = monthss.replace("-", "");
        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics/month/'+months);
        httpRequest.open('GET', api); 
        httpRequest.send(); 
        httpRequest.onreadystatechange = function() {

          if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResponse = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResponse.data);
          }
        };
      });
    });
  }); 
</script> 
<script type="text/javascript" charset="utf-8">
   var columnDefs = [ 
   {headerName: 'CITIES', field: 'city', width: 225},
   {headerName: 'TOTAL MC SAMPLE', field: 'total_mc_sample', width: 160, cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
   {headerName: 'TSEL WIN', field: 'tsel_win', width: 100,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
   {headerName: 'TSEL LOSE', field: 'tsel_lose', width: 100,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
   {headerName: 'NO TSEL', field: 'no_tsel', width: 100,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
   {headerName: 'NO COMPETITOR', field: 'no_competitor', width: 150,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}}
   ]; 
   var gridOptions = {
    defaultColDef: {
      resizable: true,
      filter: true
    },
    columnDefs: columnDefs,
    rowData: null,
    paginationAutoPageSize:true,
    pagination: true,
    headerHeight: 38,
    rowHeight: 30.2,
  };

  function onPaginationChanged() {
    console.log('onPaginationPageLoaded');

    if (gridOptions.api) {
      setText('#lbLastPageFound', gridOptions.api.paginationIsLastPageFound());
      setText('#lbPageSize', gridOptions.api.paginationGetPageSize());
      setText('#lbCurrentPage', gridOptions.api.paginationGetCurrentPage() + 1);
      setText('#lbTotalPages', gridOptions.api.paginationGetTotalPages());

      setLastButtonDisabled(!gridOptions.api.paginationIsLastPageFound());
    }  
  } 

  function setLastButtonDisabled(disabled) {
    document.querySelector('#btLast').disabled = disabled;
  }

  function setRowData(rowData) {
    allOfTheData = rowData;
    createNewDatasource();
  }

  function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();

    var selectedRowsString = []; 
    selectedRows.forEach( function(selectedRow, index) {
      if (index!==0) {
        selectedRowsString += ', ';
      }
      selectedRowsString.push(selectedRow);
    });
    return JSON.stringify({ data : selectedRowsString });
  }

  function onBtNext() {
    id = 0; 
    offset = limit;
    limit += 50;  

    console.log(limit);

    document.getElementById("btnNext").addEventListener('click', function() {
      // document.querySelector('#loose');
      var httpRequest = new XMLHttpRequest();
      $(document).ready(function(){
        $("#week").change(function() {
          var week = $("#week").val();
          search = document.getElementById("search_data").value; 
          if(search) { 
            var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics/week/'+week);
          } else { 
            var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics/week/'+week);
          }

          httpRequest.open('GET', api); 
          httpRequest.send(); 
          httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
              var httpResponse = JSON.parse(httpRequest.responseText);
              gridOptions.api.setRowData(httpResponse.data);
            }
          };
        });
      });
    });

    gridOptions.api.paginationGoToNextPage();
  }

  function onBtPrevious() { 
    id = 0; 
    offset -= limit;
    limit -= 50;

    document.getElementById("btnPrevious").addEventListener('click', function() {
      // document.querySelector('#loose');
      var httpRequest = new XMLHttpRequest();
      $(document).ready(function(){
        $("#week").change(function() {
          var week = $("#week").val();
          search = document.getElementById("search_data").value; 
          if(search) { 
            var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics/week/'+week);
          } else {
            var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics/week/'+week);
          }

          httpRequest.open('GET', api);
          httpRequest.send();
          httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
              var httpResponse = JSON.parse(httpRequest.responseText);
              gridOptions.api.setRowData(httpResponse.data);
            }
          };
        });
      });
    });

    gridOptions.api.paginationGoToPreviousPage();
  } 

  document.addEventListener('DOMContentLoaded', function() {
    // var gridDiv = document.querySelector('#loose');
    // new agGrid.Grid(gridDiv, gridOptions);
    var httpRequest = new XMLHttpRequest(); 
    $(document).ready(function(){
      $("#week").change(function() {
        var week = $("#week").val();
        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics/week/'+week);
        httpRequest.open('GET', api); 
        httpRequest.send(); 
        httpRequest.onreadystatechange = function() {

          if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResponse = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResponse.data);
          }
        };
      });
    });
  }); 
</script> 
</head>

<body>
 <section id="header" class="appear">
    <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:40px; height:40px; background-color:rgba(0,0,0,0.3);" data-300="line-height:40px; height:40px; background-color:rgba(5, 42, 62, 1);">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="fa fa-bars color-white"></span>
          </button>
          <div class="navbar-logo">
            <img data-0="width:170px;" data-300=" width:170px;" src="img/oklaa.png" alt="" style="padding-left: 50px;">
          </div>
        </div>
        <div class="navbar-collapse collapse" style="padding-right: 150px;">
          <label for="week" style="color: white; font-size: 16px; padding-left: 10px; padding-top: 4px">Weekly: &nbsp;</label>
          <input type="week" name="week" id="week" style="height: 30px; width: 145px;">
          <label for="months" style="color: white; font-size: 16px; padding-left: 10px; padding-top: 4px">Monthly: &nbsp;</label>
          <input type="month" name="months" id="months" style="height: 30px; width: 155px;">
          <ul class="nav navbar-nav" data-0="margin-top:0px;" data-300="margin-top:0px;">
            <li class="active"><a href="{{url('/')}}">Home</a>
            </li>
            <li><a href="{{url('super_4g')}}">SUPER 4G</a></li>
            <li><a href="{{url('wcc_62')}}">WCC 62</a></li>
            <li><a href="{{url('csat_48')}}">CSAT 48</a></li>
            <li><a href="{{url('logout')}}">Logout</a></li>
          </ul>
        </div>
    </div>
  </section>
  <section id="intro">
    <div class="intro-content">
      <div class="row" style="padding-top: 4%; padding-left: 0px; padding-right: 10px; margin-bottom: 0px;">
        <div class="col-lg-6" style="padding-right: 0px;">
          <div class="panel-heading" style="background-color: #800000;">
            <h4 style="font-family: calibri; color: white;"><strong>Operator Benchmark</strong></h4>
          </div>
          <div class="small-box boks boksBody" style="height: 445px;">
            <div class="inner">
              <div class="col-xs-12" style="margin-top: 10px;">
                <div class="card-header1" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> Quadrant Target
                  <i style="float:right; padding-top:4px; padding-right:7px;" class="fa fa-search-plus" data-toggle="modal" data-target="#modal1"></i>
                </div>
                <div class="modal fade" id="modal1" style="width: 100%;">
                  <div class="modal-dialog modal-lg" style="margin-top:90px;">
                    <div class="modal-content" style="background-color:#353537;"> 
                      <div class="modal-header" style="text-align:left; color: white;"> Quadrant Target
                        <button type="button" class="close" data-dismiss="modal">

                          <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                      </div>
                      <div class="modal-body" style="color:#000"> 
                        <div id="chart11" style="width:100%; height:100%;"></div>
                      </div>
                    </div> 
                  </div>
                </div>
                <div class="card-body1" style="width:100%; height:400px;" >
                  <div id="chart1" style="width:100%; height:400px;"></div>
                </div> 
              </div>
            </div>
          </div><!-- BOKSBODY-->
          <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; padding-top: 0px;">
            <div class="panel-heading" style="background-color: #800000;">
              <h4 style="font-family: calibri; color: white;"><strong>Operator Benchmark – Cities Loose</strong></h4>
            </div>

            <div class="small-box boks boksBody" style="height: 195px;">
              <div class="inner">
                <div class="col-xs-12" style="margin-top: 0px; padding-right: 0px; padding-left: 0px;">
                  <div id="loose" style="width: 100%; height: 194px;" class="ag-theme-balham-dark"></div>
                  <script type="text/javascript" charset="utf-8">
                   var columnDefs = [ 
                   {headerName: 'CITIES', field: 'city', width: 225},
                   {headerName: 'TOTAL MC SAMPLE', field: 'total_mc_sample', width: 160, cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
                   {headerName: 'TSEL WIN', field: 'tsel_win', width: 100,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
                   {headerName: 'TSEL LOSE', field: 'tsel_lose', width: 100,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
                   {headerName: 'NO TSEL', field: 'no_tsel', width: 100,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}},
                   {headerName: 'NO COMPETITOR', field: 'no_competitor', width: 150,cellStyle: function(params){return {textAlign: 'right', 'fontSize':'14px'};}}
                   ]; 
                   var gridOptions = {
                    defaultColDef: {
                      resizable: true,
                      filter: true
                    },
                    columnDefs: columnDefs,
                    rowData: null,
                    paginationAutoPageSize:true,
                    pagination: true,
                    headerHeight: 38,
                    rowHeight: 30.2,
                  };

                  function onPaginationChanged() {
                    console.log('onPaginationPageLoaded');

                    if (gridOptions.api) {
                      setText('#lbLastPageFound', gridOptions.api.paginationIsLastPageFound());
                      setText('#lbPageSize', gridOptions.api.paginationGetPageSize());
                      setText('#lbCurrentPage', gridOptions.api.paginationGetCurrentPage() + 1);
                      setText('#lbTotalPages', gridOptions.api.paginationGetTotalPages());

                      setLastButtonDisabled(!gridOptions.api.paginationIsLastPageFound());
                    }  
                  } 

                  function setLastButtonDisabled(disabled) {
                    document.querySelector('#btLast').disabled = disabled;
                  }

                  function setRowData(rowData) {
                    allOfTheData = rowData;
                    createNewDatasource();
                  }

                  function onSelectionChanged() {
                    var selectedRows = gridOptions.api.getSelectedRows();

                    var selectedRowsString = []; 
                    selectedRows.forEach( function(selectedRow, index) {
                      if (index!==0) {
                        selectedRowsString += ', ';
                      }
                      selectedRowsString.push(selectedRow);
                    });
                    return JSON.stringify({ data : selectedRowsString });
                  }

                  function onBtNext() {
                    id = 0; 
                    offset = limit;
                    limit += 50;  

                    console.log(limit);

                    document.getElementById("btnNext").addEventListener('click', function() {
                      document.querySelector('#loose');
                      var httpRequest = new XMLHttpRequest();

                      search = document.getElementById("search_data").value; 
                      if(search) { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics');
                      } else { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics');
                      }

                      httpRequest.open('GET', api); 
                      httpRequest.send(); 
                      httpRequest.onreadystatechange = function() {
                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                          var httpResponse = JSON.parse(httpRequest.responseText);
                          gridOptions.api.setRowData(httpResponse.data);
                        }
                      };
                    });

                    gridOptions.api.paginationGoToNextPage();
                  }

                  function onBtPrevious() { 
                    id = 0; 
                    offset -= limit;
                    limit -= 50;

                    document.getElementById("btnPrevious").addEventListener('click', function() {
                      document.querySelector('#loose');
                      var httpRequest = new XMLHttpRequest();

                      search = document.getElementById("search_data").value; 
                      if(search) { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics');
                      } else {
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics');
                      }

                      httpRequest.open('GET', api);
                      httpRequest.send();
                      httpRequest.onreadystatechange = function() {
                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                          var httpResponse = JSON.parse(httpRequest.responseText);
                          gridOptions.api.setRowData(httpResponse.data);
                        }
                      };
                    });

                    gridOptions.api.paginationGoToPreviousPage();
                  } 

                  document.addEventListener('DOMContentLoaded', function() {
                    var gridDiv = document.querySelector('#loose');
                    new agGrid.Grid(gridDiv, gridOptions);
                    var httpRequest = new XMLHttpRequest(); 
                    var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_analytics');
                    httpRequest.open('GET', api); 
                    httpRequest.send(); 
                    httpRequest.onreadystatechange = function() {

                      if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                        var httpResponse = JSON.parse(httpRequest.responseText);
                        gridOptions.api.setRowData(httpResponse.data);
                      }
                    };
                  }); 
                </script> 
              </div>
            </div>
          </div><!-- BOKSBODY-->
        </div>

      </div>
      <div class="col-md-6" style="padding-right: 0px;">
        <div class="container" style=" padding-top: 0px; padding-left: 0px; padding-right: 0px;">
          <ul class="nav nav-tabs" role="tablist" style="padding-top: 0px; background-color: #800000; font-family: calibri; font-weight: bold;">
            <li class="nav-link active"><a data-toggle="tab" role="tab" href="#nav-tabel">Vendor Achievement Download</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" role="tab" href="#nav-upload">Vendor Achievement Upload</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" role="tab" href="#nav-latency">Vendor Achievement Latency</a></li>
          </ul>
        <div class="tab-content">
        <div id="nav-tabel" role="tabpanel" class="tab-pane fade in active">
          <div class="card-header2" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i>
            <i style="float:right; padding-top:4px; padding-right:7px;" class="fa fa-search-plus" data-toggle="modal" data-target="#modal2"></i>
          </div>
          <div class="modal fade" id="modal2" style="width: 100%;">
            <div class="modal-dialog modal-lg" style="margin-top:90px;">
              <div class="modal-content" style="background-color:#353537;"> 
                <div class="modal-header" style="text-align:left; color: white;"> DOWNLOAD
                  <button type="button" class="close" data-dismiss="modal">

                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                  </button>
                </div>
                <div class="modal-body" style="color:#000"> 
                  <div id="chartdownload" style="width:100%; height:100%;"></div>
                </div>
              </div> 
            </div>
          </div>
          <div id="tabel" style="width: 100%; height: 650px;" class="ag-theme-balham-dark"></div>
              <script>
                var columnDefs2 = [ 
                {
                  headerName: 'PROVINCE', field: 'provinsi', width: 203,filter: 'agTextColumnFilter',cellStyle: {'border-right': '1px solid #696969'}
                },
                {
                  headerName: 'HUAWEI (mbps)',
                  headerClass: 'participant-group',
                  children: [
                  {headerName: 'TSEL', field: 'huawei_tsel', width: 70,
                  cellStyle: function(params){
                    var str = params.data.huawei_tsel;
                    var max = str.includes("+");
                    var min = str.includes("-");
                    if (max == true) {
                      return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                    }else if (min == true) {
                      return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                    }else {
                      return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                    }
                    return {textAlign: 'left'};

                  },valueFormatter: huaweiFormatter},
                  {headerName: 'OTHER', field: 'huawei_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                  ]
                },
                {
                 headerName: 'ERICSSON (mbps)',
                 headerClass: 'participant-group',
                 children: [
                 {headerName: 'TSEL', field: 'ericsson_tsel', width: 70,
                 cellStyle: function(params){
                  var str = params.data.ericsson_tsel;
                  var max = str.includes("+");
                  var min = str.includes("-");
                  if (max == true) {
                    return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                  }else if (min == true) {
                    return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                  }else {
                    return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                  }
                  return {textAlign: 'left'};
                },valueFormatter: ericssonFormatter},
                {headerName: 'OTHER', field: 'ericsson_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                ]
              },
              {
                headerName: 'ZTE (mbps)',
                headerClass: 'participant-group',
                children: [
                {headerName: 'TSEL', field: 'zte_tsel', width: 70,
                cellStyle: function(params){
                  var str = params.data.zte_tsel;
                  var max = str.includes("+");
                  var min = str.includes("-");
                  if (max == true) {
                    return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                  }else if (min == true) {
                    return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                  }else {
                    return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                  }
                  return {textAlign: 'left'};
                },valueFormatter: zteFormatter},
                {headerName: 'OTHER', field: 'zte_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                ]
              },
              {
                headerName: 'NOKIA (mbps)',
                headerClass: 'participant-group',
                children: [
                {headerName: 'TSEL', field: 'nokia_tsel', width: 70,
                cellStyle: function(params){
                  var str = params.data.nokia_tsel;
                  var max = str.includes("+");
                  var min = str.includes("-");
                  if (max == true) {
                    return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                  }else if (min == true) {
                    return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                  }else {
                    return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                  }
                  return {textAlign: 'left'};
                },valueFormatter: nokiaFormatter},
                {headerName: 'OTHER', field: 'nokia_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                ]
              }
              ]; 
              function huaweiFormatter(params) {
                var value = params.data.huawei_tsel;
                var min = value.includes("-");
                var max = value.includes("+");
                var pars = parseFloat(value);
                var fxd = pars.toFixed(2);
                console.log(fxd);

                if (min == true) {
                  return fxd.replace('-','');
                }else if(max == true){
                  return fxd.replace('+','');
                }else if(value){
                  return fxd;
                }
              }
              function ericssonFormatter(params) {
                var value = params.data.ericsson_tsel;
                var min = value.includes("-");
                var max = value.includes("+");
                var pars = parseFloat(value);
                var fxd = pars.toFixed(2);
                console.log(fxd);

                if (min == true) {
                  return fxd.replace('-','');
                }else if(max == true){
                  return fxd.replace('+','');
                }else if(value){
                  return fxd;
                }
              }
              function zteFormatter(params) {
                var value = params.data.zte_tsel;
                var min = value.includes("-");
                var max = value.includes("+");
                var pars = parseFloat(value);
                var fxd = pars.toFixed(2);
                console.log(fxd);

                if (min == true) {
                  return fxd.replace('-','');
                }else if(max == true){
                  return fxd.replace('+','');
                }else if(value){
                  return fxd;
                }
              }
              function nokiaFormatter(params) {
                var value = params.data.nokia_tsel;
                var min = value.includes("-");
                var max = value.includes("+");
                var pars = parseFloat(value);
                var fxd = pars.toFixed(2);
                console.log(fxd);

                if (min == true) {
                  return fxd.replace('-','');
                }else if(max == true){
                  return fxd.replace('+','');
                }else if(value){
                  return fxd;
                }
              }
              var gridOptions2 = {
                defaultColDef: {
                  resizable: true,
                  filter: true
                },
                columnDefs: columnDefs2,
                rowData: null,
                paginationAutoPageSize:true,
                pagination: true,
                headerHeight: 36,
                rowHeight: 33.9,
              };

              function onPaginationChanged() {
                    console.log('onPaginationPageLoaded');

                    if (gridOptions2.api) {
                      setText('#lbLastPageFound', gridOptions2.api.paginationIsLastPageFound());
                      setText('#lbPageSize', gridOptions2.api.paginationGetPageSize());
                      setText('#lbCurrentPage', gridOptions2.api.paginationGetCurrentPage() + 1);
                      setText('#lbTotalPages', gridOptions2.api.paginationGetTotalPages());

                      setLastButtonDisabled(!gridOptions2.api.paginationIsLastPageFound());
                    }  
                  } 

                  function setLastButtonDisabled(disabled) {
                    document.querySelector('#btLast').disabled = disabled;
                  }

                  function setRowData(rowData) {
                    allOfTheData = rowData;
                    createNewDatasource();
                  }

                  function onSelectionChanged() {
                    var selectedRows = gridOptions2.api.getSelectedRows();

                    var selectedRowsString = []; 
                    selectedRows.forEach( function(selectedRow, index) {
                      if (index!==0) {
                        selectedRowsString += ', ';
                      }
                      selectedRowsString.push(selectedRow);
                    });
                    return JSON.stringify({ data : selectedRowsString });
                  }

                  function onBtNext() {
                    id = 0; 
                    offset = limit;
                    limit += 50;  

                    console.log(limit);

                    document.getElementById("btnNext").addEventListener('click', function() {
                      document.querySelector('#tabel');
                      var httpRequest = new XMLHttpRequest();

                      search = document.getElementById("search_data").value; 
                      if(search) { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/download');
                      } else { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/download');
                      }

                      httpRequest.open('GET', api); 
                      httpRequest.send(); 
                      httpRequest.onreadystatechange = function() {
                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                          var httpResponse = JSON.parse(httpRequest.responseText);
                          gridOptions2.api.setRowData(httpResponse.data);
                        }
                      };
                    });

                    gridOptions2.api.paginationGoToNextPage();
                  }

                  function onBtPrevious() { 
                    id = 0; 
                    offset -= limit;
                    limit -= 50;

                    document.getElementById("btnPrevious").addEventListener('click', function() {
                      document.querySelector('#tabel');
                      var httpRequest = new XMLHttpRequest();

                      search = document.getElementById("search_data").value; 
                      if(search) { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/download');
                      } else {
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/download');
                      }

                      httpRequest.open('GET', api);
                      httpRequest.send();
                      httpRequest.onreadystatechange = function() {
                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                          var httpResponse = JSON.parse(httpRequest.responseText);
                          gridOptions2.api.setRowData(httpResponse.data);
                        }
                      };
                    });

                    gridOptions2.api.paginationGoToPreviousPage();
                  }

              document.addEventListener('DOMContentLoaded', function() {
                var gridDiv = document.querySelector('#tabel');
                new agGrid.Grid(gridDiv, gridOptions2);
                var httpRequest = new XMLHttpRequest(); 
                var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/download');
                httpRequest.open('GET', api); 
                httpRequest.send(); 
                httpRequest.onreadystatechange = function() {

                  if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                    var httpResponse = JSON.parse(httpRequest.responseText);
                    gridOptions2.api.setRowData(httpResponse.data);
                  }
                };
              }); 
            </script> 
            <style>
              .ag-header-group-cell-label {
                justify-content: center;
              }
              .participant-group {
                border: #00E7B1;
              }
            </style>
        </div>
        <div id="nav-upload" role="tabpanel" class="tab-pane fade">
          <div class="card-header3" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i>
            <i style="float:right; padding-top:4px; padding-right:7px;" class="fa fa-search-plus" data-toggle="modal" data-target="#modal3"></i>
          </div>
          <div class="modal fade" id="modal3" style="width: 100%;">
            <div class="modal-dialog modal-lg" style="margin-top:90px;">
              <div class="modal-content" style="background-color:#353537;"> 
                <div class="modal-header" style="text-align:left; color: white;"> UPLOAD
                  <button type="button" class="close" data-dismiss="modal">

                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                  </button>
                </div>
                <div class="modal-body" style="color:#000"> 
                  <div id="chartupload" style="width:100%; height:100%;"></div>
                </div>
              </div> 
            </div>
          </div>
          <div id="tabelupload" style="width: 100%; height: 650px;" class="ag-theme-balham-dark"></div>
          <script>
            var columnDefs3 = [ 
            {
              headerName: 'PROVINCE', field: 'provinsi', width: 203,filter: 'agTextColumnFilter',cellStyle: {'border-right': '1px solid #696969'}
            },
            {
              headerName: 'HUAWEI (mbps)',
              headerClass: 'participant-group',
              children: [
              {headerName: 'TSEL', field: 'huawei_tsel', width: 70,
              cellStyle: function(params){
                var str = params.data.huawei_tsel;
                var max = str.includes("+");
                var min = str.includes("-");
                if (max == true) {
                  return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                }else if (min == true) {
                  return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                }else {
                  return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                }
                return {textAlign: 'left'};

              },valueFormatter: huaweiFormatter},
              {headerName: 'OTHER', field: 'huawei_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
              ]
            },
            {
             headerName: 'ERICSSON (mbps)',
             headerClass: 'participant-group',
             children: [
             {headerName: 'TSEL', field: 'ericsson_tsel', width: 70,
             cellStyle: function(params){
              var str = params.data.ericsson_tsel;
              var max = str.includes("+");
              var min = str.includes("-");
              if (max == true) {
                return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
              }else if (min == true) {
                return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
              }else {
                return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
              }
              return {textAlign: 'left'};
            },valueFormatter: ericssonFormatter},
            {headerName: 'OTHER', field: 'ericsson_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
            ]
          },
          {
            headerName: 'ZTE (mbps)',
            headerClass: 'participant-group',
            children: [
            {headerName: 'TSEL', field: 'zte_tsel', width: 70,
            cellStyle: function(params){
              var str = params.data.zte_tsel;
              var max = str.includes("+");
              var min = str.includes("-");
              if (max == true) {
                return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
              }else if (min == true) {
                return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
              }else {
                return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
              }
              return {textAlign: 'left'};
            },valueFormatter: zteFormatter},
            {headerName: 'OTHER', field: 'zte_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
            ]
          },
          {
            headerName: 'NOKIA (mbps)',
            headerClass: 'participant-group',
            children: [
            {headerName: 'TSEL', field: 'nokia_tsel', width: 70,
            cellStyle: function(params){
              var str = params.data.nokia_tsel;
              var max = str.includes("+");
              var min = str.includes("-");
              if (max == true) {
                return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
              }else if (min == true) {
                return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
              }else {
                return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
              }
              return {textAlign: 'left'};
            },valueFormatter: nokiaFormatter},
            {headerName: 'OTHER', field: 'nokia_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
            ]
          }
          ]; 
          function huaweiFormatter(params) {
            var value = params.data.huawei_tsel;
            var min = value.includes("-");
            var max = value.includes("+");
            var pars = parseFloat(value);
            var fxd = pars.toFixed(2);
            console.log(fxd);

            if (min == true) {
              return fxd.replace('-','');
            }else if(max == true){
              return fxd.replace('+','');
            }else if(value){
              return fxd;
            }
          }
          function ericssonFormatter(params) {
            var value = params.data.ericsson_tsel;
            var min = value.includes("-");
            var max = value.includes("+");
            var pars = parseFloat(value);
            var fxd = pars.toFixed(2);
            console.log(fxd);

            if (min == true) {
              return fxd.replace('-','');
            }else if(max == true){
              return fxd.replace('+','');
            }else if(value){
              return fxd;
            }
          }
          function zteFormatter(params) {
            var value = params.data.zte_tsel;
            var min = value.includes("-");
            var max = value.includes("+");
            var pars = parseFloat(value);
            var fxd = pars.toFixed(2);
            console.log(fxd);

            if (min == true) {
              return fxd.replace('-','');
            }else if(max == true){
              return fxd.replace('+','');
            }else if(value){
              return fxd;
            }
          }
          function nokiaFormatter(params) {
            var value = params.data.nokia_tsel;
            var min = value.includes("-");
            var max = value.includes("+");
            var pars = parseFloat(value);
            var fxd = pars.toFixed(2);
            console.log(fxd);

            if (min == true) {
              return fxd.replace('-','');
            }else if(max == true){
              return fxd.replace('+','');
            }else if(value){
              return fxd;
            }
          }
          var gridOptions3 = {
            defaultColDef: {
              resizable: true,
              filter: true
            },
            columnDefs: columnDefs3,
            rowData: null,
            paginationAutoPageSize:true,
            pagination: true,
            headerHeight: 36,
            rowHeight: 33.9,
          };

          function onPaginationChanged() {
            console.log('onPaginationPageLoaded');

            if (gridOptions3.api) {
              setText('#lbLastPageFound', gridOptions3.api.paginationIsLastPageFound());
              setText('#lbPageSize', gridOptions3.api.paginationGetPageSize());
              setText('#lbCurrentPage', gridOptions3.api.paginationGetCurrentPage() + 1);
              setText('#lbTotalPages', gridOptions3.api.paginationGetTotalPages());

              setLastButtonDisabled(!gridOptions3.api.paginationIsLastPageFound());
            }  
          } 

          function setLastButtonDisabled(disabled) {
            document.querySelector('#btLast').disabled = disabled;
          }

          function setRowData(rowData) {
            allOfTheData = rowData;
            createNewDatasource();
          }

          function onSelectionChanged() {
            var selectedRows = gridOptions3.api.getSelectedRows();

            var selectedRowsString = []; 
            selectedRows.forEach( function(selectedRow, index) {
              if (index!==0) {
                selectedRowsString += ', ';
              }
              selectedRowsString.push(selectedRow);
            });
            return JSON.stringify({ data : selectedRowsString });
          }

          function onBtNext() {
            id = 0; 
            offset = limit;
            limit += 50;  

            console.log(limit);

            document.getElementById("btnNext").addEventListener('click', function() {
              document.querySelector('#tabelupload');
              var httpRequest = new XMLHttpRequest();

              search = document.getElementById("search_data").value; 
              if(search) { 
                var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/upload');
              } else { 
                var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/upload');
              }

              httpRequest.open('GET', api); 
              httpRequest.send(); 
              httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                  var httpResponse = JSON.parse(httpRequest.responseText);
                  gridOptions3.api.setRowData(httpResponse.data);
                }
              };
            });

            gridOptions3.api.paginationGoToNextPage();
          }

          function onBtPrevious() { 
            id = 0; 
            offset -= limit;
            limit -= 50;

            document.getElementById("btnPrevious").addEventListener('click', function() {
              document.querySelector('#tabelupload');
              var httpRequest = new XMLHttpRequest();

              search = document.getElementById("search_data").value; 
              if(search) { 
                var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/upload');
              } else {
                var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/upload');
              }

              httpRequest.open('GET', api);
              httpRequest.send();
              httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                  var httpResponse = JSON.parse(httpRequest.responseText);
                  gridOptions3.api.setRowData(httpResponse.data);
                }
              };
            });

            gridOptions3.api.paginationGoToPreviousPage();
          }

          document.addEventListener('DOMContentLoaded', function() {
            var gridDiv = document.querySelector('#tabelupload');
            new agGrid.Grid(gridDiv, gridOptions3);
            var httpRequest = new XMLHttpRequest(); 
            var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/upload');
            httpRequest.open('GET', api); 
            httpRequest.send(); 
            httpRequest.onreadystatechange = function() {

              if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                var httpResponse = JSON.parse(httpRequest.responseText);
                gridOptions3.api.setRowData(httpResponse.data);
              }
            };
          }); 
        </script> 
        <style>
          .ag-header-group-cell-label {
            justify-content: center;
          }
          .participant-group {
            border: #00E7B1;
          }
        </style>
        </div>
      <div id="nav-latency" role="tabpanel" class="tab-pane fade">
          <div class="card-header4" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i>
            <i style="float:right; padding-top:4px; padding-right:7px;" class="fa fa-search-plus" data-toggle="modal" data-target="#modal4"></i>
          </div>
          <div class="modal fade" id="modal4" style="width: 100%;">
            <div class="modal-dialog modal-lg" style="margin-top:90px;">
              <div class="modal-content" style="background-color:#353537;"> 
                <div class="modal-header" style="text-align:left; color: white;"> LATENCY
                  <button type="button" class="close" data-dismiss="modal">

                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                  </button>
                </div>
                <div class="modal-body" style="color:#000"> 
                  <div id="chartlatency" style="width:100%; height:100%;"></div>
                </div>
              </div> 
            </div>
          </div>
          <div id="tabellatency" style="width: 100%; height: 650px;" class="ag-theme-balham-dark"></div>
              <script>
                var columnDefs4 = [ 
                {
                  headerName: 'PROVINCE', field: 'provinsi', width: 203,filter: 'agTextColumnFilter',cellStyle: {'border-right': '1px solid #696969'}
                },
                {
                  headerName: 'HUAWEI (mbps)',
                  headerClass: 'participant-group',
                  children: [
                  {headerName: 'TSEL', field: 'huawei_tsel', width: 70,
                  cellStyle: function(params){
                    var str = params.data.huawei_tsel;
                    var max = str.includes("+");
                    var min = str.includes("-");
                    if (max == true) {
                      return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                    }else if (min == true) {
                      return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                    }else {
                      return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                    }
                    return {textAlign: 'left'};

                  },valueFormatter: huaweiFormatter},
                  {headerName: 'OTHER', field: 'huawei_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                  ]
                },
                {
                 headerName: 'ERICSSON (mbps)',
                 headerClass: 'participant-group',
                 children: [
                 {headerName: 'TSEL', field: 'ericsson_tsel', width: 70,
                 cellStyle: function(params){
                  var str = params.data.ericsson_tsel;
                  var max = str.includes("+");
                  var min = str.includes("-");
                  if (max == true) {
                    return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                  }else if (min == true) {
                    return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                  }else {
                    return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                  }
                  return {textAlign: 'left'};
                },valueFormatter: ericssonFormatter},
                {headerName: 'OTHER', field: 'ericsson_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                ]
              },
              {
                headerName: 'ZTE (mbps)',
                headerClass: 'participant-group',
                children: [
                {headerName: 'TSEL', field: 'zte_tsel', width: 70,
                cellStyle: function(params){
                  var str = params.data.zte_tsel;
                  var max = str.includes("+");
                  var min = str.includes("-");
                  if (max == true) {
                    return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                  }else if (min == true) {
                    return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                  }else {
                    return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                  }
                  return {textAlign: 'left'};
                },valueFormatter: zteFormatter},
                {headerName: 'OTHER', field: 'zte_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                ]
              },
              {
                headerName: 'NOKIA (mbps)',
                headerClass: 'participant-group',
                children: [
                {headerName: 'TSEL', field: 'nokia_tsel', width: 70,
                cellStyle: function(params){
                  var str = params.data.nokia_tsel;
                  var max = str.includes("+");
                  var min = str.includes("-");
                  if (max == true) {
                    return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                  }else if (min == true) {
                    return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                  }else {
                    return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                  }
                  return {textAlign: 'left'};
                },valueFormatter: nokiaFormatter},
                {headerName: 'OTHER', field: 'nokia_other', width: 88,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                ]
              }
              ]; 
              function huaweiFormatter(params) {
                var value = params.data.huawei_tsel;
                var min = value.includes("-");
                var max = value.includes("+");
                var pars = parseFloat(value);
                var fxd = pars.toFixed(2);
                console.log(fxd);

                if (min == true) {
                  return fxd.replace('-','');
                }else if(max == true){
                  return fxd.replace('+','');
                }else if(value){
                  return fxd;
                }
              }
              function ericssonFormatter(params) {
                var value = params.data.ericsson_tsel;
                var min = value.includes("-");
                var max = value.includes("+");
                var pars = parseFloat(value);
                var fxd = pars.toFixed(2);
                console.log(fxd);

                if (min == true) {
                  return fxd.replace('-','');
                }else if(max == true){
                  return fxd.replace('+','');
                }else if(value){
                  return fxd;
                }
              }
              function zteFormatter(params) {
                var value = params.data.zte_tsel;
                var min = value.includes("-");
                var max = value.includes("+");
                var pars = parseFloat(value);
                var fxd = pars.toFixed(2);
                console.log(fxd);

                if (min == true) {
                  return fxd.replace('-','');
                }else if(max == true){
                  return fxd.replace('+','');
                }else if(value){
                  return fxd;
                }
              }
              function nokiaFormatter(params) {
                var value = params.data.nokia_tsel;
                var min = value.includes("-");
                var max = value.includes("+");
                var pars = parseFloat(value);
                var fxd = pars.toFixed(2);
                console.log(fxd);

                if (min == true) {
                  return fxd.replace('-','');
                }else if(max == true){
                  return fxd.replace('+','');
                }else if(value){
                  return fxd;
                }
              }
              var gridOptions4 = {
                defaultColDef: {
                  resizable: true,
                  filter: true
                },
                columnDefs: columnDefs4,
                rowData: null,
                paginationAutoPageSize:true,
                pagination: true,
                headerHeight: 36,
                rowHeight: 33.9,
              };

              function onPaginationChanged() {
                    console.log('onPaginationPageLoaded');

                    if (gridOptions4.api) {
                      setText('#lbLastPageFound', gridOptions4.api.paginationIsLastPageFound());
                      setText('#lbPageSize', gridOptions4.api.paginationGetPageSize());
                      setText('#lbCurrentPage', gridOptions4.api.paginationGetCurrentPage() + 1);
                      setText('#lbTotalPages', gridOptions4.api.paginationGetTotalPages());

                      setLastButtonDisabled(!gridOptions4.api.paginationIsLastPageFound());
                    }  
                  } 

                  function setLastButtonDisabled(disabled) {
                    document.querySelector('#btLast').disabled = disabled;
                  }

                  function setRowData(rowData) {
                    allOfTheData = rowData;
                    createNewDatasource();
                  }

                  function onSelectionChanged() {
                    var selectedRows = gridOptions4.api.getSelectedRows();

                    var selectedRowsString = []; 
                    selectedRows.forEach( function(selectedRow, index) {
                      if (index!==0) {
                        selectedRowsString += ', ';
                      }
                      selectedRowsString.push(selectedRow);
                    });
                    return JSON.stringify({ data : selectedRowsString });
                  }

                  function onBtNext() {
                    id = 0; 
                    offset = limit;
                    limit += 50;  

                    console.log(limit);

                    document.getElementById("btnNext").addEventListener('click', function() {
                      document.querySelector('#tabellatency');
                      var httpRequest = new XMLHttpRequest();

                      search = document.getElementById("search_data").value; 
                      if(search) { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/latency');
                      } else { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/latency');
                      }

                      httpRequest.open('GET', api); 
                      httpRequest.send(); 
                      httpRequest.onreadystatechange = function() {
                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                          var httpResponse = JSON.parse(httpRequest.responseText);
                          gridOptions4.api.setRowData(httpResponse.data);
                        }
                      };
                    });

                    gridOptions4.api.paginationGoToNextPage();
                  }

                  function onBtPrevious() { 
                    id = 0; 
                    offset -= limit;
                    limit -= 50;

                    document.getElementById("btnPrevious").addEventListener('click', function() {
                      document.querySelector('#tabellatency');
                      var httpRequest = new XMLHttpRequest();

                      search = document.getElementById("search_data").value; 
                      if(search) { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/latency');
                      } else {
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/latency');
                      }

                      httpRequest.open('GET', api);
                      httpRequest.send();
                      httpRequest.onreadystatechange = function() {
                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                          var httpResponse = JSON.parse(httpRequest.responseText);
                          gridOptions4.api.setRowData(httpResponse.data);
                        }
                      };
                    });

                    gridOptions4.api.paginationGoToPreviousPage();
                  }

              document.addEventListener('DOMContentLoaded', function() {
                var gridDiv = document.querySelector('#tabellatency');
                new agGrid.Grid(gridDiv, gridOptions4);
                var httpRequest = new XMLHttpRequest(); 
                var api = encodeURI('http://10.54.36.49/api-ookla/public/analytics/table_vendorachievement/latency');
                httpRequest.open('GET', api); 
                httpRequest.send(); 
                httpRequest.onreadystatechange = function() {

                  if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                    var httpResponse = JSON.parse(httpRequest.responseText);
                    gridOptions4.api.setRowData(httpResponse.data);
                  }
                };
              }); 
            </script> 
            <style>
              .ag-header-group-cell-label {
                justify-content: center;
              }
              .participant-group {
                border: #00E7B1;
              }
            </style>
        </div>
    </div>
    </div>
  </div>
    <marquee behavior="scroll" direction="left" scrollamount="5" style="font-size:13px; background: rgba(17, 34, 41, 0.9); color: white; padding-top: 0px;">Last Data&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Operator Benchmark - Quadrant : {{ $lastupdate_1 }}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Operator Benchmark – Cities Loose : {{ $lastupdate_2 }}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Vendor Achievement : {{ $lastupdate_3 }}</marquee>
  </div>
</div>
</section>

<a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>
<script src="{{url('')}}/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="{{url('')}}/js/jquery.js"></script>
<script src="{{url('')}}/js/jquery.easing.1.3.js"></script>
<script src="{{url('')}}/js/bootstrap.min.js"></script>
<script src="{{url('')}}/js/jquery.isotope.min.js"></script>
<script src="{{url('')}}/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="{{url('')}}/js/skrollr.min.js"></script>
<script src="{{url('')}}/js/jquery.scrollTo.min.js"></script>
<script src="{{url('')}}/js/jquery.localScroll.min.js"></script>
<script src="{{url('')}}/js/stellar.js"></script>
<script src="{{url('')}}/js/jquery.appear.js"></script>
<script src="{{url('')}}/js/jquery.flexslider-min.js"></script>
<script src="{{url('')}}/contactform/contactform.js"></script>
<script src="{{url('')}}/js/main.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script type="text/javascript">

  Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
    '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
        [0, '#2a2a2b'],
        [1, '#3e3e40']
        ]
      },
      style: {
        fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063'
    },
    title: {
      style: {
        color: '#E0E0E3',
        fontSize: '20px'
      }
    },
    subtitle: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase'
      }
    },
    xAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
        style: {
          color: '#A0A0A3'

        }
      }
    },
    yAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
        style: {
          color: '#A0A0A3'
        }
      }
    },
    tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
        color: '#F0F0F0'
      }
    },
    plotOptions: {
      series: {
        dataLabels: {
          color: '#F0F0F3',
          style: {
            fontSize: '13px'
          }
        },
        marker: {
          lineColor: '#333'
        }
      },
      boxplot: {
        fillColor: '#505053'
      },
      candlestick: {
        lineColor: 'white'
      },
      errorbar: {
        color: 'white'
      }
    },
    legend: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      itemStyle: {
        color: '#E0E0E3'
      },
      itemHoverStyle: {
        color: '#FFF'
      },
      itemHiddenStyle: {
        color: '#606063'
      },
      title: {
        style: {
          color: '#C0C0C0'
        }
      }
    },
    credits: {
      style: {
        color: '#666'
      }
    },
    labels: {
      style: {
        color: '#707073'
      }
    },

    drilldown: {
      activeAxisLabelStyle: {
        color: '#F0F0F3'
      },
      activeDataLabelStyle: {
        color: '#F0F0F3'
      }
    },

    navigation: {
      buttonOptions: {
        symbolStroke: '#DDDDDD',
        theme: {
          fill: '#505053'
        }
      }
    },

    rangeSelector: {
      buttonTheme: {
        fill: '#505053',
        stroke: '#000000',
        style: {
          color: '#CCC'
        },
        states: {
          hover: {
            fill: '#707073',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          },
          select: {
            fill: '#000003',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          }
        }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
        backgroundColor: '#333',
        color: 'silver'
      },
      labelStyle: {
        color: 'silver'
      }
    },

    navigator: {
      handles: {
        backgroundColor: '#666',
        borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
        color: '#7798BF',
        lineColor: '#A6C7ED'
      },
      xAxis: {
        gridLineColor: '#505053'
      }
    },

    scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
    }
  };

  Highcharts.setOptions(Highcharts.theme);
   Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
      return {
        radialGradient: {
          cx: 0.5,
          cy: 0.3,
          r: 0.7
        },
        stops: [
        [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
              };
            })
  });

  var quadrantarget_hijau = <?php echo json_encode($quadrantarget_hijau); ?>;
  var quadrantarget_orange = <?php echo json_encode($quadrantarget_orange); ?>;
  var quadrantarget_red = <?php echo json_encode($quadrantarget_red); ?>;
// console.log(quadrantarget_hijau);
Highcharts.chart('chart1', {

  chart: {
    type: 'scatter',
    plotBorderWidth: 1
  },

  legend: {
    enabled: false
  },

  title: {
    text: 'Quadran Competition City Level'
  },

  legend: {
    layout: 'vertical',
    align: 'left',
    verticalAlign: 'top',
    x: 20,
    y: 0,
    floating: true,
    backgroundColor: '#800000',
    borderWidth: 1
  },
  xAxis: {
    gridLineWidth: 1,
    title: {
      text: 'OOKLA'
    },
    labels: {
      format: '{value}'
    },
    plotLines: [{
      color: '#800000',
      dashStyle: 'Solid',
      width: 3,
      value: 0,
      label: {
        rotation: 0,
        y: 15,
        style: {
          fontStyle: 'italic'
        },
        text: ''
      },
      zIndex: 3
    }]
  },

  yAxis: {
    startOnTick: false,
    endOnTick: false,
    title: {
      text: 'Open Signal'
    },
    labels: {
      format: '{value}'
    },
    maxPadding: 0.2,
    plotLines: [{
      color: '#800000',
      dashStyle: 'Solid',
      width: 3,
      value: 0,
      label: {
        align: 'right',
        style: {
          fontStyle: 'italic'
        },
        text: '',
        x: -10
      },
      zIndex: 3
    }]
  },

  tooltip: {
    useHTML: true,
    headerFormat: '<table>',
    pointFormat: '<tr><th colspan="2"><span style="font-size:14px">{point.name}</span></th></tr>' +
    '<tr><th>Detail &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.detail}</td></tr>' +
    '<tr><th>OOKLA GAP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</th><td>{point.x}</td></tr>' +
    '<tr><th>Open Signal GAP &nbsp;: &nbsp;</th><td>{point.y}</td></tr>' + 
    '<tr><th>Info OOKKLA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.info_ookla}</td></tr>' +
    '<tr><th>Info Open Signal &nbsp;&nbsp;: &nbsp;</th><td>{point.info_opensignal}</td></tr>',
    footerFormat: '</table>',
    followPointer: true
  },

  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        format: '{point.name}'
      }
    }
  },

  series: [{
    name: 'Hijau',
    color: '#00FF00',
    data: quadrantarget_hijau
  },{
    name: 'Orange',
    color: '#FF7F00',
    data: quadrantarget_orange
  },{
    name: 'Merah',
    color: '#FF0000',
    data: quadrantarget_red
  }]

});

Highcharts.chart('chart11', {

  chart: {
    type: 'scatter',
    plotBorderWidth: 1
  },

  legend: {
    enabled: false
  },

  title: {
    text: 'Quadran Competition City Level'
  },

  legend: {
    layout: 'vertical',
    align: 'left',
    verticalAlign: 'top',
    x: 20,
    y: 0,
    floating: true,
    backgroundColor: '#800000',
    borderWidth: 1
  },

    // subtitle: {
    //     text: 'Source: <a href="http://www.euromonitor.com/">Euromonitor</a> and <a href="https://data.oecd.org/">OECD</a>'
    // },

    xAxis: {
      gridLineWidth: 1,
      title: {
        text: 'OOKLA'
      },
      labels: {
        format: '{value}'
      },
      plotLines: [{
        color: '#800000',
        dashStyle: 'Solid',
        width: 3,
        value: 0,
        label: {
          rotation: 0,
          y: 15,
          style: {
            fontStyle: 'italic'
          },
          text: ''
        },
        zIndex: 3
      }]
    },

    yAxis: {
      startOnTick: false,
      endOnTick: false,
      title: {
        text: 'Open Signal'
      },
      labels: {
        format: '{value}'
      },
      maxPadding: 0.2,
      plotLines: [{
        color: '#800000',
        dashStyle: 'Solid',
        width: 3,
        value: 0,
        label: {
          align: 'right',
          style: {
            fontStyle: 'italic'
          },
          text: '',
          x: -10
        },
        zIndex: 3
      }]
    },

    tooltip: {
      useHTML: true,
      headerFormat: '<table>',
      pointFormat: '<tr><th colspan="2"><span style="font-size:14px">{point.name}</span></th></tr>' +
      '<tr><th>Detail &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.detail}</td></tr>' +
      '<tr><th>OOKLA GAP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</th><td>{point.x}</td></tr>' +
      '<tr><th>Open Signal GAP &nbsp;: &nbsp;</th><td>{point.y}</td></tr>' + 
      '<tr><th>Info OOKKLA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;</th><td>{point.info_ookla}</td></tr>' +
      '<tr><th>Info Open Signal &nbsp;&nbsp;: &nbsp;</th><td>{point.info_opensignal}</td></tr>',
      footerFormat: '</table>',
      followPointer: true
    },

    plotOptions: {
      series: {
        dataLabels: {
          enabled: true,
          format: '{point.name}'
        }
      }
    },

    series: [{
      name: 'Hijau',
      color: '#00FF00',
      data: quadrantarget_hijau
    },{
      name: 'Orange',
      color: '#FF7F00',
      data: quadrantarget_orange
    },{
      name: 'Merah',
      color: '#FF0000',
      data: quadrantarget_red
    }]

  });

</script>
</body>
</html>
