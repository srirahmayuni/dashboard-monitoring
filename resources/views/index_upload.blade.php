<!DOCTYPE html>
<html>
<head>
  <!-- BASICS -->
  <meta charset="utf-8">
  <title>DASHBOARD</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- <link rel="stylesheet" type="text/css" href="{{url('')}}/js/rs-plugin/css/settings.css" media="screen"> -->
  <link rel="stylesheet" type="text/css" href="{{url('')}}/css/isotope.css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/flexslider.css" type="text/css">
  <link rel="stylesheet" href="{{url('')}}/js/fancybox/jquery.fancybox.css" type="text/css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/bootstrap.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="{{url('')}}/css/style.css">
  <link rel="stylesheet" href="{{url('')}}/skin/default.css">
  <!-- =======================================================
    ======================================================= -->


    <!-- <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css"> -->
    <!-- iCheck -->
    <!-- <link rel="stylesheet" href="{{url('')}}/plugins/iCheck/flat/blue.css"> -->
    <!-- Morris chart -->
    <!-- <link rel="stylesheet" href="{{url('')}}/plugins/morris/morris.css"> -->
    <!-- bootstrap wysihtml5 - text editor -->
    <!-- DataTables -->
    <!-- <link rel="stylesheet" href="{{url('')}}/plugins/datatables/dataTables.bootstrap4.css"> -->
    <!-- Theme style -->
    <!-- <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css"> -->
    <!-- <link rel="stylesheet" href="{{url('')}}/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
    
    <!-- *** START TAMBAH IMPORT FONTAWESOME IYON *** -->
    <!-- <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/solid.css">
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/brands.css"> -->
    <!-- *** END TAMBAH IMPORT FONTAWESOME IYON *** -->
    <script src="https://unpkg.com/ag-grid-community@20.2.0/dist/ag-grid-community.min.js"></script>
    <script src = "https://code.highcharts.com/highcharts.js"> </script>
    <style type="text/css">
      #myScrollTable tbody {
        clear: both;
        border: 1px solid #000000;
        height: 475px;
        overflow:auto;
        float:left;
        width:100%;
      }
    </style>
    <style type="text/css">
      * {
        box-sizing: border-box;
      }

      body {
        font-family: Arial, Helvetica, sans-serif;
      }

      /* Float four columns side by side */
      .column {
        float: left;
        width: 33%;
        padding: 0 10px;
      }

      /* Remove extra left and right margins, due to padding in columns */
      .row {margin: 0 -5px;}

      /* Clear floats after the columns */
      .row:after {
        content: "";
        display: table;
        clear: both;
      }

      /* Style the counter cards */
      .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); /* this adds the "card" effect */
        padding: 30%;
        text-align: center;
        background-color: #f1f1f1;
      }

      /* Responsive columns - one column layout (vertical) on small screens */
      @media screen and (max-width: 600px) {
        .column {
          width: 100%;
          display: block;
          margin-bottom: 20px;
        }
      }
    </style>


    <style>
      /* ** START TAMBAH CSS IYON** */
      .dropClass {
        /* display: none; */
        visibility: hidden;
        opacity: 0;
        transition:
        all .5s ease;
        background-color: transparent;
        min-width: 160px;
        overflow: auto;
        z-index: 1;
        height: 0;
        /* margin-bottom: -5%; */
      }
      .dropClass a {
        color: black;
        padding: 12px 16px;
        /* text-decoration: none; */
        display: block;
      }
      .dropClass a:hover {background-color: rgba(255,255,255,.1);}
      .show {
        /* display: block; */
        visibility: visible;
        opacity: 1;
        height: 100%;
        padding: 0.5rem 1rem;
        background-color: rgba(255,255,255,.3);
        margin-bottom: 1.5%;
        border-radius: 3%;
      }
      .putar {
        transform: rotate(90deg);
        transition: all .5s ease;
      }
      .brand-image {
        line-height: .8;
        max-height: 53px;
        width: auto;
        margin-left: 0.7rem;
        margin-right: .5rem;
        margin-top: -3px;
        float: none;
        opacity: .9;
      }
      .backgroundImg {
        width: auto;
        height: 100%;
        opacity: 1;
        position: absolute;
      }
      .backgroundImg2 {
        position: fixed;
        width: 100%;
        max-height: 56px;
        margin-left: -2%;
        opacity: 1;
      }
      .nav-item:hover {
        background-color: rgba(255,255,255,.3);
        border-radius: 5%;
        transition: all .2s ease;
      }
      .berisik {
        min-height:500px !important
      }
      .tesDiv {
        z-index: -1;
        opacity: .4;
        background: url(./dist/img/tesblek.png) center center
      }
      .tesDiv .bekgron{
        z-index: 1;
        opacity: 1
      }
      /* ** END TAMBAH IYON** */
      /* ** START UI BARU** */
      #bungkus {
        background: url(./dist/img/darkwall6.jpg) center center;
      }
      .garisijo {
        background-color: #800000;
        height: 3px;
        width: 100%;
        position: center;
        bottom: 0;
        left: 0;
        /* margin-left: -6%; */
      }
      .teksboks {
        width: 85%;
        position: absolute;
        bottom: 0;
        left: 0;
      }
      .teksne{
            /* bottom: 0;
            right: 3%; */
            margin:auto;
            position: absolute;
          }
          .boksHead {
            font-size: 30px;
            /* box-shadow: 0 3px 1px 0 rgba(0, 0, 0, 0.2), 0 1px 0px 0 rgba(0, 0, 0, 0.19); */
            padding: 10px;
            font-weight: 500;
            border-radius: 0;
            background-color: rgba(255, 255, 255, .2);
            color: #96b28a;
            font-weight: bold;
            box-shadow: none;
            margin-bottom:0 !important;
          }
          .boksBody {
            height: 107px;
            /* background-color: #343a40; */
            border-radius: 0;
            background-color: rgba(255, 255, 255, .2);
            /* background-image: linear-gradient(to top, rgba(0,255,255,.2), rgba(150, 178, 138, .5)); */
            /* background-color: rgba(0, 0, 0, .5); */
            box-shadow: none;
          }
          .inner {
            padding:0 !important;
          }
          .card.chartcard {
            background-color:transparent;
            border: 0;
            border-radius: 0;
            box-shadow: none;
          }
          table {
            font-size: 14px;
            background-color: rgba(1, 14, 23,.9)!important;
            color:lightgrey;
          }
        /* tr:hover {
            background-color: rgba(255, 255, 255, .2)
            } */
            tr > td:hover {
              background-color: rgba(1, 14, 23,.9)!important;
              color:lightgrey;
            }

            .table td, .table th {
              border: 1px solid #ddd;
            }

            thead {
              background-color: rgba(1, 14, 23,.9)!important;
              color:lightgrey;
            }
            .kepanjangan {
              font-size: 10px;
            }
            .kepanjangantot {
              font-size: 12px;
            }
            /* ** END UI BARU** */

            .page-link {
              color: #fff;
              background-color: #262c2e;
            }

            .social-part .fa{
              padding-right:20px;
            }
            ul li a{
              margin-right: 20px;
            }

            .bg-light {
              color:#fff;
              background-image:linear-gradient(to right, #1c1f20, #2d3436, #2d3436, #2d3436);
            } 

            .bg-light, .bg-light a {
              color: #fff!important;
            }

            .dropdown-menu {
              background-color: #262c2e;
              color: #0F2027;
            }

            .active1 {
              background-color: #6c7173; !important;
              color: #343a40 !important;
              font-weight: 600;
            }

            .item:hover {
              background-color: #0F2027;
            }

            .invisible {
              visibility: hidden;
            }

            .dropdown-item:focus, .dropdown-item:hover {
              background-color: #6c7173;
            }

            .card {
              box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
              transition: 0.3s; 
              border-radius: 5px; /* 5px rounded corners */
            }

            .card:hover {
              box-shadow: 0 8px 50px 0 rgba(0,0,0,0.2);
            } 

            .ag-header-cell-label {
              float: none !important;
              width: auto !important;
              justify-content: left;
            }

            .ag-fresh .ag-header-cell-menu-button{
              position: absolute;
              float: none;
            }

            .container {
              max-width: 100%; !important;
            }

            #my{
              zoom: 100%;
            }

          </style>
        </head>
        <body>
          <section id="header" class="appear">
            <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:40px; height:40px; background-color:rgba(0,0,0,0.3);" data-300="line-height:40px; height:40px; background-color:rgba(5, 42, 62, 1);">
              <div class="container">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="fa fa-bars color-white"></span>
                  </button>
                  <div class="navbar-logo">
                    <!-- <a href="index.html"><img data-0="width:150px;" data-300=" width:120px;" src="img/tsel-white.png" alt=""></a> -->
                    <img data-0="width:120px;" data-300=" width:100px;" src="img/oklaa.png" alt="">
                  </div>
                </div>
                <div class="navbar-collapse collapse">
                  <ul class="nav navbar-nav" data-0="margin-top:0px;" data-300="margin-top:0px;">
                    <li class="active"><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('super_4g')}}">SUPER 4G</a></li>
                    <li><a href="{{url('wcc_62')}}">WCC 62</a></li>
                    <li><a href="{{url('csat_48')}}">CSAT 48</a></li>
                  </ul>
                </div>
                <!--/.navbar-collapse -->
              </div>
            </div>
          </section>
          <!-- awal section baris pertama-->
          <section id="intro">
            <div class="intro-content">
              <div class="row" style="padding-top: 70px; padding-left: 0px; padding-right: 0px;">
                <div class="col-lg-6" style="padding-right: 0px; padding-top: 20px;">
                  <!-- <div class="panel panel-default"> -->
                    <div class="panel-heading" style="background-color: #800000;">
                      <h4 style="font-family: calibri; color: white;"><strong>Operator Benchmark</strong></h4>
                    </div>

                    <div class="small-box boks boksBody" style="height: 475px;">
                      <div class="inner">
                        <!-- <div class="col-xs-12"> -->
                          <div class="row"> 
                            <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                              <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                                <div class="card-header1" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 4G
                                  <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal1"></i>
                                </div>
                                <!-- </div>                      -->
                                <div class="modal fade" id="modal1" style="width: 100%;">
                                  <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                    <div class="modal-content" style="background-color:#353537;"> 
                                      <div class="modal-header" style="text-align:left; color: white;"> DOWNLOAD 4G
                                        <button type="button" class="close" data-dismiss="modal">

                                          <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button>
                                      </div>
                                      <div class="modal-body" style="color:#000"> 
                                        <div id="chart11" style="width:100%; height:100%;"></div>
                                      </div>
                                    </div> 
                                  </div>
                                </div>
                                <div class="card-body1" style="width:100%; height:130px; background-color: #343a40">
                                  <div id="chart1" style="width:100%; height:130px;"></div>
                                </div> 
                              </div> 

                              <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                                <div class="card-header2" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 3G
                                  <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal2"></i>
                                </div>
                                <!-- </div>                      -->
                                <div class="modal fade" id="modal2" style="width: 100%;">
                                  <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                    <div class="modal-content" style="background-color:#353537"> 
                                      <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 3G
                                        <button type="button" class="close" data-dismiss="modal">

                                          <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button>
                                      </div>
                                      <div class="modal-body" style="color:#000"> 
                                        <div id="chart22" style="width:100%; height:100%;"></div>
                                      </div>
                                    </div> 
                                  </div>
                                </div>
                                <div class="card-body2" style="width:100%; height:130px; background-color: #343a40">
                                  <div id="chart2" style="width:100%; height:130px;"></div>
                                </div>          
                              </div>  
                            </div>

                            <div class="row"> 
                              <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                                <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                                  <div class="card-header3" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD 4G
                                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal3"></i>
                                  </div>
                                  <!-- </div>                      -->
                                  <div class="modal fade" id="modal3" style="width: 100%;">
                                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                      <div class="modal-content" style="background-color:#353537"> 
                                        <div class="modal-header" style="text-align:center; color: white;"> UPLOAD 4G
                                          <button type="button" class="close" data-dismiss="modal">

                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                          </button>
                                        </div>
                                        <div class="modal-body" style="color:#000"> 
                                          <div id="chart33" style="width:100%; height:100%;"></div>
                                        </div>
                                      </div> 
                                    </div>
                                  </div>
                                  <div class="card-body3" style="width:100%; height:130px; background-color: #343a40">
                                    <div id="chart3" style="width:100%; height:130px;"></div>
                                  </div> 
                                </div> 

                                <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                                  <div class="card-header4" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD 3G
                                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal4"></i>
                                  </div>
                                  <!-- </div>                      -->
                                  <div class="modal fade" id="modal4" style="width: 100%;">
                                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                      <div class="modal-content" style="background-color:#353537"> 
                                        <div class="modal-header" style="text-align:center; color: white;"> UPLOAD 3G
                                          <button type="button" class="close" data-dismiss="modal">

                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                          </button>
                                        </div>
                                        <div class="modal-body" style="color:#000"> 
                                          <div id="chart44" style="width:100%; height:100%;"></div>
                                        </div>
                                      </div> 
                                    </div>
                                  </div>
                                  <div class="card-body4" style="width:100%; height:130px; background-color: #343a40">
                                    <div id="chart4" style="width:100%; height:130px;"></div>
                                  </div>          
                                </div>  
                              </div>

                              <div class="row"> 
                                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                                  <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                                    <div class="card-header5" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> LATENCY 4G
                                      <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal5"></i>
                                    </div>
                                    <!-- </div>                      -->
                                    <div class="modal fade" id="modal5" style="width: 100%;">
                                      <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                        <div class="modal-content" style="background-color:#353537"> 
                                          <div class="modal-header" style="text-align:center; color: white;"> LATENCY 4G
                                            <button type="button" class="close" data-dismiss="modal">

                                              <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                            </button>
                                          </div>
                                          <div class="modal-body" style="color:#000"> 
                                            <div id="chart55" style="width:100%; height:100%;"></div>
                                          </div>
                                        </div> 
                                      </div>
                                    </div>
                                    <div class="card-body5" style="width:100%; height:130px; background-color: #343a40">
                                      <div id="chart5" style="width:100%; height:130px;"></div>
                                    </div> 
                                  </div> 

                                  <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                                    <div class="card-header6" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> LATENCY 3G
                                      <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal6"></i>
                                    </div>
                                    <!-- </div>                      -->
                                    <div class="modal fade" id="modal6" style="width: 100%;">
                                      <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                        <div class="modal-content" style="background-color:#353537"> 
                                          <div class="modal-header" style="text-align:center; color: white;"> LATENCY 3G
                                            <button type="button" class="close" data-dismiss="modal">

                                              <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                            </button>
                                          </div>
                                          <div class="modal-body" style="color:#000"> 
                                            <div id="chart66" style="width:100%; height:100%;"></div>
                                          </div>
                                        </div> 
                                      </div>
                                    </div>
                                    <div class="card-body6" style="width:100%; height:130px; background-color: #343a40">
                                      <div id="chart6" style="width:100%; height:130px;"></div>
                                    </div>          
                                  </div>  
                                </div>
                                <!-- </div> -->
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6" style="padding-top: 0px;">
                            <div class="wrap">

                              <ul class="tabs group">
                                <li><a href="{{url('/')}}">Region - Download</a></li>
                                <li><a class="active" href="{{url('index_upload')}}">Region - Upload</a></li>
                                <li><a href="{{url('index_latency')}}">Region - Latency</a></li>
                              </ul>
                              <div id="content">
                                <div id="nav-upload">
                                                <div id="area3g" style="width: 100%; height: 467px; margin-bottom: 200px;" class="ag-theme-balham-dark"></div>
                                                <script type="text/javascript" charset="utf-8">
                                                      
                                                    var columnDefs = [ 
                                                    {headerName: "REGIONAL", field: "area",
                                                    filter: 'agTextColumnFilter',
                                                    filterParams: { applyButton: true, clearButton:true },
                                                    cellStyle: {textAlign: 'left'}
                                                    },
                                                    {headerName: "MINIMAL", field: "trx_active",
                                                    filterParams: { applyButton: true },   
                                                    cellStyle: function(params) {
                                                    var str = params.data.trx_active;
                                                    var n = str.includes("-");
                                                    if (n==true) {
                                                        return {textAlign: 'left', color: '#78DE5C'};
                                                    } else {
                                                        return {textAlign: 'left', color: '#78DE5C'};
                                                    } 
                                                    }  
                                                    },

                                                    {headerName: "AVERAGE", field: "trx_unactive",
                                                    cellStyle: function(params) {
                                                    var str = params.data.trx_unactive;
                                                    var n = str.includes("-");
                                                    if (n==true) {
                                                        return {textAlign: 'left', color: '#78DE5C'};
                                                    } else {
                                                        return {textAlign: 'left', color: '#78DE5C'};
                                                    } 
                                                    }  
                                                    },
                                                    {headerName: "MAXIMAL", field: "trx_total", 
                                                    cellStyle: function(params) {
                                                    var str = params.data.trx_total;
                                                    var n = str.includes("-");
                                                    if (n==true) {
                                                        return {textAlign: 'left', color: '#78DE5C'};
                                                    } else {
                                                        return {textAlign: 'left', color: '#78DE5C'};
                                                    } 
                                                    }  
                                                    }
                                                    ]; 
                                                
                                                var gridOptions = {
                                                    defaultColDef: {
                                                        filter: true,
                                                        resizable: true,
                                                        onGridSizeChanged: () => {
                                                    gridOptions.api.sizeColumnsToFit();
                                                }    
                                                    },
                                                    columnDefs: columnDefs,
                                                    rowData: null,
                                                    domLayout: 'autoHeight',
                                                    
                                                    onFilterChanged: function() {console.log('onFilterChanged');},
                                                    onFilterModified: function() {console.log('onFilterModified');}
                                                };
                                            
                                                // setup the grid after the page has finished loading
                                                document.addEventListener('DOMContentLoaded', function() {
                                                    var gridDiv = document.querySelector('#area3g');
                                                    new agGrid.Grid(gridDiv, gridOptions);  
                                                    // console.log('data',gridDiv)
                                                    // do http request to get our sample data - not using any framework to keep the example self contained.
                                                    // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
                                                    var httpRequest = new XMLHttpRequest();
                                                    httpRequest.open('GET', 'http://10.54.36.49/api-license2/public/api/license3g/Table3G');
                                                    httpRequest.send();
                                                    httpRequest.onreadystatechange = function() {
                                                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                                                            var httpResult = JSON.parse(httpRequest.responseText);
                                                            gridOptions.api.setRowData(httpResult);
                                                        } 
                                                    };
                                                }); 
                                                </script> 
                                        </div>                                      
                                </div>

                              </div>
                            </div>

                          </div>
                          <div class="row">
                            <div class="col-lg-3" style="padding-right: 0px; padding-top: 10px;">
                              <div class="panel-heading" style="background-color: #800000;">
                                <h4 style="font-family: calibri; color: white;"><strong>InBound Roamers</strong></h4>
                              </div>
                              <div class="small-box boks boksBody" style="height: 160px;">
                                <div class="inner">
                                  <div class="row">
                                    <div class="col-md-12" style="margin-top:5px; padding-right: 15px;">
                                      <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                                        <div class="card-header7" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 4G
                                          <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal7"></i>
                                        </div>
                                        <!-- </div>                      -->
                                        <div class="modal fade" id="modal7" style="width: 100%;">
                                          <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                            <div class="modal-content" style="background-color:#353537"> 
                                              <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 4G
                                                <button type="button" class="close" data-dismiss="modal">

                                                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                                </button>
                                              </div>
                                              <div class="modal-body" style="color:#000"> 
                                                <div id="combichart2" style="width:100%; height:100%;"></div>
                                              </div>
                                            </div> 
                                          </div>
                                        </div>
                                        <div class="card-body7" style="width:100%; height:130px; background-color: #343a40">
                                          <div id="combichart" style="width:100%; height:130px;"></div>
                                        </div> 
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- </div> -->
                              </div>

                              <div class="col-lg-3 col-md-12" style="padding-right: 0px; padding-left: 5px; padding-bottom: 20px; padding-top: 10px;">
                                <!-- <div class="panel panel-default"> -->
                                  <div class="panel-heading" style="background-color: #800000;">
                                    <h4 style="font-family: calibri; color: white;"><strong>Handset Type Distribution</strong></h4>
                                  </div>
                                  <div class="small-box boks boksBody" style="height: 160px;">
                                    <div class="inner">
                                      <div class="row">
                                        <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                                          <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                                            <div class="card-header8" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> TELKOMSEL ONLY
                                              <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal8"></i>
                                            </div>
                                            <!-- </div>                      -->
                                            <div class="modal fade" id="modal8" style="width: 100%;">
                                              <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                                <div class="modal-content" style="background-color:#353537"> 
                                                  <div class="modal-header" style="text-align:center; color: white;"> TELKOMSEL ONLY
                                                    <button type="button" class="close" data-dismiss="modal">

                                                      <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body" style="color:#000"> 
                                                    <div id="piechart11" style="width:100%; height:100%;"></div>
                                                  </div>
                                                </div> 
                                              </div>
                                            </div>
                                            <div class="card-body8" style="width:100%; height:130px; background-color: #343a40">
                                              <div id="piechart1" style="width:100%; height:130px;"></div>
                                            </div> 
                                          </div>
                                          <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">
                                            <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                                              <div class="card-header9" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> ALL OPERATOR
                                                <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal9"></i>
                                              </div>
                                              <!-- </div>                      -->
                                              <div class="modal fade" id="modal9" style="width: 100%;">
                                                <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                                  <div class="modal-content" style="background-color:#353537"> 
                                                    <div class="modal-header" style="text-align:center; color: white;"> ALL OPERATOR
                                                      <button type="button" class="close" data-dismiss="modal">

                                                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                                      </button>
                                                    </div>
                                                    <div class="modal-body" style="color:#000"> 
                                                      <div id="piechart22" style="width:100%; height:100%;"></div>
                                                    </div>
                                                  </div> 
                                                </div>
                                              </div>
                                              <div class="card-body9" style="width:100%; height:130px; background-color: #343a40">
                                                <div id="piechart2" style="width:100%; height:130px;"></div>
                                              </div> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12" style="padding-right: 15px; padding-bottom: 20px; padding-top: 10px;">
                                      <!-- <div class="panel panel-default"> -->
                                        <div class="panel-heading" style="background-color: #800000;">
                                          <h4 style="font-family: calibri; color: white;"><strong>Speed Test Distribution</strong></h4>
                                        </div>
                                        <div class="small-box boks boksBody" style="height: 160px;">
                                          <div class="inner">
                                            <div class="row">
                                              <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                                                <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                                                  <div class="card-header10" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD (Base On Video Resolution)
                                                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal10"></i>
                                                  </div>
                                                  <!-- </div>                      -->
                                                  <div class="modal fade" id="modal10" style="width: 100%;">
                                                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                                      <div class="modal-content" style="background-color:#353537"> 
                                                        <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD (Base On Video Resolution)
                                                          <button type="button" class="close" data-dismiss="modal">

                                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                                          </button>
                                                        </div>
                                                        <div class="modal-body" style="color:#000"> 
                                                          <div id="line11" style="width:100%; height:100%;"></div>
                                                        </div>
                                                      </div> 
                                                    </div>
                                                  </div>
                                                  <div class="card-body10" style="width:100%; height:130px; background-color: #343a40">
                                                    <div id="line1" style="width:100%; height:130px;"></div>
                                                  </div> 
                                                </div>
                                                <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">
                                                  <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                                                    <div class="card-header11" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD (Base On File Sizes)
                                                      <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal11"></i>
                                                    </div>
                                                    <!-- </div>                      -->
                                                    <div class="modal fade" id="modal11" style="width: 100%;">
                                                      <div class="modal-dialog modal-lg" style="margin-top:90px;">
                                                        <div class="modal-content" style="background-color:#353537"> 
                                                          <div class="modal-header" style="text-align:center; color: white;"> UPLOAD (Base On File Sizes)
                                                            <button type="button" class="close" data-dismiss="modal">

                                                              <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-body" style="color:#000"> 
                                                            <div id="line22" style="width:100%; height:100%;"></div>
                                                          </div>
                                                        </div> 
                                                      </div>
                                                    </div>
                                                    <div class="card-body11" style="width:100%; height:130px; background-color: #343a40">
                                                      <div id="line2" style="width:100%; height:130px;"></div>
                                                    </div> 
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- </div> -->
                                  </div>
                                </div>
                                <!-- <div class="row justify-content-between" style="padding-top: 500px;"> -->
                                  <!-- </div> -->
                                </div>
                              </section>
                              <!-- akhir section baris pertama-->

                              <a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>

                              <!-- Javascript Library Files -->
                              <script src="{{url('')}}/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
                              <script src="{{url('')}}/js/jquery.js"></script>
                              <script src="{{url('')}}/js/jquery.easing.1.3.js"></script>
                              <script src="{{url('')}}/js/bootstrap.min.js"></script>
                              <script src="{{url('')}}/js/jquery.isotope.min.js"></script>
                              <!-- <script src="{{url('')}}/js/jquery.nicescroll.min.js"></script> -->
                              <script src="{{url('')}}/js/fancybox/jquery.fancybox.pack.js"></script>
                              <script src="{{url('')}}/js/skrollr.min.js"></script>
                              <script src="{{url('')}}/js/jquery.scrollTo.min.js"></script>
                              <script src="{{url('')}}/js/jquery.localScroll.min.js"></script>
                              <script src="{{url('')}}/js/stellar.js"></script>
                              <script src="{{url('')}}/js/jquery.appear.js"></script>
                              <script src="{{url('')}}/js/jquery.flexslider-min.js"></script>

                              <!-- Contact Form JavaScript File -->
                              <script src="{{url('')}}/contactform/contactform.js"></script>

                              <!-- Template Main Javascript File -->
                              <script src="{{url('')}}/js/main.js"></script>
                              <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
                              <script src="{{url('')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                              <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
                              <script src="{{url('')}}/js/index.js"></script>

                              <script src="https://code.highcharts.com/highcharts.js"></script>
                              <!-- <script src="https://code.highcharts.com/modules/data.js"></script> -->
                              <script src="https://code.highcharts.com/modules/drilldown.js"></script>

                             <!--  <script>
                                $(document).ready(function(){
                                 $.ajax({
                                  url:"http://10.54.36.49/api-btsonair/public/api/dashboard_2g",
                                  type:"get",
                                  success: function(result){
                                    $('#onair_2g').html(result.onair_2g);
                                  }
                                });
                               })
                             </script> -->
<!-- 
                             <script>
                              $(document).ready(function(){
                                $("#menubody").sortable({
                                  axis: "y",
                                  opacity: 0.6,
                                  placeholder: "rowlist rowplaceholder",
                                  cursor: "move",
                                  forceHelperSize: true,
                                  update: function (event, ui) {
                                    var data = $(this).sortable('serialize')+"&action=saveorder";
                                    console.log("data"+data);
                                    $.ajax({
                                      data: data,
                                      type: 'POST',
                                      url: 'ajax/ajax.php',
                                      success: function(data){
                                        console.log(data);
                                      }
                                    });
                                  }
                                }).disableSelection();
                              });
                            </script>  -->

                            <script>

                              Highcharts.theme = {
                                colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
                                '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
                                chart: {
                                  backgroundColor: {
                                    linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                                    stops: [
                                    [0, '#2a2a2b'],
                                    [1, '#3e3e40']
                                    ]
                                  },
                                  style: {
                                    fontFamily: '\'Unica One\', sans-serif'
                                  },
                                  plotBorderColor: '#606063',
                                  marginTop: 15
                                },
                                title: {
                                  style: {
                                    color: '#E0E0E3',
                                    textTransform: 'uppercase',
                                    fontSize: '20px'
                                  }
                                },
                                subtitle: {
                                  style: {
                                    color: '#E0E0E3',
                                    textTransform: 'uppercase'
                                  }
                                },
                                xAxis: {
                                  gridLineColor: '#707073',
                                  labels: {
                                    style: {
                                      color: '#E0E0E3'
                                    }
                                  },
                                  lineColor: '#707073',
                                  minorGridLineColor: '#505053',
                                  tickColor: '#707073',
                                  title: {
                                    style: {
                                      color: '#A0A0A3'

                                    }
                                  }
                                },
                                yAxis: {
                                  gridLineColor: '#707073',
                                  labels: {
                                    style: {
                                      color: '#E0E0E3'
                                    }
                                  },
                                  lineColor: '#707073',
                                  minorGridLineColor: '#505053',
                                  tickColor: '#707073',
                                  tickWidth: 1,
                                  title: {
                                    style: {
                                      color: '#A0A0A3'
                                    }
                                  }
                                },
                                tooltip: {
                                  backgroundColor: 'rgba(0, 0, 0, 0.85)',
                                  style: {
                                    color: '#F0F0F0'
                                  }
                                },
                                plotOptions: {
                                  series: {
                                    dataLabels: {
                                      color: '#B0B0B3'
                                    },
                                    marker: {
                                      lineColor: '#333'
                                    }
                                  },
                                  boxplot: {
                                    fillColor: '#505053'
                                  },
                                  candlestick: {
                                    lineColor: 'white'
                                  },
                                  errorbar: {
                                    color: 'white'
                                  }
                                },
                                legend: {
                                  itemStyle: {
                                    color: '#E0E0E3',
                                    fontSize: '9px'
                                  },
                                  itemHoverStyle: {
                                    color: '#FFF',
                                    fontSize: '9px'
                                  },
                                  itemHiddenStyle: {
                                    color: '#606063',
                                    fontSize: '9px'
                                  },
                                },
                                credits: {
                                  style: {
                                    color: '#666'
                                  }
                                },
                                labels: {
                                  style: {
                                    color: '#707073'
                                  }
                                },

                                drilldown: {
                                  activeAxisLabelStyle: {
                                    color: '#F0F0F3'
                                  },
                                  activeDataLabelStyle: {
                                    color: '#F0F0F3'
                                  }
                                },

                                navigation: {
                                  buttonOptions: {
                                    symbolStroke: '#DDDDDD',
                                    theme: {
                                      fill: '#505053'
                                    }
                                  }
                                },

        // scroll charts
        rangeSelector: {
          buttonTheme: {
            fill: '#505053',
            stroke: '#000000',
            style: {
              color: '#CCC'
            },
            states: {
              hover: {
                fill: '#707073',
                stroke: '#000000',
                style: {
                  color: 'white'
                }
              },
              select: {
                fill: '#000003',
                stroke: '#000000',
                style: {
                  color: 'white'
                }
              }
            }
          },
          inputBoxBorderColor: '#505053',
          inputStyle: {
            backgroundColor: '#333',
            color: 'silver'
          },
          labelStyle: {
            color: 'silver'
          }
        },
        
        navigator: {
          handles: {
            backgroundColor: '#666',
            borderColor: '#AAA'
          },
          outlineColor: '#CCC',
          maskFill: 'rgba(255,255,255,0.1)',
          series: {
            color: '#7798BF',
            lineColor: '#A6C7ED'
          },
          xAxis: {
            gridLineColor: '#505053'
          }
        },
        
        scrollbar: {
          barBackgroundColor: '#808083',
          barBorderColor: '#808083',
          buttonArrowColor: '#CCC',
          buttonBackgroundColor: '#606063',
          buttonBorderColor: '#606063',
          rifleColor: '#FFF',
          trackBackgroundColor: '#404043',
          trackBorderColor: '#404043'
        },
        
        // special colors for some of the
        legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
        background2: '#505053',
        dataLabelsColor: '#B0B0B3',
        textColor: '#C0C0C0',
        contrastTextColor: '#F0F0F3',
        maskColor: 'rgba(255,255,255,0.3)'
      };

    // Apply the theme
    Highcharts.setOptions(Highcharts.theme);

    var trxactive3g = <?php echo json_encode($trxactive3g); ?>;
    var trxunactive3g = <?php echo json_encode($trxunactive3g); ?>;
    var trxtotal3g = <?php echo json_encode($trxtotal3g); ?>;
    var week_trx_3g = <?php echo json_encode($week_trx_3g); ?>;
    
    Highcharts.chart('chart1', {
      title: {
        text: ' '
      },

      credits: {
        enabled: false
      },

      subtitle: {
        text: ''
      },

      yAxis: {
        title: {
          text: 'Kpbs'
        }
      },
      legend: { 
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      xAxis: {
        categories: week_trx_3g
      },

      series: [{
        name: '3',
        data: trxactive3g,
      }, {
        name: 'INDOSAT',
        data: trxunactive3g,
      },{
        name: 'SMARTFREN',
        data: trxunactive3g,
      },
      {
        name: 'XL',
        data: trxunactive3g,
      }, {
        name: 'TELKOMSEL',
        data: trxtotal3g,
      },
      ],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }
    });

    Highcharts.chart('chart11', {
      title: {
        text: ' '
      },

      credits: {
        enabled: false
      },

        // subtitle: {
        //     text: 'JUMLAH DOWNLOAD 4G'
        // },
        
        yAxis: {
          title: {
            text: 'Kpbs'
          }
        },
        legend: { 
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },
        
        xAxis: {
          categories: week_trx_3g
        },
        
        series: [{
          name: '3',
          data: trxactive3g,
        }, {
          name: 'INDOSAT',
          data: trxunactive3g,
        },{
          name: 'SMARTFREN',
          data: trxunactive3g,
        },
        {
          name: 'XL',
          data: trxunactive3g,
        }, {
          name: 'TELKOMSEL',
          data: trxtotal3g,
        },
        ],
        
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }
      });

    var trxactive3g = <?php echo json_encode($trxactive3g); ?>;
    var trxunactive3g = <?php echo json_encode($trxunactive3g); ?>;
    var trxtotal3g = <?php echo json_encode($trxtotal3g); ?>;
    var week_trx_3g = <?php echo json_encode($week_trx_3g); ?>;
    
    Highcharts.chart('chart2', {
      title: {
        text: ' '
      },

      credits: {
        enabled: false
      },

      subtitle: {
        text: ''
      },

      yAxis: {
        title: {
          text: 'Kpbs'
        }
      },
      legend: { 
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      xAxis: {
        categories: week_trx_3g
      },

      series: [{
        name: '3',
        data: trxactive3g,
      }, {
        name: 'INDOSAT',
        data: trxunactive3g,
      },{
        name: 'SMARTFREN',
        data: trxunactive3g,
      },
      {
        name: 'XL',
        data: trxunactive3g,
      }, {
        name: 'TELKOMSEL',
        data: trxtotal3g,
      },
      ],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }
    });

    Highcharts.chart('chart22', {
      title: {
        text: ' '
      },

      credits: {
        enabled: false
      },

        // subtitle: {
        //     text: 'JUMLAH DOWNLOAD 3G'
        // },
        
        yAxis: {
          title: {
            text: 'Kpbs'
          }
        },
        legend: { 
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },
        
        xAxis: {
          categories: week_trx_3g
        },
        
        series: [{
          name: '3',
          data: trxactive3g,
        }, {
          name: 'INDOSAT',
          data: trxunactive3g,
        },{
          name: 'SMARTFREN',
          data: trxunactive3g,
        },
        {
          name: 'XL',
          data: trxunactive3g,
        }, {
          name: 'TELKOMSEL',
          data: trxtotal3g,
        },
        ],
        
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }
      });
    
    var trxactive4g = <?php echo json_encode($trxactive4g); ?>;
    var trxunactive4g = <?php echo json_encode($trxunactive4g); ?>;
    var trxtotal4g = <?php echo json_encode($trxtotal4g); ?>;
    var week_trx_4g = <?php echo json_encode($week_trx_4g); ?>;
    
    Highcharts.chart('chart3', {
      title: {
        text: ' '
      },

      credits: {
        enabled: false
      },

      subtitle: {
        text: ''
      },

      yAxis: {
        title: {
          text: 'Kpbs'
        }
      },
      legend: { 
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      xAxis: {
        categories: week_trx_4g
      },

      series: [{
        name: '3',
        data: trxactive4g,
      }, {
        name: 'INDOSAT',
        data: trxunactive4g,
      },{
        name: 'SMARTFREN',
        data: trxunactive4g,
      },
      {
        name: 'XL',
        data: trxunactive4g,
      }, {
        name: 'TELKOMSEL',
        data: trxtotal4g,
      },
      ],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }
    });

    Highcharts.chart('chart33', {
      title: {
        text: ' '
      },

      credits: {
        enabled: false
      },

        // subtitle: {
        //     text: 'Jumlah UPLOAD 4G'
        // },
        
        yAxis: {
          title: {
            text: 'Kpbs'
          }
        },
        legend: { 
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },
        
        xAxis: {
          categories: week_trx_4g
        },
        
        series: [{
          name: '3',
          data: trxactive4g,
        }, {
          name: 'INDOSAT',
          data: trxunactive4g,
        },{
          name: 'SMARTFREN',
          data: trxunactive4g,
        },
        {
          name: 'XL',
          data: trxunactive4g,
        }, {
          name: 'TELKOMSEL',
          data: trxtotal4g,
        },
        ],
        
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }
      });
    
    
    Highcharts.chart('chart4', {
      title: {
        text: ''
      },

      credits: {
        enabled: false
      },

      subtitle: {
        text: ''
      },

      yAxis: {
        title: {
          text: 'Kpbs'
        }
      },
      legend: { 
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      xAxis: {
        categories: week_trx_3g
      },

      series: [{
        name: '3',
        data: trxactive3g,
      }, {
        name: 'INDOSAT',
        data: trxunactive3g,
      },{
        name: 'SMARTFREN',
        data: trxunactive3g,
      },
      {
        name: 'XL',
        data: trxunactive3g,
      }, {
        name: 'TELKOMSEL',
        data: trxtotal3g,
      },
      ],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }
    });

    Highcharts.chart('chart44', {
      title: {
        text: ''
      },

      credits: {
        enabled: false
      },

        // subtitle: {
        //     text: 'JUMLAH UPLOAD 3G'
        // },
        
        yAxis: {
          title: {
            text: 'Kpbs'
          }
        },
        legend: { 
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },
        
        xAxis: {
          categories: week_trx_3g
        },
        
        series: [{
          name: '3',
          data: trxactive3g,
        }, {
          name: 'INDOSAT',
          data: trxunactive3g,
        },{
          name: 'SMARTFREN',
          data: trxunactive3g,
        },
        {
          name: 'XL',
          data: trxunactive3g,
        }, {
          name: 'TELKOMSEL',
          data: trxtotal3g,
        },
        ],
        
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }
      });
    
    var poweractive3g = <?php echo json_encode($poweractive3g); ?>;
    var powerunactive3g = <?php echo json_encode($powerunactive3g); ?>;
    var powertotal3g = <?php echo json_encode($powertotal3g); ?>;
    var week_power_3g = <?php echo json_encode($week_power_3g); ?>;
    
    Highcharts.chart('chart5', {
      title: {
        text: ''
      },

      credits: {
        enabled: false
      },

      subtitle: {
        text: ''
      },

      yAxis: {
        title: {
          text: 'Kpbs'
        }
      },
      legend: { 
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      xAxis: {
        categories: week_power_3g
      },

      series: [{
        name: '3',
        data: trxactive3g,
      }, {
        name: 'INDOSAT',
        data: trxunactive3g,
      },{
        name: 'SMARTFREN',
        data: trxunactive3g,
      },
      {
        name: 'XL',
        data: trxunactive3g,
      }, {
        name: 'TELKOMSEL',
        data: trxtotal3g,
      },
      ],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }
    });

    Highcharts.chart('chart55', {
      title: {
        text: ''
      },

      credits: {
        enabled: false
      },

        // subtitle: {
        //     text: 'JUMLAH LATENCY 4G'
        // },
        
        yAxis: {
          title: {
            text: 'Kpbs'
          }
        },
        legend: { 
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },
        
        xAxis: {
          categories: week_power_3g
        },
        
        series: [{
          name: '3',
          data: trxactive3g,
        }, {
          name: 'INDOSAT',
          data: trxunactive3g,
        },{
          name: 'SMARTFREN',
          data: trxunactive3g,
        },
        {
          name: 'XL',
          data: trxunactive3g,
        }, {
          name: 'TELKOMSEL',
          data: trxtotal3g,
        },
        ],
        
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }
      });
    
    var poweractive4g = <?php echo json_encode($poweractive4g); ?>;
    var powerunactive4g = <?php echo json_encode($powerunactive4g); ?>;
    var powertotal4g = <?php echo json_encode($powertotal4g); ?>;
    var week_power_4g = <?php echo json_encode($week_power_4g); ?>;
    
    Highcharts.chart('chart6', {
      title: {
        text: ''
      },

      credits: {
        enabled: false
      },

      subtitle: { 
        text: ''
      },

      yAxis: {
        title: {
          text: 'Kpbs'
        }
      },
      legend: { 
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      xAxis: {
        categories: week_power_4g
      },

      series: [{
        name: '3',
        data: trxactive4g,
      }, {
        name: 'INDOSAT',
        data: trxunactive4g,
      },{
        name: 'SMARTFREN',
        data: trxunactive4g,
      },
      {
        name: 'XL',
        data: trxunactive4g,
      }, {
        name: 'TELKOMSEL',
        data: trxtotal4g,
      },
      ],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }
    });

    Highcharts.chart('chart66', {
      title: {
        text: ''
      },

      credits: {
        enabled: false
      },

        // subtitle: { 
        //     text: 'JUMLAH LATENCY 3G'
        // },
        
        yAxis: {
          title: {
            text: 'Kpbs'
          }
        },
        legend: { 
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },
        
        xAxis: {
          categories: week_power_4g
        },
        
        series: [{
          name: '3',
          data: trxunactive4g,
        }, {
          name: 'INDOSAT',
          data: trxunactive4g,
        },{
          name: 'SMARTFREN',
          data: trxunactive4g,
        },
        {
          name: 'XL',
          data: trxunactive4g,
        }, {
          name: 'TELKOMSEL',
          data: trxtotal4g,
        },
        ],
        
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }
      }); 

    Highcharts.chart('piechart1', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
        {
            name: "TELKOMSEL ONLY",
            colorByPoint: true,
            data: [
                {
                    name: "Android",
                    y: 62.74,
                    drilldown: "Android"
                },
                {
                    name: "Windows Phone",
                    y: 10.57,
                    drilldown: "Windows Phone"
                },
                {
                    name: "iPhone",
                    y: 7.23,
                    drilldown: "iPhone"
                },
                {
                    name: "Other",
                    y: 7.62,
                    drilldown: null
                }
            ]
        }
    ]
});

Highcharts.chart('piechart11', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
        {
            name: "TELKOMSEL ONLY",
            colorByPoint: true,
            data: [
                {
                    name: "Android",
                    y: 62.74,
                    drilldown: "Android"
                },
                {
                    name: "Windows Phone",
                    y: 10.57,
                    drilldown: "Windows Phone"
                },
                {
                    name: "iPhone",
                    y: 7.23,
                    drilldown: "iPhone"
                },
                {
                    name: "Other",
                    y: 7.62,
                    drilldown: null
                }
            ]
        }
    ]
});

    Highcharts.chart('piechart2', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
        {
            name: "ALL OPERATOR",
            colorByPoint: true,
            data: [
                {
                    name: "Android",
                    y: 62.74,
                    drilldown: "Android"
                },
                {
                    name: "Windows Phone",
                    y: 10.57,
                    drilldown: "Windows Phone"
                },
                {
                    name: "iPhone",
                    y: 7.23,
                    drilldown: "iPhone"
                },
                {
                    name: "Other",
                    y: 7.62,
                    drilldown: null
                }
            ]
        }
    ]
});

Highcharts.chart('piechart22', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
        {
            name: "ALL OPERATOR",
            colorByPoint: true,
            data: [
                {
                    name: "Android",
                    y: 62.74,
                    drilldown: "Android"
                },
                {
                    name: "Windows Phone",
                    y: 10.57,
                    drilldown: "Windows Phone"
                },
                {
                    name: "iPhone",
                    y: 7.23,
                    drilldown: "iPhone"
                },
                {
                    name: "Other",
                    y: 7.62,
                    drilldown: null
                }
            ]
        }
    ]
});

Highcharts.chart('combichart', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: [{
        categories: ['Maxis', 'AT&T', 'CelCom', 'Digi', 'Docomo', 'Sprint',
            '02', 'T-Mobile', 'Vodafone'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}°C',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Subscribers',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: 'Throughput',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value} mm',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        x: 60,
        verticalAlign: 'top',
        y: 0,
        floating: true,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            'rgba(255,255,255,0.25)'
    },
    series: [{
        name: 'Throughput',
        type: 'column',
        yAxis: 1,
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4],
        tooltip: {
            valueSuffix: ' mm'
        }

    }, {
        name: 'Count',
        type: 'spline',
        data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3],
        tooltip: {
            valueSuffix: '°C'
        }
    }]
});

Highcharts.chart('combichart2', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: [{
        categories: ['Maxis', 'AT&T', 'CelCom', 'Digi', 'Docomo', 'Sprint',
            '02', 'T-Mobile', 'Vodafone'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}°C',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Subscribers',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: 'Throughput',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value} mm',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        x: 120,
        verticalAlign: 'top',
        y: 100,
        floating: true,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            'rgba(255,255,255,0.25)'
    },
    series: [{
        name: 'Throughput',
        type: 'column',
        yAxis: 1,
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4],
        tooltip: {
            valueSuffix: ' mm'
        }

    }, {
        name: 'Count',
        type: 'spline',
        data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3],
        tooltip: {
            valueSuffix: '°C'
        }
    }]
});

Highcharts.chart('line1', {
    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Subscribers'
        }
    },
    xAxis: {
        title: {
            text: 'Speed Range(Kpbs)'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'center',
        verticalAlign: 'top'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: true
            },
            pointStart: 2010
        }
    },

    series: [{
        name: 'Nation Download',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
    }],


});

Highcharts.chart('line11', {
    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Subscribers'
        }
    },
    xAxis: {
        title: {
            text: 'Speed Range(Kpbs)'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'center',
        verticalAlign: 'top'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: true
            },
            pointStart: 2010
        }
    },

    series: [{
        name: 'Nation Upload',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
    }],


});

Highcharts.chart('line2', {
    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Subscribers'
        }
    },
    xAxis: {
        title: {
            text: 'Speed Range(Kpbs)'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'center',
        verticalAlign: 'top'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: true
            },
            pointStart: 2010
        }
    },

    series: [{
        name: 'Nation Upload',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
    }],


});

Highcharts.chart('line22', {
    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Subscribers'
        }
    },
    xAxis: {
        title: {
            text: 'Speed Range(Kpbs)'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'center',
        verticalAlign: 'top'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: true
            },
            pointStart: 2010
        }
    },

    series: [{
        name: 'Nation Upload',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
    }],


});
  </script>
<script>
    $(document).ready(function() {
        $('[data-toggle="toggle"]').change(function(){
            $(this).parents().next('.hide').toggle();
        });
    });
</script>
</body>

</html>
