<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>DASHBOARD OOKLA</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{url('')}}/template/tsel.jpg" rel="icon" type="image/png">
  <link rel="stylesheet" type="text/css" href="{{url('')}}/css/isotope.css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/flexslider.css" type="text/css">
  <link rel="stylesheet" href="{{url('')}}/js/fancybox/jquery.fancybox.css" type="text/css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/bootstrap.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="{{url('')}}/css/style.css">
  <link rel="stylesheet" href="{{url('')}}/skin/default.css">
  <script src="https://unpkg.com/ag-grid-enterprise@20.2.0/dist/ag-grid-enterprise.min.js"></script>  
</head>
<body>
  <section id="header" class="appear">
    <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:40px; height:40px; background-color:rgba(0,0,0,0.3);" data-300="line-height:40px; height:40px; background-color:rgba(5, 42, 62, 1);">
      <!-- <div class="container"> -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="fa fa-bars color-white"></span>
          </button>
          <div class="navbar-logo">
            <img data-0="width:170px;" data-300=" width:100px;" src="../img/oklaa.png" alt="" style="padding-left: 50px;">
          </div>
        </div>
        <div class="navbar-collapse collapse" style="padding-right: 150px;">
          <label for="week" style="color: white; font-size: 16px; padding-left: 10px; padding-top: 4px">Weekly: &nbsp;</label>
          <input type="week" name="week" id="week" style="height: 30px; width: 145px;">
          <label for="months" style="color: white; font-size: 16px; padding-left: 10px; padding-top: 4px">Monthly: &nbsp;</label>
          <input type="month" name="months" id="months" style="height: 30px; width: 155px;">
          <ul class="nav navbar-nav" data-0="margin-top:0px;" data-300="margin-top:0px;">
            <li class="active"><a href="{{url('/')}}">Home</a></li>
            <li><a href="{{url('super_4g')}}">SUPER 4G</a></li>
            <li><a href="{{url('wcc_62')}}">WCC 62</a></li>
            <li><a href="{{url('csat_48')}}">CSAT 48</a></li>
            <li><a href="{{url('logout')}}">Logout</a></li>
          </ul>
        </div>
      <!-- </div> -->
    </div>
  </section>
  <section id="intro">
    <div class="intro-content">
      <div class="row" style="padding-top: 4%; padding-left: 0px; padding-right: 10px; margin-bottom: 0px;">
        <div class="col-lg-6" style="padding-right: 0px;">
          <div class="panel-heading" style="background-color: #800000;">
            <h4 style="font-family: calibri; color: white;"><strong>Region - Map</strong></h4>
          </div>
          <div class="small-box boks boksBody" style="height: 445px;">
            <div class="inner">
              <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                <div class="card-body1" style="width:100%; height:400px;" >
                  <div id="map" style="width:100%; height:445px;"></div>
                </div> 
              </div>
            </div>
          </div><!-- BOKSBODY-->
          <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; padding-top: 5px;">
            <div class="container" style=" padding-top: 0px; padding-left: 0px; padding-right: 0px;">
              <ul class="nav nav-tabs" role="tablist" style="padding-top: 0px; background-color: #800000; font-family: calibri; font-weight: bold;">
              <!-- <li class="nav-link active"><a data-toggle="tab" role="tab" href="#nav-4G">4G</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" role="tab" href="#nav-3G">3G</a></li> -->
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">4G
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#nav-4G" data-toggle="tab" role="tab">Trend Bar</a></li>
                      <li><a href="#nav-4G-line" data-toggle="tab" role="tab">Trend Line</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">3G
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#nav-3G" data-toggle="tab" role="tab">Trend Bar</a></li>
                        <li><a href="#nav-3G-line" data-toggle="tab" role="tab">Trend Line</a></li>
                      </ul>
                    </li>
                  </ul>
            <div class="tab-content">
          <div id="nav-4G" role="tabpanel" class="tab-pane fade in active">
            <div class="small-box boks boksBody" style="height: 195px;">
              <div class="inner">
               <div class="row">
                <div class="col-md-4" style="margin-top:5px; padding-right: 0px; padding-left: 10px;">
                  <div class="card-header10" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> DOWNLOAD</i> 
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal10"></i>
                  </div>
                  <div class="modal fade" id="modal10" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="bar11" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body10" style="width:100%; height:165px; background-color: #343a40">
                    <div id="bar1" style="width:100%; height:165px;"></div>
                  </div> 
                </div>
                <div class="col-md-4" style="margin-top:5px; padding-right: 0px; padding-left: 5px;">
                  <div class="card-header11" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> UPLOAD</i> 
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal11"></i>
                  </div>
                  <div class="modal fade" id="modal11" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> UPLOAD
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="bar22" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body11" style="width:100%; height:165px; background-color: #343a40">
                    <div id="bar2" style="width:100%; height:165px;"></div>
                  </div> 
                </div>
                <div class="col-md-4" style="margin-top:5px; padding-right: 10px; padding-left: 5px;">
                  <div class="card-header12" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> LATENCY</i> 
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal12"></i>
                  </div>
                  <div class="modal fade" id="modal12" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> LATENCY
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="bar33" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body11" style="width:100%; height:165px; background-color: #343a40">
                    <div id="bar3" style="width:100%; height:165px;"></div>
                  </div> 
                </div>
              </div>
            </div>
            </div><!-- BOKSBODY-->
          </div>
          <div id="nav-4G-line" role="tabpanel" class="tab-pane fade">
            <div class="small-box boks boksBody" style="height: 195px;">
              <div class="inner">
               <div class="row">
                <div class="col-md-4" style="margin-top:5px; padding-right: 0px; padding-left: 10px;">
                  <div class="card-headerline1" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> DOWNLOAD</i> 
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modalline1"></i>
                  </div>
                  <div class="modal fade" id="modalline1" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartline11" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-bodyline1" style="width:100%; height:165px; background-color: #343a40">
                    <div id="chartline1" style="width:100%; height:165px;"></div>
                  </div> 
                </div>
                <div class="col-md-4" style="margin-top:5px; padding-right: 0px; padding-left: 5px;">
                  <div class="card-headerline2" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> UPLOAD</i> 
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modalline2"></i>
                  </div>
                  <div class="modal fade" id="modalline2" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> UPLOAD
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartline22" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-bodyline2" style="width:100%; height:165px; background-color: #343a40">
                    <div id="chartline2" style="width:100%; height:165px;"></div>
                  </div> 
                </div>
                <div class="col-md-4" style="margin-top:5px; padding-right: 10px; padding-left: 5px;">
                  <div class="card-headerline3" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> LATENCY</i> 
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modalline3"></i>
                  </div>
                  <div class="modal fade" id="modalline3" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> LATENCY
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartline33" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-bodyline3" style="width:100%; height:165px; background-color: #343a40">
                    <div id="chartline3" style="width:100%; height:165px;"></div>
                  </div> 
                </div>
              </div>
            </div>
            </div><!-- BOKSBODY-->
          </div>
          <div id="nav-3G" role="tabpanel" class="tab-pane fade">
            <div class="small-box boks boksBody" style="height: 195px;">
              <div class="inner">
                <div class="row">
                  <div class="col-md-4" style="margin-top:5px; padding-right: 0px; padding-left: 10px;">
                    <div class="card-header13" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> DOWNLOAD</i> 
                      <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal13"></i>
                    </div>
                    <div class="modal fade" id="modal13" style="width: 100%;">
                      <div class="modal-dialog modal-lg" style="margin-top:90px;">
                        <div class="modal-content" style="background-color:#353537"> 
                          <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD
                            <button type="button" class="close" data-dismiss="modal">

                              <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                          </div>
                          <div class="modal-body" style="color:#000"> 
                            <div id="bar44" style="width:100%; height:100%;"></div>
                          </div>
                        </div> 
                      </div>
                    </div>
                    <div class="card-body13" style="width:100%; height:165px; background-color: #343a40">
                      <div id="bar4" style="width:100%; height:165px;"></div>
                    </div> 
                  </div>
                  <div class="col-md-4" style="margin-top:5px; padding-right: 0px; padding-left: 5px;">
                    <div class="card-header14" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> UPLOAD</i> 
                      <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal14"></i>
                    </div>
                    <div class="modal fade" id="modal14" style="width: 100%;">
                      <div class="modal-dialog modal-lg" style="margin-top:90px;">
                        <div class="modal-content" style="background-color:#353537"> 
                          <div class="modal-header" style="text-align:center; color: white;"> UPLOAD
                            <button type="button" class="close" data-dismiss="modal">

                              <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                          </div>
                          <div class="modal-body" style="color:#000"> 
                            <div id="bar55" style="width:100%; height:100%;"></div>
                          </div>
                        </div> 
                      </div>
                    </div>
                    <div class="card-body14" style="width:100%; height:165px; background-color: #343a40">
                      <div id="bar5" style="width:100%; height:165px;"></div>
                    </div> 
                  </div>
                  <div class="col-md-4" style="margin-top:5px; padding-right: 10px; padding-left: 5px;">
                    <div class="card-header15" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> LATENCY</i> 
                      <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal15"></i>
                    </div>
                    <div class="modal fade" id="modal15" style="width: 100%;">
                      <div class="modal-dialog modal-lg" style="margin-top:90px;">
                        <div class="modal-content" style="background-color:#353537"> 
                          <div class="modal-header" style="text-align:center; color: white;"> LATENCY
                            <button type="button" class="close" data-dismiss="modal">

                              <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                          </div>
                          <div class="modal-body" style="color:#000"> 
                            <div id="bar66" style="width:100%; height:100%;"></div>
                          </div>
                        </div> 
                      </div>
                    </div>
                    <div class="card-body15" style="width:100%; height:165px; background-color: #343a40">
                      <div id="bar6" style="width:100%; height:165px;"></div>
                    </div> 
                  </div>
                </div>
              </div>
            </div><!-- BOKSBODY-->
          </div>
          <div id="nav-3G-line" role="tabpanel" class="tab-pane fade">
            <div class="small-box boks boksBody" style="height: 195px;">
              <div class="inner">
               <div class="row">
                <div class="col-md-4" style="margin-top:5px; padding-right: 0px; padding-left: 10px;">
                  <div class="card-headerline4" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> DOWNLOAD</i> 
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modalline4"></i>
                  </div>
                  <div class="modal fade" id="modalline4" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartline44" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-bodyline4" style="width:100%; height:165px; background-color: #343a40">
                    <div id="chartline4" style="width:100%; height:165px;"></div>
                  </div> 
                </div>
                <div class="col-md-4" style="margin-top:5px; padding-right: 0px; padding-left: 5px;">
                  <div class="card-headerline5" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> UPLOAD</i> 
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modalline5"></i>
                  </div>
                  <div class="modal fade" id="modalline5" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> UPLOAD
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartline55" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-bodyline5" style="width:100%; height:165px; background-color: #343a40">
                    <div id="chartline5" style="width:100%; height:165px;"></div>
                  </div> 
                </div>
                <div class="col-md-4" style="margin-top:5px; padding-right: 10px; padding-left: 5px;">
                  <div class="card-headerline6" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"> LATENCY</i> 
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modalline6"></i>
                  </div>
                  <div class="modal fade" id="modalline6" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> LATENCY
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chartline66" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-bodyline6" style="width:100%; height:165px; background-color: #343a40">
                    <div id="chartline6" style="width:100%; height:165px;"></div>
                  </div> 
                </div>
              </div>
            </div>
            </div><!-- BOKSBODY-->
          </div>
          </div>
          </div>
          </div>
        </div>
        <div class="col-md-6" style="padding-right: 0px;">
          <div class="panel-heading" style="background-color: #800000;">
            <h4 style="font-family: calibri; color: white;"><strong> Tabel City</strong></h4>
          </div>
          <div class="small-box boks boksBody" style="height: 445px;">
            <div class="inner">
              <div class="col-xs-12" style="margin-top: 0px; padding-right: 0px; padding-left: 0px;">
                <div id="tabel" style="width: 100%; height: 445px;" class="ag-theme-balham-dark"></div>
                <script type="text/javascript" charset="utf-8">
                  var columnDefs = [ 
                  {
                    headerName: 'CITY', field: 'kabupaten', width: 175,filter: 'agTextColumnFilter'
                  },
                  {
                    headerName: 'DOWNLOAD',
                    headerClass: 'participant-group',
                    children: [
                    {headerName: '3G', field: 'download3g', width: 110,
                    cellStyle: function(params) {
                      var str = params.data.download3g;
                      var max = str.includes("+");
                      var min = str.includes("-");

                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                      }

                    },valueFormatter: currencyFormatter},
                    {headerName: '4G', field: 'download4g', width: 110,
                    cellStyle: function(params){
                      var str = params.data.download4g;
                      var max = str.includes("+");
                      var min = str.includes("-");
                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                      }
                      return {textAlign: 'right','border-right': '1px solid #696969'};

                    },valueFormatter: download4gFormatter}
                    ]
                  },
                  {
                    headerName: 'UPLOAD',
                    headerClass: 'participant-group',
                    children: [
                    {headerName: '3G', field: 'upload3g', width: 110,
                    cellStyle: function(params) {
                      var str = params.data.upload3g;
                      var max = str.includes("+");
                      var min = str.includes("-");

                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                      }

                    },valueFormatter: upload3gFormatter},
                    {headerName: '4G', field: 'upload4g', width: 110,
                    cellStyle: function(params){
                      var str = params.data.upload3g;
                      var max = str.includes("+");
                      var min = str.includes("-");

                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                      }
                      return {textAlign: 'left','border-right': '1px solid #696969'};
                    },valueFormatter: upload4gFormatter}
                    ]
                  },
                  {
                    headerName: 'LATENCY',
                    headerClass: 'participant-group',
                    children: [
                    {headerName: '3G', field: 'latency3g', width: 110,
                    cellStyle: function(params) {
                      var str = params.data.latency3g;
                      var max = str.includes("+");
                      var min = str.includes("-");

                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                      }

                    },valueFormatter: latency3gFormatter},
                    {headerName: '4G', field: 'latency4g', width: 110,
                    cellStyle: function(params){
                      var str = params.data.latency4g;
                      var max = str.includes("+");
                      var min = str.includes("-");

                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize':'14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize':'14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize':'14px'};
                      }
                      return {textAlign: 'left','border-right': '1px solid #696969'};
                    },valueFormatter: latency4gFormatter}
                    ]
                  }
                ]; 
                function currencyFormatter(params) {
                  var value = params.data.download3g;
                  var min = value.includes("-");
                  var max = value.includes("+");
                  var pars = parseFloat(value);
                  var fxd = pars.toFixed(2);
                  console.log(fxd);

                  if (min == true) {
                    return fxd.replace('-','');
                  }else if(max == true){
                    return fxd.replace('+','');
                  }else if(value){
                    return fxd;
                  }
                }
                function download4gFormatter(params) {
                    var value = params.data.download4g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else{
                      return fxd;
                    }
                  }
                function upload3gFormatter(params) {
                    var value = params.data.upload3g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else{
                      return fxd;
                    }
                  }
                  function upload4gFormatter(params) {
                    var value = params.data.upload4g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else{
                      return fxd;
                    }
                  }
                  function latency3gFormatter(params) {
                    var value = params.data.latency3g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else{
                      return fxd;
                    }
                  }
                  function latency4gFormatter(params) {
                    var value = params.data.latency4g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else{
                      return fxd;
                    }
                  }
                var gridOptions = {
                  defaultColDef: {
                    resizable: true,
                    filter: true
                  },
                  columnDefs: columnDefs,
                  rowData: null,
                  onSelectionChanged: onSelectionChanged,
                  rowSelection: 'multiple',
                  paginationAutoPageSize:true,
                  pagination: true,
                  headerHeight: 36,
                  rowHeight: 30.8,
                };

                function onPaginationChanged() {
                    console.log('onPaginationPageLoaded');

                    if (gridOptions.api) {
                      setText('#lbLastPageFound', gridOptions.api.paginationIsLastPageFound());
                      setText('#lbPageSize', gridOptions.api.paginationGetPageSize());
                      setText('#lbCurrentPage', gridOptions.api.paginationGetCurrentPage() + 1);
                      setText('#lbTotalPages', gridOptions.api.paginationGetTotalPages());

                      setLastButtonDisabled(!gridOptions.api.paginationIsLastPageFound());
                    }  
                  } 

                  function setLastButtonDisabled(disabled) {
                    document.querySelector('#btLast').disabled = disabled;
                  }

                  function setRowData(rowData) {
                    allOfTheData = rowData;
                    createNewDatasource();
                  }

                  function onSelectionChanged() {
                    var selectedRows = gridOptions.api.getSelectedRows();

                    var selectedRowsString = []; 
                    selectedRows.forEach( function(selectedRow, index) {
                      if (index!==0) {
                        selectedRowsString += ', ';
                      }
                      selectedRowsString.push(selectedRow);
                    });
                    return JSON.stringify({ data : selectedRowsString });
                  }

                  function onBtNext() {
                    id = 0; 
                    offset = limit;
                    limit += 50;  

                    console.log(limit);

                    document.getElementById("btnNext").addEventListener('click', function() {
                      document.querySelector('#tabel');
                      var httpRequest = new XMLHttpRequest();

                      search = document.getElementById("search_data").value; 
                      var regional = <?php echo json_encode($regional); ?>;
                      if(search) { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/home/table_kabupaten/'+regional);
                      } else { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/home/table_kabupaten/'+regional);
                      }

                      httpRequest.open('GET', api); 
                      httpRequest.send(); 
                      httpRequest.onreadystatechange = function() {
                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                          var httpResponse = JSON.parse(httpRequest.responseText);
                          gridOptions.api.setRowData(httpResponse.data);
                        }
                      };
                    });

                    gridOptions.api.paginationGoToNextPage();
                  }

                  function onBtPrevious() { 
                    id = 0; 
                    offset -= limit;
                    limit -= 50;

                    document.getElementById("btnPrevious").addEventListener('click', function() {
                      document.querySelector('#tabel');
                      var httpRequest = new XMLHttpRequest();

                      search = document.getElementById("search_data").value; 
                      var regional = <?php echo json_encode($regional); ?>;
                      if(search) { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/home/table_kabupaten/'+regional);
                      } else {
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/home/table_kabupaten/'+regional);
                      }

                      httpRequest.open('GET', api);
                      httpRequest.send();
                      httpRequest.onreadystatechange = function() {
                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                          var httpResponse = JSON.parse(httpRequest.responseText);
                          gridOptions.api.setRowData(httpResponse.data);
                        }
                      };
                    });

                    gridOptions.api.paginationGoToPreviousPage();
                  }

                function onSelectionChanged() {
                  var selectedRows = gridOptions.api.getSelectedRows();
                  var selectedRowsString = '';
                  selectedRows.forEach( function(selectedRow, index) {
                    if (selectedRow.kabupaten){
                      var url = "{{url('city')}}/"+selectedRow.kabupaten;
                      window.location.href = url;
                    }
                  });
                } 

                document.addEventListener('DOMContentLoaded', function() {
                  var gridDiv = document.querySelector('#tabel');
                  new agGrid.Grid(gridDiv, gridOptions);
                  var httpRequest = new XMLHttpRequest(); 
                  var regional = <?php echo json_encode($regional); ?>;
                  var api = encodeURI('http://10.54.36.49/api-ookla/public/home/table_kabupaten/'+regional);
                  console.log(api)
                  httpRequest.open('GET', api); 
                  httpRequest.send(); 
                  httpRequest.onreadystatechange = function() {

                    if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                      var httpResponse = JSON.parse(httpRequest.responseText);
                      gridOptions.api.setRowData(httpResponse.data);
                    }
                  };
                }); 
              </script> 
              <style>
                .ag-header-group-cell-label {
                  justify-content: center;
                }
                .participant-group {
                  border: #00E7B1;
                }
              </style>
            </div>
          </div>
        </div><!-- BOKSBODY-->
        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; padding-top: 5px;">
          <div class="panel-heading" style="background-color: #800000;">
            <h4 style="font-family: calibri; color: white;"><strong>Speed Test Distribution</strong></h4>
          </div>
          <div class="small-box boks boksBody" style="height: 190px;">
            <div class="inner">
              <div class="col-xs-12" style="margin-top: 0px; padding-right: 0px; padding-left: 0px;">
                <div id="tabelspeed" style="width: 100%; height: 190px;" class="ag-theme-balham-dark"></div>
                <script type="text/javascript" charset="utf-8">
                var columnDefs2 = [ 
                  {
                    headerName: 'CITIES', field: 'city', width: 235,filter: 'agTextColumnFilter'
                  },
                  {
                    headerName: 'TOTAL MC SAMPLE', field: 'total_mc_sample', width: 150,filter: 'agTextColumnFilter',cellStyle: {textAlign: 'right', 'fontSize':'14px'}
                  },
                  {
                    headerName: 'TSEL WIN', field: 'tsel_win', width: 100,filter: 'agTextColumnFilter',cellStyle: {textAlign: 'right', 'fontSize':'14px'}
                  },
                  {
                    headerName: 'TSEL LOSE', field: 'tsel_lose', width: 100,filter: 'agTextColumnFilter',cellStyle: {textAlign: 'right', 'fontSize':'14px'}
                  },
                  {
                    headerName: 'NO TSEL', field: 'no_tsel', width: 100,filter: 'agTextColumnFilter',cellStyle: {textAlign: 'right', 'fontSize':'14px'}
                  },
                  {
                    headerName: 'NO COMPETITOR', field: 'no_competitor', width: 150,filter: 'agTextColumnFilter',cellStyle: {textAlign: 'right', 'fontSize':'14px'}
                  }
                ]; 
                var gridOptions2 = {
                  defaultColDef: {
                    resizable: true,
                  },
                  columnDefs: columnDefs2,
                  rowData: null,
                  onSelectionChanged: onSelectionChanged,
                  rowSelection: 'multiple',
                  paginationAutoPageSize:true,
                  pagination: true,
                  headerHeight: 35,
                  rowHeight: 30,
                };

                function onPaginationChanged() {
                    console.log('onPaginationPageLoaded');

                    if (gridOptions2.api) {
                      setText('#lbLastPageFound', gridOptions2.api.paginationIsLastPageFound());
                      setText('#lbPageSize', gridOptions2.api.paginationGetPageSize());
                      setText('#lbCurrentPage', gridOptions2.api.paginationGetCurrentPage() + 1);
                      setText('#lbTotalPages', gridOptions2.api.paginationGetTotalPages());

                      setLastButtonDisabled(!gridOptions2.api.paginationIsLastPageFound());
                    }  
                  } 

                  function setLastButtonDisabled(disabled) {
                    document.querySelector('#btLast').disabled = disabled;
                  }

                  function setRowData(rowData) {
                    allOfTheData = rowData;
                    createNewDatasource();
                  }

                  function onSelectionChanged() {
                    var selectedRows = gridOptions2.api.getSelectedRows();

                    var selectedRowsString = []; 
                    selectedRows.forEach( function(selectedRow, index) {
                      if (index!==0) {
                        selectedRowsString += ', ';
                      }
                      selectedRowsString.push(selectedRow);
                    });
                    return JSON.stringify({ data : selectedRowsString });
                  }

                  function onBtNext() {
                    id = 0; 
                    offset = limit;
                    limit += 50;  

                    console.log(limit);

                    document.getElementById("btnNext").addEventListener('click', function() {
                      document.querySelector('#tabelspeed');
                      var httpRequest = new XMLHttpRequest();

                      search = document.getElementById("search_data").value; 
                      var regional = <?php echo json_encode($regional); ?>;
                      if(search) { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_kabupaten/'+regional);
                      } else { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_kabupaten/'+regional);
                      }

                      httpRequest.open('GET', api); 
                      httpRequest.send(); 
                      httpRequest.onreadystatechange = function() {
                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                          var httpResponse = JSON.parse(httpRequest.responseText);
                          gridOptions2.api.setRowData(httpResponse.data);
                        }
                      };
                    });

                    gridOptions2.api.paginationGoToNextPage();
                  }

                  function onBtPrevious() { 
                    id = 0; 
                    offset -= limit;
                    limit -= 50;

                    document.getElementById("btnPrevious").addEventListener('click', function() {
                      document.querySelector('#tabelspeed');
                      var httpRequest = new XMLHttpRequest();

                      search = document.getElementById("search_data").value; 
                      var regional = <?php echo json_encode($regional); ?>;
                      if(search) { 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_kabupaten/'+regional);
                      } else {
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_kabupaten/'+regional);
                      }

                      httpRequest.open('GET', api);
                      httpRequest.send();
                      httpRequest.onreadystatechange = function() {
                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                          var httpResponse = JSON.parse(httpRequest.responseText);
                          gridOptions2.api.setRowData(httpResponse.data);
                        }
                      };
                    });

                    gridOptions2.api.paginationGoToPreviousPage();
                  }

                document.addEventListener('DOMContentLoaded', function() {
                  var gridDiv = document.querySelector('#tabelspeed');
                  new agGrid.Grid(gridDiv, gridOptions2);
                  var httpRequest = new XMLHttpRequest(); 
                  var regional = <?php echo json_encode($regional); ?>;
                  var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose_kabupaten/'+regional);
                  console.log(api)
                  httpRequest.open('GET', api); 
                  httpRequest.send(); 
                  httpRequest.onreadystatechange = function() {

                    if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                      var httpResponse = JSON.parse(httpRequest.responseText);
                      gridOptions2.api.setRowData(httpResponse.data);
                    }
                  };
                }); 
                </script> 
              </div>
            </div>
            </div><!-- BOKSBODY-->
          </div>
        </div>
        <marquee behavior="scroll" direction="left" scrollamount="5" style="font-size:13px; background: rgba(17, 34, 41, 0.9); color: white; padding-top: 0px;">Last Data&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Region - Map : &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Operator Benchmark : {{$lastupdate_4g}}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Tabel City : {{$last_update_tablekabupaten}}</marquee>
    </div>
  </div>
</section>

<a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>
<script src="{{url('')}}/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="{{url('')}}/js/jquery.js"></script>
<script src="{{url('')}}/js/jquery.easing.1.3.js"></script>
<script src="{{url('')}}/js/bootstrap.min.js"></script>
<script src="{{url('')}}/js/jquery.isotope.min.js"></script>
<script src="{{url('')}}/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="{{url('')}}/js/skrollr.min.js"></script>
<script src="{{url('')}}/js/jquery.scrollTo.min.js"></script>
<script src="{{url('')}}/js/jquery.localScroll.min.js"></script>
<script src="{{url('')}}/js/stellar.js"></script>
<script src="{{url('')}}/js/jquery.appear.js"></script>
<script src="{{url('')}}/js/jquery.flexslider-min.js"></script>
<script src="{{url('')}}/contactform/contactform.js"></script>
<script src="{{url('')}}/js/main.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOVfpPuuh3VHFvtoas3ZuNTt2Kp9KIkTU&callback=initMap"></script>
<script>
  function initialize() {
  var properti_peta = {
  center: new google.maps.LatLng(-6.3145891999999995, 106.9596627),
  zoom: 8,
  mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var peta = new google.maps.Map(document.getElementById("map"), properti_peta);
  }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>

  Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
    '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
        [0, '#2a2a2b'],
        [1, '#3e3e40']
        ]
      },
      style: {
        fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063',
      marginTop: 15
    },
    title: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase',
        fontSize: '20px'
      }
    },
    subtitle: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase'
      }
    },
    xAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
        style: {
          color: '#A0A0A3'

        }
      }
    },
    yAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
        style: {
          color: '#A0A0A3'
        }
      }
    },
    tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
        color: '#F0F0F0'
      }
    },
    plotOptions: {
      series: {
        dataLabels: {
          color: '#B0B0B3'
        },
        marker: {
          lineColor: '#333'
        }
      },
      boxplot: {
        fillColor: '#505053'
      },
      candlestick: {
        lineColor: 'white'
      },
      errorbar: {
        color: 'white'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
    },
    credits: {
      enabled: false
    },
    labels: {
      style: {
        color: '#707073'
      }
    },

    drilldown: {
      activeAxisLabelStyle: {
        color: '#F0F0F3'
      },
      activeDataLabelStyle: {
        color: '#F0F0F3'
      }
    },

    navigation: {
      buttonOptions: {
        symbolStroke: '#DDDDDD',
        theme: {
          fill: '#505053'
        }
      }
    },

    rangeSelector: {
      buttonTheme: {
        fill: '#505053',
        stroke: '#000000',
        style: {
          color: '#CCC'
        },
        states: {
          hover: {
            fill: '#707073',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          },
          select: {
            fill: '#000003',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          }
        }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
        backgroundColor: '#333',
        color: 'silver'
      },
      labelStyle: {
        color: 'silver'
      }
    },

    navigator: {
      handles: {
        backgroundColor: '#666',
        borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
        color: '#7798BF',
        lineColor: '#A6C7ED'
      },
      xAxis: {
        gridLineColor: '#505053'
      }
    },

    scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
    },

    legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
    background2: '#505053',
    dataLabelsColor: '#B0B0B3',
    textColor: '#C0C0C0',
    contrastTextColor: '#F0F0F3',
    maskColor: 'rgba(255,255,255,0.3)'
  };

  Highcharts.setOptions(Highcharts.theme);
  Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
      return {
        radialGradient: {
          cx: 0.5,
          cy: 0.3,
          r: 0.7
        },
        stops: [
        [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
              };
            })
  });

var region_dl_4g = <?php echo json_encode($region_dl_4g); ?>;
var tsel_dl_4g = <?php echo json_encode($tsel_dl_4g); ?>;
var three_dl_4g = <?php echo json_encode($three_dl_4g); ?>;
var isat_dl_4g = <?php echo json_encode($isat_dl_4g); ?>;
var xl_dl_4g = <?php echo json_encode($xl_dl_4g); ?>;
var sf_dl_4g = <?php echo json_encode($sf_dl_4g); ?>;

  Highcharts.chart('bar1', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      }
    },

    xAxis: {
      categories: region_dl_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_dl_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_dl_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_dl_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_dl_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_dl_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

  Highcharts.chart('bar11', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_dl_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_dl_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_dl_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_dl_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_dl_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_dl_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

 Highcharts.chart('chartline1', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      }
    },

    xAxis: {
      categories: region_dl_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_dl_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_dl_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_dl_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_dl_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_dl_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

  Highcharts.chart('chartline11', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_dl_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_dl_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_dl_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_dl_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_dl_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_dl_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

var region_ul_4g = <?php echo json_encode($region_ul_4g); ?>;
var tsel_ul_4g = <?php echo json_encode($tsel_ul_4g); ?>;
var three_ul_4g = <?php echo json_encode($three_ul_4g); ?>;
var isat_ul_4g = <?php echo json_encode($isat_ul_4g); ?>;
var xl_ul_4g = <?php echo json_encode($xl_ul_4g); ?>;
var sf_ul_4g = <?php echo json_encode($sf_ul_4g); ?>;

  Highcharts.chart('bar2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      },
    },

    xAxis: {
      categories: region_ul_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_ul_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_ul_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_ul_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_ul_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_ul_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
  Highcharts.chart('bar22', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_ul_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_ul_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_ul_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_ul_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_ul_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_ul_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

 Highcharts.chart('chartline2', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      },
    },

    xAxis: {
      categories: region_ul_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_ul_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_ul_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_ul_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_ul_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_ul_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
  Highcharts.chart('chartline22', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_ul_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_ul_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_ul_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_ul_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_ul_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_ul_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

var region_lc_4g = <?php echo json_encode($region_lc_4g); ?>;
var tsel_lc_4g = <?php echo json_encode($tsel_lc_4g); ?>;
var three_lc_4g = <?php echo json_encode($three_lc_4g); ?>;
var isat_lc_4g = <?php echo json_encode($isat_lc_4g); ?>;
var xl_lc_4g = <?php echo json_encode($xl_lc_4g); ?>;
var sf_lc_4g = <?php echo json_encode($sf_lc_4g); ?>;

  Highcharts.chart('bar3', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      },
    },

    xAxis: {
      categories: region_lc_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_lc_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_lc_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_lc_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_lc_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_lc_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
  Highcharts.chart('bar33', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_lc_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_lc_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_lc_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_lc_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_lc_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_lc_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

  Highcharts.chart('chartline3', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      },
    },

    xAxis: {
      categories: region_lc_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_lc_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_lc_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_lc_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_lc_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_lc_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
  Highcharts.chart('chartline33', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_lc_4g
    },

    series: [{
      name: 'TSEL',
      data: tsel_lc_4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_lc_4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_lc_4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_lc_4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_lc_4g,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

  /////CHART 3G////////////////////////////////////////////////////////////////////////////////////////////////////
var region_dl_3G = <?php echo json_encode($region_dl_3G); ?>;
var tsel_dl_3G = <?php echo json_encode($tsel_dl_3G); ?>;
var three_dl_3G = <?php echo json_encode($three_dl_3G); ?>;
var isat_dl_3G = <?php echo json_encode($isat_dl_3G); ?>;
var xl_dl_3G = <?php echo json_encode($xl_dl_3G); ?>;
var sf_dl_3G = <?php echo json_encode($sf_dl_3G); ?>;

  Highcharts.chart('bar4', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      }
    },

    xAxis: {
      categories: region_dl_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_dl_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_dl_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_dl_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_dl_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_dl_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

  Highcharts.chart('bar44', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_dl_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_dl_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_dl_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_dl_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_dl_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_dl_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

Highcharts.chart('chartline4', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      }
    },

    xAxis: {
      categories: region_dl_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_dl_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_dl_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_dl_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_dl_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_dl_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

  Highcharts.chart('chartline44', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_dl_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_dl_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_dl_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_dl_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_dl_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_dl_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

var region_ul_3G = <?php echo json_encode($region_ul_3G); ?>;
var tsel_ul_3G = <?php echo json_encode($tsel_ul_3G); ?>;
var three_ul_3G = <?php echo json_encode($three_ul_3G); ?>;
var isat_ul_3G = <?php echo json_encode($isat_ul_3G); ?>;
var xl_ul_3G = <?php echo json_encode($xl_ul_3G); ?>;
var sf_ul_3G = <?php echo json_encode($sf_ul_3G); ?>;

  Highcharts.chart('bar5', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      },
    },

    xAxis: {
      categories: region_ul_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_ul_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_ul_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_ul_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_ul_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_ul_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
  Highcharts.chart('bar55', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_ul_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_ul_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_ul_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_ul_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_ul_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_ul_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
Highcharts.chart('chartline5', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      },
    },

    xAxis: {
      categories: region_ul_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_ul_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_ul_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_ul_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_ul_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_ul_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
  Highcharts.chart('chartline55', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_ul_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_ul_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_ul_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_ul_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_ul_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_ul_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

var region_lc_3G = <?php echo json_encode($region_lc_3G); ?>;
var tsel_lc_3G = <?php echo json_encode($tsel_lc_3G); ?>;
var three_lc_3G = <?php echo json_encode($three_lc_3G); ?>;
var isat_lc_3G = <?php echo json_encode($isat_lc_3G); ?>;
var xl_lc_3G = <?php echo json_encode($xl_lc_3G); ?>;
var sf_lc_3G = <?php echo json_encode($sf_lc_3G); ?>;

  Highcharts.chart('bar6', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      },
    },

    xAxis: {
      categories: region_lc_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_lc_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_lc_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_lc_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_lc_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_lc_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
  Highcharts.chart('bar66', {
    chart: {
      type: 'column'
    },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_lc_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_lc_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_lc_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_lc_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_lc_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_lc_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

Highcharts.chart('chartline6', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      padding: 0,
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '10px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '10px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '10px'
      },
    },

    xAxis: {
      categories: region_lc_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_lc_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_lc_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_lc_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_lc_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_lc_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
  Highcharts.chart('chartline66', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ' '
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },

    yAxis: {
      title: {
        text: 'Kpbs'
      }
    },
    legend: { 
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    xAxis: {
      categories: region_lc_3G
    },

    series: [{
      name: 'TSEL',
      data: tsel_lc_3G,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: isat_lc_3G,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: sf_lc_3G,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xl_lc_3G,
      color: '#0000FF'

    }, {
      name: '3',
      data: three_lc_3G,
      color: '#808080'

    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

  </script>
</body>
</html>
