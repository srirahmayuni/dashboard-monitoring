<!DOCTYPE html>
<html>
<head>
  <!-- BASICS -->
  <meta charset="utf-8">
  <title>DASHBOARD OOKLA</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{url('')}}/template/tsel.jpg" rel="icon" type="image/png">
  <!-- <link rel="stylesheet" type="text/css" href="{{url('')}}/js/rs-plugin/css/settings.css" media="screen"> -->
  <link rel="stylesheet" type="text/css" href="{{url('')}}/css/isotope.css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/flexslider.css" type="text/css">
  <link rel="stylesheet" href="{{url('')}}/js/fancybox/jquery.fancybox.css" type="text/css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/bootstrap.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="{{url('')}}/css/style.css">
  <link rel="stylesheet" href="{{url('')}}/skin/default.css">
  <script src="https://unpkg.com/ag-grid-enterprise@20.2.0/dist/ag-grid-enterprise.min.js"></script>
  <script src = "https://code.highcharts.com/highcharts.js"> </script>
  <script src="{{url('')}}/js/jquery.js"></script>
  <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
   <style type="text/css">
    #myScrollTable tbody {
      clear: both;
      border: 1px solid #000000;
      height: 475px;
      overflow:auto;
      float:left;
    }
  </style>
  <style type="text/css">
    * {
      box-sizing: border-box;
    }

    body {
      font-family: Arial, Helvetica, sans-serif;
    }
    .column {
      float: left;
      width: 33%;
      padding: 0 10px;
    }

    .row {margin: 0 -5px;}

    .row:after {
      content: "";
      display: table;
      clear: both;
    }

    .card {
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); 
      padding: 30%;
      text-align: center;
      background-color: #f1f1f1;
    }

    @media screen and (max-width: 600px) {
      .column {
        width: 100%;
        display: block;
        margin-bottom: 20px;
      }
    }
  </style>


  <style>
    .dropClass {
      visibility: hidden;
      opacity: 0;
      transition:
      all .5s ease;
      background-color: transparent;
      min-width: 160px;
      overflow: auto;
      z-index: 1;
      height: 0;
    }
    .dropClass a {
      color: black;
      padding: 12px 16px;
      display: block;
    }
    .dropClass a:hover {background-color: rgba(255,255,255,.1);}
    .show {
      visibility: visible;
      opacity: 1;
      height: 200%;
      /*padding: 0.5rem 1rem;*/
      background-color: rgba(255,255,255,.3);
      /*margin-bottom: 1.5%;*/
      border-radius: 3%;
    }
    .putar {
      transform: rotate(90deg);
      transition: all .5s ease;
    }
    .brand-image {
      line-height: .8;
      max-height: 53px;
      width: auto;
      margin-left: 0.7rem;
      margin-right: .5rem;
      margin-top: -3px;
      float: none;
      opacity: .9;
    }
    .backgroundImg {
      width: auto;
      height: 100%;
      opacity: 1;
      position: absolute;
    }
    .backgroundImg2 {
      position: fixed;
      width: 100%;
      max-height: 56px;
      margin-left: -2%;
      opacity: 1;
    }
    .nav-item:hover {
      background-color: rgba(255,255,255,.3);
      border-radius: 5%;
      transition: all .2s ease;
    }
    .berisik {
      min-height:500px !important
    }
    .tesDiv {
      z-index: -1;
      opacity: .4;
      background: url(./dist/img/tesblek.png) center center
    }
    .tesDiv .bekgron{
      z-index: 1;
      opacity: 1
    }
    #bungkus {
      background: url(./dist/img/darkwall6.jpg) center center;
    }
    .garisijo {
      background-color: #800000;
      height: 3px;
      width: 100%;
      position: center;
      bottom: 0;
      left: 0;
    }
    .teksboks {
      width: 85%;
      position: absolute;
      bottom: 0;
      left: 0;
    }
    .teksne{
      margin:auto;
      position: absolute;
    }
    .boksHead {
      font-size: 30px;
      padding: 10px;
      font-weight: 500;
      border-radius: 0;
      background-color: rgba(255, 255, 255, .2);
      color: #96b28a;
      font-weight: bold;
      box-shadow: none;
      margin-bottom:0 !important;
    }
    .boksBody {
      height: 107px;
      border-radius: 0;
      background-color: rgba(255, 255, 255, .2);
      box-shadow: none;
    }
    .inner {
      padding:0 !important;
    }
    .card.chartcard {
      background-color:transparent;
      border: 0;
      border-radius: 0;
      box-shadow: none;
    }
    table {
      font-size: 14px;
      background-color: rgba(1, 14, 23,.9)!important;
      color:lightgrey;
    }
    tr > td:hover {
      background-color: rgba(1, 14, 23,.9)!important;
      color:lightgrey;
    }

    .table td, .table th {
      border: 1px solid #ddd;
    }

    thead {
      background-color: rgba(1, 14, 23,.9)!important;
      color:lightgrey;
    }
    .kepanjangan {
      font-size: 10px;
    }
    .kepanjangantot {
      font-size: 12px;
    }
    .page-link {
      color: #fff;
      background-color: #262c2e;
    }

    .social-part .fa{
      padding-right:20px;
    }
    ul li a{
      margin-right: 20px;
    }

    .bg-light {
      color:#fff;
      background-image:linear-gradient(to right, #1c1f20, #2d3436, #2d3436, #2d3436);
    } 

    .bg-light, .bg-light a {
      color: #fff!important;
    }

    .dropdown-menu {
      background-color: #262c2e;
      color: #0F2027;
    }

    .active1 {
      background-color: #6c7173; !important;
      color: #343a40 !important;
      font-weight: 600;
    }

    .item:hover {
      background-color: #0F2027;
    }

    .invisible {
      visibility: hidden;
    }

    .dropdown-item:focus, .dropdown-item:hover {
      background-color: #6c7173;
    }

    .card {
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
      transition: 0.3s; 
      border-radius: 5px; /* 5px rounded corners */
    }

    .card:hover {
      box-shadow: 0 8px 50px 0 rgba(0,0,0,0.2);
    } 

    .ag-header-cell-label {
      float: none !important;
      width: auto !important;
      justify-content: left;
    }

    .ag-fresh .ag-header-cell-menu-button{
      position: absolute;
      float: none;
    }

    .container {
      max-width: 100%; !important;
    }

    #my{
      zoom: 100%;
    }

  </style>
  <style>
    .previous {
      background-color: #f1f1f1;
      color: black;
    }

    .next {
      background-color: #800000;
      color: white;
    }

    .round {
      border-radius: 50%;
    }
  </style>
  <style type="text/css">
    .select-css {
      font-size: 14px;
      font-family: sans-serif;
      font-weight: 600;
      color: #444;
      line-height: 1;
      padding: .5em 1.4em .4em .8em;
      width: 140px;
      height: 30px;
      max-width: 100%; 
      box-sizing: border-box;
      margin: 0;
      border: 1px solid #aaa;
      box-shadow: 0 1px 0 1px rgba(0,0,0,.04);
      border-radius: .5em;
      -moz-appearance: none;
      -webkit-appearance: none;
      appearance: none;
      background-color: #fff;
      background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E'),
      linear-gradient(to bottom, #ffffff 0%,#e5e5e5 100%);
      background-repeat: no-repeat, repeat;
      background-position: right .7em top 50%, 0 0;
      background-size: .65em auto, 100%;
      margin-top: 5px;
    }
    .select-css::-ms-expand {
      display: none;
    }
    .select-css:hover {
      border-color: #888;
    }
    .select-css:focus {
      border-color: #aaa;
      box-shadow: 0 0 1px 3px rgba(59, 153, 252, .7);
      box-shadow: 0 0 0 3px -moz-mac-focusring;
      color: #222; 
      outline: none;
    }
    .select-css option {
      font-weight:normal;
    }
  </style>
<script>
    Highcharts.theme = {
      colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
      '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
      chart: {
        backgroundColor: {
          linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
          stops: [
          [0, '#2a2a2b'],
          [1, '#3e3e40']
          ]
        },
        style: {
          fontFamily: '\'Unica One\', sans-serif'
        },
        plotBorderColor: '#606063',
        marginTop: 15
      },
      title: {
        style: {
          color: '#E0E0E3',
          textTransform: 'uppercase',
          fontSize: '20px'
        }
      },
      subtitle: {
        style: {
          color: '#E0E0E3',
          textTransform: 'uppercase'
        }
      },
      xAxis: {
        gridLineColor: '#707073',
        labels: {
          style: {
            color: '#E0E0E3'
          }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        title: {
          style: {
            color: '#A0A0A3'

          }
        }
      },
      yAxis: {
        gridLineColor: '#707073',
        labels: {
          style: {
            color: '#E0E0E3'
          }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        tickWidth: 1,
        title: {
          style: {
            color: '#A0A0A3'
          }
        }
      },
      tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.85)',
        style: {
          color: '#F0F0F0'
        }
      },
      plotOptions: {
        series: {
          dataLabels: {
            color: '#B0B0B3'
          },
          marker: {
            lineColor: '#333'
          }
        },
        boxplot: {
          fillColor: '#505053'
        },
        candlestick: {
          lineColor: 'white'
        },
        errorbar: {
          color: 'white'
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '9px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '9px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '9px'
        },
      },
      credits: {
        enabled: false
      },
      labels: {
        style: {
          color: '#707073'
        }
      },

      drilldown: {
        activeAxisLabelStyle: {
          color: '#F0F0F3'
        },
        activeDataLabelStyle: {
          color: '#F0F0F3'
        }
      },

      navigation: {
        buttonOptions: {
          symbolStroke: '#DDDDDD',
          theme: {
            fill: '#505053'
          }
        }
      },

      rangeSelector: {
        buttonTheme: {
          fill: '#505053',
          stroke: '#000000',
          style: {
            color: '#CCC'
          },
          states: {
            hover: {
              fill: '#707073',
              stroke: '#000000',
              style: {
                color: 'white'
              }
            },
            select: {
              fill: '#000003',
              stroke: '#000000',
              style: {
                color: 'white'
              }
            }
          }
        },
        inputBoxBorderColor: '#505053',
        inputStyle: {
          backgroundColor: '#333',
          color: 'silver'
        },
        labelStyle: {
          color: 'silver'
        }
      },

      navigator: {
        handles: {
          backgroundColor: '#666',
          borderColor: '#AAA'
        },
        outlineColor: '#CCC',
        maskFill: 'rgba(255,255,255,0.1)',
        series: {
          color: '#7798BF',
          lineColor: '#A6C7ED'
        },
        xAxis: {
          gridLineColor: '#505053'
        }
      },

      scrollbar: {
        barBackgroundColor: '#808083',
        barBorderColor: '#808083',
        buttonArrowColor: '#CCC',
        buttonBackgroundColor: '#606063',
        buttonBorderColor: '#606063',
        rifleColor: '#FFF',
        trackBackgroundColor: '#404043',
        trackBorderColor: '#404043'
      },

      legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
      background2: '#505053',
      dataLabelsColor: '#B0B0B3',
      textColor: '#C0C0C0',
      contrastTextColor: '#F0F0F3',
      maskColor: 'rgba(255,255,255,0.3)'
    };
    $(document).ready(function(){
      $("#week").change(function() {
        var week = $("#week").val();
        $.ajax({
          url:"http://10.54.36.49/api-ookla/public/super4g/chart_download4g/"+week,
          type:"get",
          success: function(result){
            $('#chart1').empty();
            var res = JSON.parse(result); 
            var telkomseldownload4g = []
            var threedownload4g = []
            var indosatdownload4g = []
            var smartfrendownload4g = []
            var xldownload4g = []
            var week_download4g = []

            res.forEach(myFunction);
            function myFunction(value) {
              telkomseldownload4g.push(value['TELKOMSEL'])
            }
            res.forEach(myFunction2);
            function myFunction2(value2) {
              threedownload4g.push(value2['THREE'])
            }
            res.forEach(myFunction3);
            function myFunction3(value3) {
              indosatdownload4g.push(value3['INDOSAT'])
            }
            res.forEach(myFunction4);
            function myFunction4(value4) {
              smartfrendownload4g.push(value4['SMARTFREN'])
            }
            res.forEach(myFunction5);
            function myFunction5(value5) {
              xldownload4g.push(value5['XL'])
            }
            res.forEach(myFunction6);
            function myFunction6(value6) {
              week_download4g.push(value6['WEEK'])
            }
            console.log('chart1',week_download4g)

            Highcharts.chart('chart11', {
              // chart: {
              //   type: 'column'
              // },
              title: {
                text: ''
              },
              xAxis: {
                categories: week_download4g
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              navigation: {
                buttonOptions: {
                  verticalAlign: 'bottom',
                  align:'right',
                  y: -5
                }
              },
              legend: {
                itemStyle: {
                 fontSize:'12px'
               }
             },
             series: [{
              name: 'TSEL',
              data: telkomseldownload4g,
              color: '#FF0000'

            }, {
              name: 'ISAT',
              data: indosatdownload4g,
              color: '#FFD700'

            }, {
              name: 'SF',
              data: smartfrendownload4g,
              color: '#FF7F00'

            }, {
              name: 'XL',
              data: xldownload4g,
              color: '#0000FF'

            }, {
              name: '3',
              data: threedownload4g,
              color: '#FF0000'
            }]
          });

            Highcharts.chart('chart1', {
              // chart: {
              //   type: 'column'
              // },
              title: {
                text: ''
              },
              xAxis: {
                categories: week_download4g
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              exporting: {
                enabled: false
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              legend: {
                itemStyle: {
                 fontSize:'12px'
               }

             },
             series: [{
              name: 'TSEL',
              data: telkomseldownload4g,
              color: '#FF0000'

            }, {
              name: 'ISAT',
              data: indosatdownload4g,
              color: '#FFD700'

            }, {
              name: 'SF',
              data: smartfrendownload4g,
              color: '#FF7F00'

            }, {
              name: 'XL',
              data: xldownload4g,
              color: '#0000FF'

            }, {
              name: '3',
              data: threedownload4g,
              color: '#FF0000'
            }]
          });

          }
        });

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/super4g/chart_download3g/"+week,
  type:"get",
  success: function(result){
    $('#chart2').empty();
    var res = JSON.parse(result); 
    var telkomseldownload3g = []
    var threedownload3g = []
    var indosatdownload3g = []
    var smartfrendownload3g = []
    var xldownload3g = []
    var week_download3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomseldownload3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threedownload3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatdownload3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrendownload3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xldownload3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_download3g.push(value6['WEEK'])
    }
    console.log('chart2',week_download3g)

    Highcharts.chart('chart2', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_download3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: { 
         fontSize:'12px'
       }

     },
     series: [{
      name: 'TSEL',
      data: telkomseldownload3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatdownload3g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: smartfrendownload3g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xldownload3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threedownload3g,
      color: '#808080'

    }]
  });

    Highcharts.chart('chart22', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_download3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     series: [{
      name: 'TSEL',
      data: telkomseldownload3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatdownload3g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: smartfrendownload3g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xldownload3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threedownload3g,
      color: '#808080'

    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/super4g/chart_upload4g/"+week,
  type:"get",
  success: function(result){
    $('#chart3').empty();
    var res = JSON.parse(result); 
    var telkomselupload4g = []
    var threeupload4g = []
    var indosatupload4g = []
    var smartfrenupload4g = []
    var xlupload4g = []
    var week_upload4g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomselupload4g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threeupload4g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatupload4g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenupload4g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xlupload4g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_upload4g.push(value6['WEEK'])
    }
    console.log('chart3',week_upload4g)

    Highcharts.chart('chart3', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: smartfrenupload4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xlupload4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload4g,
      color: '#808080'

    }]
  });

    Highcharts.chart('chart33', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: smartfrenupload4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xlupload4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload4g,
      color: '#808080'

    }]
  });


  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/super4g/chart_upload3g/"+week,
  type:"get",
  success: function(result){
    $('#chart4').empty();
    var res = JSON.parse(result); 
    var telkomselupload3g = []
    var threeupload3g = []
    var indosatupload3g = []
    var smartfrenupload3g = []
    var xlupload3g = []
    var week_upload3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomselupload3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threeupload3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatupload3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenupload3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xlupload3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_upload3g.push(value6['WEEK'])
    }
    console.log('chart4',week_upload3g)

    Highcharts.chart('chart4', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     exporting: {
      enabled: false
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload3g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: smartfrenupload3g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xlupload3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload3g,
      color: '#808080'

    }]
  });

    Highcharts.chart('chart44', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload3g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: smartfrenupload3g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xlupload3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload3g,
      color: '#808080'

    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/super4g/chart_latency4g/"+week,
  type:"get",
  success: function(result){
    $('#chart5').empty();
    var res = JSON.parse(result); 
    var telkomsellatency4g = []
    var threelatency4g = []
    var indosatlatency4g = []
    var smartfrenlatency4g = []
    var xllatency4g = []
    var week_latency4g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomsellatency4g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threelatency4g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatlatency4g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenlatency4g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xllatency4g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_latency4g.push(value6['WEEK'])
    }
    console.log('chart5',week_latency4g)

    Highcharts.chart('chart5', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.0f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: 'TSEL',
      data: telkomsellatency4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: smartfrenlatency4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xllatency4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency4g,
      color: '#808080'

    }]
  });

    Highcharts.chart('chart55', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.0f} Ms</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     series: [{
      name: 'TSEL',
      data: telkomsellatency4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency4g,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: smartfrenlatency4g,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xllatency4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency4g,
      color: '#808080'

    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/super4g/chart_downloadwifi/"+week,
  type:"get",
  success: function(result){
    $('#chart6').empty();
    var res = JSON.parse(result); 
    var telkomselwifi = []
    var threewifi= []
    var indosatwifi = []
    var smartfrenwifi = []
    var xlwifi = []
    var week_wifi = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomselwifi.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threewifi.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatwifi.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenwifi.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xlwifi.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_wifi.push(value6['WEEK'])
    }
    console.log('chart6',week_wifi)

    Highcharts.chart('chart6', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_wifi
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselwifi,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatwifi,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: smartfrenwifi,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xlwifi,
      color: '#0000FF'

    }, {
      name: '3',
      data: threewifi,
      color: '#808080'

    }]
  });

    Highcharts.chart('chart66', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_wifi
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselwifi,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatwifi,
      color: '#FFD700'

    }, {
      name: 'SF',
      data: smartfrenwifi,
      color: '#FF7F00'

    }, {
      name: 'XL',
      data: xlwifi,
      color: '#0000FF'

    }, {
      name: '3',
      data: threewifi,
      color: '#808080'

    }]
  });

  }
});


});
});
</script>
</head>
<body>
  <section id="header" class="appear">
    <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:40px; height:40px; background-color:rgba(0,0,0,0.3);" data-300="line-height:40px; height:40px; background-color:rgba(5, 42, 62, 1);">
      <!-- <div class="container"> -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="fa fa-bars color-white"></span>
          </button>
          <div class="navbar-logo">
            <label for="week" style="color: white; font-size: 16px; padding-left: 10px; padding-top: 4px">Weekly: &nbsp;</label><input type="week" name="week" id="week" style="height: 30px;">
            <img data-0="width:170px;" data-300=" width:100px;" src="img/oklaa.png" alt="" style="padding-left: 50px;">
          </div>
        </div>
        <div class="navbar-collapse collapse" style="padding-right: 150px;">
          <ul class="nav navbar-nav" data-0="margin-top:0px;" data-300="margin-top:0px;">
            <li><a href="{{url('/')}}">Home</a>
              <div class="dropdown-menu sm-menu">
                <a class="dropdown-item " href="{{url('baseline4g')}}">OnAir Reporting 4G</a>
              </div>
            </li>
            <li class="active"><a href="{{url('super_4g')}}">SUPER 4G</a></li>
            <li><a href="{{url('wcc_62')}}">WCC 62</a></li>
            <li><a href="#">CSAT 48</a></li>
          </ul>
        </div>
      <!-- </div> -->
    </div>
  </section>
  <section id="intro">
    <div class="intro-content">
      <div class="row" style="padding-top: 50px; padding-left: 0px; padding-right: 0px;">
        <div class="col-lg-6" style="padding-right: 0px; padding-top: 20px;">
          <div class="container" style=" padding-top: 0px; padding-left: 0px; padding-right: 0px;">
            <ul class="nav nav-tabs" role="tablist" style="padding-top: 0px; background-color: #800000; font-family: calibri; font-weight: bold;">
              <li class="nav-link active"><a data-toggle="tab" role="tab" id="tombol" href="#nav-4G">Operator Benchmark</a></li>
              <li class="nav-item"><a class="nav-link" data-toggle="tab" role="tab" href="#nav-3G">List Line</a></li>
            </ul>
            <div class="tab-content">
              <div id="nav-4G" role="tabpanel" class="tab-pane fade in active">
            <div class="small-box boks boksBody" style="height: 475px;">
            <div class="inner">
              <div class="row"> 
                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                  <div class="card-header12" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i>DOWNLOAD 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal12"></i>
                  </div>
                  <div class="modal fade" id="modal12" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537;"> 
                        <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 4G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="charts11" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body12" style="width:100%; height:130px; background-color: #343a40">
                    <div id="charts1" style="width:100%; height:130px;"></div>
                  </div> 
                </div> 

               <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                  <div class="card-header13" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> WIFI
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal13"></i>
                  </div>
                  <div class="modal fade" id="modal13" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> WIFI
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="charts66" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body13" style="width:100%; height:130px; background-color: #343a40">
                    <div id="charts6" style="width:100%; height:130px;"></div>
                  </div>          
                </div>  
              </div>

              <div class="row"> 
                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                  <div class="card-header14" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal14"></i>
                  </div>
                  <div class="modal fade" id="modal14" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> UPLOAD 4G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="charts33" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body14" style="width:100%; height:130px; background-color: #343a40">
                    <div id="charts3" style="width:100%; height:130px;"></div>
                  </div> 
                </div> 

                  <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                    <div class="card-header15" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 3G
                      <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal15"></i>
                    </div>
                    <!-- </div>                      -->
                    <div class="modal fade" id="modal15" style="width: 100%;">
                      <div class="modal-dialog modal-lg" style="margin-top:90px;">
                        <div class="modal-content" style="background-color:#353537"> 
                          <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 3G
                            <button type="button" class="close" data-dismiss="modal">

                              <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                          </div>
                          <div class="modal-body" style="color:#000"> 
                            <div id="charts22" style="width:100%; height:100%;"></div>
                          </div>
                        </div> 
                      </div>
                    </div>
                    <div class="card-body15" style="width:100%; height:130px; background-color: #343a40">
                      <div id="charts2" style="width:100%; height:130px;"></div>
                    </div>          
                  </div>  
                </div>

                <div class="row"> 
                  <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                    <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                      <div class="card-header16" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> LATENCY 4G
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal16"></i>
                      </div>
                      <!-- </div>                      -->
                      <div class="modal fade" id="modal16" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537"> 
                            <div class="modal-header" style="text-align:center; color: white;"> LATENCY 4G
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="charts55" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body16" style="width:100%; height:130px; background-color: #343a40">
                        <div id="charts5" style="width:100%; height:130px;"></div>
                      </div> 
                    </div> 

                    <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                      <div class="card-header17" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 2G
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal17"></i>
                      </div>
                      <div class="modal fade" id="modal17" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537"> 
                            <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 2G
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="charts44" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body17" style="width:100%; height:130px; background-color: #343a40">
                        <div id="charts4" style="width:100%; height:130px;"></div>
                      </div>          
                    </div>  
                  </div>
                </div>
              </div>
              </div>

            <div id="nav-3G" role="tabpanel" class="tab-pane fade">
                          <div class="small-box boks boksBody" style="height: 475px;">
            <div class="inner">
              <div class="row"> 
                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                  <div class="card-header1" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal1"></i>
                  </div>
                  <!-- </div>                      -->
                  <div class="modal fade" id="modal1" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537;"> 
                        <div class="modal-header" style="text-align:left; color: white;"> DOWNLOAD 4G
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chart11" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body1" style="width:100%; height:130px; background-color: #343a40">
                    <div id="chart1" style="width:100%; height:130px;"></div>
                  </div> 
                </div> 

                <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                  <div class="card-header2" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> WIFI
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal2"></i>
                  </div>
                  <!-- </div>                      -->
                  <div class="modal fade" id="modal2" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> WIFI
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chart66" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body2" style="width:100%; height:130px; background-color: #343a40">
                    <div id="chart6" style="width:100%; height:130px;"></div>
                  </div>          
                </div>  
              </div>

              <div class="row"> 
                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                  <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                    <div class="card-header3" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD 4G
                      <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal3"></i>
                    </div>
                    <!-- </div>                      -->
                    <div class="modal fade" id="modal3" style="width: 100%;">
                      <div class="modal-dialog modal-lg" style="margin-top:90px;">
                        <div class="modal-content" style="background-color:#353537"> 
                          <div class="modal-header" style="text-align:center; color: white;"> UPLOAD 4G
                            <button type="button" class="close" data-dismiss="modal">

                              <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                          </div>
                          <div class="modal-body" style="color:#000"> 
                            <div id="chart33" style="width:100%; height:100%;"></div>
                          </div>
                        </div> 
                      </div>
                    </div>
                    <div class="card-body3" style="width:100%; height:130px; background-color: #343a40">
                      <div id="chart3" style="width:100%; height:130px;"></div>
                    </div> 
                  </div> 

                  <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                    <div class="card-header4" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 3G
                      <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal4"></i>
                    </div>
                    <!-- </div>                      -->
                    <div class="modal fade" id="modal4" style="width: 100%;">
                      <div class="modal-dialog modal-lg" style="margin-top:90px;">
                        <div class="modal-content" style="background-color:#353537"> 
                          <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 3G
                            <button type="button" class="close" data-dismiss="modal">

                              <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                          </div>
                          <div class="modal-body" style="color:#000"> 
                            <div id="chart22" style="width:100%; height:100%;"></div>
                          </div>
                        </div> 
                      </div>
                    </div>
                    <div class="card-body4" style="width:100%; height:130px; background-color: #343a40">
                      <div id="chart2" style="width:100%; height:130px;"></div>
                    </div>          
                  </div>  
                </div>

                <div class="row"> 
                  <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                    <!-- <div class="card" style="background-color: rgba(0, 0, 0, 0.2);"> -->
                      <div class="card-header5" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> LATENCY 4G
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal5"></i>
                      </div>
                      <!-- </div>                      -->
                      <div class="modal fade" id="modal5" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537"> 
                            <div class="modal-header" style="text-align:center; color: white;"> LATENCY 4G
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="chart55" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body5" style="width:100%; height:130px; background-color: #343a40">
                        <div id="chart5" style="width:100%; height:130px;"></div>
                      </div> 
                    </div> 

                    <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                      <div class="card-header6" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 2G
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal6"></i>
                      </div>
                      <div class="modal fade" id="modal6" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537"> 
                            <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 2G
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="chart44" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body6" style="width:100%; height:130px; background-color: #343a40">
                        <div id="chart4" style="width:100%; height:130px;"></div>
                      </div>          
                    </div>  
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
            <div class="col-md-6" style="padding-top: 20px; padding-bottom: 0px;">
              <div class="panel-heading" style="background-color: #800000;">
                <h4 style="font-family: calibri; color: white;"><strong> CITY</strong></h4>
              </div>
              <div class="small-box boks boksBody" style="height: 475px;">
                <div class="inner">
                  <div class="col-xs-12" style="margin-top: 0px; padding-right: 0px; padding-left: 0px;">
                    <div id="tableregion" style="width: 100%; height: 475px;" class="ag-theme-balham-dark"></div>
                    <script type="text/javascript" charset="utf-8">

                      var columnDefs = [ 
                      {
                        headerName: 'CITY', field: 'city', width: 173,filter: 'agTextColumnFilter',cellStyle: {'border-right': '1px solid #696969'}
                      },
                      {
                        headerName: 'DOWNLOAD (mbps)',
                        headerClass: 'participant-group',
                        children: [
                        {headerName: 'MAX', field: 'maxdownload', width: 70},
                        {headerName: 'MIN', field: 'mindownload', width: 70},
                        {headerName: 'AVG', field: 'avgdownload', width: 70,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                        ]
                      },
                      {
                        headerName: 'UPLOAD (mbps)',
                        headerClass: 'participant-group',
                        children: [
                        {headerName: 'MAX', field: 'maxupload', width: 70},
                        {headerName: 'MIN', field: 'minupload', width: 70},
                        {headerName: 'AVG', field: 'avgupload', width: 70,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                        ]
                      },
                      {
                        headerName: 'LATENCY (ms)',
                        headerClass: 'participant-group',
                        children: [
                        {headerName: 'MAX', field: 'maxlatency', width: 70},
                        {headerName: 'MIN', field: 'minlatency', width: 70},
                        {headerName: 'AVG', field: 'avglatency', width: 70,cellStyle: function(params){return {textAlign: 'left','border-right': '1px solid #696969'};}}
                        ]
                      }
                      ]; 

                      var gridOptions = {
                        defaultColDef: {
                          resizable: true,
                        },
                        columnDefs: columnDefs,
                        rowData: null
                      };

                      document.addEventListener('DOMContentLoaded', function() {
                        var gridDiv = document.querySelector('#tableregion');
                        new agGrid.Grid(gridDiv, gridOptions);
                        var httpRequest = new XMLHttpRequest(); 
                        var api = encodeURI('http://10.54.36.49/api-ookla/public/super4g/table_citieslose');
                        httpRequest.open('GET', api); 
                        httpRequest.send(); 
                        httpRequest.onreadystatechange = function() {
                          if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                            var httpResponse = JSON.parse(httpRequest.responseText);
                            gridOptions.api.setRowData(httpResponse.data);
                          }
                        };
                      }); 
                    </script> 
                    <style>
                      .ag-header-group-cell-label {
                        justify-content: center;
                      }
                      .participant-group {
                        border: #00E7B1;
                      }
                    </style>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="row">
            <div class="col-lg-3" style="padding-right: 0px; padding-top: 5px;padding-bottom: 0px;">
              <div class="panel-heading" style="background-color: #800000;">
                <h4 style="font-family: calibri; color: white;"><strong>InBound Roamers</strong></h4>
              </div>
              <div class="small-box boks boksBody" style="height: 160px;">
                <div class="inner">
                  <div class="row">
                    <div class="col-md-12" style="margin-top:5px; padding-right: 15px;">
                      <div class="card-header7" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> 
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal7"></i>
                      </div>
                      <div class="modal fade" id="modal7" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537"> 
                            <div class="modal-header" style="text-align:center; color: white;"> InBound Roamers
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="combichart2" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body7" style="width:100%; height:130px; background-color: #343a40">
                        <div id="combichart" style="width:100%; height:130px;"></div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-12" style="padding-right: 0px; padding-left: 5px; padding-bottom: 0px; padding-top: 5px;">
              <div class="panel-heading" style="background-color: #800000;">
                <h4 style="font-family: calibri; color: white;"><strong>Handset Type Distribution</strong></h4>
              </div>
              <div class="small-box boks boksBody" style="height: 160px;">
                <div class="inner">
                  <div class="row">
                    <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                      <div class="card-header8" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> TELKOMSEL ONLY
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal8"></i>
                      </div>
                      <div class="modal fade" id="modal8" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537"> 
                            <div class="modal-header" style="text-align:center; color: white;"> TELKOMSEL ONLY
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="piechart11" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body8" style="width:100%; height:130px; background-color: #343a40">
                        <div id="piechart1" style="width:100%; height:130px;"></div>
                      </div> 
                    </div>
                    <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">
                      <div class="card-header9" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> ALL OPERATOR
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal9"></i>
                      </div>
                      <div class="modal fade" id="modal9" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537"> 
                            <div class="modal-header" style="text-align:center; color: white;"> ALL OPERATOR
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="piechart22" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body9" style="width:100%; height:130px; background-color: #343a40">
                        <div id="piechart2" style="width:100%; height:130px;"></div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-12" style="padding-right: 15px; padding-bottom: 0px; padding-top: 5px;">
              <div class="panel-heading" style="background-color: #800000;">
                <h4 style="font-family: calibri; color: white;"><strong>Speed Test Distribution</strong></h4>
              </div>
              <div class="small-box boks boksBody" style="height: 160px;">
                <div class="inner">
                  <div class="row">
                    <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                      <div class="card-header10" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD (Base On Video Resolution)
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal10"></i>
                      </div>
                      <div class="modal fade" id="modal10" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537"> 
                            <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD (Base On Video Resolution)
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="line11" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body10" style="width:100%; height:130px; background-color: #343a40">
                        <div id="line1" style="width:100%; height:130px;"></div>
                      </div> 
                    </div>
                    <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">
                      <div class="card-header11" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD (Base On File Sizes)
                        <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal11"></i>
                      </div>
                      <div class="modal fade" id="modal11" style="width: 100%;">
                        <div class="modal-dialog modal-lg" style="margin-top:90px;">
                          <div class="modal-content" style="background-color:#353537"> 
                            <div class="modal-header" style="text-align:center; color: white;"> UPLOAD (Base On File Sizes)
                              <button type="button" class="close" data-dismiss="modal">

                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:#000"> 
                              <div id="line22" style="width:100%; height:100%;"></div>
                            </div>
                          </div> 
                        </div>
                      </div>
                      <div class="card-body11" style="width:100%; height:130px; background-color: #343a40">
                        <div id="line2" style="width:100%; height:130px;"></div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <marquee behavior="scroll" direction="left" scrollamount="5" style="font-size:13px; background: rgba(17, 34, 41, 0.9); color: white; padding-top: 0px;">Last Data&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;InBound Roamers : {{ $lastupdate_chart_inboundroamer }}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Handset Type Distribution : {{ $lastupdate }}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Speed Test Distribution : {{ $lastupdate_chart_speedtestdownload }}</marquee>
          </div>
        </section>
        <a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>
        <script src="{{url('')}}/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="{{url('')}}/js/jquery.js"></script>
        <script src="{{url('')}}/js/jquery.easing.1.3.js"></script>
        <script src="{{url('')}}/js/bootstrap.min.js"></script>
        <script src="{{url('')}}/js/jquery.isotope.min.js"></script>
        <script src="{{url('')}}/js/fancybox/jquery.fancybox.pack.js"></script>
        <script src="{{url('')}}/js/skrollr.min.js"></script>
        <script src="{{url('')}}/js/jquery.scrollTo.min.js"></script>
        <script src="{{url('')}}/js/jquery.localScroll.min.js"></script>
        <script src="{{url('')}}/js/stellar.js"></script>
        <script src="{{url('')}}/js/jquery.appear.js"></script>
        <script src="{{url('')}}/js/jquery.flexslider-min.js"></script>
        <script src="{{url('')}}/contactform/contactform.js"></script>
        <script src="{{url('')}}/js/main.js"></script>
        <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
        <script src="{{url('')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="{{url('')}}/js/index.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/data.js"></script>
        <script src="https://code.highcharts.com/modules/drilldown.js"></script>


        <script>

    Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
    '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
        [0, '#2a2a2b'],
        [1, '#3e3e40']
        ]
      },
      style: {
        fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063',
      marginTop: 15
    },
    title: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase',
        fontSize: '20px'
      }
    },
    subtitle: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase'
      }
    },
    xAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
        style: {
          color: '#A0A0A3'

        }
      }
    },
    yAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
        style: {
          color: '#A0A0A3'
        }
      }
    },
    tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
        color: '#F0F0F0'
      }
    },
    plotOptions: {
      series: {
        dataLabels: {
          color: '#B0B0B3'
        },
        marker: {
          lineColor: '#333'
        }
      },
      boxplot: {
        fillColor: '#505053'
      },
      candlestick: {
        lineColor: 'white'
      },
      errorbar: {
        color: 'white'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '9px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '9px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '9px'
      }
    },
    credits: {
      enabled: false
    },
    labels: {
      style: {
        color: '#707073'
      }
    },

    drilldown: {
      activeAxisLabelStyle: {
        color: '#F0F0F3'
      },
      activeDataLabelStyle: {
        color: '#F0F0F3'
      }
    },

    navigation: {
      buttonOptions: {
        symbolStroke: '#DDDDDD',
        theme: {
          fill: '#505053'
        }
      }
    },

    rangeSelector: {
      buttonTheme: {
        fill: '#505053',
        stroke: '#000000',
        style: {
          color: '#CCC'
        },
        states: {
          hover: {
            fill: '#707073',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          },
          select: {
            fill: '#000003',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          }
        }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
        backgroundColor: '#333',
        color: 'silver'
      },
      labelStyle: {
        color: 'silver'
      }
    },

    navigator: {
      handles: {
        backgroundColor: '#666',
        borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
        color: '#7798BF',
        lineColor: '#A6C7ED'
      },
      xAxis: {
        gridLineColor: '#505053'
      }
    },

    scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
    },

    legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
    background2: '#505053',
    dataLabelsColor: '#B0B0B3',
    textColor: '#C0C0C0',
    contrastTextColor: '#F0F0F3',
    maskColor: 'rgba(255,255,255,0.3)'
  };

  Highcharts.setOptions(Highcharts.theme);
  Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
      return {
        radialGradient: {
          cx: 0.5,
          cy: 0.3,
          r: 0.7
        },
        stops: [
        [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
              };
            })
  });

          var threedownload4g = <?php echo json_encode($threedownload4g); ?>;
          var indosatdownload4g = <?php echo json_encode($indosatdownload4g); ?>;
          var smartfrendownload4g = <?php echo json_encode($smartfrendownload4g); ?>;
          var xldownload4g = <?php echo json_encode($xldownload4g); ?>;
          var telkomseldownload4g = <?php echo json_encode($telkomseldownload4g); ?>;
          var week_download4g = <?php echo json_encode($week_download4g); ?>;

          var threedownload4g_ = <?php echo json_encode($threedownload4g[3]); ?>;
          var indosatdownload4g_ = <?php echo json_encode($indosatdownload4g[3]); ?>;
          var smartfrendownload4g_ = <?php echo json_encode($smartfrendownload4g[3]); ?>;
          var xldownload4g_ = <?php echo json_encode($xldownload4g[3]); ?>;
          var telkomseldownload4g_ = <?php echo json_encode($telkomseldownload4g[3]); ?>;
          var week_download4g_ = <?php echo json_encode($week_download4g[3]); ?>;


          Highcharts.chart('chart11', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_download4g
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },
            plotOptions: {
              column: {
                pointPadding: 0.2,
                borderWidth: 0
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           series: [{
            name: 'TSEL',
            data: telkomseldownload4g,
            color: '#FF0000'

          }, {
            name: 'ISAT',
            data: indosatdownload4g,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrendownload4g,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xldownload4g,
            color: '#0000FF'

          }, {
            name: '3',
            data: threedownload4g,
            color: '#FF0000'
          }]
        });

          Highcharts.chart('chart1', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_download4g
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            exporting: {
              enabled: false
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },
            plotOptions: {
              column: {
                pointPadding: 0.2,
                borderWidth: 0
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }

           },
           series: [{
            name: 'TSEL',
            data: telkomseldownload4g,
            color: '#FF0000'

          }, {
            name: 'ISAT',
            data: indosatdownload4g,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrendownload4g,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xldownload4g,
            color: '#0000FF'

          }, {
            name: '3',
            data: threedownload4g,
            color: '#FF0000'
          }]
        });

          Highcharts.chart('charts1', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_download4g_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            exporting: {
              enabled: false
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },
            plotOptions: {
              column: {
                pointPadding: 0.09,
                borderWidth: 0
              }
            },
            legend: {
              itemStyle: {
                color: '#E0E0E3',
                fontSize: '12px'
              },
              itemHoverStyle: {
                color: '#FFF',
                fontSize: '12px'
              },
              itemHiddenStyle: {
                color: '#606063',
                fontSize: '12px'
              },
              padding: 0
            },
            series: [{
              name: 'TSEL',
              data: [telkomseldownload4g_],
              color: '#FF0000'

            }, {
              name: 'ISAT',
              data: [indosatdownload4g_],
              color: '#FFD700'

            }, {
              name: 'SF',
              data: [smartfrendownload4g_],
              color: '#FF7F00'

            }, {
              name: 'XL',
              data: [xldownload4g_],
              color: '#0000FF'

            }, {
              name: '3',
              data: [threedownload4g_],
              color: '#808080'
            }]
          });

          Highcharts.chart('charts11', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_download4g_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },
            plotOptions: {
              column: {
                pointPadding: 0.09,
                borderWidth: 0
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            legend: {
              itemStyle: {
                color: '#E0E0E3',
                fontSize: '12px'
              },
              itemHoverStyle: {
                color: '#FFF',
                fontSize: '12px'
              },
              itemHiddenStyle: {
                color: '#606063',
                fontSize: '12px'
              }
            },
            series: [{
              name: 'TSEL',
              data: [telkomseldownload4g_],
              color: '#FF0000'

            }, {
              name: 'ISAT',
              data: [indosatdownload4g_],
              color: '#FFD700'

            }, {
              name: 'SF',
              data: [smartfrendownload4g_],
              color: '#FF7F00'

            }, {
              name: 'XL',
              data: [xldownload4g_],
              color: '#0000FF'

            }, {
              name: '3',
              data: [threedownload4g_],
              color: '#808080'
            }]
          });

          var threedownload3g = <?php echo json_encode($threedownload3g); ?>;
          var indosatdownload3g = <?php echo json_encode($indosatdownload3g); ?>;
          var smartfrendownload3g = <?php echo json_encode($smartfrendownload3g); ?>;
          var xldownload3g = <?php echo json_encode($xldownload3g); ?>;
          var telkomseldownload3g = <?php echo json_encode($telkomseldownload3g); ?>;
          var week_download3g = <?php echo json_encode($week_download3g); ?>;

          var threedownload3g_ = <?php echo json_encode($threedownload3g[3]); ?>;
          var indosatdownload3g_ = <?php echo json_encode($indosatdownload3g[3]); ?>;
          var smartfrendownload3g_ = <?php echo json_encode($smartfrendownload3g[3]); ?>;
          var xldownload3g_ = <?php echo json_encode($xldownload3g[3]); ?>;
          var telkomseldownload3g_ = <?php echo json_encode($telkomseldownload3g[3]); ?>;
          var week_download3g_ = <?php echo json_encode($week_download3g[3]); ?>;

          Highcharts.chart('chart2', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_download3g
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },
            plotOptions: {
              column: {
                pointPadding: 0.2,
                borderWidth: 0
              }
            },
            exporting: {
              enabled: false
            },
            legend: {
              itemStyle: { 
               fontSize:'12px'
             }

           },
           series: [{
            name: 'TSEL',
            data: telkomseldownload3g,
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: indosatdownload3g,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrendownload3g,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xldownload3g,
            color: '#0000FF'

          }, {
            name: '3',
            data: threedownload3g,
            color: '#808080'

          }]
        });

          Highcharts.chart('chart22', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_download3g
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },
            plotOptions: {
              column: {
                pointPadding: 0.2,
                borderWidth: 0
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           series: [{
            name: 'TSEL',
            data: telkomseldownload3g,
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: indosatdownload3g,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrendownload3g,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xldownload3g,
            color: '#0000FF'

          }, {
            name: '3',
            data: threedownload3g,
            color: '#808080'

          }]
        });

        Highcharts.chart('charts2', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_download3g_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },
            plotOptions: {
              column: {
                pointPadding: 0.09,
                borderWidth: 0
              }
            },
            exporting: {
              enabled: false
            },
            legend: {
              itemStyle: { 
               fontSize:'12px'
             }

           },
           series: [{
            name: 'TSEL',
            data: [telkomseldownload3g_],
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: [indosatdownload3g_],
            color: '#FFD700'

          }, {
            name: 'SF',
            data: [smartfrendownload3g_],
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: [xldownload3g_],
            color: '#0000FF'

          }, {
            name: '3',
            data: [threedownload3g_],
            color: '#808080'

          }]
        });

          Highcharts.chart('charts22', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_download3g_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },
            plotOptions: {
              column: {
                pointPadding: 0.09,
                borderWidth: 0
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           series: [{
            name: 'TSEL',
            data: [telkomseldownload3g_],
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: [indosatdownload3g_],
            color: '#FFD700'

          }, {
            name: 'SF',
            data: [smartfrendownload3g_],
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: [xldownload3g_],
            color: '#0000FF'

          }, {
            name: '3',
            data: [threedownload3g_],
            color: '#808080'

          }]
        });

          var threeupload4g = <?php echo json_encode($threeupload4g); ?>;
          var indosatupload4g = <?php echo json_encode($indosatupload4g); ?>;
          var smartfrenupload4g = <?php echo json_encode($smartfrenupload4g); ?>;
          var xlupload4g = <?php echo json_encode($xlupload4g); ?>;
          var telkomselupload4g = <?php echo json_encode($telkomselupload4g); ?>;
          var week_upload4g = <?php echo json_encode($week_upload4g); ?>;

          var threeupload4g_ = <?php echo json_encode($threeupload4g[3]); ?>;
          var indosatupload4g_ = <?php echo json_encode($indosatupload4g[3]); ?>;
          var smartfrenupload4g_ = <?php echo json_encode($smartfrenupload4g[3]); ?>;
          var xlupload4g_ = <?php echo json_encode($xlupload4g[3]); ?>;
          var telkomselupload4g_ = <?php echo json_encode($telkomselupload4g[3]); ?>;
          var week_upload4g_ = <?php echo json_encode($week_upload4g[3]); ?>;

          Highcharts.chart('chart3', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_upload4g
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            exporting: {
              enabled: false
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: telkomselupload4g,
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: indosatupload4g,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrenupload4g,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xlupload4g,
            color: '#0000FF'

          }, {
            name: '3',
            data: threeupload4g,
            color: '#808080'

          }]
        });

          Highcharts.chart('chart33', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_upload4g
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: telkomselupload4g,
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: indosatupload4g,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrenupload4g,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xlupload4g,
            color: '#0000FF'

          }, {
            name: '3',
            data: threeupload4g,
            color: '#808080'

          }]
        });

        Highcharts.chart('charts3', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_upload4g_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            exporting: {
              enabled: false
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.09,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: [telkomselupload4g_],
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: [indosatupload4g_],
            color: '#FFD700'

          }, {
            name: 'SF',
            data: [smartfrenupload4g_],
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: [xlupload4g_],
            color: '#0000FF'

          }, {
            name: '3',
            data: [threeupload4g_],
            color: '#808080'

          }]
        });

          Highcharts.chart('charts33', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_upload4g_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.09,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: [telkomselupload4g_],
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: [indosatupload4g_],
            color: '#FFD700'

          }, {
            name: 'SF',
            data: [smartfrenupload4g_],
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: [xlupload4g_],
            color: '#0000FF'

          }, {
            name: '3',
            data: [threeupload4g_],
            color: '#808080'

          }]
        });

          var threeupload3g = <?php echo json_encode($threeupload3g); ?>;
          var indosatupload3g = <?php echo json_encode($indosatupload3g); ?>;
          var smartfrenupload3g = <?php echo json_encode($smartfrenupload3g); ?>;
          var xlupload3g = <?php echo json_encode($xlupload3g); ?>;
          var telkomselupload3g = <?php echo json_encode($telkomselupload3g); ?>;
          var week_upload3g = <?php echo json_encode($week_upload3g); ?>;

          var threeupload3g_ = <?php echo json_encode($threeupload3g[3]); ?>;
          var indosatupload3g_ = <?php echo json_encode($indosatupload3g[3]); ?>;
          var smartfrenupload3g_ = <?php echo json_encode($smartfrenupload3g[3]); ?>;
          var xlupload3g_ = <?php echo json_encode($xlupload3g[3]); ?>;
          var telkomselupload3g_ = <?php echo json_encode($telkomselupload3g[3]); ?>;
          var week_upload3g_ = <?php echo json_encode($week_upload3g[3]); ?>;

          Highcharts.chart('chart4', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_upload3g
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           exporting: {
            enabled: false
          },
          tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: telkomselupload3g,
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: indosatupload3g,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrenupload3g,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xlupload3g,
            color: '#0000FF'

          }, {
            name: '3',
            data: threeupload3g,
            color: '#808080'

          }]
        });

          Highcharts.chart('chart44', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_upload3g
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: telkomselupload3g,
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: indosatupload3g,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrenupload3g,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xlupload3g,
            color: '#0000FF'

          }, {
            name: '3',
            data: threeupload3g,
            color: '#808080'

          }]
        });

        Highcharts.chart('charts4', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_upload3g_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           exporting: {
            enabled: false
          },
          tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: [telkomselupload3g_],
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: [indosatupload3g_],
            color: '#FFD700'

          }, {
            name: 'SF',
            data: [smartfrenupload3g_],
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: [xlupload3g_],
            color: '#0000FF'

          }, {
            name: '3',
            data: [threeupload3g_],
            color: '#808080'

          }]
        });

          Highcharts.chart('charts44', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_upload3g_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: [telkomselupload3g_],
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: [indosatupload3g_],
            color: '#FFD700'

          }, {
            name: 'SF',
            data: [smartfrenupload3g_],
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: [xlupload3g_],
            color: '#0000FF'

          }, {
            name: '3',
            data: [threeupload3g_],
            color: '#808080'

          }]
        });

          var threelatency4g = <?php echo json_encode($threelatency4g); ?>;
          var indosatlatency4g = <?php echo json_encode($indosatlatency4g); ?>;
          var smartfrenlatency4g = <?php echo json_encode($smartfrenlatency4g); ?>;
          var xllatency4g = <?php echo json_encode($xllatency4g); ?>;
          var telkomsellatency4g = <?php echo json_encode($telkomsellatency4g); ?>;
          var week_latency4g = <?php echo json_encode($week_latency4g); ?>;

          var threelatency4g_ = <?php echo json_encode($threelatency4g[3]); ?>;
          var indosatlatency4g_ = <?php echo json_encode($indosatlatency4g[3]); ?>;
          var smartfrenlatency4g_ = <?php echo json_encode($smartfrenlatency4g[3]); ?>;
          var xllatency4g_ = <?php echo json_encode($xllatency4g[3]); ?>;
          var telkomsellatency4g_ = <?php echo json_encode($telkomsellatency4g[3]); ?>;
          var week_latency4g_ = <?php echo json_encode($week_latency4g[3]); ?>;

          Highcharts.chart('chart5', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_latency4g
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Ms'
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Ms</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          exporting: {
            enabled: false
          },
          series: [{
            name: 'TSEL',
            data: telkomsellatency4g,
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: indosatlatency4g,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrenlatency4g,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xllatency4g,
            color: '#0000FF'

          }, {
            name: '3',
            data: threelatency4g,
            color: '#808080'

          }]
        });

          Highcharts.chart('chart55', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_latency4g
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Ms'
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Ms</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },
            plotOptions: {
              column: {
                pointPadding: 0.2,
                borderWidth: 0
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           series: [{
            name: 'TSEL',
            data: telkomsellatency4g,
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: indosatlatency4g,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrenlatency4g,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xllatency4g,
            color: '#0000FF'

          }, {
            name: '3',
            data: threelatency4g,
            color: '#808080'

          }]
        });

        Highcharts.chart('charts5', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_latency4g_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Ms'
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Ms</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.09,
              borderWidth: 0
            }
          },
          exporting: {
            enabled: false
          },
          series: [{
            name: 'TSEL',
            data: [telkomsellatency4g_],
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: [indosatlatency4g_],
            color: '#FFD700'

          }, {
            name: 'SF',
            data: [smartfrenlatency4g_],
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: [xllatency4g_],
            color: '#0000FF'

          }, {
            name: '3',
            data: [threelatency4g_],
            color: '#808080'

          }]
        });

          Highcharts.chart('charts55', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_latency4g_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Ms'
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Ms</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },
            plotOptions: {
              column: {
                pointPadding: 0.09,
                borderWidth: 0
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           series: [{
            name: 'TSEL',
            data: [telkomsellatency4g_],
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: [indosatlatency4g_],
            color: '#FFD700'

          }, {
            name: 'SF',
            data: [smartfrenlatency4g_],
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: [xllatency4g_],
            color: '#0000FF'

          }, {
            name: '3',
            data: [threelatency4g_],
            color: '#808080'

          }]
        });

          var threewifi = <?php echo json_encode($threewifi); ?>;
          var indosatwifi = <?php echo json_encode($indosatwifi); ?>;
          var smartfrenwifi = <?php echo json_encode($smartfrenwifi); ?>;
          var xlwifi = <?php echo json_encode($xlwifi); ?>;
          var telkomselwifi = <?php echo json_encode($telkomselwifi); ?>;
          var week_wifi = <?php echo json_encode($week_wifi); ?>;

          var threewifi_ = <?php echo json_encode($threewifi[3]); ?>;
          var indosatwifi_ = <?php echo json_encode($indosatwifi[3]); ?>;
          var smartfrenwifi_ = <?php echo json_encode($smartfrenwifi[3]); ?>;
          var xlwifi_ = <?php echo json_encode($xlwifi[3]); ?>;
          var telkomselwifi_ = <?php echo json_encode($telkomselwifi[3]); ?>;
          var week_wifi_ = <?php echo json_encode($week_wifi[3]); ?>;

          Highcharts.chart('chart6', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_wifi
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            exporting: {
              enabled: false
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: telkomselwifi,
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: indosatwifi,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrenwifi,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xlwifi,
            color: '#0000FF'

          }, {
            name: '3',
            data: threewifi,
            color: '#808080'

          }]
        });

          Highcharts.chart('chart66', {
            // chart: {
            //   type: 'column'
            // },
            title: {
              text: ''
            },
            xAxis: {
              categories: week_wifi
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: telkomselwifi,
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: indosatwifi,
            color: '#FFD700'

          }, {
            name: 'SF',
            data: smartfrenwifi,
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: xlwifi,
            color: '#0000FF'

          }, {
            name: '3',
            data: threewifi,
            color: '#808080'

          }]
        });

        Highcharts.chart('charts6', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_wifi_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            exporting: {
              enabled: false
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.09,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: [telkomselwifi_],
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: [indosatwifi_],
            color: '#FFD700'

          }, {
            name: 'SF',
            data: [smartfrenwifi_],
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: [xlwifi_],
            color: '#0000FF'

          }, {
            name: '3',
            data: [threewifi_],
            color: '#808080'

          }]
        });

          Highcharts.chart('charts66', {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [week_wifi_]
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Kbps'
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            legend: {
              itemStyle: {
               fontSize:'12px'
             }
           },
           tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} Kbps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.09,
              borderWidth: 0
            }
          },
          series: [{
            name: 'TSEL',
            data: [telkomselwifi_],
            color: '#FF0000'
          }, {
            name: 'ISAT',
            data: [indosatwifi_],
            color: '#FFD700'

          }, {
            name: 'SF',
            data: [smartfrenwifi_],
            color: '#FF7F00'

          }, {
            name: 'XL',
            data: [xlwifi_],
            color: '#0000FF'

          }, {
            name: '3',
            data: [threewifi_],
            color: '#808080'

          }]
        });

          var subscribers = <?php echo json_encode($subscribers); ?>;
          var throughput = <?php echo json_encode($throughput); ?>;
          var vendor = <?php echo json_encode($vendor); ?>;

          console.log("data",vendor);

         Highcharts.chart('combichart', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },
    xAxis: [{
      categories: vendor,
      crosshair: true
    }],
    yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: 'green'
        }
      },
      title: {
        text: 'Subscribers',
        style: {
          color: 'green'
        }
      }
    }, { // Secondary yAxis
      title: {
        text: 'Throughput',
        style: {
          color: '#008B8B'
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: '#008B8B'
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },
    exporting: {
      enabled: false
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      x: -15,
      verticalAlign: 'top',
      y: -10,
      floating: true,
      backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            '#00000'
    },
    series: [{
      name: 'Throughput',
      // color: '#008B8B',
      data: throughput,
      tooltip: {
        valueSuffix: 'Mbps'
      }

    }, {
      name: 'Subscribers',
      // color: '#FFD700',
      data: subscribers,
      tooltip: {
        valueSuffix: ''
      }
    }]
  });

   Highcharts.chart('combichart2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    subtitle: {
      text: ''
    },
    xAxis: [{
      categories: vendor,
      crosshair: true
    }],
    yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: 'green'
        }
      },
      title: {
        text: 'Subscribers',
        style: {
          color: 'green'
        }
      }
    }, { // Secondary yAxis
      title: {
        text: 'Throughput',
        style: {
          color: '#008B8B'
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: '#008B8B'
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },
    legend: {
      layout: 'vertical',
      align: 'left',
      x: 60,
      verticalAlign: 'top',
      y: 0,
      floating: true,
      backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            '#00000'
          },
          series: [{
            name: 'Throughput',
            // color: '#008B8B',
            data: throughput,
            tooltip: {
              valueSuffix: ' Mbps'
            }

          }, {
            name: 'Subscribers',
            // color: '#FFD700',
            data: subscribers,
            tooltip: {
              valueSuffix: ''
            }
          }]
        });

          var android_tsel = <?php echo json_encode($android_tsel); ?>;
          var windows_tsel = <?php echo json_encode($windows_tsel); ?>;
          var iphone_tsel = <?php echo json_encode($iphone_tsel); ?>;

          Highcharts.chart('piechart1', {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
            },
            title: {
              text: ''
            },
            tooltip: {
              headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
              pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                size: 120,
                cursor: 'pointer',
                dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.y:.0f}',
                  connectorColor: 'silver'
                }
              }
            },
            exporting: {
              enabled: false
            },
            series: [{
              name: "TELKOMSEL ONLY",
              colorByPoint: true,
              data: [
              {
                name: "Android",
                y: JSON.parse(android_tsel),
                drilldown: "Android"
              },
              {
                name: "Windows Phone",
                y: JSON.parse(windows_tsel),
                drilldown: "Windows Phone"
              },
              {
                name: "iPhone",
                y: JSON.parse(iphone_tsel),
                drilldown: "iPhone"
              }
              ]
            }]
          });

          Highcharts.chart('piechart11', {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
            },
            title: {
              text: ''
            },
            tooltip: {
              headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
              pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.y:.0f}',
                  connectorColor: 'silver'
                }
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            series: [{
              name: "TELKOMSEL ONLY",
              colorByPoint: true,
              data: [
              {
                name: "Android",
                y: JSON.parse(android_tsel),
                drilldown: "Android"
              },
              {
                name: "Windows Phone",
                y: JSON.parse(windows_tsel),
                drilldown: "Windows Phone"
              },
              {
                name: "iPhone",
                y: JSON.parse(iphone_tsel),
                drilldown: "iPhone"
              }
              ]
            }]
          });

          var android_all = <?php echo json_encode($android_all); ?>;
          var windows_all = <?php echo json_encode($windows_all); ?>;
          var iphone_all = <?php echo json_encode($iphone_all); ?>;

          Highcharts.chart('piechart2', {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
            },
            title: {
              text: ''
            },
            tooltip: {
              headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
              pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                size: 120,
                dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.y:.0f}',
                  connectorColor: 'silver'
                }
              }
            },
            exporting: {
              enabled: false
            },
            series: [{
              name: "ALL OPERATOR",
              colorByPoint: true,
              data: [
              {
                name: "Android",
                y: JSON.parse(android_all),
                drilldown: "Android"
              },
              {
                name: "Windows Phone",
                y: JSON.parse(windows_all),
                drilldown: "Windows Phone"
              },
              {
                name: "iPhone",
                y: JSON.parse(iphone_all),
                drilldown: "iPhone"
              }
              ]
            }]
          });

          Highcharts.chart('piechart22', {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
            },
            title: {
              text: ''
            },
            tooltip: {
              headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
              pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.y:.0f}',
                  connectorColor: 'silver'
                }
              }
            },
            navigation: {
              buttonOptions: {
                verticalAlign: 'bottom',
                align:'right',
                y: -5
              }
            },
            series: [{
              name: "ALL OPERATOR",
              colorByPoint: true,
              data: [
              {
                name: "Android",
                y: JSON.parse(android_all),
                drilldown: "Android"
              },
              {
                name: "Windows Phone",
                y: JSON.parse(windows_all),
                drilldown: "Windows Phone"
              },
              {
                name: "iPhone",
                y: JSON.parse(iphone_all),
                drilldown: "iPhone"
              }
              ]
            }]
          });

          var subecribersdownload = <?php echo json_encode($subecribersdownload); ?>;
          var speedrangedownload = <?php echo json_encode($speedrangedownload); ?>;

// console.log("data",subecribersdownload)
Highcharts.chart('line1', {
  chart: {
    type: 'column'
  },
  title: {
    text: ''
  },
  xAxis: {
    type: 'category',
    title: {
      text: 'Speed Range(Kbps)'
    },
    categories: speedrangedownload
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Subscribers'
    }
  },
  exporting: {
    enabled: false
  },
  legend: {
    enabled: false
  },
  tooltip: {
    pointFormat: '<b>{point.y:.0f} Kbps</b>'
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0
    }
  },
  series: [{
    name: 'Nation Download',
    data: subecribersdownload
        // ,
        // dataLabels: {
        //     enabled: true,
        //     rotation: -90,
        //     color: '#FFFFFF',
        //     align: 'right',
        //     format: '{point.y:.1f}', // one decimal
        //     y: 10, // 10 pixels down from the top
        //     style: {
        //         fontSize: '13px',
        //         fontFamily: 'Verdana, sans-serif'
        //     }
        // }
      }]
    });
Highcharts.chart('line11', {
  chart: {
    type: 'column'
  },
  title: {
    text: ''
  },
  xAxis: {
    type: 'category',
    title: {
      text: 'Speed Range(Kbps)'
    },
    categories: speedrangedownload
  },
  navigation: {
    buttonOptions: {
      verticalAlign: 'bottom',
      align:'right',
      y: -5
    }
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Subscribers'
    }
  },
  legend: {
    enabled: false
  },
  tooltip: {
    pointFormat: '<b>{point.y:.0f} Kbps</b>'
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0
    }
  },
  series: [{
    name: 'Nation Download',
    data: subecribersdownload
        // ,
        // dataLabels: {
        //     enabled: true,
        //     rotation: -90,
        //     color: '#FFFFFF',
        //     align: 'right',
        //     format: '{point.y:.1f}', // one decimal
        //     y: 10, // 10 pixels down from the top
        //     style: {
        //         fontSize: '13px',
        //         fontFamily: 'Verdana, sans-serif'
        //     }
        // }
      }]
    });

var subecribersupload = <?php echo json_encode($subecribersupload); ?>;
var speedrangeupload = <?php echo json_encode($speedrangeupload); ?>;

Highcharts.chart('line2', {
  chart: {
    type: 'column'
  },
  title: {
    text: ''
  },
  xAxis: {
    type: 'category',
    title: {
      text: 'Speed Range(Kbps)'
    },
    categories: speedrangeupload
  },
  yAxis: {
    title: {
      text: 'Subscribers'
    }
  },
  tooltip: {
   pointFormat: '<b>{point.y:.0f} Kbps</b>'
 },
 legend: {
  enabled: false
},
exporting: {
  enabled: false
},
plotOptions: {
  series: {
    borderWidth: 0
  }
},
series: [{
  name: 'Nation Upload',
  data: subecribersupload
}],


});

Highcharts.chart('line22', {
  chart: {
    type: 'column'
  },
  title: {
    text: ''
  },
  xAxis: {
    type: 'category',
    title: {
      text: 'Speed Range(Kbps)'
    },
    categories: speedrangeupload
  },
  yAxis: {
    title: {
      text: 'Subscribers'
    }
  },
  navigation: {
    buttonOptions: {
      verticalAlign: 'bottom',
      align:'right',
      y: -5
    }
  },
  tooltip: {
    pointFormat: '<b>{point.y:.0f} Kbps</b>'
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0
    }
  },
  series: [{
    name: 'Nation Upload',
    data: subecribersupload
  }],

});
</script>
<script>
  $(document).ready(function() {
    $('[data-toggle="toggle"]').change(function(){
      $(this).parents().next('.hide').toggle();
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      url:"http://10.54.36.49/api-ookla/public/home/table_regional",
      type:"get",
      success: function(result){
        $('#region_download').empty();
        var res = JSON.parse(result);
        for (var i = 0; i < res.length; i++) {
          $('#region_download').append('<tr><td>'+res[i].region+'</td><td>'+res[i].download3g+'</td><td>'+res[i].download4g+'</td><td>'+res[i].upload3g+'</td><td>'+res[i].upload4g+'</td><td>'+res[i].latency3g+'</td><td>'+res[i].latency4g+'</td></tr>');
        }
      }
    });  
  });
</script>
</body>

</html>
