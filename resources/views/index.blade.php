<?php header('Access-Control-Allow-Origin: *'); ?> 
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>DASHBOARD OOKLA</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{url('')}}/template/tsel.jpg" rel="icon" type="image/png">
  <link rel="stylesheet" type="text/css" href="{{url('')}}/css/isotope.css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/flexslider.css" type="text/css">
  <link rel="stylesheet" href="{{url('')}}/js/fancybox/jquery.fancybox.css" type="text/css" media="screen">
  <link rel="stylesheet" href="{{url('')}}/css/bootstrap.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="{{url('')}}/css/style.css">
  <link rel="stylesheet" href="{{url('')}}/skin/default.css">
  <script src="https://unpkg.com/ag-grid-enterprise@20.2.0/dist/ag-grid-enterprise.min.js"></script>
  <script src = "https://code.highcharts.com/highcharts.js"> </script>
    <script src="{{url('')}}/js/jquery.js"></script>
  <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script>
    Highcharts.theme = {
      colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
      '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
      chart: {
        backgroundColor: {
          linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
          stops: [
          [0, '#2a2a2b'],
          [1, '#3e3e40']
          ]

        },
        style: {
          fontFamily: '\'Unica One\', sans-serif'
        },
        plotBorderColor: '#606063',
        marginTop: 15
      },
      title: {
        style: {
          color: '#E0E0E3',
          textTransform: 'uppercase',
          fontSize: '20px'
        }
      },
      subtitle: {
        style: {
          color: '#E0E0E3',
          textTransform: 'uppercase'
        }
      },
      xAxis: {
        gridLineColor: '#707073',
        labels: {
          style: {
            color: '#E0E0E3'
          }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        title: {
          style: {
            color: '#A0A0A3'

          }
        }
      },
      yAxis: {
        gridLineColor: '#707073',
        labels: {
          style: {
            color: '#E0E0E3'
          }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        tickWidth: 1,
        title: {
          style: {
            color: '#A0A0A3'
          }
        }
      },
      tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.85)',
        style: {
          color: '#F0F0F0'
        }
      },
      plotOptions: {
        series: {
          dataLabels: {
            color: '#B0B0B3'
          },
          marker: {
            lineColor: '#333'
          }
        },
        boxplot: {
          fillColor: '#505053'
        },
        candlestick: {
          lineColor: 'white'
        },
        errorbar: {
          color: 'white'
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '9px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '9px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '9px'
        },
      },
      credits: {
        enabled: false
      },
      labels: {
        style: {
          color: '#707073'
        }
      },

      drilldown: {
        activeAxisLabelStyle: {
          color: '#F0F0F3'
        },
        activeDataLabelStyle: {
          color: '#F0F0F3'
        }
      },

      navigation: {
        buttonOptions: {
          symbolStroke: '#DDDDDD',
          theme: {
            fill: '#505053'
          }
        }
      },

      rangeSelector: {
        buttonTheme: {
          fill: '#505053',
          stroke: '#000000',
          style: {
            color: '#CCC'
          },
          states: {
            hover: {
              fill: '#707073',
              stroke: '#000000',
              style: {
                color: 'white'
              }
            },
            select: {
              fill: '#000003',
              stroke: '#000000',
              style: {
                color: 'white'
              }
            }
          }
        },
        inputBoxBorderColor: '#505053',
        inputStyle: {
          backgroundColor: '#333',
          color: 'silver'
        },
        labelStyle: {
          color: 'silver'
        }
      },

      navigator: {
        handles: {
          backgroundColor: '#666',
          borderColor: '#AAA'
        },
        outlineColor: '#CCC',
        maskFill: 'rgba(255,255,255,0.1)',
        series: {
          color: '#7798BF',
          lineColor: '#A6C7ED'
        },
        xAxis: {
          gridLineColor: '#505053'
        }
      },

      scrollbar: {
        barBackgroundColor: '#808083',
        barBorderColor: '#808083',
        buttonArrowColor: '#CCC',
        buttonBackgroundColor: '#606063',
        buttonBorderColor: '#606063',
        rifleColor: '#FFF',
        trackBackgroundColor: '#404043',
        trackBorderColor: '#404043'
      },

      legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
      background2: '#505053',
      dataLabelsColor: '#B0B0B3',
      textColor: '#C0C0C0',
      contrastTextColor: '#F0F0F3',
      maskColor: 'rgba(255,255,255,0.3)'
    };
    $(document).ready(function(){
      $("#week").change(function() {
        var week = $("#week").val();
        $.ajax({
          url:"http://10.54.36.49/api-ookla/public/home/chart_download_current4g/week/"+week,
          type:"get",
          success: function(result){
            // $('#chart1').empty();
            var res = JSON.parse(result); 
            var telkomseldownload4g = res['TELKOMSEL']
            var threedownload4g = res['THREE']
            var indosatdownload4g = res['INDOSAT']
            var smartfrendownload4g = res['SMARTFREN']
            var xldownload4g = res['XL']
            var week_download4g = res['WEEK']

            Highcharts.chart('chart11', {
              chart: {
                type: 'column'
              },
              title: {
                text: ''
              },
              xAxis: {
                categories: [week_download4g]
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.09,
                  borderWidth: 0
                }
              },
              navigation: {
                buttonOptions: {
                  verticalAlign: 'bottom',
                  align:'right',
                  y: -5
                }
              },
              legend: {
                itemStyle: {
                  color: '#E0E0E3',
                  fontSize: '12px'
                },
                itemHoverStyle: {
                  color: '#FFF',
                  fontSize: '12px'
                },
                itemHiddenStyle: {
                  color: '#606063',
                  fontSize: '12px'
                }
              },
              series: [{
                name: 'TSEL',
                data: [telkomseldownload4g] ,
                color: '#FF0000'

              }, {
                name: 'ISAT',
                data: [indosatdownload4g] ,
                color: '#FFD700'

              }, {
                name: 'XL',
                data: [xldownload4g] ,
                color: '#0000FF'

              }, {
                name: '3',
                data: [threedownload4g] ,
                color: '#808080'
              }, {
                name: 'SF',
                data: [smartfrendownload4g] ,
                color: '#FF7F00'

              }]
            });

            Highcharts.chart('chart1', {
              chart: {
                type: 'column'
              },
              title: {
                text: ''
              },
              xAxis: {
                categories: [week_download4g]
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              exporting: {
                enabled: false
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.09,
                  borderWidth: 0
                }
              },
              legend: {
                itemStyle: {
                  color: '#E0E0E3',
                  fontSize: '12px'
                },
                itemHoverStyle: {
                  color: '#FFF',
                  fontSize: '12px'
                },
                itemHiddenStyle: {
                  color: '#606063',
                  fontSize: '12px'
                },
                padding: 0
              },
              series: [{
                name: 'TSEL',
                data: [telkomseldownload4g],
                color: '#FF0000'

              }, {
                name: 'ISAT',
                data: [indosatdownload4g],
                color: '#FFD700'

              }, {
                name: 'XL',
                data: [xldownload4g],
                color: '#0000FF'

              }, {
                name: '3',
                data: [threedownload4g],
                color: '#808080'
              }, {
                name: 'SF',
                data: [smartfrendownload4g],
                color: '#FF7F00'

              }]
            });


          }
        });

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_download_history4g/week/"+week,
  type:"get",
  success: function(result){
            // $('#chart1').empty();
            var res = JSON.parse(result); 
            var telkomseldownload4g = []
            var threedownload4g = []
            var indosatdownload4g = []
            var smartfrendownload4g = []
            var xldownload4g = []
            var week_download4g = []

            res.forEach(myFunction);
            function myFunction(value) {
              telkomseldownload4g.push(value['TELKOMSEL'])
            }
            res.forEach(myFunction2);
            function myFunction2(value2) {
              threedownload4g.push(value2['THREE'])
            }
            res.forEach(myFunction3);
            function myFunction3(value3) {
              indosatdownload4g.push(value3['INDOSAT'])
            }
            res.forEach(myFunction4);
            function myFunction4(value4) {
              smartfrendownload4g.push(value4['SMARTFREN'])
            }
            res.forEach(myFunction5);
            function myFunction5(value5) {
              xldownload4g.push(value5['XL'])
            }
            res.forEach(myFunction6);
            function myFunction6(value6) {
              week_download4g.push(value6['WEEK'])
            }

            Highcharts.chart('charts1', {
              title: {
                text: ''
              },
              xAxis: {
                categories: week_download4g
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              exporting: {
                enabled: false
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              legend: {
                itemStyle: {
                  color: '#E0E0E3',
                  fontSize: '12px'
                },
                itemHoverStyle: {
                  color: '#FFF',
                  fontSize: '12px'
                },
                itemHiddenStyle: {
                  color: '#606063',
                  fontSize: '12px'
                }
              },
              series: [{
                name: 'TSEL',
                data: telkomseldownload4g,
                color: '#FF0000'

              }, {
                name: 'ISAT',
                data: indosatdownload4g,
                color: '#FFD700'

              }, {
                name: 'XL',
                data: xldownload4g,
                color: '#0000FF'

              }, {
                name: '3',
                data: threedownload4g,
                color: '#808080'
              }, {
                name: 'SF',
                data: smartfrendownload4g,
                color: '#FF7F00'

              }]
            });

            Highcharts.chart('charts11', {
              title: {
                text: ''
              },
              xAxis: {
                categories: week_download4g
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              navigation: {
                buttonOptions: {
                  verticalAlign: 'bottom',
                  align:'right',
                  y: -5
                }
              },
              legend: {
                itemStyle: {
                  color: '#E0E0E3',
                  fontSize: '12px'
                },
                itemHoverStyle: {
                  color: '#FFF',
                  fontSize: '12px'
                },
                itemHiddenStyle: {
                  color: '#606063',
                  fontSize: '12px'
                }
              },
              series: [{
                name: 'TSEL',
                data: telkomseldownload4g,
                color: '#FF0000'

              }, {
                name: 'ISAT',
                data: indosatdownload4g,
                color: '#FFD700'

              }, {
                name: 'XL',
                data: xldownload4g,
                color: '#0000FF'

              }, {
                name: '3',
                data: threedownload4g,
                color: '#808080'
              }, {
                name: 'SF',
                data: smartfrendownload4g,
                color: '#FF7F00'

              }]
            });


          }
        });

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_download_current3g/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart2').empty();
    var res = JSON.parse(result); 
    var telkomseldownload3g = res['TELKOMSEL']
    var threedownload3g = res['THREE']
    var indosatdownload3g = res['INDOSAT']
    var smartfrendownload3g = res['SMARTFREN']
    var xldownload3g = res['XL']
    var week_download3g = res['WEEK']

    Highcharts.chart('chart22', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_download3g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
    // exporting: {
    //   enabled: false
    // },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
    series: [{
      name: 'TSEL',
      data: [telkomseldownload3g],
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: [indosatdownload3g],
      color: '#FFD700'

    }, {
      name: 'XL',
      data: [xldownload3g],
      color: '#0000FF'

    }, {
      name: '3',
      data: [threedownload3g],
      color: '#808080'

    }, {
      name: 'SF',
      data: [smartfrendownload3g],
      color: '#FF7F00'

    }]
  });

    Highcharts.chart('chart2', {
     chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_download3g]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
    series: [{
      name: 'TSEL',
      data: [telkomseldownload3g],
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: [indosatdownload3g],
      color: '#FFD700'

    }, {
      name: 'XL',
      data: [xldownload3g],
      color: '#0000FF'

    }, {
      name: '3',
      data: [threedownload3g],
      color: '#808080'

    }, {
      name: 'SF',
      data: [smartfrendownload3g],
      color: '#FF7F00'

    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_download_history3g/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart2').empty();
    var res = JSON.parse(result); 
    var telkomseldownload3g = []
    var threedownload3g = []
    var indosatdownload3g = []
    var smartfrendownload3g = []
    var xldownload3g = []
    var week_download3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomseldownload3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threedownload3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatdownload3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrendownload3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xldownload3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_download3g.push(value6['WEEK'])
    } 

  Highcharts.chart('charts2', {
    // chart: {
    //   type: 'column'
    // },
    exporting: {
      enabled: false
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts22', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g,
    color: '#FF7F00'

  }]
});


  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_upload_current4g/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart3').empty();
    var res = JSON.parse(result); 
    var telkomselupload4g = res['TELKOMSEL']
    var threeupload4g = res['THREE']
    var indosatupload4g = res['INDOSAT']
    var smartfrenupload4g = res['SMARTFREN']
    var xlupload4g = res['XL']
    var week_upload4g = res['WEEK']

    Highcharts.chart('chart3', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_upload4g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        },
        padding: 0
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
      series: [{
        name: 'TSEL',
        data: [telkomselupload4g],
        color: '#FF0000'
      }, {
        name: 'ISAT',
        data: [indosatupload4g],
        color: '#FFD700'

      }, {
        name: 'XL',
        data: [xlupload4g],
        color: '#0000FF'

      }, {
        name: '3',
        data: [threeupload4g],
        color: '#808080'

      }, {
        name: 'SF',
        data: [smartfrenupload4g],
        color: '#FF7F00'

      }]
    });

    Highcharts.chart('chart33', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_upload4g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
      series: [{
        name: 'TSEL',
        data: [telkomselupload4g],
        color: '#FF0000'
      }, {
        name: 'ISAT',
        data: [indosatupload4g],
        color: '#FFD700'

      }, {
        name: 'XL',
        data: [xlupload4g],
        color: '#0000FF'

      }, {
        name: '3',
        data: [threeupload4g],
        color: '#808080'

      }, {
        name: 'SF',
        data: [smartfrenupload4g],
        color: '#FF7F00'

      }]
    });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_upload_history4g/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart3').empty();
    var res = JSON.parse(result); 
    var telkomselupload4g = []
    var threeupload4g = []
    var indosatupload4g = []
    var smartfrenupload4g = []
    var xlupload4g = []
    var week_upload4g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomselupload4g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threeupload4g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatupload4g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenupload4g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xlupload4g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_upload4g.push(value6['WEEK'])
    }

    Highcharts.chart('charts3', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload4g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts33', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload4g,
      color: '#FF7F00'

    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_upload_current3g/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart4').empty();
    var res = JSON.parse(result); 
    var telkomselupload3g = res['TELKOMSEL']
    var threeupload3g = res['THREE']
    var indosatupload3g = res['INDOSAT']
    var smartfrenupload3g = res['SMARTFREN']
    var xlupload3g = res['XL']
    var week_upload3g = res['WEEK']

    Highcharts.chart('chart4', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_upload3g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        },
        padding: 0
      },
      exporting: {
        enabled: false
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
      series: [{
        name: 'TSEL',
        data: [telkomselupload3g],
        color: '#FF0000'
      }, {
        name: 'ISAT',
        data: [indosatupload3g],
        color: '#FFD700'

      }, {
        name: 'XL',
        data: [xlupload3g],
        color: '#0000FF'

      }, {
        name: '3',
        data: [threeupload3g],
        color: '#808080'

      }, {
        name: 'SF',
        data: [smartfrenupload3g],
        color: '#FF7F00'

      }]
    });

    Highcharts.chart('chart44', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_upload3g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        }
      },
  //  exporting: {
  //   enabled: false
  // },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomselupload3g],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatupload3g],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xlupload3g],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threeupload3g],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenupload3g],
    color: '#FF7F00'

  }]
});

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_upload_history3g/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart4').empty();
    var res = JSON.parse(result); 
    var telkomselupload3g = []
    var threeupload3g = []
    var indosatupload3g = []
    var smartfrenupload3g = []
    var xlupload3g = []
    var week_upload3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomselupload3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threeupload3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatupload3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenupload3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xlupload3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_upload3g.push(value6['WEEK'])
    }
    console.log('chart4',week_upload3g)

    Highcharts.chart('charts4', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     exporting: {
      enabled: false
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload3g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts44', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload3g,
      color: '#FF7F00'

    }]
  });


  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_latency_current4g/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart5').empty();
    var res = JSON.parse(result); 
    var telkomsellatency4g = res['TELKOMSEL']
    var threelatency4g = res['THREE']
    var indosatlatency4g = res['INDOSAT']
    var smartfrenlatency4g = res['SMARTFREN']
    var xllatency4g = res['XL']
    var week_latency4g = res['WEEK']

    Highcharts.chart('chart5', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_latency4g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        },
        padding: 0
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
      exporting: {
        enabled: false
      },
      series: [{
        name: 'TSEL',
        data: [telkomsellatency4g],
        color: '#FF0000'
      }, {
        name: 'ISAT',
        data: [indosatlatency4g],
        color: '#FFD700'

      }, {
        name: 'XL',
        data: [xllatency4g],
        color: '#0000FF'

      }, {
        name: '3',
        data: [threelatency4g],
        color: '#808080'

      }, {
        name: 'SF',
        data: [smartfrenlatency4g],
        color: '#FF7F00'

      }]
    });

    Highcharts.chart('chart55', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_latency4g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        }
      },
      series: [{
        name: 'TSEL',
        data: [telkomsellatency4g],
        color: '#FF0000'
      }, {
        name: 'ISAT',
        data: [indosatlatency4g],
        color: '#FFD700'

      }, {
        name: 'XL',
        data: [xllatency4g],
        color: '#0000FF'

      }, {
        name: '3',
        data: [threelatency4g],
        color: '#808080'

      }, {
        name: 'SF',
        data: [smartfrenlatency4g],
        color: '#FF7F00'

      }]
    });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_latency_history4g/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart5').empty();
    var res = JSON.parse(result); 
    var telkomsellatency4g = []
    var threelatency4g = []
    var indosatlatency4g = []
    var smartfrenlatency4g = []
    var xllatency4g = []
    var week_latency4g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomsellatency4g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threelatency4g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatlatency4g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenlatency4g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xllatency4g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_latency4g.push(value6['WEEK'])
    }
    console.log('chart5',week_latency4g)

    Highcharts.chart('charts5', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: 'TSEL',
      data: telkomsellatency4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency4g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts55', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     series: [{
      name: 'TSEL',
      data: telkomsellatency4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency4g,
      color: '#FF7F00'

    }]
  });


  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_latency_current3g/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var telkomsellatency3g = res['TELKOMSEL']
    var threelatency3g = res['THREE']
    var indosatlatency3g = res['INDOSAT']
    var smartfrenlatency3g = res['SMARTFREN']
    var xllatency3g = res['XL']
    var week_latency3g = res['WEEK']

    Highcharts.chart('chart6', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_latency3g]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomsellatency3g],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatlatency3g],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xllatency3g],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threelatency3g],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenlatency3g],
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart66', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_latency3g]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
   legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomsellatency3g],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatlatency3g],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xllatency3g],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threelatency3g],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenlatency3g],
    color: '#FF7F00'

  }]
});

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_latency_history3g/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var telkomsellatency3g = []
    var threelatency3g = []
    var indosatlatency3g = []
    var smartfrenlatency3g = []
    var xllatency3g = []
    var week_latency3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomsellatency3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threelatency3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatlatency3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenlatency3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xllatency3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_latency3g.push(value6['WEEK'])
    }


    Highcharts.chart('charts6', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomsellatency3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency3g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts66', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomsellatency3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency3g,
      color: '#FF7F00'

    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_handsettsel/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var android_tsel = []
    var windows_tsel = []
    var iphone_tsel = []

    res.forEach(myFunction);
    function myFunction(value) {
      android_tsel.push(value['android'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      iphone_tsel.push(value2['iphone'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      windows_tsel.push(value3['windows'])
    }

    Highcharts.chart('piechart1', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        size: 120,
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: "TELKOMSEL ONLY",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_tsel),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_tsel),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_tsel),
        drilldown: "iPhone"
      }
      ]
    }]
  });


  Highcharts.chart('piechart11', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    series: [{
      name: "TELKOMSEL ONLY",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_tsel),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_tsel),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_tsel),
        drilldown: "iPhone"
      }
      ]
    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_handsetall/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var android_all = []
    var windows_all = []
    var iphone_all = []

    res.forEach(myFunction);
    function myFunction(value) {
      android_all.push(value['android'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      iphone_all.push(value2['iphone'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      windows_all.push(value3['windows'])
    }

    Highcharts.chart('piechart2', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        size: 120,
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: "ALL OPERATOR",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_all),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_all),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_all),
        drilldown: "iPhone"
      }
      ]
    }]
  });

  Highcharts.chart('piechart22', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    series: [{
      name: "ALL OPERATOR",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_all),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_all),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_all),
        drilldown: "iPhone"
      }
      ]
    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_nasionaldownload/week/"+week,
  type:"get",
  success: function(result){
    var res = JSON.parse(result);
      if (res == ''){
        $('#nasionaldownload').html("Data Not Found");
      }
      else{
        $('#nasionaldownload').html(res.download);
      }
    }
    });
$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_nasionalupload/week/"+week,
  type:"get",
  success: function(result){
    var res = JSON.parse(result);
    if (res == ''){
      $('#nasionalupload').html("Data Not Found");
    }
    else{
      $('#nasionalupload').html(res.download);
    }
  }
});
$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_nasionallatency/week/"+week,
  type:"get",
  success: function(result){
    var res = JSON.parse(result);
    if (res == ''){
      $('#nasionallatency').html("Data Not Found");
    }
    else{
      $('#nasionallatency').html(res.download);
    }
  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_inboundroamer/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var subscribers = []
    var throughput = []
    var vendor = []

    res.forEach(myFunction);
    function myFunction(value) {
      subscribers.push(value['subscribers'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      throughput.push(value2['throughput'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      vendor.push(value3['vendor'])
    }

    Highcharts.chart('combichart', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },
    xAxis: [{
      categories: vendor,
      crosshair: true
    }],
    yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: 'green'
        }
      },
      title: {
        text: 'Subscribers',
        style: {
          color: 'green'
        }
      }
    }, { // Secondary yAxis
      title: {
        text: 'Throughput',
        style: {
          color: '#008B8B'
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: '#008B8B'
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },
    exporting: {
      enabled: false
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      x: -15,
      verticalAlign: 'top',
      y: -10,
      floating: true,
      backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            '#00000'
    },
    series: [{
      name: 'Throughput',
      // color: '#008B8B',
      data: throughput,
      tooltip: {
        valueSuffix: 'Mbps'
      }

    }, {
      name: 'Subscribers',
      // color: '#FFD700',
      data: subscribers,
      tooltip: {
        valueSuffix: ''
      }
    }]
  });

  Highcharts.chart('combichart2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    subtitle: {
      text: ''
    },
    xAxis: [{
      categories: vendor,
      crosshair: true
    }],
    yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: 'green'
        }
      },
      title: {
        text: 'Subscribers',
        style: {
          color: 'green'
        }
      }
    }, { // Secondary yAxis
      title: {
        text: 'Throughput',
        style: {
          color: '#008B8B'
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: '#008B8B'
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },
    legend: {
      layout: 'vertical',
      align: 'left',
      x: 60,
      verticalAlign: 'top',
      y: 0,
      floating: true,
      backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            '#00000'
          },
          series: [{
            name: 'Throughput',
            // color: '#008B8B',
            data: throughput,
            tooltip: {
              valueSuffix: ' Mbps'
            }

          }, {
            name: 'Subscribers',
            // color: '#FFD700',
            data: subscribers,
            tooltip: {
              valueSuffix: ''
            }
          }]
        });

  }
});
$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_speedtestdownload/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var subecribersdownload = []
    var speedrangedownload = []

    res.forEach(myFunction);
    function myFunction(value) {
      subecribersdownload.push(value['subecribers'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      speedrangedownload.push(value2['speedrange'])
    }

    Highcharts.chart('line1', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangedownload
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Subscribers'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Download',
      data: subecribersdownload
    }]
  });

  Highcharts.chart('line11', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangedownload
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Subscribers'
      }
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Download',
      data: subecribersdownload
    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_speedtestupload/week/"+week,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var subecribersupload = []
    var speedrangeupload = []

    res.forEach(myFunction);
    function myFunction(value) {
      subecribersupload.push(value['subecribers'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      speedrangeupload.push(value2['speedrange'])
    }

    Highcharts.chart('line2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangeupload
    },
    yAxis: {
      title: {
        text: 'Subscribers'
      }
    },
    tooltip: {
     pointFormat: '<b>{point.y:.0f} Kbps</b>'
   },
   legend: {
    enabled: false
  },
  exporting: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0
    }
  },
  series: [{
    name: 'Nation Upload',
    data: subecribersupload
  }],


});

  Highcharts.chart('line22', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangeupload
    },
    yAxis: {
      title: {
        text: 'Subscribers'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    legend: {
      layout: 'vertical',
      align: 'center',
      verticalAlign: 'top'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Upload',
      data: subecribersupload
    }],

  });

  }
});


});
});
</script>

<script>
    Highcharts.theme = {
      colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
      '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
      chart: {
        backgroundColor: {
          linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
          stops: [
          [0, '#2a2a2b'],
          [1, '#3e3e40']
          ]
        },
        style: {
          fontFamily: '\'Unica One\', sans-serif'
        },
        plotBorderColor: '#606063',
        marginTop: 15
      },
      title: {
        style: {
          color: '#E0E0E3',
          textTransform: 'uppercase',
          fontSize: '20px'
        }
      },
      subtitle: {
        style: {
          color: '#E0E0E3',
          textTransform: 'uppercase'
        }
      },
      xAxis: {
        gridLineColor: '#707073',
        labels: {
          style: {
            color: '#E0E0E3'
          }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        title: {
          style: {
            color: '#A0A0A3'

          }
        }
      },
      yAxis: {
        gridLineColor: '#707073',
        labels: {
          style: {
            color: '#E0E0E3'
          }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        tickWidth: 1,
        title: {
          style: {
            color: '#A0A0A3'
          }
        }
      },
      tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.85)',
        style: {
          color: '#F0F0F0'
        }
      },
      plotOptions: {
        series: {
          dataLabels: {
            color: '#B0B0B3'
          },
          marker: {
            lineColor: '#333'
          }
        },
        boxplot: {
          fillColor: '#505053'
        },
        candlestick: {
          lineColor: 'white'
        },
        errorbar: {
          color: 'white'
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '9px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '9px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '9px'
        },
      },
      credits: {
        enabled: false
      },
      labels: {
        style: {
          color: '#707073'
        }
      },

      drilldown: {
        activeAxisLabelStyle: {
          color: '#F0F0F3'
        },
        activeDataLabelStyle: {
          color: '#F0F0F3'
        }
      },

      navigation: {
        buttonOptions: {
          symbolStroke: '#DDDDDD',
          theme: {
            fill: '#505053'
          }
        }
      },

      rangeSelector: {
        buttonTheme: {
          fill: '#505053',
          stroke: '#000000',
          style: {
            color: '#CCC'
          },
          states: {
            hover: {
              fill: '#707073',
              stroke: '#000000',
              style: {
                color: 'white'
              }
            },
            select: {
              fill: '#000003',
              stroke: '#000000',
              style: {
                color: 'white'
              }
            }
          }
        },
        inputBoxBorderColor: '#505053',
        inputStyle: {
          backgroundColor: '#333',
          color: 'silver'
        },
        labelStyle: {
          color: 'silver'
        }
      },

      navigator: {
        handles: {
          backgroundColor: '#666',
          borderColor: '#AAA'
        },
        outlineColor: '#CCC',
        maskFill: 'rgba(255,255,255,0.1)',
        series: {
          color: '#7798BF',
          lineColor: '#A6C7ED'
        },
        xAxis: {
          gridLineColor: '#505053'
        }
      },

      scrollbar: {
        barBackgroundColor: '#808083',
        barBorderColor: '#808083',
        buttonArrowColor: '#CCC',
        buttonBackgroundColor: '#606063',
        buttonBorderColor: '#606063',
        rifleColor: '#FFF',
        trackBackgroundColor: '#404043',
        trackBorderColor: '#404043'
      },

      legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
      background2: '#505053',
      dataLabelsColor: '#B0B0B3',
      textColor: '#C0C0C0',
      contrastTextColor: '#F0F0F3',
      maskColor: 'rgba(255,255,255,0.3)'
    };
    $(document).ready(function(){
      $("#months").change(function() {
        var monthss = $("#months").val();
        var months = monthss.replace("-", "");
        console.log("link",months);
        $.ajax({
          url:"http://10.54.36.49/api-ookla/public/home/chart_download_current4g/month/"+months,
          type:"get",
          success: function(result){
            // $('#chart1').empty();
            var res = JSON.parse(result); 
            var telkomseldownload4g = res['TELKOMSEL']
            var threedownload4g = res['THREE']
            var indosatdownload4g = res['INDOSAT']
            var smartfrendownload4g = res['SMARTFREN']
            var xldownload4g = res['XL']
            var week_download4g = res['WEEK']

            Highcharts.chart('chart11', {
              chart: {
                type: 'column'
              },
              title: {
                text: ''
              },
              xAxis: {
                categories: [week_download4g]
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.09,
                  borderWidth: 0
                }
              },
              navigation: {
                buttonOptions: {
                  verticalAlign: 'bottom',
                  align:'right',
                  y: -5
                }
              },
              legend: {
                itemStyle: {
                  color: '#E0E0E3',
                  fontSize: '12px'
                },
                itemHoverStyle: {
                  color: '#FFF',
                  fontSize: '12px'
                },
                itemHiddenStyle: {
                  color: '#606063',
                  fontSize: '12px'
                }
              },
              series: [{
                name: 'TSEL',
                data: [telkomseldownload4g] ,
                color: '#FF0000'

              }, {
                name: 'ISAT',
                data: [indosatdownload4g] ,
                color: '#FFD700'

              }, {
                name: 'XL',
                data: [xldownload4g] ,
                color: '#0000FF'

              }, {
                name: '3',
                data: [threedownload4g] ,
                color: '#808080'
              }, {
                name: 'SF',
                data: [smartfrendownload4g] ,
                color: '#FF7F00'

              }]
            });

            Highcharts.chart('chart1', {
              chart: {
                type: 'column'
              },
              title: {
                text: ''
              },
              xAxis: {
                categories: [week_download4g]
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              exporting: {
                enabled: false
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.09,
                  borderWidth: 0
                }
              },
              legend: {
                itemStyle: {
                  color: '#E0E0E3',
                  fontSize: '12px'
                },
                itemHoverStyle: {
                  color: '#FFF',
                  fontSize: '12px'
                },
                itemHiddenStyle: {
                  color: '#606063',
                  fontSize: '12px'
                },
                padding: 0
              },
              series: [{
                name: 'TSEL',
                data: [telkomseldownload4g],
                color: '#FF0000'

              }, {
                name: 'ISAT',
                data: [indosatdownload4g],
                color: '#FFD700'

              }, {
                name: 'XL',
                data: [xldownload4g],
                color: '#0000FF'

              }, {
                name: '3',
                data: [threedownload4g],
                color: '#808080'
              }, {
                name: 'SF',
                data: [smartfrendownload4g],
                color: '#FF7F00'

              }]
            });


          }
        });

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_download_history4g/month/"+months,
  type:"get",
  success: function(result){
            // $('#chart1').empty();
            var res = JSON.parse(result); 
            var telkomseldownload4g = []
            var threedownload4g = []
            var indosatdownload4g = []
            var smartfrendownload4g = []
            var xldownload4g = []
            var week_download4g = []

            res.forEach(myFunction);
            function myFunction(value) {
              telkomseldownload4g.push(value['TELKOMSEL'])
            }
            res.forEach(myFunction2);
            function myFunction2(value2) {
              threedownload4g.push(value2['THREE'])
            }
            res.forEach(myFunction3);
            function myFunction3(value3) {
              indosatdownload4g.push(value3['INDOSAT'])
            }
            res.forEach(myFunction4);
            function myFunction4(value4) {
              smartfrendownload4g.push(value4['SMARTFREN'])
            }
            res.forEach(myFunction5);
            function myFunction5(value5) {
              xldownload4g.push(value5['XL'])
            }
            res.forEach(myFunction6);
            function myFunction6(value6) {
              week_download4g.push(value6['WEEK'])
            }

            Highcharts.chart('charts1', {
              title: {
                text: ''
              },
              xAxis: {
                categories: week_download4g
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              exporting: {
                enabled: false
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              legend: {
                itemStyle: {
                  color: '#E0E0E3',
                  fontSize: '12px'
                },
                itemHoverStyle: {
                  color: '#FFF',
                  fontSize: '12px'
                },
                itemHiddenStyle: {
                  color: '#606063',
                  fontSize: '12px'
                }
              },
              series: [{
                name: 'TSEL',
                data: telkomseldownload4g,
                color: '#FF0000'

              }, {
                name: 'ISAT',
                data: indosatdownload4g,
                color: '#FFD700'

              }, {
                name: 'XL',
                data: xldownload4g,
                color: '#0000FF'

              }, {
                name: '3',
                data: threedownload4g,
                color: '#808080'
              }, {
                name: 'SF',
                data: smartfrendownload4g,
                color: '#FF7F00'

              }]
            });

            Highcharts.chart('charts11', {
              title: {
                text: ''
              },
              xAxis: {
                categories: week_download4g
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Kbps'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              navigation: {
                buttonOptions: {
                  verticalAlign: 'bottom',
                  align:'right',
                  y: -5
                }
              },
              legend: {
                itemStyle: {
                  color: '#E0E0E3',
                  fontSize: '12px'
                },
                itemHoverStyle: {
                  color: '#FFF',
                  fontSize: '12px'
                },
                itemHiddenStyle: {
                  color: '#606063',
                  fontSize: '12px'
                }
              },
              series: [{
                name: 'TSEL',
                data: telkomseldownload4g,
                color: '#FF0000'

              }, {
                name: 'ISAT',
                data: indosatdownload4g,
                color: '#FFD700'

              }, {
                name: 'XL',
                data: xldownload4g,
                color: '#0000FF'

              }, {
                name: '3',
                data: threedownload4g,
                color: '#808080'
              }, {
                name: 'SF',
                data: smartfrendownload4g,
                color: '#FF7F00'

              }]
            });


          }
        });

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_download_current3g/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart2').empty();
    var res = JSON.parse(result); 
    var telkomseldownload3g = res['TELKOMSEL']
    var threedownload3g = res['THREE']
    var indosatdownload3g = res['INDOSAT']
    var smartfrendownload3g = res['SMARTFREN']
    var xldownload3g = res['XL']
    var week_download3g = res['WEEK']

    Highcharts.chart('chart22', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_download3g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
    // exporting: {
    //   enabled: false
    // },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
    series: [{
      name: 'TSEL',
      data: [telkomseldownload3g],
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: [indosatdownload3g],
      color: '#FFD700'

    }, {
      name: 'XL',
      data: [xldownload3g],
      color: '#0000FF'

    }, {
      name: '3',
      data: [threedownload3g],
      color: '#808080'

    }, {
      name: 'SF',
      data: [smartfrendownload3g],
      color: '#FF7F00'

    }]
  });

    Highcharts.chart('chart2', {
     chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_download3g]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
    series: [{
      name: 'TSEL',
      data: [telkomseldownload3g],
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: [indosatdownload3g],
      color: '#FFD700'

    }, {
      name: 'XL',
      data: [xldownload3g],
      color: '#0000FF'

    }, {
      name: '3',
      data: [threedownload3g],
      color: '#808080'

    }, {
      name: 'SF',
      data: [smartfrendownload3g],
      color: '#FF7F00'

    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_download_history3g/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart2').empty();
    var res = JSON.parse(result); 
    var telkomseldownload3g = []
    var threedownload3g = []
    var indosatdownload3g = []
    var smartfrendownload3g = []
    var xldownload3g = []
    var week_download3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomseldownload3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threedownload3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatdownload3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrendownload3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xldownload3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_download3g.push(value6['WEEK'])
    } 

  Highcharts.chart('charts2', {
    // chart: {
    //   type: 'column'
    // },
    exporting: {
      enabled: false
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts22', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g,
    color: '#FF7F00'

  }]
});


  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_upload_current4g/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart3').empty();
    var res = JSON.parse(result); 
    var telkomselupload4g = res['TELKOMSEL']
    var threeupload4g = res['THREE']
    var indosatupload4g = res['INDOSAT']
    var smartfrenupload4g = res['SMARTFREN']
    var xlupload4g = res['XL']
    var week_upload4g = res['WEEK']

    Highcharts.chart('chart3', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_upload4g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        },
        padding: 0
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
      series: [{
        name: 'TSEL',
        data: [telkomselupload4g],
        color: '#FF0000'
      }, {
        name: 'ISAT',
        data: [indosatupload4g],
        color: '#FFD700'

      }, {
        name: 'XL',
        data: [xlupload4g],
        color: '#0000FF'

      }, {
        name: '3',
        data: [threeupload4g],
        color: '#808080'

      }, {
        name: 'SF',
        data: [smartfrenupload4g],
        color: '#FF7F00'

      }]
    });

    Highcharts.chart('chart33', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_upload4g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
      series: [{
        name: 'TSEL',
        data: [telkomselupload4g],
        color: '#FF0000'
      }, {
        name: 'ISAT',
        data: [indosatupload4g],
        color: '#FFD700'

      }, {
        name: 'XL',
        data: [xlupload4g],
        color: '#0000FF'

      }, {
        name: '3',
        data: [threeupload4g],
        color: '#808080'

      }, {
        name: 'SF',
        data: [smartfrenupload4g],
        color: '#FF7F00'

      }]
    });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_upload_history4g/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart3').empty();
    var res = JSON.parse(result); 
    var telkomselupload4g = []
    var threeupload4g = []
    var indosatupload4g = []
    var smartfrenupload4g = []
    var xlupload4g = []
    var week_upload4g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomselupload4g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threeupload4g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatupload4g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenupload4g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xlupload4g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_upload4g.push(value6['WEEK'])
    }

    Highcharts.chart('charts3', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload4g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts33', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload4g,
      color: '#FF7F00'

    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_upload_current3g/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart4').empty();
    var res = JSON.parse(result); 
    var telkomselupload3g = res['TELKOMSEL']
    var threeupload3g = res['THREE']
    var indosatupload3g = res['INDOSAT']
    var smartfrenupload3g = res['SMARTFREN']
    var xlupload3g = res['XL']
    var week_upload3g = res['WEEK']

    Highcharts.chart('chart4', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_upload3g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        },
        padding: 0
      },
      exporting: {
        enabled: false
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
      series: [{
        name: 'TSEL',
        data: [telkomselupload3g],
        color: '#FF0000'
      }, {
        name: 'ISAT',
        data: [indosatupload3g],
        color: '#FFD700'

      }, {
        name: 'XL',
        data: [xlupload3g],
        color: '#0000FF'

      }, {
        name: '3',
        data: [threeupload3g],
        color: '#808080'

      }, {
        name: 'SF',
        data: [smartfrenupload3g],
        color: '#FF7F00'

      }]
    });

    Highcharts.chart('chart44', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_upload3g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        }
      },
  //  exporting: {
  //   enabled: false
  // },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomselupload3g],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatupload3g],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xlupload3g],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threeupload3g],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenupload3g],
    color: '#FF7F00'

  }]
});

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_upload_history3g/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart4').empty();
    var res = JSON.parse(result); 
    var telkomselupload3g = []
    var threeupload3g = []
    var indosatupload3g = []
    var smartfrenupload3g = []
    var xlupload3g = []
    var week_upload3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomselupload3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threeupload3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatupload3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenupload3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xlupload3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_upload3g.push(value6['WEEK'])
    }
    console.log('chart4',week_upload3g)

    Highcharts.chart('charts4', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     exporting: {
      enabled: false
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload3g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts44', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_upload3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Kbps'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomselupload3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatupload3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xlupload3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threeupload3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenupload3g,
      color: '#FF7F00'

    }]
  });


  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_latency_current4g/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart5').empty();
    var res = JSON.parse(result); 
    var telkomsellatency4g = res['TELKOMSEL']
    var threelatency4g = res['THREE']
    var indosatlatency4g = res['INDOSAT']
    var smartfrenlatency4g = res['SMARTFREN']
    var xllatency4g = res['XL']
    var week_latency4g = res['WEEK']

    Highcharts.chart('chart5', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_latency4g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        },
        padding: 0
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
      exporting: {
        enabled: false
      },
      series: [{
        name: 'TSEL',
        data: [telkomsellatency4g],
        color: '#FF0000'
      }, {
        name: 'ISAT',
        data: [indosatlatency4g],
        color: '#FFD700'

      }, {
        name: 'XL',
        data: [xllatency4g],
        color: '#0000FF'

      }, {
        name: '3',
        data: [threelatency4g],
        color: '#808080'

      }, {
        name: 'SF',
        data: [smartfrenlatency4g],
        color: '#FF7F00'

      }]
    });

    Highcharts.chart('chart55', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [week_latency4g]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.09,
          borderWidth: 0
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3',
          fontSize: '12px'
        },
        itemHoverStyle: {
          color: '#FFF',
          fontSize: '12px'
        },
        itemHiddenStyle: {
          color: '#606063',
          fontSize: '12px'
        }
      },
      series: [{
        name: 'TSEL',
        data: [telkomsellatency4g],
        color: '#FF0000'
      }, {
        name: 'ISAT',
        data: [indosatlatency4g],
        color: '#FFD700'

      }, {
        name: 'XL',
        data: [xllatency4g],
        color: '#0000FF'

      }, {
        name: '3',
        data: [threelatency4g],
        color: '#808080'

      }, {
        name: 'SF',
        data: [smartfrenlatency4g],
        color: '#FF7F00'

      }]
    });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_latency_history4g/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart5').empty();
    var res = JSON.parse(result); 
    var telkomsellatency4g = []
    var threelatency4g = []
    var indosatlatency4g = []
    var smartfrenlatency4g = []
    var xllatency4g = []
    var week_latency4g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomsellatency4g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threelatency4g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatlatency4g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenlatency4g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xllatency4g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_latency4g.push(value6['WEEK'])
    }
    console.log('chart5',week_latency4g)

    Highcharts.chart('charts5', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: 'TSEL',
      data: telkomsellatency4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency4g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts55', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency4g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     series: [{
      name: 'TSEL',
      data: telkomsellatency4g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency4g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency4g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency4g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency4g,
      color: '#FF7F00'

    }]
  });


  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_latency_current3g/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var telkomsellatency3g = res['TELKOMSEL']
    var threelatency3g = res['THREE']
    var indosatlatency3g = res['INDOSAT']
    var smartfrenlatency3g = res['SMARTFREN']
    var xllatency3g = res['XL']
    var week_latency3g = res['WEEK']

    Highcharts.chart('chart6', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_latency3g]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomsellatency3g],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatlatency3g],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xllatency3g],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threelatency3g],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenlatency3g],
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart66', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: [week_latency3g]
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
   legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: [telkomsellatency3g],
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: [indosatlatency3g],
    color: '#FFD700'

  }, {
    name: 'XL',
    data: [xllatency3g],
    color: '#0000FF'

  }, {
    name: '3',
    data: [threelatency3g],
    color: '#808080'

  }, {
    name: 'SF',
    data: [smartfrenlatency3g],
    color: '#FF7F00'

  }]
});

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_latency_history3g/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var telkomsellatency3g = []
    var threelatency3g = []
    var indosatlatency3g = []
    var smartfrenlatency3g = []
    var xllatency3g = []
    var week_latency3g = []

    res.forEach(myFunction);
    function myFunction(value) {
      telkomsellatency3g.push(value['TELKOMSEL'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      threelatency3g.push(value2['THREE'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      indosatlatency3g.push(value3['INDOSAT'])
    }
    res.forEach(myFunction4);
    function myFunction4(value4) {
      smartfrenlatency3g.push(value4['SMARTFREN'])
    }
    res.forEach(myFunction5);
    function myFunction5(value5) {
      xllatency3g.push(value5['XL'])
    }
    res.forEach(myFunction6);
    function myFunction6(value6) {
      week_latency3g.push(value6['WEEK'])
    }


    Highcharts.chart('charts6', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomsellatency3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency3g,
      color: '#FF7F00'

    }]
  });
    Highcharts.chart('charts66', {
      // chart: {
      //   type: 'column'
      // },
      title: {
        text: ''
      },
      xAxis: {
        categories: week_latency3g
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Ms'
        }
      },
      navigation: {
        buttonOptions: {
          verticalAlign: 'bottom',
          align:'right',
          y: -5
        }
      },
      legend: {
        itemStyle: {
         fontSize:'12px'
       }
     },
     tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'TSEL',
      data: telkomsellatency3g,
      color: '#FF0000'
    }, {
      name: 'ISAT',
      data: indosatlatency3g,
      color: '#FFD700'

    }, {
      name: 'XL',
      data: xllatency3g,
      color: '#0000FF'

    }, {
      name: '3',
      data: threelatency3g,
      color: '#808080'

    }, {
      name: 'SF',
      data: smartfrenlatency3g,
      color: '#FF7F00'

    }]
  });

  }
});


$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_inboundroamer/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var subscribers = []
    var throughput = []
    var vendor = []

    res.forEach(myFunction);
    function myFunction(value) {
      subscribers.push(value['subscribers'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      throughput.push(value2['throughput'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      vendor.push(value3['vendor'])
    }

    Highcharts.chart('combichart', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },
    xAxis: [{
      categories: vendor,
      crosshair: true
    }],
    yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: 'green'
        }
      },
      title: {
        text: 'Subscribers',
        style: {
          color: 'green'
        }
      }
    }, { // Secondary yAxis
      title: {
        text: 'Throughput',
        style: {
          color: '#008B8B'
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: '#008B8B'
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },
    exporting: {
      enabled: false
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      x: -15,
      verticalAlign: 'top',
      y: -10,
      floating: true,
      backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            '#00000'
    },
    series: [{
      name: 'Throughput',
      // color: '#008B8B',
      data: throughput,
      tooltip: {
        valueSuffix: 'Mbps'
      }

    }, {
      name: 'Subscribers',
      // color: '#FFD700',
      data: subscribers,
      tooltip: {
        valueSuffix: ''
      }
    }]
  });

  Highcharts.chart('combichart2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    subtitle: {
      text: ''
    },
    xAxis: [{
      categories: vendor,
      crosshair: true
    }],
    yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: 'green'
        }
      },
      title: {
        text: 'Subscribers',
        style: {
          color: 'green'
        }
      }
    }, { // Secondary yAxis
      title: {
        text: 'Throughput',
        style: {
          color: '#008B8B'
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: '#008B8B'
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },
    legend: {
      layout: 'vertical',
      align: 'left',
      x: 60,
      verticalAlign: 'top',
      y: 0,
      floating: true,
      backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            '#00000'
          },
          series: [{
            name: 'Throughput',
            // color: '#008B8B',
            data: throughput,
            tooltip: {
              valueSuffix: ' Mbps'
            }

          }, {
            name: 'Subscribers',
            // color: '#FFD700',
            data: subscribers,
            tooltip: {
              valueSuffix: ''
            }
          }]
        });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_speedtestdownload/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var subecribersdownload = []
    var speedrangedownload = []

    res.forEach(myFunction);
    function myFunction(value) {
      subecribersdownload.push(value['subecribers'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      speedrangedownload.push(value2['speedrange'])
    }

    Highcharts.chart('line1', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangedownload
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Subscribers'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Download',
      data: subecribersdownload
    }]
  });

  Highcharts.chart('line11', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangedownload
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Subscribers'
      }
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Download',
      data: subecribersdownload
    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_speedtestupload/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var subecribersupload = []
    var speedrangeupload = []

    res.forEach(myFunction);
    function myFunction(value) {
      subecribersupload.push(value['subecribers'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      speedrangeupload.push(value2['speedrange'])
    }

    Highcharts.chart('line2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangeupload
    },
    yAxis: {
      title: {
        text: 'Subscribers'
      }
    },
    tooltip: {
     pointFormat: '<b>{point.y:.0f} Kbps</b>'
   },
   legend: {
    enabled: false
  },
  exporting: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0
    }
  },
  series: [{
    name: 'Nation Upload',
    data: subecribersupload
  }],


});

  Highcharts.chart('line22', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangeupload
    },
    yAxis: {
      title: {
        text: 'Subscribers'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    legend: {
      layout: 'vertical',
      align: 'center',
      verticalAlign: 'top'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Upload',
      data: subecribersupload
    }],

  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_handsettsel/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var android_tsel = []
    var windows_tsel = []
    var iphone_tsel = []

    res.forEach(myFunction);
    function myFunction(value) {
      android_tsel.push(value['android'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      iphone_tsel.push(value2['iphone'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      windows_tsel.push(value3['windows'])
    }

    Highcharts.chart('piechart1', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        size: 120,
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: "TELKOMSEL ONLY",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_tsel),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_tsel),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_tsel),
        drilldown: "iPhone"
      }
      ]
    }]
  });


  Highcharts.chart('piechart11', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    series: [{
      name: "TELKOMSEL ONLY",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_tsel),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_tsel),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_tsel),
        drilldown: "iPhone"
      }
      ]
    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_handsetall/month/"+months,
  type:"get",
  success: function(result){
    // $('#chart6').empty();
    var res = JSON.parse(result); 
    var android_all = []
    var windows_all = []
    var iphone_all = []

    res.forEach(myFunction);
    function myFunction(value) {
      android_all.push(value['android'])
    }
    res.forEach(myFunction2);
    function myFunction2(value2) {
      iphone_all.push(value2['iphone'])
    }
    res.forEach(myFunction3);
    function myFunction3(value3) {
      windows_all.push(value3['windows'])
    }

    Highcharts.chart('piechart2', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        size: 120,
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: "ALL OPERATOR",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_all),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_all),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_all),
        drilldown: "iPhone"
      }
      ]
    }]
  });

  Highcharts.chart('piechart22', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    series: [{
      name: "ALL OPERATOR",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_all),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_all),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_all),
        drilldown: "iPhone"
      }
      ]
    }]
  });

  }
});

$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_nasionaldownload/month/"+months,
  type:"get",
  success: function(result){
    var res = JSON.parse(result);
      if (res == ''){
        $('#nasionaldownload').html("Data Not Found");
      }
      else{
        $('#nasionaldownload').html(res.download);
      }
    }
    });
$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_nasionalupload/month/"+months,
  type:"get",
  success: function(result){
    var res = JSON.parse(result);
    if (res == ''){
      $('#nasionalupload').html("Data Not Found");
    }
    else{
      $('#nasionalupload').html(res.download);
    }
  }
});
$.ajax({
  url:"http://10.54.36.49/api-ookla/public/home/chart_nasionallatency/month/"+months,
  type:"get",
  success: function(result){
    var res = JSON.parse(result);
    if (res == ''){
      $('#nasionallatency').html("Data Not Found");
    }
    else{
      $('#nasionallatency').html(res.download);
    }
  }
});


});
});
</script>

<script>
  var test = [];

  var columnDefs = [ 
  {
    headerName: 'REGIONAL', field: 'region', width: 146.5,filter: 'agTextColumnFilter',cellStyle: {'border-right': '1px solid #696969'}
  },
  {
    headerName: 'DOWNLOAD (mbps)',
    headerClass: 'participant-group',
    children: [
    {headerName: '3G', field: 'download3g', width: 69, 
    cellStyle: function(params) {
      var str = params.data.download3g;
      var max = str.includes("+");
      var min = str.includes("-");

      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }

    },valueFormatter: currencyFormatter},
    {headerName: '4G', field: 'download4g', width: 69,
    cellStyle: function(params){
      var str = params.data.download4g;
      var max = str.includes("+");
      var min = str.includes("-");
      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }
      return {textAlign: 'right','border-right': '1px solid #696969'};

    },valueFormatter: download4gFormatter}
    ]
  },
  {
    headerName: 'UPLOAD (mbps)',
    headerClass: 'participant-group',
    children: [
    {headerName: '3G', field: 'upload3g', width: 64,
    cellStyle: function(params) {
      var str = params.data.upload3g;
      var max = str.includes("+");
      var min = str.includes("-");

      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }

    },valueFormatter: upload3gFormatter},
    {headerName: '4G', field: 'upload4g', width: 64,
    cellStyle: function(params){
      var str = params.data.upload3g;
      var max = str.includes("+");
      var min = str.includes("-");

      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }
      return {textAlign: 'left','border-right': '1px solid #696969'};
    },valueFormatter: upload4gFormatter}
    ]
  },
  {
    headerName: 'LATENCY (ms)',
    headerClass: 'participant-group',
    children: [
    {headerName: '3G', field: 'latency3g', width: 64,
    cellStyle: function(params) {
      var str = params.data.latency3g;
      var max = str.includes("+");
      var min = str.includes("-");

      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }

    },valueFormatter: latency3gFormatter},
    {headerName: '4G', field: 'latency4g', width: 64,
    cellStyle: function(params){
      var str = params.data.latency4g;
      var max = str.includes("+");
      var min = str.includes("-");

      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }
      return {textAlign: 'left','border-right': '1px solid #696969'};
    },valueFormatter: latency4gFormatter}
    ]
  }
  ]; 
  function currencyFormatter(params) {
    var value = params.data.download3g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    console.log(fxd);

    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else if(value){
      return fxd;
    }
  }
  function download4gFormatter(params) {
    var value = params.data.download4g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else{
      return fxd;
    }
  }
  function upload3gFormatter(params) {
    var value = params.data.upload3g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else{
      return fxd;
    }
  }
  function upload4gFormatter(params) {
    var value = params.data.upload4g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else{
      return fxd;
    }
  }
  function latency3gFormatter(params) {
    var value = params.data.latency3g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else{
      return fxd;
    }
  }
  function latency4gFormatter(params) {
    var value = params.data.latency4g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else{
      return fxd;
    }
  }
  var gridOptions = {
    defaultColDef: {
      editable: false,
      resizable: true,
      filter: true,
    },
    columnTypes: { 
      EditableColumn: {editable: true},
    },
    debug: true,
    rowSelection: 'multiple',
    columnDefs: columnDefs,
    rowData: null,
    domLayout: 'autoHeight',
    suppressScrollOnNewData: true,
    rowHeight: 36.5,
    headerHeight: 39,
    onSelectionChanged: onSelectionChanged,
    rowSelection: 'multiple'
  };
  function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();
    var selectedRowsString = '';
    selectedRows.forEach( function(selectedRow, index) {
      if (selectedRow.region){
        var url = "{{url('region')}}/"+selectedRow.region;
        window.location.href = url;
      }
    });
  }
  document.addEventListener('DOMContentLoaded', function() {
    var httpRequest = new XMLHttpRequest(); 
    $(document).ready(function(){
      $("#months").change(function() {
        var monthss = $("#months").val();
        var months = monthss.replace("-", "");
        var api = encodeURI('http://10.54.36.49/api-ookla/public/home/table_regional/month/'+months);
        httpRequest.open('GET', api); 
        httpRequest.send(); 
        httpRequest.onreadystatechange = function() {
          if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResponse = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResponse.data);
          }
        };
      });
    });  
  });
</script> 

<script>
  var test = [];

  var columnDefs = [ 
  {
    headerName: 'REGIONAL', field: 'region', width: 146.5,filter: 'agTextColumnFilter',cellStyle: {'border-right': '1px solid #696969'}
  },
  {
    headerName: 'DOWNLOAD (mbps)',
    headerClass: 'participant-group',
    children: [
    {headerName: '3G', field: 'download3g', width: 69, 
    cellStyle: function(params) {
      var str = params.data.download3g;
      var max = str.includes("+");
      var min = str.includes("-");

      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }

    },valueFormatter: currencyFormatter},
    {headerName: '4G', field: 'download4g', width: 69,
    cellStyle: function(params){
      var str = params.data.download4g;
      var max = str.includes("+");
      var min = str.includes("-");
      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }
      return {textAlign: 'right','border-right': '1px solid #696969'};

    },valueFormatter: download4gFormatter}
    ]
  },
  {
    headerName: 'UPLOAD (mbps)',
    headerClass: 'participant-group',
    children: [
    {headerName: '3G', field: 'upload3g', width: 64,
    cellStyle: function(params) {
      var str = params.data.upload3g;
      var max = str.includes("+");
      var min = str.includes("-");

      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }

    },valueFormatter: upload3gFormatter},
    {headerName: '4G', field: 'upload4g', width: 64,
    cellStyle: function(params){
      var str = params.data.upload3g;
      var max = str.includes("+");
      var min = str.includes("-");

      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }
      return {textAlign: 'left','border-right': '1px solid #696969'};
    },valueFormatter: upload4gFormatter}
    ]
  },
  {
    headerName: 'LATENCY (ms)',
    headerClass: 'participant-group',
    children: [
    {headerName: '3G', field: 'latency3g', width: 64,
    cellStyle: function(params) {
      var str = params.data.latency3g;
      var max = str.includes("+");
      var min = str.includes("-");

      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }

    },valueFormatter: latency3gFormatter},
    {headerName: '4G', field: 'latency4g', width: 64,
    cellStyle: function(params){
      var str = params.data.latency4g;
      var max = str.includes("+");
      var min = str.includes("-");

      if (max == true) {
        return {textAlign: 'left', color: '#78DE5C'};
      }else if (min == true) {
        return {textAlign: 'left', color: '#FFD700'};
      }else {
        return {textAlign: 'left', color: '#ffffff'};
      }
      return {textAlign: 'left','border-right': '1px solid #696969'};
    },valueFormatter: latency4gFormatter}
    ]
  }
  ]; 
  function currencyFormatter(params) {
    var value = params.data.download3g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    console.log(fxd);

    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else if(value){
      return fxd;
    }
  }
  function download4gFormatter(params) {
    var value = params.data.download4g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else{
      return fxd;
    }
  }
  function upload3gFormatter(params) {
    var value = params.data.upload3g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else{
      return fxd;
    }
  }
  function upload4gFormatter(params) {
    var value = params.data.upload4g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else{
      return fxd;
    }
  }
  function latency3gFormatter(params) {
    var value = params.data.latency3g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else{
      return fxd;
    }
  }
  function latency4gFormatter(params) {
    var value = params.data.latency4g;
    var min = value.includes("-");
    var max = value.includes("+");
    var pars = parseFloat(value);
    var fxd = pars.toFixed(2);
    if (min == true) {
      return fxd.replace('-','');
    }else if(max == true){
      return fxd.replace('+','');
    }else{
      return fxd;
    }
  }
  var gridOptions = {
    defaultColDef: {
      editable: false,
      resizable: true,
      filter: true,
    },
    columnTypes: { 
      EditableColumn: {editable: true},
    },
    debug: true,
    rowSelection: 'multiple',
    columnDefs: columnDefs,
    rowData: null,
    domLayout: 'autoHeight',
    suppressScrollOnNewData: true,
    rowHeight: 36.5,
    headerHeight: 39,
    onSelectionChanged: onSelectionChanged,
    rowSelection: 'multiple'
  };
  function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();
    var selectedRowsString = '';
    selectedRows.forEach( function(selectedRow, index) {
      if (selectedRow.region){
        var url = "{{url('region')}}/"+selectedRow.region;
        window.location.href = url;
      }
    });
  }
  document.addEventListener('DOMContentLoaded', function() {
    var httpRequest = new XMLHttpRequest(); 
    $(document).ready(function(){
      $("#week").change(function() {
        var week = $("#week").val();
        var api = encodeURI('http://10.54.36.49/api-ookla/public/home/table_regional/week/'+week);
        httpRequest.open('GET', api); 
        httpRequest.send(); 
        httpRequest.onreadystatechange = function() {
          if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResponse = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResponse.data);
          }
        };
      });
    });  
  });
</script> 
</head>
<body>
  <section id="header" class="appear">
    <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:40px; height:40px; background-color:rgba(0,0,0,0.3);" data-300="line-height:40px; height:40px; background-color:rgba(5, 42, 62, 1);">
      <!-- <div class="container"> -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="fa fa-bars color-white"></span>
          </button>
          <div class="navbar-logo">
            <img data-0="width:170px;" data-300=" width:100px;" src="img/oklaa.png" alt="" style="padding-left: 50px;">
          </div>
        </div>
        <div class="navbar-collapse collapse" style="padding-right: 150px;">
          <label for="week" style="color: white; font-size: 16px; padding-left: 10px; padding-top: 4px">Weekly: &nbsp;</label>
          <input type="week" name="week" id="week" style="height: 30px; width: 145px;">
          <label for="months" style="color: white; font-size: 16px; padding-left: 10px; padding-top: 4px">Monthly: &nbsp;</label>
          <input type="month" name="months" id="months" style="height: 30px; width: 155px;">
<!--           <style type="text/css">
            .month {
              position: relative;
              width: 132px; height: 30px;
              color: white;
            }

            .month:before {
              content: attr(data-date);
              display: inline-block;
              color: black;
            }

            .month::-webkit-datetime-edit, .month::-webkit-inner-spin-button,  {
              display: none;
            }

            .month::-webkit-calendar-picker-indicator {
              position: absolute;
              top: 4px;
              right: 0;
              color: black;
              opacity: 1;
            }
          </style> -->
          <!-- <input type="month" id="months" name="months"> -->
          <ul class="nav navbar-nav" data-0="margin-top:0px;" data-300="margin-top:0px;">
            <li class="active"><a href="{{url('/')}}">Home</a>
            </li>
            <li><a href="{{url('super_4g')}}">SUPER 4G</a></li>
            <li><a href="{{url('wcc_62')}}">WCC 62</a></li>
            <li><a href="{{url('csat_48')}}">CSAT 48</a></li>
            <li><a href="{{url('logout')}}">Logout</a></li>
          </ul>
        </div>
      <!-- </div> -->
    </div>
  </section>
  <section id="intro">
    <div class="intro-content">
      <div class="row" style="padding-top: 45px; padding-left: 0px; padding-right: 0px;">
        <div class="col-lg-6" style="padding-right: 0px; padding-top: 20px;">
          <div class="container" style=" padding-top: 0px; padding-left: 0px; padding-right: 0px;">
            <ul class="nav nav-tabs" role="tablist" style="padding-top: 0px; background-color: #800000; font-family: calibri; font-weight: bold;">
              <!-- <li class="nav-item"><a class="nav-link" href="{{url('operator_benchmark')}}">Operator Benchmark</a></li> -->
              <li class="nav-link active"><a data-toggle="tab" role="tab" id="tombol" href="#nav-4G">Operator Benchmark</a></li>
              <li class="nav-item"><a class="nav-link" data-toggle="tab" role="tab" href="#nav-3G">Trend Line</a></li>
              <!-- <li class="nav-item"><a href="{{url('operator_benchmark')}}">Analytical Operator Benchmark</a></li> -->
            </ul>
<!--           <div class="panel-heading" style="background-color: #800000;">
            <a href="{{url('#')}}"><h4 style="font-family: calibri; color: white;"><strong>Operator Benchmark</strong></h4></a>
          </div> -->
          <div class="tab-content">
          <div id="nav-4G" role="tabpanel" class="tab-pane fade in active">
          <div class="small-box boks boksBody" style="height: 482px;">
            <div class="inner">
              <div class="row"> 
                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                  <div class="card-header1" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal1"></i>
                  </div>
                  <div class="modal fade" id="modal1" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537;"> 
                        <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 4G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chart11" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body1" style="width:100%; height:132px; background-color: #343a40">
                    <div id="chart1" style="width:100%; height:132px;"></div>
                  </div> 
                </div> 

                <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                  <div class="card-header2" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 3G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal2"></i>
                  </div>
                  <div class="modal fade" id="modal2" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 3G
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chart22" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body2" style="width:100%; height:132px; background-color: #343a40">
                    <div id="chart2" style="width:100%; height:132px;"></div>
                  </div>          
                </div>  
              </div>

              <div class="row"> 
                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                  <div class="card-header3" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal3"></i>
                  </div>
                  <div class="modal fade" id="modal3" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> UPLOAD 4G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chart33" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body3" style="width:100%; height:132px; background-color: #343a40">
                    <div id="chart3" style="width:100%; height:132px;"></div>
                  </div> 
                </div> 

                <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                  <div class="card-header4" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD 3G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal4"></i>
                  </div>
                  <div class="modal fade" id="modal4" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> UPLOAD 3G
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chart44" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body4" style="width:100%; height:132px; background-color: #343a40">
                    <div id="chart4" style="width:100%; height:132px;"></div>
                  </div>          
                </div>  
              </div>
              <div class="row"> 
                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                  <div class="card-header5" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> LATENCY 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal5"></i>
                  </div>
                  <div class="modal fade" id="modal5" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> LATENCY 4G
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chart55" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body5" style="width:100%; height:132px; background-color: #343a40">
                    <div id="chart5" style="width:100%; height:132px;"></div>
                  </div> 
                </div> 

                <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                  <div class="card-header6" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> LATENCY 3G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal6"></i>
                  </div>
                  <div class="modal fade" id="modal6" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> LATENCY 3G
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="chart66" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body6" style="width:100%; height:132px; background-color: #343a40">
                    <div id="chart6" style="width:100%; height:132px;"></div>
                  </div>          
                </div>  
              </div>
            </div>
          </div>
        </div>
        <div id="nav-3G" role="tabpanel" class="tab-pane fade">
            <div class="small-box boks boksBody" style="height: 482px;">
            <div class="inner">
              <div class="row"> 
                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                  <div class="card-header12" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i>DOWNLOAD 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal12"></i>
                  </div>
                  <div class="modal fade" id="modal12" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537;"> 
                        <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 4G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="charts11" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body12" style="width:100%; height:132px; background-color: #343a40">
                    <div id="charts1" style="width:100%; height:132px;"></div>
                  </div> 
                </div> 

                <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                  <div class="card-header13" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD 3G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal13"></i>
                  </div>
                  <div class="modal fade" id="modal13" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD 3G
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="charts22" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body13" style="width:100%; height:132px; background-color: #343a40">
                    <div id="charts2" style="width:100%; height:132px;"></div>
                  </div>          
                </div>  
              </div>

              <div class="row"> 
                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                  <div class="card-header14" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal14"></i>
                  </div>
                  <div class="modal fade" id="modal14" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> UPLOAD 4G
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="charts33" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body14" style="width:100%; height:132px; background-color: #343a40">
                    <div id="charts3" style="width:100%; height:132px;"></div>
                  </div> 
                </div> 

                <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                  <div class="card-header15" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD 3G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal15"></i>
                  </div>
                  <div class="modal fade" id="modal15" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> UPLOAD 3G
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="charts44" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body15" style="width:100%; height:132px; background-color: #343a40">
                    <div id="charts4" style="width:100%; height:132px;"></div>
                  </div>          
                </div>  
              </div>
              <div class="row"> 
                <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
                  <div class="card-header16" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> LATENCY 4G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal16"></i>
                  </div>
                  <div class="modal fade" id="modal16" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> LATENCY 4G
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="charts55" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body16" style="width:100%; height:132px; background-color: #343a40">
                    <div id="charts5" style="width:100%; height:132px;"></div>
                  </div> 
                </div> 

                <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">  
                  <div class="card-header17" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> LATENCY 3G
                    <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal17"></i>
                  </div>
                  <div class="modal fade" id="modal17" style="width: 100%;">
                    <div class="modal-dialog modal-lg" style="margin-top:90px;">
                      <div class="modal-content" style="background-color:#353537"> 
                        <div class="modal-header" style="text-align:center; color: white;"> LATENCY 3G
                          <button type="button" class="close" data-dismiss="modal">

                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:#000"> 
                          <div id="charts66" style="width:100%; height:100%;"></div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="card-body17" style="width:100%; height:132px; background-color: #343a40">
                    <div id="charts6" style="width:100%; height:132px;"></div>
                  </div>          
                </div>  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4" style="padding-top: 20px; padding-bottom: 0px;">
          <div class="panel-heading" style="background-color: #800000;">
            <a href="#" id="tombol2"><h4 style="font-family: calibri; color: white;"><strong>Region</strong></h4></a>
          </div>
          <div class="small-box boks boksBody" style="height: 470px;">
            <div class="inner">
              <div class="col-xs-12" style="margin-top: 0px; padding-right: 0px; padding-left: 0px;">
                <div id="tableregion" style="width: 100%; height: 480px;" class="ag-theme-balham-dark"></div>
                <script type="text/javascript" charset="utf-8">

                  var test = [];

                  var columnDefs = [ 
                  {
                    headerName: 'REGIONAL', field: 'region', width: 146.5,filter: 'agTextColumnFilter',cellStyle: {'border-right': '1px solid #696969'}
                  },
                  {
                    headerName: 'DOWNLOAD (mbps)',
                    headerClass: 'participant-group',
                    children: [
                    {headerName: '3G', field: 'download3g', width: 69,
                    cellStyle: function(params) {
                      var str = params.data.download3g;
                      var max = str.includes("+");
                      var min = str.includes("-");

                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize': '14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize': '14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize': '14px'};
                      }

                    },valueFormatter: currencyFormatter},
                    {headerName: '4G', field: 'download4g', width: 69,
                    cellStyle: function(params){
                      var str = params.data.download4g;
                      var max = str.includes("+");
                      var min = str.includes("-");
                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize': '14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize': '14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize': '14px'};
                      }
                      return {textAlign: 'right','border-right': '1px solid #696969'};

                    },valueFormatter: download4gFormatter}
                    ]
                  },
                  {
                    headerName: 'UPLOAD (mbps)',
                    headerClass: 'participant-group',
                    children: [
                    {headerName: '3G', field: 'upload3g', width: 64,
                    cellStyle: function(params) {
                      var str = params.data.upload3g;
                      var max = str.includes("+");
                      var min = str.includes("-");

                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize': '14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize': '14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize': '14px'};
                      }

                    },valueFormatter: upload3gFormatter},
                    {headerName: '4G', field: 'upload4g', width: 64,
                    cellStyle: function(params){
                      var str = params.data.upload3g;
                      var max = str.includes("+");
                      var min = str.includes("-");

                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize': '14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize': '14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize': '14px'};
                      }
                      return {textAlign: 'left','border-right': '1px solid #696969'};
                    },valueFormatter: upload4gFormatter}
                    ]
                  },
                  {
                    headerName: 'LATENCY (ms)',
                    headerClass: 'participant-group',
                    children: [
                    {headerName: '3G', field: 'latency3g', width: 64,
                    cellStyle: function(params) {
                      var str = params.data.latency3g;
                      var max = str.includes("+");
                      var min = str.includes("-");

                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize': '14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize': '14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize': '14px'};
                      }

                    },valueFormatter: latency3gFormatter},
                    {headerName: '4G', field: 'latency4g', width: 64,
                    cellStyle: function(params){
                      var str = params.data.latency4g;
                      var max = str.includes("+");
                      var min = str.includes("-");

                      if (max == true) {
                        return {textAlign: 'left', color: '#78DE5C', 'fontSize': '14px'};
                      }else if (min == true) {
                        return {textAlign: 'left', color: '#FFD700', 'fontSize': '14px'};
                      }else {
                        return {textAlign: 'left', color: '#ffffff', 'fontSize': '14px'};
                      }
                      return {textAlign: 'left','border-right': '1px solid #696969'};
                    },valueFormatter: latency4gFormatter}
                    ]
                  }
                  ]; 
                  function currencyFormatter(params) {
                    var value = params.data.download3g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    console.log(fxd);

                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else if(value){
                      return fxd;
                    }
                  }
                  function download4gFormatter(params) {
                    var value = params.data.download4g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else{
                      return fxd;
                    }
                  }
                  function upload3gFormatter(params) {
                    var value = params.data.upload3g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else{
                      return fxd;
                    }
                  }
                  function upload4gFormatter(params) {
                    var value = params.data.upload4g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else{
                      return fxd;
                    }
                  }
                  function latency3gFormatter(params) {
                    var value = params.data.latency3g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else{
                      return fxd;
                    }
                  }
                  function latency4gFormatter(params) {
                    var value = params.data.latency4g;
                    var min = value.includes("-");
                    var max = value.includes("+");
                    var pars = parseFloat(value);
                    var fxd = pars.toFixed(2);
                    if (min == true) {
                      return fxd.replace('-','');
                    }else if(max == true){
                      return fxd.replace('+','');
                    }else{
                      return fxd;
                    }
                  }
                  var gridOptions = {
                    defaultColDef: {
                      editable: false,
                      resizable: true,
                      filter: true,
                    },
                    columnTypes: { 
                      EditableColumn: {editable: true},
                    },
                    debug: true,
                    rowSelection: 'multiple',
                    columnDefs: columnDefs,
                    rowData: null,
                    domLayout: 'autoHeight',
                    suppressScrollOnNewData: true,
                    rowHeight: 36.5,
                    headerHeight: 39,
                    onSelectionChanged: onSelectionChanged,
                    rowSelection: 'multiple'
                  };
                  function onSelectionChanged() {
                    var selectedRows = gridOptions.api.getSelectedRows();
                    var selectedRowsString = '';
                    selectedRows.forEach( function(selectedRow, index) {
                      if (selectedRow.region){
                        var monthss = $("#months").val();
                        var months = monthss.replace("-", "");
                        var week = $("#week").val();
                        if (months) {
                          var url = "{{url('regionmonth')}}/"+selectedRow.region+"/month/"+months;
                          window.location.href = url;
                        }
                        if (week) {
                          var url2 = "{{url('regionweek')}}/"+selectedRow.region+"/week/"+week;
                          window.location.href = url2;
                        }
                        if (months == false && week == false) {
                          var url3 = "{{url('region')}}/"+selectedRow.region;
                          window.location.href = url3;
                        }
                      }
                    });
                  }
                  document.addEventListener('DOMContentLoaded', function() {
                    var gridDiv = document.querySelector('#tableregion');
                    new agGrid.Grid(gridDiv, gridOptions);
                    var httpRequest = new XMLHttpRequest(); 
                    var api = encodeURI('http://10.54.36.49/api-ookla/public/home/table_regional');
                    httpRequest.open('GET', api); 
                    httpRequest.send(); 
                    httpRequest.onreadystatechange = function() {
                      if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                        var httpResponse = JSON.parse(httpRequest.responseText);
                        gridOptions.api.setRowData(httpResponse.data);
                      }
                    };
                  }); 
                </script> 
                <style>
                  .ag-header-group-cell-label {
                    justify-content: center;
                  }
                  .participant-group {
                    border: #00E7B1;
                  }
                </style>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-2" style="padding-top: 20px; padding-left: 0px;">
          <div class="panel-heading" style="background-color: #800000;">
            <h4 style="font-family: calibri; color: white;"><strong>ALL NETWORK</strong></h4>
          </div>
          <div class="small-box boks boksBody" style="height: 480px; margin-bottom: 0px;">
            <div class="inner">
                <div class="row">
                  <div class="col-xs-12 " id="bok1" data-toggle="modal" data-target="#modalChart2g" style="padding-left: 20px; padding-right: 20px; padding-top: 10px;">
                  <div class="small-box headerBox boksHead1 boksHead" style="font-size: 16px; background-color: #191919" align="right">
                    Nasional - Download <span style="font-size: 12px"> (Mbps)</span>
                  </div>
                  <div class="small-box boks boksBody" style="height: 100px; margin-bottom: 7px; background-color: #191919">
                    <div class="inner">
                      <div class="col-xs-12">
                        <div class="huge row" style="font-size: 40px;" >
                          <h1 id="nasionaldownload" class="teksne" style="right:10%; margin-top: 20px; color: #ffffff;"><i class="fa fa-spinner fa-spin"></i></h1>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="garisijo"></div>
                  </div><!-- BOKSBODY-->
                </div>
                <div class="col-xs-12 " id="bok1" data-toggle="modal" data-target="#modalChart2g" style="padding-left: 20px; padding-right: 20px; padding-top: 10px;">
                  <div class="small-box headerBox boksHead1 boksHead" style="font-size: 16px; background-color: #191919" align="right">
                    Nasional - Upload <span style="font-size: 12px"> (Mbps)</span>
                  </div>
                  <div class="small-box boks boksBody" style="height: 100px; margin-bottom: 7px; background-color: #191919">
                    <div class="inner">
                      <div class="col-xs-12">
                        <div class="huge row" style="font-size: 40px;" >
                          <h1 id="nasionalupload" class="teksne" style="right:10%; margin-top: 20px; color: #ffffff;"><i class="fa fa-spinner fa-spin"></i></h1>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="garisijo"></div>
                  </div><!-- BOKSBODY-->
                </div>
                <div class="col-xs-12 " id="bok1" data-toggle="modal" data-target="#modalChart2g" style="padding-left: 20px; padding-right: 20px; padding-top: 10px;">
                  <div class="small-box headerBox boksHead1 boksHead" style="font-size: 16px; background-color: #191919" align="right">
                    Nasional - Latency <span style="font-size: 12px"> (Ms)</span>
                  </div>
                  <div class="small-box boks boksBody" style="height: 100px; margin-bottom: 7px;background-color: #191919">
                    <div class="inner">
                      <div class="col-xs-12">
                        <div class="huge row" style="font-size: 40px;" >
                          <h1 id="nasionallatency" class="teksne" style="right:10%; margin-top: 20px; color: #ffffff;"><i class="fa fa-spinner fa-spin"></i></h1>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="garisijo"></div>
                  </div><!-- BOKSBODY-->
                </div>
                </div>
           </div>
         </div>
   </div>
 </div>
 <div class="row">
  <div class="col-lg-3" style="padding-right: 0px; padding-top: 5px; padding-bottom: 0px;">
    <div class="panel-heading" style="background-color: #800000;">
      <h4 style="font-family: calibri; color: white;"><strong>InBound Roamers</strong></h4>
    </div>
    <div class="small-box boks boksBody" style="height: 160px;">
      <div class="inner">
        <div class="row">
          <div class="col-md-12" style="margin-top:5px; padding-right: 15px;">
            <div class="card-header7" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> 
              <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal7"></i>
            </div>
            <div class="modal fade" id="modal7" style="width: 100%;">
              <div class="modal-dialog modal-lg" style="margin-top:90px;">
                <div class="modal-content" style="background-color:#353537"> 
                  <div class="modal-header" style="text-align:center; color: white;"> InBound Roamers
                    <button type="button" class="close" data-dismiss="modal">

                      <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                  </div>
                  <div class="modal-body" style="color:#000"> 
                    <div id="combichart2" style="width:100%; height:100%;"></div>
                  </div>
                </div> 
              </div>
            </div>
            <div class="card-body7" style="width:100%; height:132px; background-color: #343a40">
              <div id="combichart" style="width:100%; height:132px;"></div>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-md-12" style="padding-right: 0px; padding-left: 5px; padding-bottom: 0px; padding-top: 5px;">
    <div class="panel-heading" style="background-color: #800000;">
      <h4 style="font-family: calibri; color: white;"><strong>Handset Type Distribution</strong></h4>
    </div>
    <div class="small-box boks boksBody" style="height: 160px;">
      <div class="inner">
        <div class="row">
          <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
            <div class="card-header8" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> TELKOMSEL ONLY
              <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal8"></i>
            </div>
            <div class="modal fade" id="modal8" style="width: 100%;">
              <div class="modal-dialog modal-lg" style="margin-top:90px;">
                <div class="modal-content" style="background-color:#353537"> 
                  <div class="modal-header" style="text-align:center; color: white;"> TELKOMSEL ONLY
                    <button type="button" class="close" data-dismiss="modal">

                      <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                  </div>
                  <div class="modal-body" style="color:#000"> 
                    <div id="piechart11" style="width:100%; height:100%;"></div>
                  </div>
                </div> 
              </div>
            </div>
            <div class="card-body8" style="width:100%; height:132px; background-color: #343a40">
              <div id="piechart1" style="width:100%; height:132px;"></div>
            </div> 
          </div>
          <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">
            <div class="card-header9" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> ALL OPERATOR
              <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal9"></i>
            </div>
            <div class="modal fade" id="modal9" style="width: 100%;">
              <div class="modal-dialog modal-lg" style="margin-top:90px;">
                <div class="modal-content" style="background-color:#353537"> 
                  <div class="modal-header" style="text-align:center; color: white;"> ALL OPERATOR
                    <button type="button" class="close" data-dismiss="modal">

                      <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                  </div>
                  <div class="modal-body" style="color:#000"> 
                    <div id="piechart22" style="width:100%; height:100%;"></div>
                  </div>
                </div> 
              </div>
            </div>
            <div class="card-body9" style="width:100%; height:132px; background-color: #343a40">
              <div id="piechart2" style="width:100%; height:132px;"></div>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-12" style="padding-right: 15px; padding-bottom: 0px; padding-top: 5px;">
    <div class="panel-heading" style="background-color: #800000;">
      <h4 style="font-family: calibri; color: white;"><strong>Speed Test Distribution</strong></h4>
    </div>
    <div class="small-box boks boksBody" style="height: 160px;">
      <div class="inner">
        <div class="row">
          <div class="col-md-6" style="margin-top:5px; padding-right: 0px;">
            <div class="card-header10" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> DOWNLOAD (Base On Video Resolution)
              <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal10"></i>
            </div>
            <div class="modal fade" id="modal10" style="width: 100%;">
              <div class="modal-dialog modal-lg" style="margin-top:90px;">
                <div class="modal-content" style="background-color:#353537"> 
                  <div class="modal-header" style="text-align:center; color: white;"> DOWNLOAD (Base On Video Resolution)
                    <button type="button" class="close" data-dismiss="modal">

                      <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                  </div>
                  <div class="modal-body" style="color:#000"> 
                    <div id="line11" style="width:100%; height:100%;"></div>
                  </div>
                </div> 
              </div>
            </div>
            <div class="card-body10" style="width:100%; height:132px; background-color: #343a40">
              <div id="line1" style="width:100%; height:132px;"></div>
            </div> 
          </div>
          <div class="col-md-6" style="margin-top:5px; padding-right: 15px; padding-left: 5px;">
            <div class="card-header11" style="border-radius: 8px 8px 0 0; padding-left: 10px; background-image:linear-gradient(to right, #283048, #859398, #283048); font-weight:bold; color:#bbb9df; width: 100%;"><i class="fa fa-chart-line"></i> UPLOAD (Base On File Sizes)
              <i style="float:right; padding-top:4px; padding-right:7px" class="fa fa-search-plus" data-toggle="modal" data-target="#modal11"></i>
            </div>
            <div class="modal fade" id="modal11" style="width: 100%;">
              <div class="modal-dialog modal-lg" style="margin-top:90px;">
                <div class="modal-content" style="background-color:#353537"> 
                  <div class="modal-header" style="text-align:center; color: white;"> UPLOAD (Base On File Sizes)
                    <button type="button" class="close" data-dismiss="modal">

                      <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                  </div>
                  <div class="modal-body" style="color:#000"> 
                    <div id="line22" style="width:100%; height:100%;"></div>
                  </div>
                </div> 
              </div>
            </div>
            <div class="card-body11" style="width:100%; height:132px; background-color: #343a40">
              <div id="line2" style="width:100%; height:132px;"></div>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <marquee behavior="scroll" direction="left" scrollamount="5" style="font-size:13px; background: rgba(17, 34, 41, 0.9); color: white; padding-top: 0px;">Last Data&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;InBound Roamers : {{ $lastupdate_chart_inboundroamer }}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Handset Type Distribution : {{ $lastupdate }}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Speed Test Distribution : {{ $lastupdate_chart_speedtestdownload }}</marquee>
</div>
</div>
</section>
<script src="{{url('')}}/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="{{url('')}}/js/jquery.js"></script>
<script src="{{url('')}}/js/jquery.easing.1.3.js"></script>
<script src="{{url('')}}/js/bootstrap.min.js"></script>
<script src="{{url('')}}/js/jquery.isotope.min.js"></script>
<script src="{{url('')}}/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="{{url('')}}/js/skrollr.min.js"></script>
<script src="{{url('')}}/js/jquery.scrollTo.min.js"></script>
<script src="{{url('')}}/js/jquery.localScroll.min.js"></script>
<script src="{{url('')}}/js/stellar.js"></script>
<script src="{{url('')}}/js/jquery.appear.js"></script>
<script src="{{url('')}}/js/jquery.flexslider-min.js"></script>
<script src="{{url('')}}/contactform/contactform.js"></script>
<script src="{{url('')}}/js/main.js"></script>
<script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
<script src="{{url('')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="{{url('')}}/js/index.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<script>

  Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
    '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
        [0, '#2a2a2b'],
        [1, '#3e3e40']
        ]
      },
      style: {
        fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063',
      marginTop: 15
    },
    title: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase',
        fontSize: '20px'
      }
    },
    subtitle: {
      style: {
        color: '#E0E0E3',
        textTransform: 'uppercase'
      }
    },
    xAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
        style: {
          color: '#A0A0A3'

        }
      }
    },
    yAxis: {
      gridLineColor: '#707073',
      labels: {
        style: {
          color: '#E0E0E3'
        }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
        style: {
          color: '#A0A0A3'
        }
      }
    },
    tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
        color: '#F0F0F0'
      }
    },
    plotOptions: {
      series: {
        dataLabels: {
          color: '#B0B0B3'
        },
        marker: {
          lineColor: '#333'
        }
      },
      boxplot: {
        fillColor: '#505053'
      },
      candlestick: {
        lineColor: 'white'
      },
      errorbar: {
        color: 'white'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '9px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '9px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '9px'
      }
    },
    credits: {
      enabled: false
    },
    labels: {
      style: {
        color: '#707073'
      }
    },

    drilldown: {
      activeAxisLabelStyle: {
        color: '#F0F0F3'
      },
      activeDataLabelStyle: {
        color: '#F0F0F3'
      }
    },

    navigation: {
      buttonOptions: {
        symbolStroke: '#DDDDDD',
        theme: {
          fill: '#505053'
        }
      }
    },

    rangeSelector: {
      buttonTheme: {
        fill: '#505053',
        stroke: '#000000',
        style: {
          color: '#CCC'
        },
        states: {
          hover: {
            fill: '#707073',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          },
          select: {
            fill: '#000003',
            stroke: '#000000',
            style: {
              color: 'white'
            }
          }
        }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
        backgroundColor: '#333',
        color: 'silver'
      },
      labelStyle: {
        color: 'silver'
      }
    },

    navigator: {
      handles: {
        backgroundColor: '#666',
        borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
        color: '#7798BF',
        lineColor: '#A6C7ED'
      },
      xAxis: {
        gridLineColor: '#505053'
      }
    },

    scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
    },

    legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
    background2: '#505053',
    dataLabelsColor: '#B0B0B3',
    textColor: '#C0C0C0',
    contrastTextColor: '#F0F0F3',
    maskColor: 'rgba(255,255,255,0.3)'
  };

  Highcharts.setOptions(Highcharts.theme);
  Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
      return {
        radialGradient: {
          cx: 0.5,
          cy: 0.3,
          r: 0.7
        },
        stops: [
        [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
              };
            })
  });

  var threedownload4g = <?php echo json_encode($threedownload4g); ?>;
  var indosatdownload4g = <?php echo json_encode($indosatdownload4g); ?>;
  var smartfrendownload4g = <?php echo json_encode($smartfrendownload4g); ?>;
  var xldownload4g = <?php echo json_encode($xldownload4g); ?>;
  var telkomseldownload4g = <?php echo json_encode($telkomseldownload4g); ?>;
  var week_download4g = <?php echo json_encode($week_download4g); ?>;

  var threedownload4g_current = <?php echo json_encode($threedownload4g_current); ?>;
  var indosatdownload4g_current = <?php echo json_encode($indosatdownload4g_current); ?>;
  var smartfrendownload4g_current = <?php echo json_encode($smartfrendownload4g_current); ?>;
  var xldownload4g_current = <?php echo json_encode($xldownload4g_current); ?>;
  var telkomseldownload4g_current = <?php echo json_encode($telkomseldownload4g_current); ?>;
  var week_download4g_current = <?php echo json_encode($week_download4g_current); ?>;


  Highcharts.chart('chart11', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download4g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload4g_current,
    color: '#FF0000'

  }, {
    name: 'ISAT',
    data: indosatdownload4g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload4g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload4g_current,
    color: '#808080'
  }, {
    name: 'SF',
    data: smartfrendownload4g_current,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart1', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download4g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    exporting: {
      enabled: false
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
   },
   series: [{
    name: 'TSEL',
    data: telkomseldownload4g_current,
    color: '#FF0000'

  }, {
    name: 'ISAT',
    data: indosatdownload4g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload4g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload4g_current,
    color: '#808080'
  }, {
    name: 'SF',
    data: smartfrendownload4g_current,
    color: '#FF7F00'

  }]
});

  var threedownload3g = <?php echo json_encode($threedownload3g); ?>;
  var indosatdownload3g = <?php echo json_encode($indosatdownload3g); ?>;
  var smartfrendownload3g = <?php echo json_encode($smartfrendownload3g); ?>;
  var xldownload3g = <?php echo json_encode($xldownload3g); ?>;
  var telkomseldownload3g = <?php echo json_encode($telkomseldownload3g); ?>;
  var week_download3g = <?php echo json_encode($week_download3g); ?>;

  var threedownload3g_current = <?php echo json_encode($threedownload3g_current); ?>;
  var indosatdownload3g_current = <?php echo json_encode($indosatdownload3g_current); ?>;
  var smartfrendownload3g_current = <?php echo json_encode($smartfrendownload3g_current); ?>;
  var xldownload3g_current = <?php echo json_encode($xldownload3g_current); ?>;
  var telkomseldownload3g_current = <?php echo json_encode($telkomseldownload3g_current); ?>;
  var week_download3g_current = <?php echo json_encode($week_download3g_current); ?>;

Highcharts.chart('chart22', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    // exporting: {
    //   enabled: false
    // },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g_current,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g_current,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g_current,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart2', {
   chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g_current,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g_current,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g_current,
    color: '#FF7F00'

  }]
});

  var threeupload4g = <?php echo json_encode($threeupload4g); ?>;
  var indosatupload4g = <?php echo json_encode($indosatupload4g); ?>;
  var smartfrenupload4g = <?php echo json_encode($smartfrenupload4g); ?>;
  var xlupload4g = <?php echo json_encode($xlupload4g); ?>;
  var telkomselupload4g = <?php echo json_encode($telkomselupload4g); ?>;
  var week_upload4g = <?php echo json_encode($week_upload4g); ?>;

  var threeupload4g_current = <?php echo json_encode($threeupload4g_current); ?>;
  var indosatupload4g_current = <?php echo json_encode($indosatupload4g_current); ?>;
  var smartfrenupload4g_current = <?php echo json_encode($smartfrenupload4g_current); ?>;
  var xlupload4g_current = <?php echo json_encode($xlupload4g_current); ?>;
  var telkomselupload4g_current = <?php echo json_encode($telkomselupload4g_current); ?>;
  var week_upload4g_current = <?php echo json_encode($week_upload4g_current); ?>;

  Highcharts.chart('chart3', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload4g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload4g_current,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload4g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload4g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload4g_current,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload4g_current,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart33', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload4g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload4g_current,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload4g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload4g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload4g_current,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload4g_current,
    color: '#FF7F00'

  }]
});

  var threeupload3g = <?php echo json_encode($threeupload3g); ?>;
  var indosatupload3g = <?php echo json_encode($indosatupload3g); ?>;
  var smartfrenupload3g = <?php echo json_encode($smartfrenupload3g); ?>;
  var xlupload3g = <?php echo json_encode($xlupload3g); ?>;
  var telkomselupload3g = <?php echo json_encode($telkomselupload3g); ?>;
  var week_upload3g = <?php echo json_encode($week_upload3g); ?>;

  var threeupload3g_current = <?php echo json_encode($threeupload3g_current); ?>;
  var indosatupload3g_current = <?php echo json_encode($indosatupload3g_current); ?>;
  var smartfrenupload3g_current = <?php echo json_encode($smartfrenupload3g_current); ?>;
  var xlupload3g_current = <?php echo json_encode($xlupload3g_current); ?>;
  var telkomselupload3g_current = <?php echo json_encode($telkomselupload3g_current); ?>;
  var week_upload3g_current = <?php echo json_encode($week_upload3g_current); ?>;

Highcharts.chart('chart4', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload3g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   exporting: {
    enabled: false
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload3g_current,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload3g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload3g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload3g_current,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload3g_current,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart44', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload3g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
  //  exporting: {
  //   enabled: false
  // },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload3g_current,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload3g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload3g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload3g_current,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload3g_current,
    color: '#FF7F00'

  }]
});

  var threelatency4g = <?php echo json_encode($threelatency4g); ?>;
  var indosatlatency4g = <?php echo json_encode($indosatlatency4g); ?>;
  var smartfrenlatency4g = <?php echo json_encode($smartfrenlatency4g); ?>;
  var xllatency4g = <?php echo json_encode($xllatency4g); ?>;
  var telkomsellatency4g = <?php echo json_encode($telkomsellatency4g); ?>;
  var week_latency4g = <?php echo json_encode($week_latency4g); ?>;

  var threelatency4g_current = <?php echo json_encode($threelatency4g_current); ?>;
  var indosatlatency4g_current = <?php echo json_encode($indosatlatency4g_current); ?>;
  var smartfrenlatency4g_current = <?php echo json_encode($smartfrenlatency4g_current); ?>;
  var xllatency4g_current = <?php echo json_encode($xllatency4g_current); ?>;
  var telkomsellatency4g_current = <?php echo json_encode($telkomsellatency4g_current); ?>;
  var week_latency4g_current = <?php echo json_encode($week_latency4g_current); ?>;

  Highcharts.chart('chart5', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency4g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  exporting: {
    enabled: false
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency4g_current,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency4g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency4g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency4g_current,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency4g_current,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart55', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency4g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.09,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomsellatency4g_current,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency4g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency4g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency4g_current,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency4g_current,
    color: '#FF7F00'

  }]
});

  var threelatency3g = <?php echo json_encode($threelatency3g); ?>;
  var indosatlatency3g = <?php echo json_encode($indosatlatency3g); ?>;
  var smartfrenlatency3g = <?php echo json_encode($smartfrenlatency3g); ?>;
  var xllatency3g = <?php echo json_encode($xllatency3g); ?>;
  var telkomsellatency3g = <?php echo json_encode($telkomsellatency3g); ?>;
  var week_latency3g = <?php echo json_encode($week_latency3g); ?>;

  var threelatency3g_current = <?php echo json_encode($threelatency3g_current); ?>;
  var indosatlatency3g_current = <?php echo json_encode($indosatlatency3g_current); ?>;
  var smartfrenlatency3g_current = <?php echo json_encode($smartfrenlatency3g_current); ?>;
  var xllatency3g_current = <?php echo json_encode($xllatency3g_current); ?>;
  var telkomsellatency3g_current = <?php echo json_encode($telkomsellatency3g_current); ?>;
  var week_latency3g_current = <?php echo json_encode($week_latency3g_current); ?>;

Highcharts.chart('chart6', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency3g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      },
      padding: 0
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency3g_current,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency3g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency3g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency3g_current,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency3g_current,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('chart66', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency3g_current
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
   legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.09,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency3g_current,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency3g_current,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency3g_current,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency3g_current,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency3g_current,
    color: '#FF7F00'

  }]
});


  Highcharts.chart('charts1', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload4g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts11', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload4g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts3', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   exporting: {
    enabled: false
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload4g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts33', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload4g,
    color: '#FF7F00'

  }]
});


  Highcharts.chart('charts5', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency4g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts55', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency4g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
   legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency4g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency4g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency4g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency4g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency4g,
    color: '#FF7F00'

  }]
});


  Highcharts.chart('charts2', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts22', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_download3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   series: [{
    name: 'TSEL',
    data: telkomseldownload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatdownload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xldownload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threedownload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrendownload3g,
    color: '#FF7F00'

  }]
});


  Highcharts.chart('charts4', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   exporting: {
    enabled: false
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts44', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_upload3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Kbps'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Kbps</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomselupload3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatupload3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xlupload3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threeupload3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenupload3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts6', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency3g,
    color: '#FF7F00'

  }]
});

  Highcharts.chart('charts66', {
    // chart: {
    //   type: 'column'
    // },
    title: {
      text: ''
    },
    xAxis: {
      categories: week_latency3g
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Ms'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
   legend: {
      itemStyle: {
        color: '#E0E0E3',
        fontSize: '12px'
      },
      itemHoverStyle: {
        color: '#FFF',
        fontSize: '12px'
      },
      itemHiddenStyle: {
        color: '#606063',
        fontSize: '12px'
      }
    },
   tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.2f} Ms</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'TSEL',
    data: telkomsellatency3g,
    color: '#FF0000'
  }, {
    name: 'ISAT',
    data: indosatlatency3g,
    color: '#FFD700'

  }, {
    name: 'XL',
    data: xllatency3g,
    color: '#0000FF'

  }, {
    name: '3',
    data: threelatency3g,
    color: '#808080'

  }, {
    name: 'SF',
    data: smartfrenlatency3g,
    color: '#FF7F00'

  }]
});

  var android_tsel = <?php echo json_encode($android_tsel); ?>;
  var windows_tsel = <?php echo json_encode($windows_tsel); ?>;
  var iphone_tsel = <?php echo json_encode($iphone_tsel); ?>;
  Highcharts.chart('piechart1', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        size: 120,
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: "TELKOMSEL ONLY",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_tsel),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_tsel),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_tsel),
        drilldown: "iPhone"
      }
      ]
    }]
  });


  Highcharts.chart('piechart11', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    series: [{
      name: "TELKOMSEL ONLY",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_tsel),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_tsel),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_tsel),
        drilldown: "iPhone"
      }
      ]
    }]
  });

  var android_all = <?php echo json_encode($android_all); ?>;
  var windows_all = <?php echo json_encode($windows_all); ?>;
  var iphone_all = <?php echo json_encode($iphone_all); ?>;

  Highcharts.chart('piechart2', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        size: 120,
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: "ALL OPERATOR",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_all),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_all),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_all),
        drilldown: "iPhone"
      }
      ]
    }]
  });

  Highcharts.chart('piechart22', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}<br/>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.0f}',
          connectorColor: 'silver'
        }
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    series: [{
      name: "ALL OPERATOR",
      colorByPoint: true,
      data: [
      {
        name: "Android",
        y: JSON.parse(android_all),
        drilldown: "Android"
      },
      {
        name: "Windows Phone",
        y: JSON.parse(windows_all),
        drilldown: "Windows Phone"
      },
      {
        name: "iPhone",
        y: JSON.parse(iphone_all),
        drilldown: "iPhone"
      }
      ]
    }]
  });


  var subscribers = <?php echo json_encode($subscribers); ?>;
  var throughput = <?php echo json_encode($throughput); ?>;
  var vendor = <?php echo json_encode($vendor); ?>;

  Highcharts.chart('combichart', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    subtitle: {
      text: ''
    },
    xAxis: [{
      categories: vendor,
      crosshair: true
    }],
    yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: 'green'
        }
      },
      title: {
        text: 'Subscribers',
        style: {
          color: 'green'
        }
      }
    }, { // Secondary yAxis
      title: {
        text: 'Throughput',
        style: {
          color: '#008B8B'
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: '#008B8B'
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },
    exporting: {
      enabled: false
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      x: -15,
      verticalAlign: 'top',
      y: -10,
      floating: true,
      backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            '#00000'
    },
    series: [{
      name: 'Throughput',
      // color: '#008B8B',
      data: throughput,
      tooltip: {
        valueSuffix: 'Mbps'
      }

    }, {
      name: 'Subscribers',
      // color: '#FFD700',
      data: subscribers,
      tooltip: {
        valueSuffix: ''
      }
    }]
  });

  Highcharts.chart('combichart2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    subtitle: {
      text: ''
    },
    xAxis: [{
      categories: vendor,
      crosshair: true
    }],
    yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: 'green'
        }
      },
      title: {
        text: 'Subscribers',
        style: {
          color: 'green'
        }
      }
    }, { // Secondary yAxis
      title: {
        text: 'Throughput',
        style: {
          color: '#008B8B'
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: '#008B8B'
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },
    legend: {
      layout: 'vertical',
      align: 'left',
      x: 60,
      verticalAlign: 'top',
      y: 0,
      floating: true,
      backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            '#00000'
          },
          series: [{
            name: 'Throughput',
            // color: '#008B8B',
            data: throughput,
            tooltip: {
              valueSuffix: ' Mbps'
            }

          }, {
            name: 'Subscribers',
            // color: '#FFD700',
            data: subscribers,
            tooltip: {
              valueSuffix: ''
            }
          }]
        });

  var subecribersdownload = <?php echo json_encode($subecribersdownload); ?>;
  var speedrangedownload = <?php echo json_encode($speedrangedownload); ?>;

  Highcharts.chart('line1', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangedownload
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Subscribers'
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Download',
      data: subecribersdownload
    }]
  });

  Highcharts.chart('line11', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangedownload
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Subscribers'
      }
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Download',
      data: subecribersdownload
    }]
  });

  var subecribersupload = <?php echo json_encode($subecribersupload); ?>;
  var speedrangeupload = <?php echo json_encode($speedrangeupload); ?>;

  Highcharts.chart('line2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangeupload
    },
    yAxis: {
      title: {
        text: 'Subscribers'
      }
    },
    tooltip: {
     pointFormat: '<b>{point.y:.0f} Kbps</b>'
   },
   legend: {
    enabled: false
  },
  exporting: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0
    }
  },
  series: [{
    name: 'Nation Upload',
    data: subecribersupload
  }],


});

  Highcharts.chart('line22', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Speed Range(Kbps)'
      },
      categories: speedrangeupload
    },
    yAxis: {
      title: {
        text: 'Subscribers'
      }
    },
    navigation: {
      buttonOptions: {
        verticalAlign: 'bottom',
        align:'right',
        y: -5
      }
    },
    tooltip: {
      pointFormat: '<b>{point.y:.0f} Kbps</b>'
    },
    legend: {
      layout: 'vertical',
      align: 'center',
      verticalAlign: 'top'
    },
    plotOptions: {
      series: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Nation Upload',
      data: subecribersupload
    }],

  });
</script>
<script>
  $(document).ready(function() {
    $('[data-toggle="toggle"]').change(function(){
      $(this).parents().next('.hide').toggle();
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      url:"http://10.54.36.49/api-ookla/public/home/table_regional",
      type:"get",
      success: function(result){
        $('#region_download').empty();
        var res = JSON.parse(result);
        for (var i = 0; i < res.length; i++) {
          $('#region_download').append('<tr><td>'+res[i].region+'</td><td>'+res[i].download3g+'</td><td>'+res[i].download4g+'</td><td>'+res[i].upload3g+'</td><td>'+res[i].upload4g+'</td><td>'+res[i].latency3g+'</td><td>'+res[i].latency4g+'</td></tr>');
        }
      }
    });
    $.ajax({
      url:"http://10.54.36.49/api-ookla/public/home/chart_nasionaldownload",
      type:"get",
      success: function(result){
        var res = JSON.parse(result);
        $('#nasionaldownload').html(res.download);
        // console.log('data',res.download);
      }
    });
    $.ajax({
      url:"http://10.54.36.49/api-ookla/public/home/chart_nasionalupload",
      type:"get",
      success: function(result){
        var res = JSON.parse(result);
        $('#nasionalupload').html(res.download);
      }
    });
    $.ajax({
      url:"http://10.54.36.49/api-ookla/public/home/chart_nasionallatency",
      type:"get",
      success: function(result){
        var res = JSON.parse(result);
        $('#nasionallatency').html(res.download);
      }
    });
  });
</script>
<script type="text/javascript">
  var nativePicker = document.querySelector('.nativeWeekPicker');
  var fallbackPicker = document.querySelector('.fallbackWeekPicker');
  var fallbackLabel = document.querySelector('.fallbackLabel');

  var yearSelect = document.querySelector('#year');
  var weekSelect = document.querySelector('#fallbackWeek');

  fallbackPicker.style.display = 'none';
  fallbackLabel.style.display = 'none';

  var test = document.createElement('input');
  test.type = 'week';

  if(test.type === 'text') {
    nativePicker.style.display = 'none';
    fallbackPicker.style.display = 'block';
    fallbackLabel.style.display = 'block';

    populateWeeks();
  }

  function populateWeeks() {
    for(var i = 1; i <= 52; i++) {
      var option = document.createElement('option');
      option.textContent = (i < 10) ? ("0" + i) : i;
      weekSelect.appendChild(option);
    }
  }
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $("#tombol").click(function() {
    var monthss = $("#months").val();
    var months = monthss.replace("-", "");
    var week = $("#week").val();
    if (months) {
      var url = "{{url('operator_benchmark_home_month')}}/"+months;
      window.open(url);
    }
    if (week) {
      var url2 = "{{url('operator_benchmark_home_week')}}/"+week;
      window.open(url2);
    }
    if (months == false && week == false) {
      var url3 = "{{url('operator_benchmark_home')}}/";
      window.open(url3);
    }
    // window.open("{{('operator_benchmark_home')}}");
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function(){
  $("#tombol2").click(function() {
    window.open("{{('chart_region')}}");
  });
});
</script>
</body>

</html>
