<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> --}}
        <link rel="stylesheet" href="{{url('')}}/css/app.css">
        <link rel="stylesheet" href="{{url('')}}/css/style.css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: "Arial";
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <!-- @if(Session::has('error'))
            <div class="">
                {{Session::get('error')}}
            </div>
            @endif -->
            <div class="content">
                <div class="title m-b-md">
                    <img src="{{url('')}}/assets/logo/Telkomsel.jpg" alt="Logo Telkomsel" width="512px" height="250px">
                </div>

                <form class="" action="{{url('login')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <label class="control-label" for="">Username</label>
                        <input class="form-control" type="text" name="username" value="" placeholder="David John">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="">Password</label>
                        <input class="form-control" type="password" name="password" value="" placeholder="*******">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" name="button">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </body>

    <script type="text/javascript" src="{{url('')}}/js/app.js"></script>
</html>
