<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;
use App\Export\BtsOnAirExport;
use App\Export\ResumeSiteExport;
use App\Export\ResumeNeExport;
use App\Export\ResumeNodinBtsExport;
use App\Export\BtsOnAirExportMon;
use App\Export\ResumeSiteExportMon; 
use App\Export\ResumeNeExportMon;
use App\Export\ResumeNodinBtsExportMon;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Export\BaselineOnAirBts4g;
use App\Export\BaselineOnAirBts3g;
use App\Export\BaselineOnAirBts2g;
use DataTables; 
use App\Charts\SampleChart;
use App\User;
use DateTime;
use DB;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    protected $data_user;

    public function __construct()
    {
      session_start();
      // if (!isset($_SESSION['username'])) {
      //     return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/');
      // }
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function ldap(Request $request)
    {
        // session_start();
        // session_destroy();

      return view('login_view_ldap');
    }

    public function logout()
    {
      return view('logout');
    }
    
    public function proses_login(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $check_login = Api::check_login($username,$password);

        $_SESSION['username'] = $username;
        $_SESSION['password'] = $password;

        return view('login_success')
        ->with('username', $_SESSION['username'])
        ->with('password', $_SESSION['password']);
    }
    
    public function halamanawal(){
      if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
      }else{
          return view('halamanawal');
      }
    }

    public function chart_region()
    {
      // chart region 4g download
      if (isset($_SESSION["username"]) != 'admin') {
            return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
      }else{
          $chart_regional4g_download = Api::chart_regional4g_download(); 
          $region_regional4g = array();
          foreach($chart_regional4g_download as $value) {
           $regional4g_download[] = $value['region'];
          } 
          $download_regional4g = array();
          foreach($chart_regional4g_download as $value) {
           $downloadregional4g[] = $value['download'];
          }
          $downloadregional_4g = array_map('intval', $downloadregional4g); 

          $chart_regional4g_upload = Api::chart_regional4g_upload(); 
          foreach($chart_regional4g_upload as $value) {
           $regional4g_upload[] = $value['region'];
          } 
          $upload_regional4g = array();
          foreach($chart_regional4g_upload as $value) {
           $uploadregional4g[] = $value['upload'];
          }
          $uploadregional_4g = array_map('intval', $uploadregional4g); 

          $chart_regional4g_latency = Api::chart_regional4g_latency(); 
          foreach($chart_regional4g_latency as $value) {
           $regional4g_latency[] = $value['region'];
          }
          $latency_regional4g = array();
          foreach($chart_regional4g_latency as $value) {
           $latencyregional4g[] = $value['latency'];
          }
          $latencyregional_4g = array_map('intval', $latencyregional4g); 

          // chart region 3g
          $chart_regional3g_download = Api::chart_regional3g_download(); 
          foreach($chart_regional3g_download as $value) {
           $regional3g_download[] = $value['region'];
          }
          $download_regional3g = array();
          foreach($chart_regional3g_download as $value) {
           $downloadregional3g[] = $value['download'];
          }
          $downloadregional_3g = array_map('intval', $downloadregional3g); 

          $chart_regional3g_upload = Api::chart_regional3g_upload(); 
          foreach($chart_regional3g_upload as $value) {
           $regional3g_upload[] = $value['region'];
          }
          $upload_regional3g = array();
          foreach($chart_regional3g_upload as $value) {
           $uploadregional3g[] = $value['upload'];
          }
          $uploadregional_3g = array_map('intval', $uploadregional3g); 

          $chart_regional3g_latency = Api::chart_regional3g_latency(); 
          foreach($chart_regional3g_latency as $value) {
           $regional3g_latency[] = $value['region'];
          }
          $latency_regional3g = array();
          foreach($chart_regional3g_latency as $value) {
           $latencyregional3g[] = $value['latency'];
          }
          $latencyregional_3g = array_map('intval', $latencyregional3g); 

          foreach($chart_regional4g_download as $value) {
           $last_update_download = date_format (new DateTime($value['last_update']), 'd F Y');
          }
          $lastupdate_download = str_replace("'"," ",$last_update_download);

          foreach($chart_regional4g_upload as $value) {
           $last_update_upload = date_format (new DateTime($value['last_update']), 'd F Y');
          }
          $lastupdate_upload = str_replace("'"," ",$last_update_upload);

          foreach($chart_regional4g_latency as $value) {
           $last_update_latency = date_format (new DateTime($value['last_update']), 'd F Y');
          }
          $lastupdate_latency = str_replace("'"," ",$last_update_latency);


          return view('chart_region',compact('regional4g_download','regional4g_upload','regional4g_latency','regional3g_download','regional3g_upload','regional3g_latency','downloadregional_4g','uploadregional_4g','latencyregional_4g','downloadregional_3g','uploadregional_3g','latencyregional_3g','lastupdate_download','lastupdate_upload','lastupdate_latency'));
        }
    }

    // public function chart1()
    // {
    //       $chart_download4g = Api::chart_download4g(); 

    //   // $three_download4g = array();
    //   foreach($chart_download4g as $value) {
    //    $threedownload4g[] = $value['THREE'];
    //   }
    //   // print_r($chart_download4g);
    //   // die();
    //   // $threedownload4g = array_map('intval', $three_download4g);

    //   $indosat_download4g = array();
    //   foreach($chart_download4g as $value) {
    //    $indosatdownload4g[] = $value['INDOSAT'];
    //   }
    //   // $indosatdownload4g = array_map('intval', $indosat_download4g);

    //   $smartfren_download4g = array();
    //   foreach($chart_download4g as $value) {
    //    $smartfrendownload4g[] = $value['SMARTFREN'];
    //   }
    //   // $smartfrendownload4g = array_map('intval', $smartfren_download4g); 

    //   $xl_download4g = array();
    //   foreach($chart_download4g as $value) {
    //    $xldownload4g[] = $value['XL'];
    //   }
    //   // $xldownload4g = array_map('intval', $xl_download4g); 

    //   $telkomsel_download4g = array();
    //   foreach($chart_download4g as $value) {
    //    $telkomseldownload4g[] = $value['TELKOMSEL'];
    //   }
    //   // $telkomseldownload4g = array_map('intval', $telkomsel_download4g);

    //   $week_download4g = array();
    //   foreach($chart_download4g as $value) {
    //    $week_download4g[] = $value['WEEK'];
    //   }

    //   //download 3g
    //   $chart_download3g = Api::chart_download3g(); 

    //   $three_download3g = array();
    //   foreach($chart_download3g as $value) {
    //    $threedownload3g[] = $value['THREE'];
    //   }
    //   // $threedownload3g = array_map('intval', $three_download3g);

    //   $indosat_download3g = array();
    //   foreach($chart_download3g as $value) {
    //    $indosatdownload3g[] = $value['INDOSAT'];
    //   }
    //   // $indosatdownload3g = array_map('intval', $indosat_download3g);

    //   $smartfren_download3g = array();
    //   foreach($chart_download3g as $value) {
    //    $smartfrendownload3g[] = $value['SMARTFREN'];
    //   }
    //   // $smartfrendownload3g = array_map('intval', $smartfren_download3g); 

    //   $xl_download3g = array();
    //   foreach($chart_download3g as $value) {
    //    $xldownload3g[] = $value['XL'];
    //   }
    //   // $xldownload3g = array_map('intval', $xl_download3g); 

    //   $telkomsel_download3g = array();
    //   foreach($chart_download3g as $value) {
    //    $telkomseldownload3g[] = $value['TELKOMSEL'];
    //   }
    //   // $telkomseldownload3g = array_map('intval', $telkomsel_download3g);

    //   $week_download3g = array();
    //   foreach($chart_download3g as $value) {
    //    $week_download3g[] = $value['WEEK'];
    //   }

    //   return view('chart1', compact('threedownload4g','indosatdownload4g','smartfrendownload4g','xldownload4g','telkomseldownload4g','week_download4g','threedownload3g','indosatdownload3g','smartfrendownload3g','xldownload3g','telkomseldownload3g','week_download3g'));
    // }

    public function index()
    {
      if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
      }else{
        $chart_download4g = Api::chart_download4g();
        $chart_download4g_current = Api::chart_download4g_current(); 
        //history
        foreach($chart_download4g as $value) {
         $threedownload4g[] = $value['THREE'];
        }
        $indosat_download4g = array();
        foreach($chart_download4g as $value) {
         $indosatdownload4g[] = $value['INDOSAT'];
        }
        $smartfren_download4g = array();
        foreach($chart_download4g as $value) {
         $smartfrendownload4g[] = $value['SMARTFREN'];
        }
        $xl_download4g = array();
        foreach($chart_download4g as $value) {
         $xldownload4g[] = $value['XL'];
        }
        $telkomsel_download4g = array();
        foreach($chart_download4g as $value) {
         $telkomseldownload4g[] = $value['TELKOMSEL'];
        }
        $week_download4g = array();
        foreach($chart_download4g as $value) {
         $week_download4g[] = $value['WEEK'];
        }
        //current
        $threedownload4g_current[] = $chart_download4g_current['INDOSAT'];

        $indosatdownload4g_current[] = $chart_download4g_current['INDOSAT'];
        
        $smartfrendownload4g_current[] = $chart_download4g_current['SMARTFREN'];
        
        $xldownload4g_current[] = $chart_download4g_current['XL'];

        $telkomseldownload4g_current[] = $chart_download4g_current['TELKOMSEL'];
        
        $week_download4g_current[] = $chart_download4g_current['WEEK'];

        //download 3g
        $chart_download3g = Api::chart_download3g(); 
        $chart_download3g_current = Api::chart_download3g_current(); 
        //history
        $three_download3g = array();
        foreach($chart_download3g as $value) {
         $threedownload3g[] = $value['THREE'];
        }
        $indosat_download3g = array();
        foreach($chart_download3g as $value) {
         $indosatdownload3g[] = $value['INDOSAT'];
        }
        $smartfren_download3g = array();
        foreach($chart_download3g as $value) {
         $smartfrendownload3g[] = $value['SMARTFREN'];
        }
        $xl_download3g = array();
        foreach($chart_download3g as $value) {
         $xldownload3g[] = $value['XL'];
        }
        $telkomsel_download3g = array();
        foreach($chart_download3g as $value) {
         $telkomseldownload3g[] = $value['TELKOMSEL'];
        }
        $week_download3g = array();
        foreach($chart_download3g as $value) {
         $week_download3g[] = $value['WEEK'];
        }
        //current

        $threedownload3g_current[] = $chart_download3g_current['THREE'];

        $indosatdownload3g_current[] = $chart_download3g_current['INDOSAT'];

        $smartfrendownload3g_current[] = $chart_download3g_current['SMARTFREN'];

        $xldownload3g_current[] = $chart_download3g_current['XL'];

        $telkomseldownload3g_current[] = $chart_download3g_current['TELKOMSEL'];

        $week_download3g_current[] = $chart_download3g_current['WEEK'];


        //upload 4g
        $chart_upload4g = Api::chart_upload4g();
        $chart_upload4g_current = Api::chart_upload4g_current(); 

        $three_upload4g = array();
        foreach($chart_upload4g as $value) {
         $threeupload4g[] = $value['THREE'];
        }
        $indosat_upload4g = array();
        foreach($chart_upload4g as $value) {
         $indosatupload4g[] = $value['INDOSAT'];
        }
        $smartfren_upload4g = array();
        foreach($chart_upload4g as $value) {
         $smartfrenupload4g[] = $value['SMARTFREN'];
        }
        $xl_upload4g = array();
        foreach($chart_upload4g as $value) {
         $xlupload4g[] = $value['XL'];
        }
        $telkomsel_upload4g = array();
        foreach($chart_upload4g as $value) {
         $telkomselupload4g[] = $value['TELKOMSEL'];
        }
        $week_upload4g = array();
        foreach($chart_upload4g as $value) {
         $week_upload4g[] = $value['WEEK'];
        }
        //current

        $threeupload4g_current[] = $chart_upload4g_current['THREE'];

        $indosatupload4g_current[] = $chart_upload4g_current['INDOSAT'];

        $smartfrenupload4g_current[] = $chart_upload4g_current['SMARTFREN'];

        $xlupload4g_current[] = $chart_upload4g_current['XL'];

        $telkomselupload4g_current[] = $chart_upload4g_current['TELKOMSEL'];
        
        $week_upload4g_current[] = $chart_upload4g_current['WEEK'];

        //upload 3g
        $chart_upload3g = Api::chart_upload3g(); 
        $chart_upload3g_current = Api::chart_upload3g_current(); 

        $three_upload3g = array();
        foreach($chart_upload3g as $value) {
         $threeupload3g[] = $value['THREE'];
        }
        $indosat_upload3g = array();
        foreach($chart_upload3g as $value) {
         $indosatupload3g[] = $value['INDOSAT'];
        }
        $smartfren_upload3g = array();
        foreach($chart_upload3g as $value) {
         $smartfrenupload3g[] = $value['SMARTFREN'];
        }
        $xl_upload3g = array();
        foreach($chart_upload3g as $value) {
         $xlupload3g[] = $value['XL'];
        }
        $telkomsel_upload3g = array();
        foreach($chart_upload3g as $value) {
         $telkomselupload3g[] = $value['TELKOMSEL'];
        }
        $week_upload3g = array();
        foreach($chart_upload3g as $value) {
         $week_upload3g[] = $value['WEEK'];
        }
        //current

        $threeupload3g_current[] = $chart_upload3g_current['THREE'];

        $indosatupload3g_current[] = $chart_upload3g_current['INDOSAT'];

        $smartfrenupload3g_current[] = $chart_upload3g_current['SMARTFREN'];

        $xlupload3g_current[] = $chart_upload3g_current['XL'];

        $telkomselupload3g_current[] = $chart_upload3g_current['TELKOMSEL'];

        $week_upload3g_current[] = $chart_upload3g_current['WEEK'];

        //latency 4g
        $chart_latency4g = Api::chart_latency4g(); 
        $chart_latency4g_current = Api::chart_latency4g_current(); 

        $three_latency4g = array();
        foreach($chart_latency4g as $value) {
         $threelatency4g[] = $value['THREE'];
        }
        $indosat_latency4g = array();
        foreach($chart_latency4g as $value) {
         $indosatlatency4g[] = $value['INDOSAT'];
        }
        $smartfren_latency4g = array();
        foreach($chart_latency4g as $value) {
         $smartfrenlatency4g[] = $value['SMARTFREN'];
        }
        $xl_latency4g = array();
        foreach($chart_latency4g as $value) {
         $xllatency4g[] = $value['XL'];
        }
        $telkomsel_latency4g = array();
        foreach($chart_latency4g as $value) {
         $telkomsellatency4g[] = $value['TELKOMSEL'];
        }
        $week_latency4g = array();
        foreach($chart_latency4g as $value) {
         $week_latency4g[] = $value['WEEK'];
        }
        //current

        $threelatency4g_current[] = $chart_latency4g_current['THREE'];

        $indosatlatency4g_current[] = $chart_latency4g_current['INDOSAT'];

        $smartfrenlatency4g_current[] = $chart_latency4g_current['SMARTFREN'];

        $xllatency4g_current[] = $chart_latency4g_current['XL'];

        $telkomsellatency4g_current[] = $chart_latency4g_current['TELKOMSEL'];

        $week_latency4g_current[] = $chart_latency4g_current['WEEK'];

        //latency 3g
        $chart_latency3g = Api::chart_latency3g(); 
        $chart_latency3g_current = Api::chart_latency3g_current(); 

        $three_latency3g = array();
        foreach($chart_latency3g as $value) {
         $threelatency3g[] = $value['THREE'];
        }
        $indosat_latency3g = array();
        foreach($chart_latency3g as $value) {
         $indosatlatency3g[] = $value['INDOSAT'];
        }
        $smartfren_latency3g = array();
        foreach($chart_latency3g as $value) {
         $smartfrenlatency3g[] = $value['SMARTFREN'];
        }
        $xl_latency3g = array();
        foreach($chart_latency3g as $value) {
         $xllatency3g[] = $value['XL'];
        }
        $telkomsel_latency3g = array();
        foreach($chart_latency3g as $value) {
         $telkomsellatency3g[] = $value['TELKOMSEL'];
        }
        $week_latency3g = array();
        foreach($chart_latency3g as $value) {
         $week_latency3g[] = $value['WEEK'];
        }
        //current

        $threelatency3g_current[] = $chart_latency3g_current['THREE'];

        $indosatlatency3g_current[] = $chart_latency3g_current['INDOSAT'];

        $smartfrenlatency3g_current[] = $chart_latency3g_current['SMARTFREN'];

        $xllatency3g_current[] = $chart_latency3g_current['XL'];

        $telkomsellatency3g_current[] = $chart_latency3g_current['TELKOMSEL'];

        $week_latency3g_current[] = $chart_latency3g_current['WEEK'];


        $chart_speedtestdownload = Api::chart_speedtestdownload(); 
        foreach($chart_speedtestdownload as $value) {
         $last_update_chart_speedtestdownload = $value['last_update'];
        }
        $lastupdate_chart_speedtestdownload = str_replace("'"," ",$last_update_chart_speedtestdownload);

        $subecribers_download = array();
        foreach($chart_speedtestdownload as $value) {
         $subecribers_download[] = $value['subecribers'];
        }
        $subecribersdownload = array_map('intval', $subecribers_download);

        $speed_range_download = array();
        foreach($chart_speedtestdownload as $value) {
         $speedrangedownload[] = $value['speedrange'];
        }
        // $speedrangedownload = array_map('intval', $speed_range_download);

        $chart_speedtestupload = Api::chart_speedtestupload(); 
        $subecribers_upload = array();
        foreach($chart_speedtestupload as $value) {
         $subecribers_upload[] = $value['subecribers'];
        }
        $subecribersupload = array_map('intval', $subecribers_upload);

        $speed_range_upload = array();
        foreach($chart_speedtestupload as $value) {
         $speedrangeupload[] = $value['speedrange'];
        }
        // $speedrangeupload = array_map('intval', $speed_range_upload);

        //chart handset tsel
        $chart_handsettsel = Api::chart_handsettsel(); 
        foreach($chart_handsettsel as $value) {
         $last_update = $value['last_update'];
        }
        $lastupdate = str_replace("'"," ",$last_update);

        $androidtsel = array();
        foreach($chart_handsettsel as $value) {
         $androidtsel[] = $value['android'];
        }
        $android_tsel = array_map('intval', $androidtsel);

        $iphonetsel = array();
        foreach($chart_handsettsel as $value) {
         $iphonetsel[] = $value['iphone'];
        }
        $iphone_tsel = array_map('intval', $iphonetsel);

        $windowstsel = array();
        foreach($chart_handsettsel as $value) {
         $windowstsel[] = $value['windows'];
        }
        $windows_tsel = array_map('intval', $windowstsel);

        //chart handset all
        $chart_handsetall = Api::chart_handsetall(); 
        $androidall = array();
        foreach($chart_handsetall as $value) {
         $androidall[] = $value['android'];
        }
        $android_all = array_map('intval', $androidall);

        $iphoneall = array();
        foreach($chart_handsetall as $value) {
         $iphoneall[] = $value['iphone'];
        }
        $iphone_all = array_map('intval', $iphoneall);

        $windowsall = array();
        foreach($chart_handsetall as $value) {
         $windowsall[] = $value['windows'];
        }
        $windows_all = array_map('intval', $windowsall);

        //chart inbound
        $chart_inboundroamer = Api::chart_inboundroamer(); 

        foreach($chart_inboundroamer as $value) {
         $last_update_chart_inboundroamer = $value['last_update'];
        }
        $lastupdate_chart_inboundroamer = str_replace("'"," ",$last_update_chart_inboundroamer);

        $subscribers_ = array();
        foreach($chart_inboundroamer as $value) {
         $subscribers_[] = $value['subscribers'];
        }
        $subscribers = array_map('intval', $subscribers_);

        $throughput_ = array();
        foreach($chart_inboundroamer as $value) {
         $throughput_[] = $value['throughput'];
        }
        $throughput = array_map('intval', $throughput_);

        $vendor = array();
        foreach($chart_inboundroamer as $value) {
         $vendor[] = $value['vendor'];
        }
        
        $tahun = date('Y');
        $month= DB::table('ookla_regional_monthly')->selectRaw("REPLACE(last_update, '-', '') as 'last_update'")->groupBy('last_update')->orderby('last_update', 'DESC')->get();

          $region = array('Sumbagut','Sumbagteng','Sumbagsel','Jabotabek','Jawa Barat','Jawa Tengah','Jawa Timur','Kalimantan Barat','Kalimantan Tengah','Kalimantan Timur','NTT','NTB','PAPUA','MALUKU');
          $band = array('WEEKLY');

          return view('index', compact('region','band','threedownload4g','indosatdownload4g','smartfrendownload4g','xldownload4g','telkomseldownload4g','week_download4g','threedownload3g','indosatdownload3g','smartfrendownload3g','xldownload3g','telkomseldownload3g','week_download3g','threeupload4g','indosatupload4g','smartfrenupload4g','xlupload4g','telkomselupload4g','week_upload4g','threeupload3g','indosatupload3g','smartfrenupload3g','xlupload3g','telkomselupload3g','week_upload3g','threelatency4g','indosatlatency4g','smartfrenlatency4g','xllatency4g','telkomsellatency4g','week_latency4g','threelatency3g','indosatlatency3g','smartfrenlatency3g','xllatency3g','telkomsellatency3g','week_latency3g','subecribersdownload','speedrangedownload','subecribersupload','speedrangeupload','android_tsel','iphone_tsel','windows_tsel','android_all','iphone_all','windows_all','subscribers','throughput','vendor','tahun','month','lastupdate','lastupdate_chart_inboundroamer','lastupdate_chart_speedtestdownload','threedownload4g_current','indosatdownload4g_current','smartfrendownload4g_current','xldownload4g_current','telkomseldownload4g_current','week_download4g_current','threedownload3g_current','indosatdownload3g_current','smartfrendownload3g_current','xldownload3g_current','telkomseldownload3g_current','week_download3g_current','threeupload4g_current','indosatupload4g_current','smartfrenupload4g_current','xlupload4g_current','telkomselupload4g_current','week_upload4g_current','threeupload3g_current','indosatupload3g_current','smartfrenupload3g_current','xlupload3g_current','telkomselupload3g_current','week_upload3g_current','threelatency3g_current','indosatlatency3g_current','smartfrenlatency3g_current','xllatency3g_current','telkomsellatency3g_current','week_latency3g_current','threelatency4g_current','indosatlatency4g_current','smartfrenlatency4g_current','xllatency4g_current','telkomsellatency4g_current','week_latency4g_current'));

      }
    }

    public function super_4g()
    {
      if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
      }else{
        $chart_download4g_super4g = Api::chart_download4g_super4g(); 

        $three_download4g = array();
        foreach($chart_download4g_super4g as $value) {
         $three_download4g[] = $value['THREE'];
        }
        $threedownload4g = array_map('intval', $three_download4g);

        $indosat_download4g = array();
        foreach($chart_download4g_super4g as $value) {
         $indosat_download4g[] = $value['INDOSAT'];
        }
        $indosatdownload4g = array_map('intval', $indosat_download4g);

        $smartfren_download4g = array();
        foreach($chart_download4g_super4g as $value) {
         $smartfren_download4g[] = $value['SMARTFREN'];
        }
        $smartfrendownload4g = array_map('intval', $smartfren_download4g); 

        $xl_download4g = array();
        foreach($chart_download4g_super4g as $value) {
         $xl_download4g[] = $value['XL'];
        }
        $xldownload4g = array_map('intval', $xl_download4g); 

        $telkomsel_download4g = array();
        foreach($chart_download4g_super4g as $value) {
         $telkomsel_download4g[] = $value['TELKOMSEL'];
        }
        $telkomseldownload4g = array_map('intval', $telkomsel_download4g);

        $week_download4g = array();
        foreach($chart_download4g_super4g as $value) {
         $week_download4g[] = $value['WEEK'];
        }

        //download 3g
        $chart_download3g_super4g = Api::chart_download3g_super4g(); 

        $three_download3g = array();
        foreach($chart_download3g_super4g as $value) {
         $three_download3g[] = $value['THREE'];
        }
        $threedownload3g = array_map('intval', $three_download3g);

        $indosat_download3g = array();
        foreach($chart_download3g_super4g as $value) {
         $indosat_download3g[] = $value['INDOSAT'];
        }
        $indosatdownload3g = array_map('intval', $indosat_download3g);

        $smartfren_download3g = array();
        foreach($chart_download3g_super4g as $value) {
         $smartfren_download3g[] = $value['SMARTFREN'];
        }
        $smartfrendownload3g = array_map('intval', $smartfren_download3g); 

        $xl_download3g = array();
        foreach($chart_download3g_super4g as $value) {
         $xl_download3g[] = $value['XL'];
        }
        $xldownload3g = array_map('intval', $xl_download3g); 

        $telkomsel_download3g = array();
        foreach($chart_download3g_super4g as $value) {
         $telkomsel_download3g[] = $value['TELKOMSEL'];
        }
        $telkomseldownload3g = array_map('intval', $telkomsel_download3g);

        $week_download3g = array();
        foreach($chart_download3g_super4g as $value) {
         $week_download3g[] = $value['WEEK'];
        }

        //upload 4g
        $chart_upload4g_super4g = Api::chart_upload4g_super4g(); 

        $three_upload4g = array();
        foreach($chart_upload4g_super4g as $value) {
         $three_upload4g[] = $value['THREE'];
        }
        $threeupload4g = array_map('intval', $three_upload4g);

        $indosat_upload4g = array();
        foreach($chart_upload4g_super4g as $value) {
         $indosat_upload4g[] = $value['INDOSAT'];
        }
        $indosatupload4g = array_map('intval', $indosat_upload4g);

        $smartfren_upload4g = array();
        foreach($chart_upload4g_super4g as $value) {
         $smartfren_upload4g[] = $value['SMARTFREN'];
        }
        $smartfrenupload4g = array_map('intval', $smartfren_upload4g); 

        $xl_upload4g = array();
        foreach($chart_upload4g_super4g as $value) {
         $xl_upload4g[] = $value['XL'];
        }
        $xlupload4g = array_map('intval', $xl_upload4g); 

        $telkomsel_upload4g = array();
        foreach($chart_upload4g_super4g as $value) {
         $telkomsel_upload4g[] = $value['TELKOMSEL'];
        }
        $telkomselupload4g = array_map('intval', $telkomsel_upload4g);

        $week_upload4g = array();
        foreach($chart_upload4g_super4g as $value) {
         $week_upload4g[] = $value['WEEK'];
        }

        //download 2g
        $chart_download2g_super4g = Api::chart_download2g_super4g(); 

        $three_upload3g = array();
        foreach($chart_download2g_super4g as $value) {
         $three_upload3g[] = $value['THREE'];
        }
        $threeupload3g = array_map('intval', $three_upload3g);

        $indosat_upload3g = array();
        foreach($chart_download2g_super4g as $value) {
         $indosat_upload3g[] = $value['INDOSAT'];
        }
        $indosatupload3g = array_map('intval', $indosat_upload3g);

        $smartfren_upload3g = array();
        foreach($chart_download2g_super4g as $value) {
         $smartfren_upload3g[] = $value['SMARTFREN'];
        }
        $smartfrenupload3g = array_map('intval', $smartfren_upload3g); 

        $xl_upload3g = array();
        foreach($chart_download2g_super4g as $value) {
         $xl_upload3g[] = $value['XL'];
        }
        $xlupload3g = array_map('intval', $xl_upload3g); 

        $telkomsel_upload3g = array();
        foreach($chart_download2g_super4g as $value) {
         $telkomsel_upload3g[] = $value['TELKOMSEL'];
        }
        $telkomselupload3g = array_map('intval', $telkomsel_upload3g);

        $week_upload3g = array();
        foreach($chart_download2g_super4g as $value) {
         $week_upload3g[] = $value['WEEK'];
        }

        //latency 4g
        $chart_latency4g_super4g = Api::chart_latency4g_super4g(); 

        $three_latency4g = array();
        foreach($chart_latency4g_super4g as $value) {
         $three_latency4g[] = $value['THREE'];
        }
        $threelatency4g = array_map('intval', $three_latency4g);

        $indosat_latency4g = array();
        foreach($chart_latency4g_super4g as $value) {
         $indosat_latency4g[] = $value['INDOSAT'];
        }
        $indosatlatency4g = array_map('intval', $indosat_latency4g);

        $smartfren_latency4g = array();
        foreach($chart_latency4g_super4g as $value) {
         $smartfren_latency4g[] = $value['SMARTFREN'];
        }
        $smartfrenlatency4g = array_map('intval', $smartfren_latency4g); 

        $xl_latency4g = array();
        foreach($chart_latency4g_super4g as $value) {
         $xl_latency4g[] = $value['XL'];
        }
        $xllatency4g = array_map('intval', $xl_latency4g); 

        $telkomsel_latency4g = array();
        foreach($chart_latency4g_super4g as $value) {
         $telkomsel_latency4g[] = $value['TELKOMSEL'];
        }
        $telkomsellatency4g = array_map('intval', $telkomsel_latency4g);

        $week_latency4g = array();
        foreach($chart_latency4g_super4g as $value) {
         $week_latency4g[] = $value['WEEK'];
        }

        //WIFI
        $chart_downloadwifi = Api::chart_downloadwifi(); 

        $three_wifi = array();
        foreach($chart_downloadwifi as $value) {
         $three_wifi[] = $value['THREE'];
        }
        $threewifi = array_map('intval', $three_wifi);

        $indosat_wifi = array();
        foreach($chart_downloadwifi as $value) {
         $indosat_wifi[] = $value['INDOSAT'];
        }
        $indosatwifi = array_map('intval', $indosat_wifi);

        $smartfren_wifi = array();
        foreach($chart_downloadwifi as $value) {
         $smartfren_wifi[] = $value['SMARTFREN'];
        }
        $smartfrenwifi = array_map('intval', $smartfren_wifi); 

        $xl_wifi = array();
        foreach($chart_downloadwifi as $value) {
         $xl_wifi[] = $value['XL'];
        }
        $xlwifi = array_map('intval', $xl_wifi); 

        $telkomsel_wifi = array();
        foreach($chart_downloadwifi as $value) {
         $telkomsel_wifi[] = $value['TELKOMSEL'];
        }
        $telkomselwifi = array_map('intval', $telkomsel_wifi);

        $week_wifi = array();
        foreach($chart_downloadwifi as $value) {
         $week_wifi[] = $value['WEEK'];
        }
        //////////////////
        
        $chart_speedtestdownload_super4g = Api::chart_speedtestdownload_super4g(); 
         foreach($chart_speedtestdownload_super4g as $value) {
         $last_update_chart_speedtestdownload = date_format (new DateTime($value['last_update']), 'd F Y');
        }
        $lastupdate_chart_speedtestdownload = str_replace("'"," ",$last_update_chart_speedtestdownload);

        $subecribers_download = array();
        foreach($chart_speedtestdownload_super4g as $value) {
         $subecribersdownload[] = $value['subscibers'];
        }
        // $subecribersdownload = array_map('intval', $subecribers_download);

        $speed_range_download = array();
        foreach($chart_speedtestdownload_super4g as $value) {
         $speedrangedownload[] = $value['speedrange'];
        }
        // $speedrangedownload = array_map('intval', $speed_range_download);

        $chart_speedtestupload_super4g = Api::chart_speedtestupload_super4g(); 
        $subecribers_upload = array();
        foreach($chart_speedtestupload_super4g as $value) {
         $subecribersupload[] = $value['subecribers'];
        }
        // $subecribersupload = array_map('intval', $subecribers_upload);

        $speed_range_upload = array();
        foreach($chart_speedtestupload_super4g as $value) {
         $speedrangeupload[] = $value['speedrange'];
        }
        // $speedrangeupload = array_map('intval', $speed_range_upload);

        //chart handset tsel
        $chart_handsettsel_super4g = Api::chart_handsettsel_super4g(); 

        $last_update = date_format (new DateTime($chart_handsettsel_super4g['last_update']), 'd F Y');
        $lastupdate = str_replace("'"," ",$last_update);

        $android_tsel = $chart_handsettsel_super4g['android'];
        $iphone_tsel = $chart_handsettsel_super4g['iphone'];
        $windows_tsel = $chart_handsettsel_super4g['windows'];


        //chart handset all
        $chart_handsetall_super4g = Api::chart_handsetall_super4g(); 
        $android_all = $chart_handsetall_super4g['android'];
        $iphone_all = $chart_handsetall_super4g['iphone'];
        $windows_all = $chart_handsetall_super4g['windows'];

        ////////////////////
        //chart inbound
        $chart_inboundroamer = Api::chart_inboundroamer(); 
        foreach($chart_inboundroamer as $value) {
         $last_update_chart_inboundroamer = date_format (new DateTime($value['last_update']), 'd F Y');
        }
        $lastupdate_chart_inboundroamer = str_replace("'"," ",$last_update_chart_inboundroamer);

        $subscribers_ = array();
        foreach($chart_inboundroamer as $value) {
         $subscribers_[] = $value['subscribers'];
        }
        $subscribers = array_map('intval', $subscribers_);

        $throughput_ = array();
        foreach($chart_inboundroamer as $value) {
         $throughput_[] = $value['throughput'];
        }
        $throughput = array_map('intval', $throughput_);

        $vendor = array();
        foreach($chart_inboundroamer as $value) {
         $vendor[] = $value['vendor'];
        }

        $tahun = date('Y');

        return view('super_4g', compact('threedownload4g','indosatdownload4g','smartfrendownload4g','xldownload4g','telkomseldownload4g','week_download4g','threedownload3g','indosatdownload3g','smartfrendownload3g','xldownload3g','telkomseldownload3g','week_download3g','threeupload4g','indosatupload4g','smartfrenupload4g','xlupload4g','telkomselupload4g','week_upload4g','threeupload3g','indosatupload3g','smartfrenupload3g','xlupload3g','telkomselupload3g','week_upload3g','threelatency4g','indosatlatency4g','smartfrenlatency4g','xllatency4g','telkomsellatency4g','week_latency4g','subecribersdownload','speedrangedownload','subecribersupload','speedrangeupload','android_tsel','iphone_tsel','windows_tsel','android_all','iphone_all','windows_all','subscribers','throughput','vendor','tahun','lastupdate_chart_speedtestdownload','lastupdate','lastupdate_chart_inboundroamer','lastupdate_chart_speedtestdownload','threewifi','indosatwifi','smartfrenwifi','xlwifi','telkomselwifi','week_wifi'));
      }
    }

    public function wcc_62()
    {
      if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
      }else{
        return view('wcc_62');
      }
    }

    public function csat_48()
    {
      if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
      }else{
        $region = array('Medan','Palembang','Jakarta Pusat','Jakarta Barat','Jakarta Timur','Jakarta Utara','Bandung','Surabaya','Balikpapan','Makasar');

        return view('csat_48', compact('region'));
      }
    }

    public function operator_benchmark()
    {
      if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
      }else{
        $quadran_target_analytics = Api::quadran_target_analytics(); 
        foreach($quadran_target_analytics as $value) {
         $last_update1 = $value['last_update'];
       }
       $lastupdate_1 = str_replace("'"," ",$last_update1);

       foreach($quadran_target_analytics as $value) {
         $quadrantarget_hijau = $value['hijau'];
       }
       foreach($quadran_target_analytics as $value) {
         $quadrantarget_orange = $value['orange'];
       }
       foreach($quadran_target_analytics as $value) {
         $quadrantarget_red = $value['red'];
       }

       $table_vendorachievements_analytics = Api::table_vendorachievement_analytics(); 
       $table_vendorachievement_analytics = $table_vendorachievements_analytics["data"];
       foreach($table_vendorachievement_analytics as $value) {
         $last_update2 = date_format (new DateTime($value['last_update']), 'd F Y');
       }
       $lastupdate_2 = str_replace("'"," ",$last_update2);

       $table_citiesloses_analytics = Api::table_citieslose_analytics(); 
       $table_citieslose_analytics = $table_citiesloses_analytics["data"];
       foreach($table_citieslose_analytics as $value) {
         $last_update3 = $value['last_update'];
       }
       $lastupdate_3 = str_replace("'"," ",$last_update3);

       return view('operator_benchmark',compact('quadrantarget_hijau','quadrantarget_orange','quadrantarget_red','lastupdate_1','lastupdate_2','lastupdate_3'));
     }
     
   }

   public function operator_benchmark_home()
   {
    if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
    }else{
       $quadran_target_home = Api::quadran_target_home(); 
        foreach($quadran_target_home as $value) {
         $last_update1 = $value['last_update'];
       }
       $lastupdate_1 = str_replace("'"," ",$last_update1);

       foreach($quadran_target_home as $value) {
         $quadrantarget_win_lebihbesar = $value['win_lebihbesar'];
       }
       foreach($quadran_target_home as $value) {
         $quadrantarget_win_lebihkecil = $value['win_lebihkecil'];
       }
       foreach($quadran_target_home as $value) {
         $quadrantarget_lose_lebihbesar = $value['lose_lebihbesar'];
       }
       foreach($quadran_target_home as $value) {
        $quadrantarget_lose_lebihkecil = $value['lose_lebihkecil'];
       }

       $table_citiesloses_home = Api::table_citieslose_home(); 
       $table_citieslose_home = $table_citiesloses_home["data"];
       foreach($table_citieslose_home as $value) {
         $last_update3 = $value['last_update'];
       }
       $lastupdate_3 = str_replace("'"," ",$last_update3);

       $chart_vendorachievement_download = Api::chart_vendorachievement(); 
       foreach($chart_vendorachievement_download as $value) {
         $provinsi[] = $value['provinsi'];
       }
       foreach($chart_vendorachievement_download as $value) {
         $download[] = round($value['download'],2);
       }

       $chart_vendorachievement_upload = Api::chart_vendorachievement_upload(); 
       foreach($chart_vendorachievement_upload as $value) {
         $provinsi_upload[] = $value['provinsi'];
       }
       foreach($chart_vendorachievement_upload as $value) {
         $upload[] = round($value['download'],2);
       }

       $chart_vendorachievement_latency = Api::chart_vendorachievement_latency(); 
       foreach($chart_vendorachievement_latency as $value) {
         $provinsi_latency[] = $value['provinsi'];
       }
       foreach($chart_vendorachievement_latency as $value) {
         $latency[] = round($value['download'],2);
       }

       return view('operator_benchmark_home',compact('lastupdate_1','lastupdate_3','provinsi','download','quadrantarget_win_lebihbesar','quadrantarget_win_lebihkecil','quadrantarget_lose_lebihbesar','quadrantarget_lose_lebihkecil','provinsi_upload','upload','provinsi_latency','latency'));
     }
   }

   public function operator_benchmark_home_month($months)
   {
     if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
      }else{
       $months = $months;

       $quadran_target_home = Api::quadran_target_home_month($months); 
        foreach($quadran_target_home as $value) {
         $last_update1 = $value['last_update'];
       }
       $lastupdate_1 = str_replace("'"," ",$last_update1);

       foreach($quadran_target_home as $value) {
         $quadrantarget_win_lebihbesar = $value['win_lebihbesar'];
       }
       foreach($quadran_target_home as $value) {
         $quadrantarget_win_lebihkecil = $value['win_lebihkecil'];
       }
       foreach($quadran_target_home as $value) {
         $quadrantarget_lose_lebihbesar = $value['lose_lebihbesar'];
       }
       foreach($quadran_target_home as $value) {
        $quadrantarget_lose_lebihkecil = $value['lose_lebihkecil'];
       }

       // $table_vendorachievements_home = Api::table_vendorachievement_home($months); 
       // $table_vendorachievement_home = $table_vendorachievements_home["data"];
       // foreach($table_vendorachievement_home as $value) {
       //   $last_update2 = $value['last_update'];
       // }
       // $lastupdate_2 = str_replace("'"," ",$last_update2);

       $table_citieslose_home_months = Api::table_citieslose_home_month($months); 
       $table_citieslose_home_month = $table_citieslose_home_months["data"];
       foreach($table_citieslose_home_month as $value) {
         $last_update3 = $value['last_update'];
       }
       $lastupdate_3 = str_replace("'"," ",$last_update3);

       $chart_vendorachievement_download = Api::chart_vendorachievement_month($months); 
       foreach($chart_vendorachievement_download as $value) {
         $provinsi[] = $value['provinsi'];
       }
       foreach($chart_vendorachievement_download as $value) {
         $download[] = round($value['download'],2);
       }

       $chart_vendorachievement_upload = Api::chart_vendorachievement_upload_month($months); 
       foreach($chart_vendorachievement_upload as $value) {
         $provinsi_upload[] = $value['provinsi'];
       }
       foreach($chart_vendorachievement_upload as $value) {
         $upload[] = round($value['download'],2);
       }

       $chart_vendorachievement_latency = Api::chart_vendorachievement_latency_month($months); 
       foreach($chart_vendorachievement_latency as $value) {
         $provinsi_latency[] = $value['provinsi'];
       }
       foreach($chart_vendorachievement_latency as $value) {
         $latency[] = round($value['download'],2);
       }

       return view('operator_benchmark_home_month',compact('months','lastupdate_1','lastupdate_3','provinsi','download','quadrantarget_win_lebihbesar','quadrantarget_win_lebihkecil','quadrantarget_lose_lebihbesar','quadrantarget_lose_lebihkecil','provinsi_upload','upload','provinsi_latency','latency'));
     }
   }

   public function operator_benchmark_home_week($week)
   {
    if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
    }else{
       $week = $week;

       $quadran_target_home = Api::quadran_target_home_week($week); 
        foreach($quadran_target_home as $value) {
         $last_update1 = $value['last_update'];
       }
       $lastupdate_1 = str_replace("'"," ",$last_update1);

       foreach($quadran_target_home as $value) {
         $quadrantarget_win_lebihbesar = $value['win_lebihbesar'];
       }
       foreach($quadran_target_home as $value) {
         $quadrantarget_win_lebihkecil = $value['win_lebihkecil'];
       }
       foreach($quadran_target_home as $value) {
         $quadrantarget_lose_lebihbesar = $value['lose_lebihbesar'];
       }
       foreach($quadran_target_home as $value) {
        $quadrantarget_lose_lebihkecil = $value['lose_lebihkecil'];
       }

       $table_vendorachievements_home = Api::table_vendorachievement_home(); 
       $table_vendorachievement_home = $table_vendorachievements_home["data"];
       foreach($table_vendorachievement_home as $value) {
         $last_update2 = date_format (new DateTime($value['last_update']), 'd F Y');
       }
       $lastupdate_2 = str_replace("'"," ",$last_update2);

       $table_citieslose_home_weeks = Api::table_citieslose_home_week($week); 
       $table_citieslose_home_week = $table_citieslose_home_weeks["data"];
       foreach($table_citieslose_home_week as $value) {
         $last_update3 = $value['last_update'];
       }
       $lastupdate_3 = str_replace("'"," ",$last_update3);

       $chart_vendorachievement_download = Api::chart_vendorachievement_week($week); 
       foreach($chart_vendorachievement_download as $value) {
         $provinsi[] = $value['provinsi'];
       }
       foreach($chart_vendorachievement_download as $value) {
         $download[] = round($value['download'],2);
       }

       $chart_vendorachievement_upload = Api::chart_vendorachievement_upload_week($week); 
       foreach($chart_vendorachievement_upload as $value) {
         $provinsi_upload[] = $value['provinsi'];
       }
       foreach($chart_vendorachievement_upload as $value) {
         $upload[] = round($value['download'],2);
       }

       $chart_vendorachievement_latency = Api::chart_vendorachievement_latency_week($week); 
       foreach($chart_vendorachievement_latency as $value) {
         $provinsi_latency[] = $value['provinsi'];
       }
       foreach($chart_vendorachievement_latency as $value) {
         $latency[] = round($value['download'],2);
       }

       return view('operator_benchmark_home_week',compact('week','lastupdate_1','lastupdate_2','lastupdate_3','provinsi','download','quadrantarget_win_lebihbesar','quadrantarget_win_lebihkecil','quadrantarget_lose_lebihbesar','quadrantarget_lose_lebihkecil','provinsi_upload','upload','provinsi_latency','latency'));
     }
   }

   public function region($regional)
   {
    if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
    }else{
      $regional = $regional;

      $chart_speedtestdl_kab_4G = Api::chart_speedtestdl_kab_4G($regional);
      $region_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $region_dl_4g[] = $value['REGOIN'];
      }

      $tsel_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $tsel_dl_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $three_dl_4g[] = round($value['THREE'],2);
      }

      $isat_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $isat_dl_4g[] = round($value['INDOSAT'],2);
      }

      $xl_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $xl_dl_4g[] = round($value['XL'],2);
      }

      $sf_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $sf_dl_4g[] = round($value['SMARTFREN'],2);
      }

      foreach($chart_speedtestdl_kab_4G as $value) {
         $lastupdate_4g = $value['LAST_UPDATE'];
      }
      // $lastupdate_4g = str_replace("'"," ",$last_update_4g);

      //UPLOAD 4G
      $chart_speedtestul_kab_4G = Api::chart_speedtestul_kab_4G($regional);
      $region_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $region_ul_4g[] = $value['REGOIN'];
      }

      $tsel_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $tsel_ul_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $three_ul_4g[] = round($value['THREE'],2);
      }

      $isat_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $isat_ul_4g[] = round($value['INDOSAT'],2);
      }

      $xl_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $xl_ul_4g[] = round($value['XL'],2);
      }

      $sf_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $sf_ul_4g[] = round($value['SMARTFREN'],2);
      }

      //latency 4G
      $chart_speedtestlc_kab_4G = Api::chart_speedtestlc_kab_4G($regional);
      $region_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $region_lc_4g[] = $value['REGOIN'];
      }

      $tsel_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $tsel_lc_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $three_lc_4g[] = round($value['THREE'],2);
      }

      $isat_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $isat_lc_4g[] = round($value['INDOSAT'],2);
      }

      $xl_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $xl_lc_4g[] = round($value['XL'],2);
      }

      $sf_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $sf_lc_4g[] = round($value['SMARTFREN'],2);
      }

      //DOWNLOAD 3G
      $chart_speedtestdl_kab_3G = Api::chart_speedtestdl_kab_3G($regional);
      $region_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $region_dl_3G[] = $value['REGOIN'];
      }

      $tsel_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $tsel_dl_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $three_dl_3G[] = round($value['THREE'],2);
      }

      $isat_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $isat_dl_3G[] = round($value['INDOSAT'],2);
      }

      $xl_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $xl_dl_3G[] = round($value['XL'],2);
      }

      $sf_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $sf_dl_3G[] = round($value['SMARTFREN'],2);
      }

      // foreach($chart_speedtestdl_kab_3G as $value) {
      //    $last_update_3G = date_format (new DateTime($value['LAST_UPDATE']), 'd F Y');
      // }
      // $lastupdate_3G = str_replace("'"," ",$last_update_3G);

      //UPLOAD 3G
      $chart_speedtestul_kab_3G = Api::chart_speedtestul_kab_3G($regional);
      $region_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $region_ul_3G[] = $value['REGOIN'];
      }

      $tsel_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $tsel_ul_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $three_ul_3G[] = round($value['THREE'],2);
      }

      $isat_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $isat_ul_3G[] = round($value['INDOSAT'],2);
      }

      $xl_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $xl_ul_3G[] = round($value['XL'],2);
      }

      $sf_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $sf_ul_3G[] = round($value['SMARTFREN'],2);
      }

      //latency 3G
      $chart_speedtestlc_kab_3G = Api::chart_speedtestlc_kab_3G($regional);
      $region_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $region_lc_3G[] = $value['REGOIN'];
      }

      $tsel_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $tsel_lc_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $three_lc_3G[] = round($value['THREE'],2);
      }

      $isat_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $isat_lc_3G[] = round($value['INDOSAT'],2);
      }

      $xl_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $xl_lc_3G[] = round($value['XL'],2);
      }

      $sf_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $sf_lc_3G[] = round($value['SMARTFREN'],2);
      }


      // $table_kabupatens = Api::table_kabupaten($regional); 
      // $table_kabupaten = $table_kabupatens["data"];
      // foreach($table_kabupaten as $value) {
      //  $last_update_tablekabupaten = $value['last_update'];
      // }

      $table_citieslose_kabupatens = Api::table_citieslose_kabupaten($regional); 
      $table_citieslose_kabupaten = $table_citieslose_kabupatens["data"];
      foreach($table_citieslose_kabupaten as $value) {
       $last_update_tablekabupaten = $value['last_update'];
      }
      // $last_update_tablekabupaten = str_replace("'"," ",$last_update_table_kabupaten);

      return view('region', compact('regional','region_dl_4g','tsel_dl_4g','three_dl_4g','isat_dl_4g','xl_dl_4g','sf_dl_4g','region_ul_4g','tsel_ul_4g','three_ul_4g','isat_ul_4g','xl_ul_4g','sf_ul_4g','region_lc_4g','tsel_lc_4g','three_lc_4g','isat_lc_4g','xl_lc_4g','sf_lc_4g','lastupdate_4g','last_update_tablekabupaten','region_dl_3G','tsel_dl_3G','three_dl_3G','isat_dl_3G','xl_dl_3G','sf_dl_3G','region_ul_3G','tsel_ul_3G','three_ul_3G','isat_ul_3G','xl_ul_3G','sf_ul_3G','region_lc_3G','tsel_lc_3G','three_lc_3G','isat_lc_3G','xl_lc_3G','sf_lc_3G'));
    }
   }

   public function regionmonth($regional, $months)
   {
    if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
    }else{
      $regional = $regional;
      $months = $months;

      $chart_speedtestdl_kab_4G = Api::chart_speedtestdl_kab_4G_month($regional,$months);
      $region_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $region_dl_4g[] = $value['REGOIN'];
      }

      $tsel_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $tsel_dl_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $three_dl_4g[] = round($value['THREE'],2);
      }

      $isat_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $isat_dl_4g[] = round($value['INDOSAT'],2);
      }

      $xl_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $xl_dl_4g[] = round($value['XL'],2);
      }

      $sf_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $sf_dl_4g[] = round($value['SMARTFREN'],2);
      }

      foreach($chart_speedtestdl_kab_4G as $value) {
         $lastupdate_4g = $value['LAST_UPDATE'];
      }
      // $lastupdate_4g = str_replace("'"," ",$last_update_4g);

      //UPLOAD 4G
      $chart_speedtestul_kab_4G = Api::chart_speedtestul_kab_4G_month($regional,$months);
      $region_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $region_ul_4g[] = $value['REGOIN'];
      }

      $tsel_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $tsel_ul_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $three_ul_4g[] = round($value['THREE'],2);
      }

      $isat_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $isat_ul_4g[] = round($value['INDOSAT'],2);
      }

      $xl_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $xl_ul_4g[] = round($value['XL'],2);
      }

      $sf_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $sf_ul_4g[] = round($value['SMARTFREN'],2);
      }

      //latency 4G
      $chart_speedtestlc_kab_4G = Api::chart_speedtestlc_kab_4G_month($regional,$months);
      $region_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $region_lc_4g[] = $value['REGOIN'];
      }

      $tsel_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $tsel_lc_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $three_lc_4g[] = round($value['THREE'],2);
      }

      $isat_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $isat_lc_4g[] = round($value['INDOSAT'],2);
      }

      $xl_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $xl_lc_4g[] = round($value['XL'],2);
      }

      $sf_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $sf_lc_4g[] = round($value['SMARTFREN'],2);
      }

      //DOWNLOAD 3G
      $chart_speedtestdl_kab_3G = Api::chart_speedtestdl_kab_3G_month($regional,$months);
      $region_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $region_dl_3G[] = $value['REGOIN'];
      }

      $tsel_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $tsel_dl_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $three_dl_3G[] = round($value['THREE'],2);
      }

      $isat_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $isat_dl_3G[] = round($value['INDOSAT'],2);
      }

      $xl_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $xl_dl_3G[] = round($value['XL'],2);
      }

      $sf_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $sf_dl_3G[] = round($value['SMARTFREN'],2);
      }

      // foreach($chart_speedtestdl_kab_3G as $value) {
      //    $last_update_3G = date_format (new DateTime($value['LAST_UPDATE']), 'd F Y');
      // }
      // $lastupdate_3G = str_replace("'"," ",$last_update_3G);

      //UPLOAD 3G
      $chart_speedtestul_kab_3G = Api::chart_speedtestul_kab_3G_month($regional,$months);
      $region_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $region_ul_3G[] = $value['REGOIN'];
      }

      $tsel_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $tsel_ul_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $three_ul_3G[] = round($value['THREE'],2);
      }

      $isat_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $isat_ul_3G[] = round($value['INDOSAT'],2);
      }

      $xl_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $xl_ul_3G[] = round($value['XL'],2);
      }

      $sf_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $sf_ul_3G[] = round($value['SMARTFREN'],2);
      }

      //latency 3G
      $chart_speedtestlc_kab_3G = Api::chart_speedtestlc_kab_3G_month($regional,$months);
      $region_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $region_lc_3G[] = $value['REGOIN'];
      }

      $tsel_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $tsel_lc_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $three_lc_3G[] = round($value['THREE'],2);
      }

      $isat_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $isat_lc_3G[] = round($value['INDOSAT'],2);
      }

      $xl_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $xl_lc_3G[] = round($value['XL'],2);
      }

      $sf_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $sf_lc_3G[] = round($value['SMARTFREN'],2);
      }


      $table_citieslose_kabupaten_months = Api::table_citieslose_kabupaten_month($regional,$months); 
      $table_citieslose_kabupaten_month = $table_citieslose_kabupaten_months["data"];
      foreach($table_citieslose_kabupaten_month as $value) {
       $last_update_tablekabupaten = $value['last_update'];
      }
      // $last_update_tablekabupaten = str_replace("'"," ",$last_update_table_kabupaten);

      return view('regionmonth', compact('regional','months','region_dl_4g','tsel_dl_4g','three_dl_4g','isat_dl_4g','xl_dl_4g','sf_dl_4g','region_ul_4g','tsel_ul_4g','three_ul_4g','isat_ul_4g','xl_ul_4g','sf_ul_4g','region_lc_4g','tsel_lc_4g','three_lc_4g','isat_lc_4g','xl_lc_4g','sf_lc_4g','lastupdate_4g','last_update_tablekabupaten','region_dl_3G','tsel_dl_3G','three_dl_3G','isat_dl_3G','xl_dl_3G','sf_dl_3G','region_ul_3G','tsel_ul_3G','three_ul_3G','isat_ul_3G','xl_ul_3G','sf_ul_3G','region_lc_3G','tsel_lc_3G','three_lc_3G','isat_lc_3G','xl_lc_3G','sf_lc_3G'));
    }
   }

   public function regionweek($regional, $week)
   {
    if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
    }else{
      $regional = $regional;
      $week = $week;

      $chart_speedtestdl_kab_4G = Api::chart_speedtestdl_kab_4G_week($regional,$week);
      $region_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $region_dl_4g[] = $value['REGOIN'];
      }

      $tsel_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $tsel_dl_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $three_dl_4g[] = round($value['THREE'],2);
      }

      $isat_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $isat_dl_4g[] = round($value['INDOSAT'],2);
      }

      $xl_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $xl_dl_4g[] = round($value['XL'],2);
      }

      $sf_dl_4g = array();
      foreach($chart_speedtestdl_kab_4G as $value) {
       $sf_dl_4g[] = round($value['SMARTFREN'],2);
      }

      foreach($chart_speedtestdl_kab_4G as $value) {
         $lastupdate_4g = $value['LAST_UPDATE'];
      }
      // $lastupdate_4g = str_replace("'"," ",$last_update_4g);

      //UPLOAD 4G
      $chart_speedtestul_kab_4G = Api::chart_speedtestul_kab_4G_week($regional,$week);
      $region_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $region_ul_4g[] = $value['REGOIN'];
      }

      $tsel_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $tsel_ul_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $three_ul_4g[] = round($value['THREE'],2);
      }

      $isat_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $isat_ul_4g[] = round($value['INDOSAT'],2);
      }

      $xl_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $xl_ul_4g[] = round($value['XL'],2);
      }

      $sf_ul_4g = array();
      foreach($chart_speedtestul_kab_4G as $value) {
       $sf_ul_4g[] = round($value['SMARTFREN'],2);
      }

      //latency 4G
      $chart_speedtestlc_kab_4G = Api::chart_speedtestlc_kab_4G_week($regional,$week);
      $region_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $region_lc_4g[] = $value['REGOIN'];
      }

      $tsel_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $tsel_lc_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $three_lc_4g[] = round($value['THREE'],2);
      }

      $isat_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $isat_lc_4g[] = round($value['INDOSAT'],2);
      }

      $xl_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $xl_lc_4g[] = round($value['XL'],2);
      }

      $sf_lc_4g = array();
      foreach($chart_speedtestlc_kab_4G as $value) {
       $sf_lc_4g[] = round($value['SMARTFREN'],2);
      }

      //DOWNLOAD 3G
      $chart_speedtestdl_kab_3G = Api::chart_speedtestdl_kab_3G_week($regional,$week);
      $region_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $region_dl_3G[] = $value['REGOIN'];
      }

      $tsel_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $tsel_dl_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $three_dl_3G[] = round($value['THREE'],2);
      }

      $isat_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $isat_dl_3G[] = round($value['INDOSAT'],2);
      }

      $xl_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $xl_dl_3G[] = round($value['XL'],2);
      }

      $sf_dl_3G = array();
      foreach($chart_speedtestdl_kab_3G as $value) {
       $sf_dl_3G[] = round($value['SMARTFREN'],2);
      }

      // foreach($chart_speedtestdl_kab_3G as $value) {
      //    $last_update_3G = date_format (new DateTime($value['LAST_UPDATE']), 'd F Y');
      // }
      // $lastupdate_3G = str_replace("'"," ",$last_update_3G);

      //UPLOAD 3G
      $chart_speedtestul_kab_3G = Api::chart_speedtestul_kab_3G_week($regional,$week);
      $region_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $region_ul_3G[] = $value['REGOIN'];
      }

      $tsel_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $tsel_ul_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $three_ul_3G[] = round($value['THREE'],2);
      }

      $isat_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $isat_ul_3G[] = round($value['INDOSAT'],2);
      }

      $xl_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $xl_ul_3G[] = round($value['XL'],2);
      }

      $sf_ul_3G = array();
      foreach($chart_speedtestul_kab_3G as $value) {
       $sf_ul_3G[] = round($value['SMARTFREN'],2);
      }

      //latency 3G
      $chart_speedtestlc_kab_3G = Api::chart_speedtestlc_kab_3G_week($regional,$week);
      $region_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $region_lc_3G[] = $value['REGOIN'];
      }

      $tsel_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $tsel_lc_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $three_lc_3G[] = round($value['THREE'],2);
      }

      $isat_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $isat_lc_3G[] = round($value['INDOSAT'],2);
      }

      $xl_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $xl_lc_3G[] = round($value['XL'],2);
      }

      $sf_lc_3G = array();
      foreach($chart_speedtestlc_kab_3G as $value) {
       $sf_lc_3G[] = round($value['SMARTFREN'],2);
      }


      $table_citieslose_kabupaten_weeks = Api::table_citieslose_kabupaten_week($regional,$week); 
      $table_citieslose_kabupaten_week = $table_citieslose_kabupaten_weeks["data"];
      foreach($table_citieslose_kabupaten_week as $value) {
       $last_update_tablekabupaten = $value['last_update'];
      }
      // $last_update_tablekabupaten = str_replace("'"," ",$last_update_table_kabupaten);

      return view('regionweek', compact('regional','week','region_dl_4g','tsel_dl_4g','three_dl_4g','isat_dl_4g','xl_dl_4g','sf_dl_4g','region_ul_4g','tsel_ul_4g','three_ul_4g','isat_ul_4g','xl_ul_4g','sf_ul_4g','region_lc_4g','tsel_lc_4g','three_lc_4g','isat_lc_4g','xl_lc_4g','sf_lc_4g','lastupdate_4g','last_update_tablekabupaten','region_dl_3G','tsel_dl_3G','three_dl_3G','isat_dl_3G','xl_dl_3G','sf_dl_3G','region_ul_3G','tsel_ul_3G','three_ul_3G','isat_ul_3G','xl_ul_3G','sf_ul_3G','region_lc_3G','tsel_lc_3G','three_lc_3G','isat_lc_3G','xl_lc_3G','sf_lc_3G'));
    }
   }

   public function city($kabupaten)
   {
    if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
    }else{
      $kabupaten = $kabupaten;

      $chart_speedtestdl_des_4G = Api::chart_speedtestdl_des_4G($kabupaten);
      $region_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $region_dl_4g[] = $value['CITY'];
      }

      $tsel_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $tsel_dl_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $three_dl_4g[] = round($value['THREE'],2);
      }

      $isat_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $isat_dl_4g[] = round($value['INDOSAT'],2);
      }

      $xl_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $xl_dl_4g[] = round($value['XL'],2);
      }

      $sf_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $sf_dl_4g[] = round($value['SMARTFREN'],2);
      }

      foreach($chart_speedtestdl_des_4G as $value) {
         $lastupdate_4g = $value['LAST_UPDATE'];
      }
      // $lastupdate_4g = str_replace("'"," ",$last_update_4g);

      //UPLOAD 4G
      $chart_speedtestul_des_4G = Api::chart_speedtestul_des_4G($kabupaten);
      $region_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $region_ul_4g[] = $value['CITY'];
      }

      $tsel_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $tsel_ul_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $three_ul_4g[] = round($value['THREE'],2);
      }

      $isat_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $isat_ul_4g[] = round($value['INDOSAT'],2);
      }

      $xl_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $xl_ul_4g[] = round($value['XL'],2);
      }

      $sf_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $sf_ul_4g[] = round($value['SMARTFREN'],2);
      }

      //latency 4G
      $chart_speedtestlc_des_4G = Api::chart_speedtestlc_des_4G($kabupaten);
      $region_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $region_lc_4g[] = $value['CITY'];
      }

      $tsel_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $tsel_lc_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $three_lc_4g[] = round($value['THREE'],2);
      }

      $isat_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $isat_lc_4g[] = round($value['INDOSAT'],2);
      }

      $xl_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $xl_lc_4g[] = round($value['XL'],2);
      }

      $sf_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $sf_lc_4g[] = round($value['SMARTFREN'],2);
      }

      //DOWNLOAD 3G
      $chart_speedtestdl_des_3G = Api::chart_speedtestdl_des_3G($kabupaten);
      $region_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $region_dl_3G[] = $value['CITY'];
      }

      $tsel_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $tsel_dl_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $three_dl_3G[] = round($value['THREE'],2);
      }

      $isat_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $isat_dl_3G[] = round($value['INDOSAT'],2);
      }

      $xl_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $xl_dl_3G[] = round($value['XL'],2);
      }

      $sf_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $sf_dl_3G[] = round($value['SMARTFREN'],2);
      }

      // foreach($chart_speedtestdl_kab_3G as $value) {
      //    $last_update_3G = date_format (new DateTime($value['LAST_UPDATE']), 'd F Y');
      // }
      // $lastupdate_3G = str_replace("'"," ",$last_update_3G);

      //UPLOAD 3G
      $chart_speedtestul_des_3G = Api::chart_speedtestul_des_3G($kabupaten);
      $region_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $region_ul_3G[] = $value['CITY'];
      }

      $tsel_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $tsel_ul_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $three_ul_3G[] = round($value['THREE'],2);
      }

      $isat_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $isat_ul_3G[] = round($value['INDOSAT'],2);
      }

      $xl_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $xl_ul_3G[] = round($value['XL'],2);
      }

      $sf_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $sf_ul_3G[] = round($value['SMARTFREN'],2);
      }

      //latency 3G
      $chart_speedtestlc_des_3G = Api::chart_speedtestlc_des_3G($kabupaten);
      $region_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $region_lc_3G[] = $value['CITY'];
      }

      $tsel_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $tsel_lc_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $three_lc_3G[] = round($value['THREE'],2);
      }

      $isat_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $isat_lc_3G[] = round($value['INDOSAT'],2);
      }

      $xl_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $xl_lc_3G[] = round($value['XL'],2);
      }

      $sf_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $sf_lc_3G[] = round($value['SMARTFREN'],2);
      }

      $table_desas = Api::table_desa($kabupaten); 
      $table_desa = $table_desas["data"];
      foreach($table_desa as $value) {
       $last_update_tabledesa = $value['last_update'];
      }
      // $last_update_tabledesa = str_replace("'"," ",$last_update_table_desa);
      // $get = collect(Api::dataSysinfo($regional));

       $chart_speedtestdownload = Api::chart_speedtestdownload_city($kabupaten); 
        foreach($chart_speedtestdownload as $value) {
         $lastupdate_chart_speedtestdownload = $value['last_update'];
       }

       $subecribers_download = array();
       foreach($chart_speedtestdownload as $value) {
         $subecribers_download[] = $value['subecribers'];
       }
       $subecribersdownload = array_map('intval', $subecribers_download);

       $speed_range_download = array();
       foreach($chart_speedtestdownload as $value) {
         $speedrangedownload[] = $value['speedrange'];
       }
          // $speedrangedownload = array_map('intval', $speed_range_download);

       $chart_speedtestupload = Api::chart_speedtestupload_city($kabupaten); 
       $subecribers_upload = array();
       foreach($chart_speedtestupload as $value) {
         $subecribers_upload[] = $value['subecribers'];
       }
       $subecribersupload = array_map('intval', $subecribers_upload);

       $speed_range_upload = array();
       foreach($chart_speedtestupload as $value) {
         $speedrangeupload[] = $value['speedrange'];
       }
      return view('city', compact('kabupaten','region_dl_4g','tsel_dl_4g','three_dl_4g','isat_dl_4g','xl_dl_4g','sf_dl_4g','region_ul_4g','tsel_ul_4g','three_ul_4g','isat_ul_4g','xl_ul_4g','sf_ul_4g','region_lc_4g','tsel_lc_4g','three_lc_4g','isat_lc_4g','xl_lc_4g','sf_lc_4g','lastupdate_4g','last_update_tabledesa','region_dl_3G','tsel_dl_3G','three_dl_3G','isat_dl_3G','xl_dl_3G','sf_dl_3G','region_ul_3G','tsel_ul_3G','three_ul_3G','isat_ul_3G','xl_ul_3G','sf_ul_3G','region_lc_3G','tsel_lc_3G','three_lc_3G','isat_lc_3G','xl_lc_3G','sf_lc_3G','subecribersdownload','speedrangedownload','subecribersupload','speedrangeupload','lastupdate_chart_speedtestdownload'));
    }
   }

   public function citymonth($kabupaten, $months)
   {
    if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
    }else{
      $kabupaten = $kabupaten;
      $months = $months;

      $chart_speedtestdl_des_4G = Api::chart_speedtestdl_des_4G_month($kabupaten, $months);
      $region_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $region_dl_4g[] = $value['CITY'];
      }

      $tsel_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $tsel_dl_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $three_dl_4g[] = round($value['THREE'],2);
      }

      $isat_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $isat_dl_4g[] = round($value['INDOSAT'],2);
      }

      $xl_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $xl_dl_4g[] = round($value['XL'],2);
      }

      $sf_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $sf_dl_4g[] = round($value['SMARTFREN'],2);
      }

      foreach($chart_speedtestdl_des_4G as $value) {
         $lastupdate_4g = $value['LAST_UPDATE'];
      }
      // $lastupdate_4g = str_replace("'"," ",$last_update_4g);

      //UPLOAD 4G
      $chart_speedtestul_des_4G = Api::chart_speedtestul_des_4G_month($kabupaten, $months);
      $region_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $region_ul_4g[] = $value['CITY'];
      }

      $tsel_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $tsel_ul_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $three_ul_4g[] = round($value['THREE'],2);
      }

      $isat_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $isat_ul_4g[] = round($value['INDOSAT'],2);
      }

      $xl_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $xl_ul_4g[] = round($value['XL'],2);
      }

      $sf_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $sf_ul_4g[] = round($value['SMARTFREN'],2);
      }

      //latency 4G
      $chart_speedtestlc_des_4G = Api::chart_speedtestlc_des_4G_month($kabupaten,$months);
      $region_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $region_lc_4g[] = $value['CITY'];
      }

      $tsel_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $tsel_lc_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $three_lc_4g[] = round($value['THREE'],2);
      }

      $isat_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $isat_lc_4g[] = round($value['INDOSAT'],2);
      }

      $xl_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $xl_lc_4g[] = round($value['XL'],2);
      }

      $sf_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $sf_lc_4g[] = round($value['SMARTFREN'],2);
      }

      //DOWNLOAD 3G
      $chart_speedtestdl_des_3G = Api::chart_speedtestdl_des_3G_month($kabupaten,$months);
      $region_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $region_dl_3G[] = $value['CITY'];
      }

      $tsel_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $tsel_dl_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $three_dl_3G[] = round($value['THREE'],2);
      }

      $isat_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $isat_dl_3G[] = round($value['INDOSAT'],2);
      }

      $xl_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $xl_dl_3G[] = round($value['XL'],2);
      }

      $sf_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $sf_dl_3G[] = round($value['SMARTFREN'],2);
      }

      // foreach($chart_speedtestdl_kab_3G as $value) {
      //    $last_update_3G = date_format (new DateTime($value['LAST_UPDATE']), 'd F Y');
      // }
      // $lastupdate_3G = str_replace("'"," ",$last_update_3G);

      //UPLOAD 3G
      $chart_speedtestul_des_3G = Api::chart_speedtestul_des_3G_month($kabupaten,$months);
      $region_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $region_ul_3G[] = $value['CITY'];
      }

      $tsel_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $tsel_ul_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $three_ul_3G[] = round($value['THREE'],2);
      }

      $isat_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $isat_ul_3G[] = round($value['INDOSAT'],2);
      }

      $xl_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $xl_ul_3G[] = round($value['XL'],2);
      }

      $sf_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $sf_ul_3G[] = round($value['SMARTFREN'],2);
      }

      //latency 3G
      $chart_speedtestlc_des_3G = Api::chart_speedtestlc_des_3G_month($kabupaten,$months);
      $region_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $region_lc_3G[] = $value['CITY'];
      }

      $tsel_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $tsel_lc_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $three_lc_3G[] = round($value['THREE'],2);
      }

      $isat_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $isat_lc_3G[] = round($value['INDOSAT'],2);
      }

      $xl_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $xl_lc_3G[] = round($value['XL'],2);
      }

      $sf_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $sf_lc_3G[] = round($value['SMARTFREN'],2);
      }

      $table_desas = Api::table_desa_month($kabupaten,$months); 
      $table_desa = $table_desas["data"];
      foreach($table_desa as $value) {
       $last_update_tabledesa = $value['last_update'];
      }
      // $last_update_tabledesa = str_replace("'"," ",$last_update_table_desa);
      // $get = collect(Api::dataSysinfo($regional));

        $chart_speedtestdownload = Api::chart_speedtestdownload(); 
        foreach($chart_speedtestdownload as $value) {
         $last_update_chart_speedtestdownload = $value['last_update'];
       }
       $lastupdate_chart_speedtestdownload = str_replace("'"," ",$last_update_chart_speedtestdownload);

       $subecribers_download = array();
       foreach($chart_speedtestdownload as $value) {
         $subecribers_download[] = $value['subecribers'];
       }
       $subecribersdownload = array_map('intval', $subecribers_download);

       $speed_range_download = array();
       foreach($chart_speedtestdownload as $value) {
         $speedrangedownload[] = $value['speedrange'];
       }
          // $speedrangedownload = array_map('intval', $speed_range_download);

       $chart_speedtestupload = Api::chart_speedtestupload(); 
       $subecribers_upload = array();
       foreach($chart_speedtestupload as $value) {
         $subecribers_upload[] = $value['subecribers'];
       }
       $subecribersupload = array_map('intval', $subecribers_upload);

       $speed_range_upload = array();
       foreach($chart_speedtestupload as $value) {
         $speedrangeupload[] = $value['speedrange'];
       }
      return view('citymonth', compact('kabupaten','months','region_dl_4g','tsel_dl_4g','three_dl_4g','isat_dl_4g','xl_dl_4g','sf_dl_4g','region_ul_4g','tsel_ul_4g','three_ul_4g','isat_ul_4g','xl_ul_4g','sf_ul_4g','region_lc_4g','tsel_lc_4g','three_lc_4g','isat_lc_4g','xl_lc_4g','sf_lc_4g','lastupdate_4g','last_update_tabledesa','region_dl_3G','tsel_dl_3G','three_dl_3G','isat_dl_3G','xl_dl_3G','sf_dl_3G','region_ul_3G','tsel_ul_3G','three_ul_3G','isat_ul_3G','xl_ul_3G','sf_ul_3G','region_lc_3G','tsel_lc_3G','three_lc_3G','isat_lc_3G','xl_lc_3G','sf_lc_3G','subecribersdownload','speedrangedownload','subecribersupload','speedrangeupload'));
    }
   }

   public function cityweek($kabupaten, $week)
   {
    if (isset($_SESSION["username"]) != 'admin') {
          return Redirect::to('http://10.54.36.49/dashboard-monitoring/public/login/')->with('failed','Login gagal');
    }else{
      $kabupaten = $kabupaten;
      $week = $week;

      $chart_speedtestdl_des_4G = Api::chart_speedtestdl_des_4G_week($kabupaten, $week);
      $region_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $region_dl_4g[] = $value['CITY'];
      }

      $tsel_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $tsel_dl_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $three_dl_4g[] = round($value['THREE'],2);
      }

      $isat_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $isat_dl_4g[] = round($value['INDOSAT'],2);
      }

      $xl_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $xl_dl_4g[] = round($value['XL'],2);
      }

      $sf_dl_4g = array();
      foreach($chart_speedtestdl_des_4G as $value) {
       $sf_dl_4g[] = round($value['SMARTFREN'],2);
      }

      foreach($chart_speedtestdl_des_4G as $value) {
         $lastupdate_4g = $value['LAST_UPDATE'];
      }
      // $lastupdate_4g = str_replace("'"," ",$last_update_4g);

      //UPLOAD 4G
      $chart_speedtestul_des_4G = Api::chart_speedtestul_des_4G_week($kabupaten, $week);
      $region_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $region_ul_4g[] = $value['CITY'];
      }

      $tsel_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $tsel_ul_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $three_ul_4g[] = round($value['THREE'],2);
      }

      $isat_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $isat_ul_4g[] = round($value['INDOSAT'],2);
      }

      $xl_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $xl_ul_4g[] = round($value['XL'],2);
      }

      $sf_ul_4g = array();
      foreach($chart_speedtestul_des_4G as $value) {
       $sf_ul_4g[] = round($value['SMARTFREN'],2);
      }

      //latency 4G
      $chart_speedtestlc_des_4G = Api::chart_speedtestlc_des_4G_week($kabupaten,$week);
      $region_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $region_lc_4g[] = $value['CITY'];
      }

      $tsel_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $tsel_lc_4g[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $three_lc_4g[] = round($value['THREE'],2);
      }

      $isat_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $isat_lc_4g[] = round($value['INDOSAT'],2);
      }

      $xl_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $xl_lc_4g[] = round($value['XL'],2);
      }

      $sf_lc_4g = array();
      foreach($chart_speedtestlc_des_4G as $value) {
       $sf_lc_4g[] = round($value['SMARTFREN'],2);
      }

      //DOWNLOAD 3G
      $chart_speedtestdl_des_3G = Api::chart_speedtestdl_des_3G_week($kabupaten,$week);
      $region_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $region_dl_3G[] = $value['CITY'];
      }

      $tsel_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $tsel_dl_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $three_dl_3G[] = round($value['THREE'],2);
      }

      $isat_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $isat_dl_3G[] = round($value['INDOSAT'],2);
      }

      $xl_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $xl_dl_3G[] = round($value['XL'],2);
      }

      $sf_dl_3G = array();
      foreach($chart_speedtestdl_des_3G as $value) {
       $sf_dl_3G[] = round($value['SMARTFREN'],2);
      }

      // foreach($chart_speedtestdl_kab_3G as $value) {
      //    $last_update_3G = date_format (new DateTime($value['LAST_UPDATE']), 'd F Y');
      // }
      // $lastupdate_3G = str_replace("'"," ",$last_update_3G);

      //UPLOAD 3G
      $chart_speedtestul_des_3G = Api::chart_speedtestul_des_3G_week($kabupaten,$week);
      $region_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $region_ul_3G[] = $value['CITY'];
      }

      $tsel_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $tsel_ul_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $three_ul_3G[] = round($value['THREE'],2);
      }

      $isat_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $isat_ul_3G[] = round($value['INDOSAT'],2);
      }

      $xl_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $xl_ul_3G[] = round($value['XL'],2);
      }

      $sf_ul_3G = array();
      foreach($chart_speedtestul_des_3G as $value) {
       $sf_ul_3G[] = round($value['SMARTFREN'],2);
      }

      //latency 3G
      $chart_speedtestlc_des_3G = Api::chart_speedtestlc_des_3G_week($kabupaten,$week);
      $region_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $region_lc_3G[] = $value['CITY'];
      }

      $tsel_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $tsel_lc_3G[] = round($value['TELKOMSEL'],2);
      }

      $three_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $three_lc_3G[] = round($value['THREE'],2);
      }

      $isat_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $isat_lc_3G[] = round($value['INDOSAT'],2);
      }

      $xl_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $xl_lc_3G[] = round($value['XL'],2);
      }

      $sf_lc_3G = array();
      foreach($chart_speedtestlc_des_3G as $value) {
       $sf_lc_3G[] = round($value['SMARTFREN'],2);
      }

      $table_desas = Api::table_desa_week($kabupaten,$week); 
      $table_desa = $table_desas["data"];
      foreach($table_desa as $value) {
       $last_update_tabledesa = $value['last_update'];
      }
      // $last_update_tabledesa = str_replace("'"," ",$last_update_table_desa);
      // $get = collect(Api::dataSysinfo($regional));

       $chart_speedtestdownload = Api::chart_speedtestdownload(); 
        foreach($chart_speedtestdownload as $value) {
         $last_update_chart_speedtestdownload = $value['last_update'];
       }
       $lastupdate_chart_speedtestdownload = str_replace("'"," ",$last_update_chart_speedtestdownload);

       $subecribers_download = array();
       foreach($chart_speedtestdownload as $value) {
         $subecribers_download[] = $value['subecribers'];
       }
       $subecribersdownload = array_map('intval', $subecribers_download);

       $speed_range_download = array();
       foreach($chart_speedtestdownload as $value) {
         $speedrangedownload[] = $value['speedrange'];
       }
          // $speedrangedownload = array_map('intval', $speed_range_download);

       $chart_speedtestupload = Api::chart_speedtestupload(); 
       $subecribers_upload = array();
       foreach($chart_speedtestupload as $value) {
         $subecribers_upload[] = $value['subecribers'];
       }
       $subecribersupload = array_map('intval', $subecribers_upload);

       $speed_range_upload = array();
       foreach($chart_speedtestupload as $value) {
         $speedrangeupload[] = $value['speedrange'];
       }
      return view('cityweek', compact('kabupaten','week','region_dl_4g','tsel_dl_4g','three_dl_4g','isat_dl_4g','xl_dl_4g','sf_dl_4g','region_ul_4g','tsel_ul_4g','three_ul_4g','isat_ul_4g','xl_ul_4g','sf_ul_4g','region_lc_4g','tsel_lc_4g','three_lc_4g','isat_lc_4g','xl_lc_4g','sf_lc_4g','lastupdate_4g','last_update_tabledesa','region_dl_3G','tsel_dl_3G','three_dl_3G','isat_dl_3G','xl_dl_3G','sf_dl_3G','region_ul_3G','tsel_ul_3G','three_ul_3G','isat_ul_3G','xl_ul_3G','sf_ul_3G','region_lc_3G','tsel_lc_3G','three_lc_3G','isat_lc_3G','xl_lc_3G','sf_lc_3G','subecribersdownload','speedrangedownload','subecribersupload','speedrangeupload'));
    }
   }
   
}
