<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
 
class Api extends Model 
{      
    // const URL_API_LICENSE = 'http://10.54.36.49/api-license2/public/';  
      
    const URL_API_OOKLA = 'http://10.54.36.49/api-ookla/public/';  

    static function chart_download4g() 
    {
        $url = self::URL_API_OOKLA.'home/chart_download_history4g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_download4g");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_download3g()  
    {
        $url = self::URL_API_OOKLA.'home/chart_download_history3g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_download3g");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_upload4g() 
    {
        $url = self::URL_API_OOKLA.'home/chart_upload_history4g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_upload4g");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_upload3g()  
    {
        $url = self::URL_API_OOKLA.'home/chart_upload_history3g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_upload3g");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_latency4g() 
    {
        $url = self::URL_API_OOKLA.'home/chart_latency_history4g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_latency4g");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_latency3g() 
    {
        $url = self::URL_API_OOKLA.'home/chart_latency_history3g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_latency3g");
        }

        return json_decode($res->getBody(), true);
    }
////
    static function chart_download4g_current() 
    {
        $url = self::URL_API_OOKLA.'home/chart_download_current4g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_download4g_current");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_download3g_current()  
    {
        $url = self::URL_API_OOKLA.'home/chart_download_current3g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_download3g_current");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_upload4g_current() 
    {
        $url = self::URL_API_OOKLA.'home/chart_upload_current4g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_upload4g_current");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_upload3g_current()  
    {
        $url = self::URL_API_OOKLA.'home/chart_upload_current3g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_upload3g_current");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_latency4g_current() 
    {
        $url = self::URL_API_OOKLA.'home/chart_latency_current4g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_latency4g_current");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_latency3g_current() 
    {
        $url = self::URL_API_OOKLA.'home/chart_latency_current3g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_latency3g_current");
        }

        return json_decode($res->getBody(), true);
    }
    ////

    static function chart_speedtestdownload()  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdownload';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdownload");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestupload()  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestupload';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestupload");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestdownload_city($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdownloadkabupaten/'.$kabupaten;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdownload_city");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestupload_city($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestuploadkabupaten/'.$kabupaten;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestupload_city");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestdownload_citymonth($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdownloadkabupaten/'.$kabupaten;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdownload_citymonth");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestupload_citymonth($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestuploadkabupaten/'.$kabupaten;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestupload_citymonth");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestdownload_cityweek($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdownloadkabupaten/'.$kabupaten;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdownload_cityweek");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestupload_cityweek($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestuploadkabupaten/'.$kabupaten;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestupload_cityweek");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_handsettsel()  
    {
        $url = self::URL_API_OOKLA.'home/chart_handsettsel';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_handsettsel");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_handsetall()  
    {
        $url = self::URL_API_OOKLA.'home/chart_handsetall';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_handsetall");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_inboundroamer()  
    {
        $url = self::URL_API_OOKLA.'home/chart_inboundroamer';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_inboundroamer");
        }

        return json_decode($res->getBody(), true);
    }

    //super 4g

    static function chart_download4g_super4g() 
    {
        $url = self::URL_API_OOKLA.'super4g/chart_download4g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_download4g");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_download3g_super4g()  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_download3g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_download3g");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_upload4g_super4g() 
    {
        $url = self::URL_API_OOKLA.'super4g/chart_upload4g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_upload4g");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_download2g_super4g()  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_download2g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_download2g");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_latency4g_super4g() 
    {
        $url = self::URL_API_OOKLA.'super4g/chart_latency4g';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_latency4g");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_downloadwifi() 
    {
        $url = self::URL_API_OOKLA.'super4g/chart_downloadwifi';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_wifi");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestdownload_super4g()  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_speedtestdownload';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdownload");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestupload_super4g()  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_speedtestupload';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestupload");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_handsettsel_super4g()  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_handsettsel';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_handsettsel");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_handsetall_super4g()  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_handsetall';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_handsetall");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_inboundroamer_super4g()  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_inboundroamer';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_inboundroamer");
        }

        return json_decode($res->getBody(), true);
    }

    static function quadran_target_home()  
    {
        $url = self::URL_API_OOKLA.'super4g/quadran_target';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi quadran_target_home");
        }

        return json_decode($res->getBody(), true);
    }

    static function quadran_target_home_month($months)  
    {
        $url = self::URL_API_OOKLA.'super4g/quadran_target/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi quadran_target_home_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function quadran_target_home_week($week)  
    {
        $url = self::URL_API_OOKLA.'super4g/quadran_target/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi quadran_target_home_week");
        }

        return json_decode($res->getBody(), true);
    }

    static function quadran_target_analytics()  
    {
        $url = self::URL_API_OOKLA.'analytics/quadran_target';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi quadran_target_analytics");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_regional4g_download()  
    {
        $url = self::URL_API_OOKLA.'home/chart_regional4g/download';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_regional4g_download");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_regional4g_upload()  
    {
        $url = self::URL_API_OOKLA.'home/chart_regional4g/upload';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_regional4g_upload");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_regional4g_latency()  
    {
        $url = self::URL_API_OOKLA.'home/chart_regional4g/latency';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_regional4g_latency");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_regional3g_download()  
    {
        $url = self::URL_API_OOKLA.'home/chart_regional3g/download';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_regional3g_download");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_regional3g_upload()  
    {
        $url = self::URL_API_OOKLA.'home/chart_regional3g/upload';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_regional3g_upload");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_regional3g_latency()  
    {
        $url = self::URL_API_OOKLA.'home/chart_regional3g/latency';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_regional3g_latency");
        }

        return json_decode($res->getBody(), true);
    }

    static function table_vendorachievement_home()  
    {
        $url = self::URL_API_OOKLA.'super4g/table_vendorachievement';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_vendorachievement_home");
        }

        return json_decode($res->getBody(), true);
    } 

    static function table_vendorachievement_analytics()  
    {
        $url = self::URL_API_OOKLA.'analytics/table_vendorachievement';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_vendorachievement_analytics");
        }

        return json_decode($res->getBody(), true);
    } 

    static function table_citieslose_home()  
    {
        $url = self::URL_API_OOKLA.'super4g/table_citieslose_quadranookla';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_citieslose_quadranookla");
        }

        return json_decode($res->getBody(), true);
    } 

    static function table_citieslose_home_month($months)  
    {
        $url = self::URL_API_OOKLA.'super4g/table_citieslose_quadranookla/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_citieslose_home_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function table_citieslose_home_week($week)  
    {
        $url = self::URL_API_OOKLA.'super4g/table_citieslose_quadranookla/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_citieslose_home_week");
        }

        return json_decode($res->getBody(), true);
    } 

    static function table_citieslose_analytics()  
    {
        $url = self::URL_API_OOKLA.'super4g/table_citieslose_analytics';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_citieslose_analytics");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestdl_kab_4G($regional)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_kab/'.$regional.'/4G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_kab_4G");
        }

        return json_decode($res->getBody(), true);
    }

     static function chart_speedtestdl_kab_4G_month($regional,$months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_kab/'.$regional.'/4G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_kab_4G_month");
        }

        return json_decode($res->getBody(), true);
    }  

     static function chart_speedtestdl_kab_4G_week($regional,$week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_kab/'.$regional.'/4G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_kab_4G_week");
        }

        return json_decode($res->getBody(), true);
    }   

    static function chart_speedtestdl_kab_3G($regional)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_kab/'.$regional.'/3G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_kab_3G");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestdl_kab_3G_month($regional,$months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_kab/'.$regional.'/3G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_kab_3G_month");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestdl_kab_3G_week($regional,$week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_kab/'.$regional.'/3G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_kab_3G_week");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestul_kab_4G($regional)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_kab/'.$regional.'/4G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_kab_4G");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestul_kab_4G_month($regional,$months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_kab/'.$regional.'/4G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_kab_4G_month");
        }

        return json_decode($res->getBody(), true);
    } 

     static function chart_speedtestul_kab_4G_week($regional,$week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_kab/'.$regional.'/4G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_kab_4G_week");
        }

        return json_decode($res->getBody(), true);
    }     

    static function chart_speedtestul_kab_3G($regional)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_kab/'.$regional.'/3G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_kab_3G");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestul_kab_3G_month($regional,$months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_kab/'.$regional.'/3G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_kab_3G_month");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestul_kab_3G_week($regional,$week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_kab/'.$regional.'/3G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_kab_3G_week");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestlc_kab_4G($regional)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_kab/'.$regional.'/4G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_kab_4G");
        }

        return json_decode($res->getBody(), true);
    }    

    static function chart_speedtestlc_kab_4G_month($regional,$months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_kab/'.$regional.'/4G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_kab_4G_month");
        }

        return json_decode($res->getBody(), true);
    } 

     static function chart_speedtestlc_kab_4G_week($regional,$week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_kab/'.$regional.'/4G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_kab_4G_week");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestlc_kab_3G($regional)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_kab/'.$regional.'/3G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_kab_3G");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestlc_kab_3G_month($regional, $months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_kab/'.$regional.'/3G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_kab_3G_month");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestlc_kab_3G_week($regional, $week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_kab/'.$regional.'/3G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_kab_3G_week");
        }

        return json_decode($res->getBody(), true);
    }

    //CITY
    static function chart_speedtestdl_des_4G($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_des/'.$kabupaten.'/4G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_des_4G");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestdl_des_4G_month($kabupaten,$months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_des/'.$kabupaten.'/4G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_des_4G_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestdl_des_4G_week($kabupaten,$week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_des/'.$kabupaten.'/4G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_des_4G_week");
        }

        return json_decode($res->getBody(), true);
    }     

    static function chart_speedtestdl_des_3G($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_des/'.$kabupaten.'/3G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_des_3G");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestdl_des_3G_month($kabupaten, $months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_des/'.$kabupaten.'/3G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_des_3G_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestdl_des_3G_week($kabupaten, $week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestdl_des/'.$kabupaten.'/3G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestdl_des_3G_week");
        }

        return json_decode($res->getBody(), true);
    }  

    static function chart_speedtestul_des_4G($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_des/'.$kabupaten.'/4G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_des_4G");
        }

        return json_decode($res->getBody(), true);
    }  

    static function chart_speedtestul_des_4G_month($kabupaten,$months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_des/'.$kabupaten.'/4G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_des_4G_month");
        }

        return json_decode($res->getBody(), true);
    }     

    static function chart_speedtestul_des_4G_week($kabupaten,$week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_des/'.$kabupaten.'/4G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_des_4G_week");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestul_des_3G($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_des/'.$kabupaten.'/3G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_des_3G");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestul_des_3G_month($kabupaten,$months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_des/'.$kabupaten.'/3G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_des_3G_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestul_des_3G_week($kabupaten,$week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestul_des/'.$kabupaten.'/3G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestul_des_3G_week");
        }

        return json_decode($res->getBody(), true);
    } 

    static function chart_speedtestlc_des_4G($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_des/'.$kabupaten.'/4G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_des_4G");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestlc_des_4G_month($kabupaten, $months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_des/'.$kabupaten.'/4G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_des_4G_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestlc_des_4G_week($kabupaten, $week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_des/'.$kabupaten.'/4G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_des_4G_week");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestlc_des_3G($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_des/'.$kabupaten.'/3G';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_des_3G");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestlc_des_3G_month($kabupaten, $months)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_des/'.$kabupaten.'/3G/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_des_3G_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_speedtestlc_des_3G_week($kabupaten, $week)  
    {
        $url = self::URL_API_OOKLA.'home/chart_speedtestlc_des/'.$kabupaten.'/3G/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_speedtestlc_des_3G_week");
        }

        return json_decode($res->getBody(), true);
    } 
    //

    static function table_desa($kabupaten)  
    {
        $url = self::URL_API_OOKLA.'super4g/table_citieslose_desa/'.$kabupaten;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_desa");
        }

        return json_decode($res->getBody(), true);
    }

    static function table_desa_month($kabupaten, $months)  
    {
        $url = self::URL_API_OOKLA.'super4g/table_citieslose_desa/'.$kabupaten.'/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_desa_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function table_desa_week($kabupaten, $week)  
    {
        $url = self::URL_API_OOKLA.'super4g/table_citieslose_desa/'.$kabupaten.'/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_desa_week");
        }

        return json_decode($res->getBody(), true);
    }

    static function table_kabupaten($regional)  
    {
        $url = self::URL_API_OOKLA.'home/table_kabupaten/'.$regional;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_kabupaten");
        }

        return json_decode($res->getBody(), true);
    }

    static function table_citieslose_kabupaten($regional)  
    {
        $url = self::URL_API_OOKLA.'super4g/table_citieslose_kabupaten/'.$regional;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_citieslose_kabupaten");
        }

        return json_decode($res->getBody(), true);
    }

    static function table_citieslose_kabupaten_month($regional, $months)  
    {
        $url = self::URL_API_OOKLA.'super4g/table_citieslose_kabupaten/'.$regional.'/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_citieslose_kabupaten_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function table_citieslose_kabupaten_week($regional, $week)  
    {
        $url = self::URL_API_OOKLA.'super4g/table_citieslose_kabupaten/'.$regional.'/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_citieslose_kabupaten_week");
        }

        return json_decode($res->getBody(), true);
    }

    static function table_kabupaten_month($regional, $months)  
    {
        $url = self::URL_API_OOKLA.'home/table_kabupaten/'.$regional.'/month/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_kabupaten_month");
        }

        return json_decode($res->getBody(), true);
    } 

    static function table_kabupaten_week($regional, $week)  
    {
        $url = self::URL_API_OOKLA.'home/table_kabupaten/'.$regional.'/week/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi table_kabupaten_week");
        }

        return json_decode($res->getBody(), true);
    }   

    static function chart_vendorachievement()  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_vendorachievement/';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_vendorachievement_analytics_download()  
    {
        $url = self::URL_API_OOKLA.'analytics/chart_vendorachievement/download';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_analytics_download");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_vendorachievement_analytics_upload()  
    {
        $url = self::URL_API_OOKLA.'analytics/chart_vendorachievement/upload';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_analytics_upload");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_vendorachievement_analytics_latency()  
    {
        $url = self::URL_API_OOKLA.'analytics/chart_vendorachievement/latency';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_analytics_latency");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_vendorachievement_month($months)  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_vendorachievement/month/download/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_vendorachievement_week($week)  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_vendorachievement/week/download/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_week");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_vendorachievement_upload()  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_vendorachievement/week/upload';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_upload");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_vendorachievement_upload_month($months)  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_vendorachievement/month/upload/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_upload_month");
        }

        return json_decode($res->getBody(), true);
    }  

    static function chart_vendorachievement_upload_week($week)  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_vendorachievement/week/upload/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_upload_week");
        }

        return json_decode($res->getBody(), true);
    }  

    static function chart_vendorachievement_latency()  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_vendorachievement/week/latency';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_latency");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_vendorachievement_latency_month($months)  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_vendorachievement/month/latency/'.$months;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_latency_month");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_vendorachievement_latency_week($week)  
    {
        $url = self::URL_API_OOKLA.'super4g/chart_vendorachievement/week/latency/'.$week;
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi chart_vendorachievement_latency_week");
        }

        return json_decode($res->getBody(), true);
    }

    static function check_login($username,$password)  
    {
        $url = self::URL_API_OOKLA.'login/auth';
        $client = new \GuzzleHttp\Client();
        try { 
            $res = $client->post($url,['form_params' => ['username' => $username, 'password' => $password]]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi check_login");
        }

        return json_decode($res->getBody(), true);
    }


}
